---
id: 3195
title: On Peppers and Pipes
date: 2011-09-19T07:00:32+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=3195
permalink: /2011/09/19/on-peppers-and-pipes/
tinymce_signature_post_setting:
  - 'yes'
categories:
  - Rural Living
---
<span id="On_Pipes8230">

<h3>
  On Pipes&#8230;
</h3></span> 

Hubby quit smoking&#8230;well, sort of.

He was on a self-imposed slowdown for about a month. He said he couldn&#8217;t justify the expense while he wasn&#8217;t working and needed to quit. Also, Hubby has always wanted to try a pipe. He felt that before he could do this he would have to give up smoking completely. He did quit completely about 3 weeks ago. After 2 weeks he ordered his tobacco.

Hubby ordered some tobacco online (a whole lot cheaper) and bought a cheap corn cob pipe. He now sits outside in the late afternoon or early evening, in his rocking chair, enjoying his new found pleasure.

Apparently, It takes a while to get the hang of pipe smoking, and Hubby is working on his technique!

<a rel="attachment wp-att-3196" href="http://quiltingthefarm.vius.ca/2011/09/on-peppers-and-pipes/pipe1/"><img class="alignnone size-full wp-image-3196" title="pipe1" src="http://quiltingthefarm.plaidsheep.ca/files/2011/09/pipe1.jpg" alt="" width="412" height="309" /></a>

<a rel="attachment wp-att-3198" href="http://quiltingthefarm.vius.ca/2011/09/on-peppers-and-pipes/pipe3/"><img class="alignnone size-full wp-image-3198" title="pipe3" src="http://quiltingthefarm.plaidsheep.ca/files/2011/09/pipe3.jpg" alt="" width="412" height="309" /></a>

<a rel="attachment wp-att-3199" href="http://quiltingthefarm.vius.ca/2011/09/on-peppers-and-pipes/pipe2/"><img class="alignnone size-full wp-image-3199" title="pipe2" src="http://quiltingthefarm.plaidsheep.ca/files/2011/09/pipe2.jpg" alt="" width="412" height="309" /></a>

<a rel="attachment wp-att-3197" href="http://quiltingthefarm.vius.ca/2011/09/on-peppers-and-pipes/pipe4/"><img class="alignnone size-full wp-image-3197" title="pipe4" src="http://quiltingthefarm.plaidsheep.ca/files/2011/09/pipe4.jpg" alt="" width="412" height="309" /></a><a rel="attachment wp-att-3196" href="http://quiltingthefarm.vius.ca/2011/09/on-peppers-and-pipes/pipe1/"></a>

If you are wondering, I had nothing to do with the quitting. I have never asked Hubby to quit smoking (he&#8217;s always been an outdoor smoker) because it&#8217;s something he really enjoys. Hubby enjoys the process of smoking; the pipe gives him that pleasure while being so much easier on the budget.

<span id="On_Peppers">

<h3>
  On Peppers
</h3></span> 

I planted chili pepper seeds last spring. This is what I have now.

<a rel="attachment wp-att-3201" href="http://quiltingthefarm.vius.ca/2011/09/on-peppers-and-pipes/hotpeppers2/"><img class="alignnone size-full wp-image-3201" title="hotpeppers2" src="http://quiltingthefarm.plaidsheep.ca/files/2011/09/hotpeppers2.jpg" alt="" width="309" height="412" /></a>

Beautiful, colourful peppers _and_ they are ripening!

<a rel="attachment wp-att-3200" href="http://quiltingthefarm.vius.ca/2011/09/on-peppers-and-pipes/hotpeppers/"><img class="alignnone size-full wp-image-3200" title="hotpeppers" src="http://quiltingthefarm.plaidsheep.ca/files/2011/09/hotpeppers.jpg" alt="" width="412" height="309" /></a>

These are &#8220;Twilight&#8221; hot peppers or Capsicum frutescens. The peppers grow upright in clusters. Hubby pointed out that they look like Christmas lights.

They are supposed to be very hot when ripe (they turn from purple, to yellow, to orange, to red). I can&#8217;t wait to try them out.

<a rel="attachment wp-att-3198" href="http://quiltingthefarm.vius.ca/2011/09/on-peppers-and-pipes/pipe3/"></a>

<a rel="attachment wp-att-3199" href="http://quiltingthefarm.vius.ca/2011/09/on-peppers-and-pipes/pipe2/"></a>

<a rel="attachment wp-att-3196" href="http://quiltingthefarm.vius.ca/2011/09/on-peppers-and-pipes/pipe1/"></a>