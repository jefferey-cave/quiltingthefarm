---
id: 2819
title: Project Day
date: 2011-08-15T07:11:48+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2819
permalink: /2011/08/15/project-day/
tinymce_signature_post_setting:
  - 'yes'
categories:
  - Crafts
---
**_Hubby has his project&#8230;._**

**<a rel="attachment wp-att-2821" href="http://quiltingthefarm.vius.ca/2011/08/project-day/finished-woodstack/"><img class="alignnone size-full wp-image-2821" title="finished-woodstack" src="http://quiltingthefarm.plaidsheep.ca/files/2011/08/finished-woodstack.jpg" alt="" width="412" height="309" /></a>**

a 7x7x7 wood stack! He&#8217;s finished stacking and is just putting the tarp on to keep the stack dry.

_**I have mine&#8230;**_

_<a rel="attachment wp-att-2822" href="http://quiltingthefarm.vius.ca/2011/08/project-day/old-dresser/"><img class="alignnone size-full wp-image-2822" title="old-dresser" src="http://quiltingthefarm.plaidsheep.ca/files/2011/08/old-dresser.jpg" alt="" width="412" height="309" /></a>_

An old dresser that I picked up at a local antique/junk store.

<a rel="attachment wp-att-2820" href="http://quiltingthefarm.vius.ca/2011/08/project-day/dresser-sanded-top/"><img class="alignnone size-full wp-image-2820" title="dresser-sanded-top" src="http://quiltingthefarm.plaidsheep.ca/files/2011/08/dresser-sanded-top.jpg" alt="" width="412" height="309" /></a>

The top has now been sanded (or rather scraped off), and I&#8217;m trying to get rid of the awful &#8220;crackle&#8221; surface on the side. One day this will stand in my downstairs bathroom&#8230;one day.

_**And we have ours&#8230;**_

_**<a rel="attachment wp-att-2823" href="http://quiltingthefarm.vius.ca/2011/08/project-day/reno1/"><img class="alignnone size-full wp-image-2823" title="reno1" src="http://quiltingthefarm.plaidsheep.ca/files/2011/08/reno1.jpg" alt="" width="412" height="309" /></a>**_

House repairs and renovations&#8230;so much fun&#8230;NOT!