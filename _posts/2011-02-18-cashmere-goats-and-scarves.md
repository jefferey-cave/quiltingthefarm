---
id: 338
title: 'On Cashmere Goats &amp; Scarves'
date: 2011-02-18T09:23:27+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=338
permalink: /2011/02/18/cashmere-goats-and-scarves/
image: /files/2011/01/knitting.jpg
categories:
  - Crafts
tags:
  - crafts
  - farm animals
  - goats
  - knitting
  - knitting pattern
  - yarn
---
Before we purchased our homestead here in Nova Scotia, we spent a few days on the Northumberland Shore, and this is where we enjoyed our first taste of ‘maritime hospitality’.

One of the first stops we made was to <a href="http://www.earthartcashmere.com" target="_blank">Earth Arts Cashmere Farm</a>. Chris and Christina Silver raise cashmere goats and sell their yarn and related products in a small store on the side of their farmhouse. They also have a small barnyard of critters; ducks, chickens, miniature ponies, and a rather cute donkey named “Willow”. <!--more-->

Before we left, I purchased a ball (skein) of their wonderful cashmere yarn. I am now knitting a wonderfully soft, lacy scarf for someone (possibly me) to enjoy. 

<p style="text-align: center">
  <a href="http://quiltingthefarm.plaidsheep.ca/files/2011/01/knitting.jpg"><img class="size-medium wp-image-289 aligncenter" title="knitting" src="http://quiltingthefarm.plaidsheep.ca/files/2011/01/knitting-300x225.jpg" alt="" width="300" height="225" /></a>
</p>

**PATTERN**

With  10 mm (US 15) needles, cast on 46 stiches

Knit 6 rows.

_Start pattern_

Row 1. Knit 4 (yarn over, knit 2 together) until last 4 stitches, knit 4

This one row forms the lacy pattern…it couldn’t be easier. I’m usually a master at getting lost in my knitting pattern and having to unpick, but even I can (mostly) manage this.

Work 9 rows of pattern

Next Row Knit 6 rows

Repeat these 15 rows (9 pattern, 6 knit)  until you have a scarf the length you need 

Cast off.

I don&#8217;t know how warm it will keep you but you&#8217;ll certainly be the &#8220;height of fashion&#8221; on the farm!

I&#8217;m linking to&#8230;
  
[<img src="http://i185.photobucket.com/albums/x4/bigbassgirl/button4-4-1.jpg" border="0" alt="" />](http://djerenew.blogspot.com/)