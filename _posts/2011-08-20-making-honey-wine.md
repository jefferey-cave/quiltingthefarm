---
id: 2875
title: Making Honey Wine
date: 2011-08-20T07:00:50+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2875
permalink: /2011/08/20/making-honey-wine/
tinymce_signature_post_setting:
  - 'yes'
aktt_notify_twitter:
  - 'yes'
aktt_tweeted:
  - "1"
categories:
  - Kitchen
---
Yesterday we made honey wine or mead as we know it. We have made several batches of mead in the past, but with moving etc we haven&#8217;t made one in a while.

Several months ago we purchased 2 30lb tubs of honey. This batch used one of those tubs (I filled a couple of jars for the pantry first), 12 lemons, 12 teabags and a package of wine yeast.

<a rel="attachment wp-att-2876" href="http://quiltingthefarm.vius.ca/2011/08/making-honey-wine/lemons/"><img class="alignnone size-full wp-image-2876" title="lemons" src="http://quiltingthefarm.plaidsheep.ca/files/2011/08/lemons.jpg" alt="" width="412" height="309" /></a>

We used our largest stock pot for boiling the water and making the &#8220;tea&#8221;. As we need 23 litres in the end, we have to use 2 full pots. After having issues in the past, we only filled the pot 1/4 way the first time around. You have to add the honey to the pot to dissolve it, and in past years we&#8217;ve run out of room (we know this cause Hubby keeps a mead journal&#8230;did I tell you he&#8217;s an analyst???).

We boiled up the first batch of water with the juice of 12 lemons. I peeled half of the lemons and threw the entire lemon in. In previous years we have cut the lemons in half and just added them skin and all to the pot, but we found the last batch to have a slightly bitter taste. After that mixture was boiled for about 15 minutes we poured in the honey. This is not the easiest of things to do! Both Hubby and I, and the kitchen were sticky all afternoon. After the honey melted we poured everything into the plastic &#8216;primary&#8217;. We then boiled up more water to fill to 23 litres.

<a rel="attachment wp-att-2877" href="http://quiltingthefarm.vius.ca/2011/08/making-honey-wine/honey-lemon-mixture/"><img class="alignnone size-full wp-image-2877" title="honey-lemon-mixture" src="http://quiltingthefarm.plaidsheep.ca/files/2011/08/honey-lemon-mixture.jpg" alt="" width="412" height="309" /></a>

After leaving it to sit and cool, Hubby added a package of yeast, We waited for it to start bubbling. It did not. Hubby added another package of yeast. Still nothing 🙁

We aren&#8217;t sure what is happening, or rather what isn&#8217;t happening. I hope we can figure it out, it was a lot of work and a lot of honey.