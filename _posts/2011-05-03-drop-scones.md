---
id: 1820
title: Drop Scones
date: 2011-05-03T06:23:33+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=1820
permalink: /2011/05/03/drop-scones/
tinymce_signature_post_setting:
  - ""
categories:
  - Kitchen
  - Recipes
---
When I first moved to Canada there was a Marks & Spencers store in the mall. I love the food they sell, but unfortunately they didn&#8217;t sell it for very long in Canada. One of my favourtes were &#8220;Scotch Pancakes&#8221;. I would purchase a package or two every time I visited the mall. They kept them in the freezer and sometimes I wanted to eat them so badly that I would try and _defrost them between my hands on the way home and eat them still slightly frozen in the middle!_ (don&#8217;t tell another soul will you?) 

After Marks & Spencers was no more, I couldn&#8217;t get my fix so I had to find another way. I went through old recipe books and publications and tried different recipes and nothing was close until I found this recipe for Drop Scones. Drop scones don&#8217;t sound very pancake like, but that&#8217;s just what they are&#8230;pancakes&#8230; Scotch pancakes, the same as I used to buy a Marks & Spencers! 

<div id="attachment_1821" style="width: 448px" class="wp-caption alignnone">
  <a rel="attachment wp-att-1821" href="http://quiltingthefarm.vius.ca/2011/05/drop-scones/vluu-p1200-samsung-p1200/"><img class="size-full wp-image-1821  " title="drop scones" src="http://quiltingthefarm.plaidsheep.ca/files/2011/04/drop-scones.jpg" alt="drop scones" width="438" height="518" /></a>
  
  <p class="wp-caption-text">
    photo courtesy http://theenglishkitchen.blogspot.com
  </p>
</div>

 

<span id="Drop_Scones">

<h2>
  Drop Scones 
</h2></span> 

  * 1 cup flour
  * pinch salt 
  * 1/4 tsp baking soda 
  * 3 Tablespoons sugar 
  * 1 egg 
  * 1/2 cup milk with 1 tsp lemon juice stirred in 
  * 1 Tablespoon butter, melted 

  1. Mix flour, salt and baking soda in bowl. Make a well in the centre and add sugar, egg and butter. Stir. Add enough milk to make a smooth batter. 
  2. Heat frying pan or griddle. Drop spoonfuls of batter (2 &#8211; 3 inches in diameter) onto the hot pan. Cook until the surface has lots of bubbles, flip the pancakes over and cook the other side. 
  3. Serve warm with butter and jam or just eat as is. Cold drop scones can also be toasted and served with your choice of preserves (mine never last that long!). 

<p style="text-align: center">
  ******
</p>