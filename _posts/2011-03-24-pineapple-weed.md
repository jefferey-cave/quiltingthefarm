---
id: 1363
title: Pineapple Weed
date: 2011-03-24T07:06:54+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=1363
permalink: /2011/03/24/pineapple-weed/
categories:
  - Kitchen
tags:
  - country life
  - food
  - frugal
  - garden
  - land
---
<span id="Pineapple_Weed_is8230well8230_a_weed.">

<h2>
  Pineapple Weed is&#8230;well&#8230; a weed.
</h2></span> 

It&#8217;s real name is Matricaria matricioides, to some it is known as wild or false chamomile. It grows in the crappiest of places like rocky open ground, pastures, waste ground, your driveway or the cracks in the pavement.

I discovered the wonders of this tiny weed a couple of years ago. While researching some other herbs, I came across a photo and description of pineapple weed and realized I had seen this in our own backyard.

<div id="attachment_1388" style="width: 458px" class="wp-caption alignnone">
  <a rel="attachment wp-att-1388" href="http://quiltingthefarm.vius.ca/2011/03/pineapple-weed/pineapple-weed/"><img class="size-full wp-image-1388 " title="pineapple weed" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/pineapple-weed.jpg" alt="pineapple weed" width="448" height="393" /></a>
  
  <p class="wp-caption-text">
    Photo courtesy Wikipedia
  </p>
</div>

Pineapple Weed is very easy to identify. When the flower heads are crushed or rubbed between your<!--more--> fingers, they smell wonderful; a cross between apple and pineapple (hence it&#8217;s name). 

Just like chamomile, Pineapple Weed makes a tasty tea. Use the fresh or dried flower heads, adding a teaspoonful to a mug of boiling water and allowing to seep for 10 minutes. Sweeten with honey is desired. _**I made a batch of this as an iced tea for a summer party and everyone thought it was wonderful.**_

It is also similar to chamomile in some of its medicinal uses, although it is milder. The plant is harvested in the summer and is dried and stored for later use. You can use it for stomach aches and colds, help with menstrual problems and also as a mild relaxant (don&#8217;t recommend smoking it though!). Pineapple Weed can be used topically for mild itching and sores (I haven&#8217;t tried this).

_So next time you see this tasty little weed; pick some for yourself and enjoy a relaxing cup of tea._

<p style="text-align: center">
  ******
</p>