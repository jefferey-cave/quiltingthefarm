---
id: 2040
title: Dandelion Jelly
date: 2011-05-24T06:45:58+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2040
permalink: /2011/05/24/dandelion-jelly/
image: /files/2011/05/dandelion-jelly.jpg
categories:
  - Kitchen
  - Recipes
---
One of our missions when we decided to move here was to eat what we grow not neccessarily grow what we eat. I don&#8217;t much care for things like turnip and rutabagas but they grow and keep well into winter when there are no other fresh vegetables. I will learn to like them. Our apples may not be the tastiest, but they are free for the picking and they make wonderful jellies and sauces.

As I have mentioned before, like most gardens, we have dandelions in abundance. So taking the &#8220;eat what you grow&#8221; mantra to heart, I figure we will eat some of those &#8220;weeds&#8221;. On Saturday I picked a lot of dandelions. I sat at the dining room table and picked all the petals off each and every head until I had 4-5 cups of yellow petals. The work was surprisingly zen. This was the start of my dandelion jelly. Dandelion jelly tastes remarkably like honey.

<a rel="attachment wp-att-2044" href="http://quiltingthefarm.vius.ca/2011/05/dandelion-jelly/dandelion-heads/"><img class="alignnone size-full wp-image-2044" title="dandelion-heads" src="http://quiltingthefarm.plaidsheep.ca/files/2011/05/dandelion-heads.jpg" alt="dandelion heads" width="412" height="309" /></a>

<div id="attachment_2045" style="width: 422px" class="wp-caption alignnone">
  <a rel="attachment wp-att-2045" href="http://quiltingthefarm.vius.ca/2011/05/dandelion-jelly/dandelion-petals/"><img class="size-full wp-image-2045 " title="dandelion-petals" src="http://quiltingthefarm.plaidsheep.ca/files/2011/05/dandelion-petals.jpg" alt="dandelion petals" width="412" height="309" /></a>
  
  <p class="wp-caption-text">
    make sure to remove any green bits otherwise the jelly will be bitter
  </p>
</div>

Here is the complete recipe:

  * 4-5 heaping cups of fresh dandelion petals
  * 4 cups boiling water
  * 2 tablespoons lemon juice
  * 4 1/2 cups sugar
  * 1 package pectin (Certo)
  * food colouring (optional)

  1. Pour boiling water over petals and let steep for 2 hours.
  2. Drain through cheesecloth lined colander. Press all liquid from petals.
  3. Add lemon juice, sugar, pectin and food colouring if used. Bring to roiling boil and stir until sugar is dissolved. Boil hard for one minute. Skim.
  4. Pour into sterilized  jars and seal. Place in a water bath for 10 minutes.

<a rel="attachment wp-att-2042" href="http://quiltingthefarm.vius.ca/2011/05/dandelion-jelly/dandelion-jelly-in-water-bath/"><img class="alignnone size-full wp-image-2042" title="dandelion-jelly-in-water-bath" src="http://quiltingthefarm.plaidsheep.ca/files/2011/05/dandelion-jelly-in-water-bath.jpg" alt="dandelion-jelly-in-water-bath" width="412" height="309" /></a>

<a rel="attachment wp-att-2043" href="http://quiltingthefarm.vius.ca/2011/05/dandelion-jelly/dandelion-jelly/"><img class="alignnone size-full wp-image-2043" title="dandelion-jelly" src="http://quiltingthefarm.plaidsheep.ca/files/2011/05/dandelion-jelly.jpg" alt="dandelion jelly" width="412" height="309" /></a>