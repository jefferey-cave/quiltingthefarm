---
id: 1801
title: Hubby, the Homestead Hero.
date: 2011-05-17T06:20:24+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=1801
permalink: /2011/05/17/hubby-the-homestead-hero/
categories:
  - Rural Living
---
Today, I want to thank my Hubby for being&#8230;well, my hubby, best friend and homesteading hero.

> The hubby who has moved across country to become a farmer instead of a Systems Analyst.
> 
> The hubby who left behind his friends for an opportunity at a different kind of life.
> 
> The hubby who has taken on an apprenticeship so our homesteading lives can lived a little more smoothly.
> 
> The hubby who  built benches and shelves so my visiting family were  more comfortable.
> 
> The hubby who always builds the fire to keep me warm.
> 
> The hubby who gets up at the crack of dawn, even though there&#8217;s no real need.
> 
> The hubby who takes out the garbage and compost bin.
> 
> The hubby who does the laundry so I don&#8217;t have to navigate the basement stairs.
> 
> The hubby who doesn&#8217;t cook, but will make me cheese and crackers if I need him to.
> 
> The hubby who always drives, even though he prefers being the passenger.
> 
> The hubby who takes pride in building the chicken coop.
> 
> The hubby who spent most of yesterday _**fixing my computer!**_

_**Thank You!**_

**_<a rel="attachment wp-att-1954" href="http://quiltingthefarm.vius.ca/2011/05/hubby-the-homestead-hero/hero-hubby/"><img class="alignnone size-full wp-image-1954" title="hero-hubby" src="http://quiltingthefarm.plaidsheep.ca/files/2011/05/hero-hubby.jpg" alt="hubby, my homestead hero" width="338" height="450" /></a>_**