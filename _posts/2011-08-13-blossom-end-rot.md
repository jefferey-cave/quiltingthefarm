---
id: 2794
title: Blossom End Rot
date: 2011-08-13T07:00:11+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2794
permalink: /2011/08/13/blossom-end-rot/
tinymce_signature_post_setting:
  - 'yes'
categories:
  - Garden
---
When I went out to pick some veggies yesterday, I came across several tomatoes that look like there have their bottom halves missing. On further investigation, the tomatoes are still whole, but have black leathery sunken &#8220;skin&#8221; bottoms. These are the classic signs of blossom end rot&#8230;not very pretty.

<a rel="attachment wp-att-2799" href="http://quiltingthefarm.vius.ca/2011/08/blossom-end-rot/blossom-end-rot2/"><img class="alignnone size-full wp-image-2799" title="blossom-end-rot2" src="http://quiltingthefarm.plaidsheep.ca/files/2011/08/blossom-end-rot2.jpg" alt="" width="412" height="309" /></a>

<a rel="attachment wp-att-2798" href="http://quiltingthefarm.vius.ca/2011/08/blossom-end-rot/blossom-end-rot-2/"><img class="alignnone size-full wp-image-2798" title="blossom-end-rot" src="http://quiltingthefarm.plaidsheep.ca/files/2011/08/blossom-end-rot.jpg" alt="" width="412" height="309" /></a>

Blossom end rot is not a disease, or pest problem , but rather a physiological disorder caused by low calcium. This condition arise from low calcium levels in the soil, drought, very wet soil, or fluctuations in moisture due to rain, overwatering and high humidity.  Not surprisingly, the stress of this season with its horrible humidity, long dry spells and high rainfall weeks, seems to have taken a toll on my tomatoes. I am, of course, not without blame 🙁 I should have mulched my tomato plants weeks ago, as soon as the soil was warm. I should have paid more attention to watering during the dry spells. I should have tested my soil (I don&#8217;t feel bad about that as I didn&#8217;t know anything about testing soil). There&#8217;s lots of coulda&#8230;woulda&#8230;shoulda&#8230;

What can I do now? The disorder doesn&#8217;t spread, but if the tomato plant has a problem&#8230;it has a problem. Mulching the plants will help keep the moisture level more constant, and that&#8217;s what I&#8217;ll be doing this morning. Adding a fertillizer of crushed eggshell is also a good idea, and I have lots of those (eggshells, not good ideas!). Hopefully the weather for the rest of the summer will be more even, but unfortunately, I can&#8217;t do much about that.

I hope I don&#8217;t lose too many, I am so looking forward to canning my own salsa and pizza sauce. If all else fails there&#8217;s always the local grocery store and a fresh start next year with a lot more knowlege under my belt.