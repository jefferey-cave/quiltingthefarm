---
id: 2590
title: Some Downfalls of Squarefoot Gardening
date: 2011-07-20T06:55:18+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2590
permalink: /2011/07/20/some-downfalls-of-squarefoot-gardening/
tinymce_signature_post_setting:
  - 'yes'
categories:
  - Garden
---
As we are new to the gardening game, we have chosen to use the technique of squarefoot gardening. Gardening this way seemed more organized and less overwhelming for a beginner gardener. Hubby built some raised beds and laid out the grid with string this past spring. We set up a spread sheet for the blocks and planted each block according to the recommendations in the book. So far this year I have noticed some problems with the methods suggested, mostly, there are conflicts with the blocks. Plants in some blocks overshadow plants in another.

  * The swiss chard we planted is growing quicker than the kale in the adjacent block, shading the kale from the sun.
  * Tomatoes need way more room than the suggested spacing. They have taken over other blocks.
  * Parsnips were slow at germinating and the rutabaga leaves are smothering them before they get a good start.
  * Terrible germination of the lettuces meant replanting seeds several times, but by then the surrounding squares were overgrowing the lettuce blocks
  * The suggested two or three seeds in a hole is not enough. Many of my seeds didn&#8217;t germinate well ( these were fresh heritage/heirloom seeds from a local company). and I had to replant.
  * Beans and peas needed a trellis placed at back of the bed but they are getting blocked by the tomatoes that already have a head start by being planted indoors first.

Now, I&#8217;m not totally giving up on squarefoot gardening, but I realize it does have limitations. Although the premise is to make it easier for the beginner, I believe that you need a little more knowledge in what to plant where. Next year I will know more, and be able to revise my planting layout. There are definitely some things I like about this way of gardening;  I can (kind of) tell where the seedlings are as opposed to the weeds, and the idea of weeding by block is less overwhelming than a long row.

Revisions for next year will be:

  1. plant tomatoes in their own raised bed.
  2. plant slow germinating plants close to one another with no larger plants nearby, and
  3. Use row planting for certain plants
  4. plant way more seeds!

Next spring, we will be adding a large tilled bed for row planting some veggies and we will be adding a 4&#215;4 raised bed that we are going to build from the bricks we just pulled from our old chimney wall. Our very small gardening start will be getting just a little bigger 🙂