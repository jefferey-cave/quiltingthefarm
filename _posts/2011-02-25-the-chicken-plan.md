---
id: 836
title: The Chicken Plan
date: 2011-02-25T08:30:29+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=836
permalink: /2011/02/25/the-chicken-plan/
image: /files/2011/02/coop.gif
categories:
  - Rural Living
tags:
  - chickens
  - coop
  - country life
  - farm animals
  - land
---
Hubby and I have decided to order chicks in April for May delivery.  This will be our first time caring for chickens and we’re nervous and excited! We will be ordering 19 week old brown layers.

We have chosen the “shed” as the new coop. The “shed” is where we are keeping our wood at the moment, but we are making other arrangements for that next winter.

The shed was originally a baby barn type structure. It has been attached to the <!--more-->garage at one end, and has a door to the outside at the other. There is a boarded over small (chicken?) door and a boarded over window that we will have to re-open. The whole shed is insulated and drywalled.

Below is the plan as it stands right now (subject to the usual _mind_ changes of course). We are hoping to be able to free range our chickens but do realize we are in an area that will have predators, so a covered yard is a neccessity.

<div id="attachment_837" style="width: 356px" class="wp-caption aligncenter">
  <a href="http://quiltingthefarm.plaidsheep.ca/files/2011/02/coop.gif"><img class="size-full wp-image-837" title="coop" src="http://quiltingthefarm.plaidsheep.ca/files/2011/02/coop.gif" alt="chicken coop layout" width="346" height="462" /></a>
  
  <p class="wp-caption-text">
    Chicken coop layout
  </p>
</div>

We have decided to start small, with just a half a dozen layers and possibly (just possibly) a rooster (everyone seems to be trying to get rid of one). In time I expect we will add to our flock, but we’ll see how it goes.

<p style="text-align: center">
  ******
</p>