---
id: 1709
title: Flowers and Frogs
date: 2011-04-13T06:03:33+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=1709
permalink: /2011/04/13/flowers-and-frogs/
categories:
  - Rural Living
---
The homestead is awake with sounds and sights of Spring. The birds are singing, the frogs are chirping and the grasses are rustling in the breeze.  _**Life is Beautiful.**_

<a rel="attachment wp-att-1711" href="http://quiltingthefarm.vius.ca/2011/04/flowers-and-frogs/pond-sunny/"><img class="alignnone size-full wp-image-1711" title="pond-sunny" src="http://quiltingthefarm.plaidsheep.ca/files/2011/04/pond-sunny.jpg" alt="pond in spring" width="412" height="309" /></a>

The pond is alive with little tadpoles and tiny skimming water bugs.

<a rel="attachment wp-att-1712" href="http://quiltingthefarm.vius.ca/2011/04/flowers-and-frogs/tadpoles/"><img class="alignnone size-full wp-image-1712" title="tadpoles" src="http://quiltingthefarm.plaidsheep.ca/files/2011/04/tadpoles.jpg" alt="tadpoles hiding" width="412" height="309" /></a>

There are tadpoles&#8230;honest! You just have to look very closely. Yesterday I saw so many, but today they must known I would showing up with my camera &#8217;cause they made themselves pretty scarce 🙁

<a rel="attachment wp-att-1713" href="http://quiltingthefarm.vius.ca/2011/04/flowers-and-frogs/first-dandelion/"><img class="alignnone size-full wp-image-1713" title="first-dandelion" src="http://quiltingthefarm.plaidsheep.ca/files/2011/04/first-dandelion.jpg" alt="first dandelion of spring" width="412" height="303" /></a>

The first dandelion of Spring. Stewie our rabbit will be happy, dandelions are his favourite food.

<a rel="attachment wp-att-1710" href="http://quiltingthefarm.vius.ca/2011/04/flowers-and-frogs/bluebells/"><img class="alignnone size-full wp-image-1710" title="bluebells" src="http://quiltingthefarm.plaidsheep.ca/files/2011/04/bluebells.jpg" alt="bluebells on a lawn" width="412" height="309" /></a>

This is not my yard, but it is only 5 minutes down the road. See the snowdrops in the background and crocuses and bluebells. It almost takes my breath away.

<span id="Spring_has_most_certainly_sprung_in_our_neck_of_the_woods">

<h3>
  <span style="color: #008000">Spring has most certainly sprung in our neck of the woods!</span>
</h3></span> 

<h3 style="text-align: center">
  <span style="color: #008000"> </span>
</h3>

<h3 style="text-align: center">
  <span style="color: #008000">******</span>
</h3>