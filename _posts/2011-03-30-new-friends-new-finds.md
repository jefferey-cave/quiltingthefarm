---
id: 1486
title: New Friends, New Finds
date: 2011-03-30T07:00:07+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=1486
permalink: /2011/03/30/new-friends-new-finds/
categories:
  - Rural Living
tags:
  - country life
  - friends
  - frugal
  - fruit
  - Nova Scotia
---
We have company coming in a few weeks and our old homestead isn&#8217;t ready yet. We are down to the wire and still missing some vital pieces for the comfort of our guests.

Yesterday, I found an old dresser for sale on <a title="Kijiji" href="http://annapolis.kijiji.ca/" target="_blank">Kijiji</a>. We really need a dresser for our spare room (our guests would appreciate somewhere to put their clothes), so at $30 we thought it would do. I made arrangements to pick up the dresser this afternoon. I got the address and Hubby and I set off. The place was supposed to be only about 10 minutes away, but <!--more-->in complete 

_newbie style_ we ended up not being able to find it! We drove back and forth, to and fro on the road it was supposed to be on, but that address didn&#8217;t seem to exist.

On one of our **_to and fro&#8217;s_** we spied a large rug standing by the side of the road. On it was a sign, &#8220;FREE&#8221; it read. I made hubby turn the car around to go inspect my possible prize. While deciding whether or not this giant rug would fit in our car, the owner came wandering out to say &#8220;Hi&#8221;.

We met Fred. Fred is a transplant to the area too. He&#8217;s been here since 2003 and homesteads like many of the other people we have met. Fred and his wife raise chickens and pigs but much more excitingly they have a cider press! We will return in the Fall with some of our apples. After chatting awhile, we left Fred and ended up across the road eating fruit cake with people we&#8217;ve never met before&#8230;you gotta love Nova Scotia!

We did eventually find the house we were looking for.  We paid for the dresser, stuffed it in the back of the car beside the 15ft rug (thank you Hyundai!) and drove home.

It turned out to be a fine day for _**treasure**_ hunting!

<p style="text-align: center">
  ******
</p>