---
id: 1038
title: The Ugly Dresser Makeover
date: 2011-03-09T07:17:35+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=1038
permalink: /2011/03/09/the-ugly-dresser-makeover/
image: /files/2011/03/finished-dresser5.jpg
categories:
  - Crafts
tags:
  - crafts
  - decorating
  - DIY
  - frugal
---
<span id="This_is_a_dresser_I_picked_up_from_the_side_of_the_road.">

<h3>
  This is a dresser I picked up from the side of the road.
</h3></span> 

As you can see it was in very bad shape. In fact it was so bad it wouldn&#8217;t even sell for peanuts at the yard sale! 

<a rel="attachment wp-att-1041" href="http://quiltingthefarm.vius.ca/2011/03/the-ugly-dresser-makeover/dresser2/"><img class="size-medium wp-image-1041  alignnone" title="old dresser" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/dresser2-300x225.jpg" alt="old dresser" width="300" height="225" /></a> 

The top was in really bad shape. The veneer had bowed and cracked and had to be removed.

[<img class="size-medium wp-image-1042 alignnone" title="dresser top" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/dresser3-300x225.jpg" alt="dresser top" width="300" height="225" />](http://quiltingthefarm.plaidsheep.ca/files/2011/03/dresser3.jpg) 

The handles were a strange mix of metal and orange plastic!

[<img class="alignnone size-medium wp-image-1039" title="dresser_handle" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/dresser_handle-300x225.jpg" alt="dresser handle" width="300" height="225" />](http://quiltingthefarm.plaidsheep.ca/files/2011/03/dresser_handle.jpg)

I sanded the entire dresser by hand before remembering we purchased an electric sander, duh. I helped hubby glue it back together&#8230;it was a bit like putting a jigsaw puzzle together, then I gave it 2 coats of primer left over from my dining room.

[<img class="alignnone size-medium wp-image-1045" title="dresser_primed2" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/dresser_primed2-300x225.jpg" alt="primed dresser" width="300" height="225" />](http://quiltingthefarm.plaidsheep.ca/files/2011/03/dresser_primed2.jpg)

I used a yellow paint that was an oops paint from the local hardware store. I think I paid $6 for it. I&#8217;m rather pleased with the colour. It&#8217;s not one I&#8217;d usually choose.

<a rel="attachment wp-att-1049" href="http://quiltingthefarm.vius.ca/2011/03/the-ugly-dresser-makeover/yellow-dresser1/"><img class="size-medium wp-image-1049  alignnone" title="yellow-dresser1" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/yellow-dresser1-300x225.jpg" alt="dresser painted yellow" width="300" height="225" /></a> 

The handle spacing was 4 inches and I was unable to find affordable ones to fit these holes. If I had been thinking I would have filled the original holes and drilled new ones for smaller handles. Oh well, it&#8217;s too late for that. Instead I painted the metal parts with black spray paint and replaced them, without the plastic!, back on, but turned upside down. I&#8217;m convinced they look better this way 🙂

[<img class="alignnone size-medium wp-image-1046" title="dresser-handle" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/dresser-handle-300x225.jpg" alt="dresser handle" width="300" height="225" />](http://quiltingthefarm.plaidsheep.ca/files/2011/03/dresser-handle.jpg)  

And tada! here is the completed dresser in all her sunshine glory.

<a rel="attachment wp-att-1048" href="http://quiltingthefarm.vius.ca/2011/03/the-ugly-dresser-makeover/finished-dresser5/"><img class="size-medium wp-image-1048 alignnone" title="finished-dresser5" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/finished-dresser5-300x242.jpg" alt="yellow dresser" width="300" height="242" /></a>

I&#8217;m linking this post with <a title="Thrifty Decor Chick" href="http://thriftydecorchick.blogspot.com/2011/03/before-and-after-party-kitchen-nook.html" target="_blank">Thrifty Decor Chick</a> before & after party

and <a title="Elements Interiors" href="http://www.elements-interiors.net/2011/03/whassup-wednesday.html" target="_blank">Elements Interiors</a>

<a href="http://www.elements-interiors.net" target="_blank"><img src="http://i142.photobucket.com/albums/r87/mmoo1978/whassup1.png" alt="" /></a>