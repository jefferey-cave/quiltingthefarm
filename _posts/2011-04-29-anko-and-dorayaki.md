---
id: 1763
title: Anko and Dorayaki
date: 2011-04-29T06:34:08+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=1763
permalink: /2011/04/29/anko-and-dorayaki/
categories:
  - Kitchen
  - Recipes
---
Hubby and I both enjoy beans. We eat a lot of them in main dishes and soups, but what about dessert? Does the thought of beans in dessert seem downright unappealing?

The Japanese people certainly don&#8217;t think so. They enjoy a sweet paste called “anko”. It is basically <a title="Adzuki beans" href="http://en.wikipedia.org/wiki/Azuki_bean" target="_blank">adzuki beans</a> cooked with sugar and water. This paste is used to fill sweet buns, sweet rice cakes, and dumplings.

I have heard that anko is better for you as it high in protein and low in calories and lacks the high calorie ingredients of North American desserts. I think someone forgot to mention all the sugar that has to be added to make it sweet!

Hubby and I enjoy “anko” in this traditional dessert called Dorayaki, which is anko sandwiched between two sweet pancakes. The little pancakes are delicious.

Have a go for yourself. The recipe is a little time consuming but very simple. (sorry I forgot to take photos!)

** Anko (Adzuki Bean Paste)**

1 cup adzuki (sometimes called aduki) beans
  
3/4 cup sugar
  
1/4 tsp salt
  
Water

Soak adzuki beans in water overnight.

Heat adzuki beans in a pot with one cup of water. When they come to a boil, add two more cups of cold water. When they come to a boil again, drain the beans in a colander.

Return the beans to the pot, add three cups of new water, and cook over high heat. When the beans begin to jump around after the water comes to a boil, turn the heat down to low and simmer until the beans are soft, about one hour. If necessary, add water so that the beans are always covered.  Skim off any foam that appears on the surface.

When the beans are soft (can be mashed easily between your finger and thumb), drain them in a colander.

Return the beans to the pot and mix in the sugar.

Mash the beans continuously over low to medium heat until almost all the water has evaporated and a paste has formed, about 15 minutes (could be up to 35 minutes).

Add the salt and stir over medium heat for about 5 minutes more.

Remove from heat and transfer the paste to a container. Let cool

The paste may be kept in the refrigerator for three days and then frozen. (Makes 2 cups.) or used right away in this wonerful dessert&#8230;

**Dorayaki (Sweet-Filled Pancakes)**

2 cups adzuki bean paste
  
1 cup flour
  
1/2 cup sugar
  
1 tablespoon honey
  
1/2 teaspoon baking powder
  
2 eggs
  
1/4-1/2 cup water

In a mixing bowl whisk eggs with sugar and honey.

Add flour and baking powder.

Slowly add water while whisking. Whisk until smooth.

Drop spoonfuls of batter on a lightly oiled fry pan or griddle to make pancakes that are about 3 inches in diameter. When bubbles start to appear, turn over the pancakes and cook briefly on the other side until golden.

Spread 1-2 tablespoons of adzuki bean paste on one pancake, and cover it with another to make a sandwich.

 (Makes about 10 dorayaki.)