---
id: 462
title: More Snow
date: 2011-02-02T10:45:27+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=462
permalink: /2011/02/02/more-snow/
image: /files/2011/02/tractor.jpg
categories:
  - Rural Living
tags:
  - snowstorms
  - tractor
  - weather
  - winter
---
We are just at the beginning of another snowstorm. The aftermath of the last one can be seen <a href="http://quiltingthefarm.vius.ca/2011/01/digging-out/" target="_self">here</a>. Hubby wants his own tractor and plough now :), and, quite frankly, with our snow removal bill going from an estimated ~$200 per winter, to $100+ for just January,  it looks like it might be a good idea.

<div id="attachment_463" style="width: 160px" class="wp-caption alignnone">
  <a href="http://www.kubota.ca/en/productdetail.aspx?trail=0|30|37|100|281&prodid=282"><img class="size-thumbnail wp-image-463" title="tractor" src="http://quiltingthefarm.plaidsheep.ca/files/2011/02/tractor-150x150.jpg" alt="" width="150" height="150" /></a>
  
  <p class="wp-caption-text">
    Hubby wants one of these Kubota tractors
  </p>
</div>