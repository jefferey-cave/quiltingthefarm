---
id: 3055
title: The Staghorn Sumac
date: 2011-09-03T06:55:47+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=3055
permalink: /2011/09/03/the-staghorn-sumac/
tinymce_signature_post_setting:
  - 'yes'
categories:
  - Kitchen
---
Did you know that the berries of the Staghorn Sumac are edible? Well they are, and they make a wonderful lemonade like beverage.

<a rel="attachment wp-att-3056" href="http://quiltingthefarm.vius.ca/2011/09/the-staghorn-sumac/stag-sumac/"><img class="alignnone size-full wp-image-3056" title="stag-sumac" src="http://quiltingthefarm.plaidsheep.ca/files/2011/09/stag-sumac.jpg" alt="" width="412" height="309" /></a>

The staghorn sumac has tiny fuzzy berries

<a rel="attachment wp-att-3057" href="http://quiltingthefarm.vius.ca/2011/09/the-staghorn-sumac/close-up-staghorn-sumac/"><img class="alignnone size-full wp-image-3057" title="close-up-staghorn-sumac" src="http://quiltingthefarm.plaidsheep.ca/files/2011/09/close-up-staghorn-sumac.jpg" alt="" width="412" height="309" /></a>

Steep them in hot or cold water for a few hours (or overnight). Strain and serve. Stir in a little honey if you like it a bit sweeter.

<a rel="attachment wp-att-3058" href="http://quiltingthefarm.vius.ca/2011/09/the-staghorn-sumac/glass-juice/"><img class="alignnone size-full wp-image-3058" title="glass-juice" src="http://quiltingthefarm.plaidsheep.ca/files/2011/09/glass-juice.jpg" alt="" width="412" height="309" /></a>

Staghorn sumac juice can also be used to make jelly, fruit syrup and delicious wine. The jelly is good with roasted meats or cheese.

<a rel="attachment wp-att-3056" href="http://quiltingthefarm.vius.ca/2011/09/the-staghorn-sumac/stag-sumac/"></a>

I went out and harvested the berries from my 2 small trees this morning. I didn&#8217;t get very much so I&#8217;ll have to seek out a few more trees. We found a bunch growing along the side of the road, but they seem awfully tall and I obviously can&#8217;t haul a ladder down the mountain in our subcompact car! I&#8217;ll have to come up with a new plan 🙂

<a rel="attachment wp-att-3056" href="http://quiltingthefarm.vius.ca/2011/09/the-staghorn-sumac/stag-sumac/"></a>

<a rel="attachment wp-att-3056" href="http://quiltingthefarm.vius.ca/2011/09/the-staghorn-sumac/stag-sumac/"></a>