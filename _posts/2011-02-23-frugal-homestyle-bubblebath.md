---
id: 215
title: Frugal Homestyle Bubble Bath
date: 2011-02-23T08:24:49+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=215
permalink: /2011/02/23/frugal-homestyle-bubblebath/
categories:
  - Kitchen
  - Recipes
tags:
  - frugal
  - gifts
  - recipes
---
This old farm house has a huge bathroom and every once in a while, I enjoy a long soak in the bath tub. Now, every Christmas and birthday I ask for bath stuff (you know, scrubs, fizzies, foaming bubbles) and every year I receive, well, none, nada, zippo.  I have to take it upon myself to come up with my own. As buying my own foaming bath just doesn’t sit well with me, I raid the kitchen cupboards for <!--more-->something interesting. My meagerly stocked pantry offers up&#8230;

_dried milk powder,
  
honey,
  
liquid soap, and
  
veggie oil
  
pure vanilla extract_

_[<img class="size-medium wp-image-283 alignnone" title="bubble-bath" src="http://quiltingthefarm.plaidsheep.ca/files/2011/01/bubble-bath-300x225.jpg" alt="" width="300" height="225" />](http://quiltingthefarm.plaidsheep.ca/files/2011/01/bubble-bath.jpg)
  
_ 
  
Here we go with the recipe for &#8216;Vanilla Honey Milk Bath&#8217;

  * 1 cup oil (I used veggie)
  * 1/2 cup honey
  * 1/2 cup liquid soap
  * 1 Tbsp vanilla extract
  * ¼ cup dried milk powder

Put the oil into a bowl and carefully stir in remaining ingredients until mixture is fully blended. Pour into a pretty jar (I used a large canning jar). NOTE:  As the mixture separates when left to stand, shake gently before use.

So now, after a long day puttering around the homestead, I can enjoy a long, luxurious soak and silky smooth skin.