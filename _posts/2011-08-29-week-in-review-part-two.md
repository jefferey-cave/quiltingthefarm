---
id: 2989
title: This Week in Review, Part Two
date: 2011-08-29T07:00:24+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2989
permalink: /2011/08/29/week-in-review-part-two/
tinymce_signature_post_setting:
  - 'yes'
categories:
  - Rural Living
---
**On to the garden.**

I haven&#8217;t done much else with the garden, not even weeding. It&#8217;s pretty much at the fend-for-yourself stage now. With the garden tractor out of commission, it&#8217;s getting overgrown around the beds. The beds themselves are fairing better; probably because they are so full that the weeds can&#8217;t get much of a foot-hold.

I mulched my tomatoes when I found out about the blossom end rot they had acquired. I&#8217;m pleased to say the mulching worked&#8230;or seemed to work, but I&#8217;m taking credit for it anyway;) Small victories are what it&#8217;s all about. I have lots of beautiful red tomatoes and many green and orange ones waiting in the wings. I should think about canning some sauce or salsa.

I made refrigerator bread and butter pickles. I normally don&#8217;t like pickles, but I&#8217;m addicted to these. Now I&#8217;m wishing I planted more cucumbers. I am going to pickle the purple cabbage next.

It looks like the squash have become bored with the garden beds and have decided to make a break for it! They have made it out of the raised beds and over the fence! I wonder how much further they will go before the frost puts an end to their antics. I planted squash because it seemed like a good idea. I have rarely eaten squash so I have no idea what to do with them once they are ripe. I don&#8217;t even know what kind they are, I just picked the easy ones. This Fall is going to be interesting supper-wise.

**On to the Chickens.**

The chickens have been out free-ranging. The Cochins, who have never free-ranged before were not at all sure this was a good idea. Our Rooster, Alexander the Great followed the &#8220;Reds&#8221; out the first day we opened the gate. He wandered up to the garage and promptly got himself lost! He panicked and tried to get back _through_ the fence, finding himself in rather a predicament. Hubby rescued him and scooted him back in the yard. He didn&#8217;t venture out again for 4 days! It&#8217;s strange. I thought the chickens would love to be out and about but mostly they just hang out in their yard. Once in a while one or two will wander over to check out what we&#8217;re doing then toddle back home. I guess they have everything they need there, and are happy.

The hens are laying well, and we have managed to give lots of eggs away to our neighbours. I have been making and baking and trying to use as many as possible. The normal question asked around here is &#8220;What would you like to eat&#8230;that&#8217;s eggs?&#8221; Huevos rancheros is a favourite meal for supper. I can make it, but I&#8217;m damned if I can say it! My pronunciation always has Hubby in fits of laughter. I should make him do the cooking&#8230;no&#8230; wait, then I&#8217;ll have to do jobs that I don&#8217;t like&#8230;I&#8217;ll stick with the cooking, thank you very much!

**Other odds and ends.**

There was a hurricane, now a tropical storm, headed close(ish) to Nova Scotia. We have been getting ready for wind and rain and basic inclemant weather. We don&#8217;t expect much to happen, but it&#8217;s good practice, and points out where you&#8217;re vulnerbilities lay. We have gathered up all the outside furniture (this amounts to two chairs), the small trailer and any other odds and ends and put them in the garage. I&#8217;ve harvested all my ripe fruit and veg, or at least as much as we can use and store. We have collected a few buckets for catching water coming in through the roof&#8230;yes, the 5 year old metal, guaranteed for a lifetime roof (not our lifetime apparently), and got out and readied the camping lamps, flashlights and stove in case of power failure. If there&#8217;s no post tomorrow you&#8217;ll know we lost power!