---
id: 1667
title: Friday on the Farm
date: 2011-04-09T06:22:03+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=1667
permalink: /2011/04/09/friday-on-the-farm/
categories:
  - Rural Living
tags:
  - baking
  - country life
  - DIY
  - food
---
We seem to have become a haven for that little red-breasted fellow, the robin. At any given time there are about 15 or so robins sitting outside on the driveway, watching us. It&#8217;s all a bit unnerving!

Anyway, on with my post.

Yesterday afternoon, hubby and I emptied our compost container into the new wattle fenced compost bin!

If you&#8217;ve been following along, you&#8217;ll know that Hubby has been learning how to build wattle fencing. He decided to build something small and simple for his first effort. He&#8217;s not that impressed with the results (we did giggle about it&#8230;a lot), but being a first attempt and learning experience he was relatively pleased to come out of it with something useful and usable.

Hubby will be building more wattle fencing now that he has figured out some of the ways he went wrong, so I may still get my raised garden beds yet!

As for me, I have been building a bench to put at the end of our bed. I&#8217;m not ready to show you photos yet, but I&#8217;m thrilled with the way it turned out. It looks good and it&#8217;s servicable!

I have also been baking bread for the freezer and making big batches of granola as we have family coming in 2 weeks time.