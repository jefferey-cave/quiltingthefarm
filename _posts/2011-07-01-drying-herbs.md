---
id: 2400
title: Drying Herbs
date: 2011-07-01T07:23:00+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2400
permalink: /2011/07/01/drying-herbs/
categories:
  - Garden
---
I have herbs to dry this year. I didn&#8217;t plant most of them, but I&#8217;m sure going to enjoy them. Air drying herbs is any easy and cheap way to dry fresh herbs.  The slow process of air drying allows the moisture to evaporate slowly leaving most of the sought after oils behind in the leaf of the herb. Although this process works best for herbs like Dill, Marjoram, Oregano, Rosemary, Summer Savory and Thyme, I&#8217;m planning on doing the higher moisture herbs like basil, mint and chives as well. I don&#8217;t have a dehydrator and have no plans to purchase one yet. I will probably freeze some herbs in ice cube trays as well.

<img class="alignnone size-full wp-image-2401" title="drying-herbs" src="http://quiltingthefarm.plaidsheep.ca/files/2011/06/drying-herbs.jpg" alt="drying herbs" width="412" height="309" />

Because we have had so many rainy days, I&#8217;ve been on hold in the herb collecting/drying process. The sun started to shine the other morning and I waited until all the dew had dried up, but before the rains started again, and harvested golden oregano, thyme and some pineapple weed.

I am trying to keep this up all summer so I have drying herbs as part of my ongoing gardening effort . There is already so much to do at the end of the season, I don&#8217;t want to add another big job to that.  This way I can break the work down into manageable chunks and have a good supply of herbs ready to go.

I tie my herbs in bunches and hang upside down until they are dry and &#8220;crispy&#8221; . I then discard the woody stems and put the leaves in a jar. I like to store my dried herbs in the freezer, but that&#8217;s dependent on the space I have. Keeping the leaves whole and crushing them just before use is supposed to be the best way of using, but I sometimes crush then as soon as they are dry as I like the &#8220;instant&#8221; use when I&#8217;m busy.

Linking to Farmgirl Friday Blog Hop over at <a href="http://www.deborahjeansdandelionhouse.com/2011/06/farmgirl-friday-blog-hop-14.html" target="_blank">Dandelion House</a>

[](http://quiltingthefarm.plaidsheep.ca/files/2011/03/Barn-Hop.jpg)