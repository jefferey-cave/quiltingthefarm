---
id: 846
title: Ordering Our First Seeds
date: 2011-02-26T09:17:53+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=846
permalink: /2011/02/26/ordering-our-first-seeds/
categories:
  - Garden
tags:
  - country life
  - food
  - garden
  - grains
  - home
---
We have just put in our first seed order from [Annapolis Seeds](http://www.annapolisseeds.com/), here in the Annapolis Valley!  We have ordered:

  * bush type tomatoes,
  * a lettuce mix,
  * green beans,
  * peas,
  * corn,
  * squash, and
  * cucumber.

We are going to try growing a small grain crop of Black-Seeded Amaranth (this will be a  just small test patch), and the root crops, rutabaga, carrots, parsnips and turnips (for winter keeping). We will also need to plant some broccoli, radishes, onions, potatoes, beets, swiss chard and sweet bell peppers, but we still need to order these.

The planters next to the house might make for a kitchen type garden with herbs etc. They seem to get <!--more-->plenty of morning and afternoon sun so I’m hopeful something will grow there.

I am also going to use a couple of planters to try my hand at growing hot peppers (friends of ours did really well with theirs). There is a deck on the west side of the house that may get warm enough for them.

Well, that will be about it for our first real garden. It already seems slightly overwhelming, but I think if we just build a couple of 4&#215;4 raised beds plus a small patch for corn and another for amaranth, we should be okay for a small crop. We certainly won’t be feeding ourselves off this harvest but it will give us a better idea of what grows well and what we need.

I forgot about sunflowers! We also would like to plant a patch of  Black Oil Sunflowers. It looks like I have a whole lot of work to do &#8230;once the snow eventually melts!

<p style="text-align: center">
  I&#8217;m linking up to Farm Friend Friday!
</p>

<p style="text-align: center">
   <a href="http://www.verdefarm.com" target="_blank"><img src="http://i1091.photobucket.com/albums/i390/VerdeFarm/farmgirlfridaybuttonforamycopy150.jpg" alt="" /></a>
</p>

<p style="text-align: center">
  ******
</p>