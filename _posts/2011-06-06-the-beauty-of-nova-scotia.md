---
id: 2154
title: The Beauty of Nova Scotia
date: 2011-06-06T06:30:13+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2154
permalink: /2011/06/06/the-beauty-of-nova-scotia/
categories:
  - Rural Living
---
A spring tour of our homestead. Enjoy your visit.

<div id="attachment_2160" style="width: 422px" class="wp-caption alignnone">
  <a rel="attachment wp-att-2160" href="http://quiltingthefarm.vius.ca/2011/06/the-beauty-of-nova-scotia/spring-driveway/"><img class="size-full wp-image-2160" title="spring-driveway" src="http://quiltingthefarm.plaidsheep.ca/files/2011/06/spring-driveway.jpg" alt="driveway in spring" width="412" height="309" /></a>
  
  <p class="wp-caption-text">
    Driveway in spring
  </p>
</div>

<div id="attachment_2164" style="width: 422px" class="wp-caption alignnone">
  <a rel="attachment wp-att-2164" href="http://quiltingthefarm.vius.ca/2011/06/the-beauty-of-nova-scotia/orchard-and-garden/"><img class="size-full wp-image-2164" title="orchard-and-garden" src="http://quiltingthefarm.plaidsheep.ca/files/2011/06/orchard-and-garden.jpg" alt="orchard and garden" width="412" height="309" /></a>
  
  <p class="wp-caption-text">
    Apple orchard with garden boxes in behind
  </p>
</div>

<div id="attachment_2165" style="width: 422px" class="wp-caption alignnone">
  <a rel="attachment wp-att-2165" href="http://quiltingthefarm.vius.ca/2011/06/the-beauty-of-nova-scotia/blueberries/"><img class="size-full wp-image-2165" title="blueberries" src="http://quiltingthefarm.plaidsheep.ca/files/2011/06/blueberries.jpg" alt="lowbush blueberries" width="412" height="309" /></a>
  
  <p class="wp-caption-text">
    Lowbush blueberries
  </p>
</div>

<div id="attachment_2158" style="width: 319px" class="wp-caption alignnone">
  <a rel="attachment wp-att-2158" href="http://quiltingthefarm.vius.ca/2011/06/the-beauty-of-nova-scotia/favourite-spot/"><img class="size-full wp-image-2158" title="favourite-spot" src="http://quiltingthefarm.plaidsheep.ca/files/2011/06/favourite-spot.jpg" alt="my favourite spot" width="309" height="412" /></a>
  
  <p class="wp-caption-text">
    my favourite spot
  </p>
</div>

<a rel="attachment wp-att-2156" href="http://quiltingthefarm.vius.ca/2011/06/the-beauty-of-nova-scotia/columbine/"><img class="alignnone size-full wp-image-2156" title="columbine" src="http://quiltingthefarm.plaidsheep.ca/files/2011/06/columbine.jpg" alt="columbine" width="412" height="309" /></a>

<a rel="attachment wp-att-2163" href="http://quiltingthefarm.vius.ca/2011/06/the-beauty-of-nova-scotia/freshcut-grass/"><img class="size-full wp-image-2163" title="freshcut-grass" src="http://quiltingthefarm.plaidsheep.ca/files/2011/06/freshcut-grass.jpg" alt="freshcut grass to trail" width="412" height="309" /></a>

<a rel="attachment wp-att-2177" href="http://quiltingthefarm.vius.ca/2011/06/the-beauty-of-nova-scotia/hummingbird-hang-out/"><img class="alignnone size-full wp-image-2177" title="hummingbird-hang-out" src="http://quiltingthefarm.plaidsheep.ca/files/2011/06/hummingbird-hang-out.jpg" alt="flowering quince" width="412" height="309" /></a>

<a rel="attachment wp-att-2162" href="http://quiltingthefarm.vius.ca/2011/06/the-beauty-of-nova-scotia/washing-line/"><img class="alignnone size-full wp-image-2162" title="washing-line" src="http://quiltingthefarm.plaidsheep.ca/files/2011/06/washing-line.jpg" alt="washing line" width="412" height="309" /></a>

<div id="attachment_2157" style="width: 422px" class="wp-caption alignnone">
  <a rel="attachment wp-att-2157" href="http://quiltingthefarm.vius.ca/2011/06/the-beauty-of-nova-scotia/lupins-and-tree/"><img class="size-full wp-image-2157" title="lupins-and-tree" src="http://quiltingthefarm.plaidsheep.ca/files/2011/06/lupins-and-tree.jpg" alt="Lupins and apple tree down driveway" width="412" height="309" /></a>
  
  <p class="wp-caption-text">
    Lupins and apple tree down driveway
  </p>
</div>

<div id="attachment_2155" style="width: 422px" class="wp-caption alignnone">
  <a rel="attachment wp-att-2155" href="http://quiltingthefarm.vius.ca/2011/06/the-beauty-of-nova-scotia/blackberries/"><img class="size-full wp-image-2155" title="blackberries" src="http://quiltingthefarm.plaidsheep.ca/files/2011/06/blackberries.jpg" alt="blackberries" width="412" height="309" /></a>
  
  <p class="wp-caption-text">
    blackberries
  </p>
</div>

<a rel="attachment wp-att-2159" href="http://quiltingthefarm.vius.ca/2011/06/the-beauty-of-nova-scotia/pink-lupin/"><img class="alignnone size-full wp-image-2159" title="pink-lupin" src="http://quiltingthefarm.plaidsheep.ca/files/2011/06/pink-lupin.jpg" alt="pink lupin" width="309" height="412" /></a>

<a rel="attachment wp-att-2169" href="http://quiltingthefarm.vius.ca/2011/06/the-beauty-of-nova-scotia/strawberries/"><img class="size-full wp-image-2169" title="strawberries" src="http://quiltingthefarm.plaidsheep.ca/files/2011/06/strawberries.jpg" alt="strawberries" width="412" height="312" /></a>

<a rel="attachment wp-att-2178" href="http://quiltingthefarm.vius.ca/2011/06/the-beauty-of-nova-scotia/apple-blossom/"><img class="alignnone size-full wp-image-2178" title="apple-blossom" src="http://quiltingthefarm.plaidsheep.ca/files/2011/06/apple-blossom.jpg" alt="apple blossom" width="412" height="309" /></a>

<a rel="attachment wp-att-2170" href="http://quiltingthefarm.vius.ca/2011/06/the-beauty-of-nova-scotia/quince-and-crab/"></a>

<p style="text-align: center">
  ******
</p>