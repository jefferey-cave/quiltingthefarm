---
id: 1349
title: Taking Stock
date: 2011-03-26T07:14:49+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=1349
permalink: /2011/03/26/taking-stock/
image: /files/2011/03/birdhouse.jpg
categories:
  - Rural Living
---
While hubby has been outside building his <a title="Wattling" href="http://quiltingthefarm.vius.ca/2011/03/wattling/" target="_blank">wattle fence</a> I&#8217;ve been taking stock of the homestead and what it has to offer

<a rel="attachment wp-att-1354" href="http://quiltingthefarm.vius.ca/2011/03/taking-stock/rocks/"><img class="alignnone size-full wp-image-1354" title="rocks" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/rocks.jpg" alt="large rocks" width="412" height="309" /></a>

We have lots of these old and gnarly apple trees. Fortunately they are quite productive; offering us several different kinds of apples for eating and cooking.  In the background you can see how very rocky our landscape is. These are huge blocks of granite.

<a rel="attachment wp-att-1355" href="http://quiltingthefarm.vius.ca/2011/03/taking-stock/strawberry-beds1/"><img class="alignnone size-full wp-image-1355" title="strawberry-beds1" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/strawberry-beds1.jpg" alt="strawberry beds" width="412" height="309" /></a>

These are old strawberry beds, a little rustic perhaps, but hopefully the strawberries will still be viable. They need a good clean up.

<a rel="attachment wp-att-1358" href="http://quiltingthefarm.vius.ca/2011/03/taking-stock/chicken-area/"><img class="alignnone size-full wp-image-1358" title="chicken-area" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/chicken-area.jpg" alt="chicken run" width="412" height="309" /></a>

Here is the area I thought good for a chicken yard, or perhaps a garden.

If we use it as a garden we will need to remove those large trees. I&#8217;m not even sure it will be okay to leave the trees if we decided to put chickens here. I don&#8217;t want predators sitting up in the branches waiting for their next meal to walk by!

<address>
  <strong>What would you do with it?</strong>
</address>

<a rel="attachment wp-att-1350" href="http://quiltingthefarm.vius.ca/2011/03/taking-stock/strawberry-beds-2/"></a><a rel="attachment wp-att-1350" href="http://quiltingthefarm.vius.ca/2011/03/taking-stock/strawberry-beds-2/"></a>

<a rel="attachment wp-att-1352" href="http://quiltingthefarm.vius.ca/2011/03/taking-stock/kitchen-planter/"><img class="alignnone size-full wp-image-1352" title="kitchen-planter" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/kitchen-planter.jpg" alt="kitchen planter" width="412" height="309" /></a>

This planter outside the house gets lots of sun. I think this will be suitable for a small kitchen garden. I&#8217;m not sure what has been growing in here, but it sure is ugly in the winter 🙁

<a rel="attachment wp-att-1351" href="http://quiltingthefarm.vius.ca/2011/03/taking-stock/birdhouse/"><img class="alignnone size-full wp-image-1351" title="birdhouse" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/birdhouse.jpg" alt="old birdhouse" width="412" height="309" /></a>

This neat old birdhouse shows itself now the snow has melted. There are a couple of these left behind, so I&#8217;ll have to find somewhere to hand them back up.

<a rel="attachment wp-att-1353" href="http://quiltingthefarm.vius.ca/2011/03/taking-stock/pond-ice/"><img class="alignnone size-full wp-image-1353" title="pond-ice" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/pond-ice.jpg" alt="pond still has ice" width="412" height="309" /></a>

Our largest pond still has a thin coating of ice&#8230;a very thin coating. This is the pond Hubby wants to clean and raise fish in starting in 2012.

<a rel="attachment wp-att-1357" href="http://quiltingthefarm.vius.ca/2011/03/taking-stock/outhouse/"><img class="alignnone size-full wp-image-1357" title="outhouse" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/outhouse.jpg" alt="old outhouse" width="412" height="309" /></a>

This is the old farm outhouse. I&#8217;m so glad I don&#8217;t have to use it. I guess it might come in handy if the power is out for long, or we have a neighbourhood party!