---
id: 2996
title: After the Storm
date: 2011-08-30T07:00:17+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2996
permalink: /2011/08/30/after-the-storm/
tinymce_signature_post_setting:
  - 'yes'
categories:
  - Rural Living
---
The hurricane, which became a tropical storm and then a post tropical something or another, has passed us by. We didn&#8217;t get much of the predicted rain, but boy, was there ever a wind storm. Hubby and I were awakened many times during the night. Wind  and rain storms are aspecially interesting when you have a metal roof. It&#8217;s like living with a freight train in your head!

After the wind had died down somewhat, we let the ansy chickens out, fully expecting to see one go whizzing by the window during the next wind gust. Fortunately they all stayed grounded, and we still have 12 of them.

We took a quick walk around the immediate property, there was no real damage, a few small branches and twigs here and there was all we found. A couple of larger branches had fallen further up the trail, but not on the trail itself, so no clean up will be required. The driveway was covered in apples, most of which had fallen hard on gravel and weren&#8217;t really salvagable. We did fill 3 grocery bags with decent windfalls from only two trees, and hubby put the squished and damaged ones into the compost pile. Amazingly the trees are still heavy with fruit.

Thankfully everything else was much the same as it was when we went to bed.