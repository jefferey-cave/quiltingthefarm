---
id: 1257
title: Starting Seeds
date: 2011-03-20T06:54:36+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=1257
permalink: /2011/03/20/starting-seeds/
image: /files/2011/03/seed-tray3.jpg
categories:
  - Garden
tags:
  - country life
  - food
  - garden
  - gardening
  - home
  - house
  - seeds
  - sprouting
---
As my <a title="Seeds arrived in mail" href="http://quiltingthefarm.vius.ca/2011/03/seeds-arrived-today/" target="_self">seeds arrived in the mail</a> last week, I thought I should get on starting a few herbs and veggies indoors.

Unfortunately, we don&#8217;t have a great place for starting seeds.

We have no greenhouse as yet so that means starting seeds in the house.

The large windows of the house all face North, as does the &#8220;sun room&#8221;.  The mud room faces East, gets good light, but isn&#8217;t heated with the rest of the house. The windows on the south side are are very small (pantry and bathroom), and blocked by the garage.

What to do, what to do?

At the top of the stairs we have a south facing window. There is already a cupboard underneath the window, so Hubby added 2 more shelves across the window. We are hoping that the seeds get enough light up here.

<a rel="attachment wp-att-1258" href="http://quiltingthefarm.vius.ca/2011/03/starting-seeds/seed-trays-all/"><img class="alignnone size-full wp-image-1258" title="seed-trays-all" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/seed-trays-all.jpg" alt="seed trays on window shelves" width="412" height="309" /></a>

I used some plastic tubs from the mixed greens I bought (for the rabbit!). I have been saving these up for about 8 months and I have lots.

<a rel="attachment wp-att-1259" href="http://quiltingthefarm.vius.ca/2011/03/starting-seeds/seed-tray1/"><img class="alignnone size-full wp-image-1259" title="seed-tray1" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/seed-tray1.jpg" alt="seed tray" width="412" height="309" /></a>

While sorting through the barn I came across this old seed propagator, so I planted some tomatoes, cucumbers and peppers in the trays. I still have room for something else in the end tray, not sure just what yet.

<a rel="attachment wp-att-1261" href="http://quiltingthefarm.vius.ca/2011/03/starting-seeds/seed-tray3/"><img class="alignnone size-full wp-image-1261" title="seed-tray3" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/seed-tray3.jpg" alt="seed propagator" width="412" height="309" /></a>

I&#8217;m not sure whether or not I&#8217;ll have any luck with these, but time will tell, so I&#8217;m staying patient and watching for little green shoots!

[<img class="alignnone size-full wp-image-1317" title="Barn-Hop" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/Barn-Hop.jpg" alt="" width="191" height="184" />](http://homesteadrevival.blogspot.com/2011/03/barn-hop-5.html)

<p style="text-align: center">
  ******
</p>