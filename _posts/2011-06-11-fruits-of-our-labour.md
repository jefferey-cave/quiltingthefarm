---
id: 2225
title: Fruits of Our Labour
date: 2011-06-11T06:49:18+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2225
permalink: /2011/06/11/fruits-of-our-labour/
categories:
  - Rural Living
---
It has been 5 days now and the chickens are settling in nicely. They have been outside foraging for weeds and bugs all day and enjoying the sunshine. Some are more adventurous than others but none stray far from the coop.

Alexander the Great crowed for the very first time this morning. He has a deep crow that doesn&#8217;t carry that far and I could barely hear him unless I was close to the coop. It was good to hear (I&#8217;m sure I won&#8217;t be saying that in a few weeks!).

One of the chickens was acting a little strange this morning. She (Agatha) had herself tucked in the corner and didn&#8217;t want to go outside with the others. Well, guess what? She laid an egg&#8230;our very first egg!

<div id="attachment_2226" style="width: 422px" class="wp-caption alignnone">
  <a rel="attachment wp-att-2226" href="http://quiltingthefarm.vius.ca/2011/06/fruits-of-our-labour/first-egg/"><img class="size-full wp-image-2226" title="first-egg" src="http://quiltingthefarm.plaidsheep.ca/files/2011/06/first-egg.jpg" alt="first egg" width="412" height="292" /></a>
  
  <p class="wp-caption-text">
    our first egg!
  </p>
</div>

When we get our second egg, we will have a celebration breakfast. We will eat the eggs along with some home cured bacon (not ours) and homemade bread (ours) and start to enjoy the fruits of our labour.