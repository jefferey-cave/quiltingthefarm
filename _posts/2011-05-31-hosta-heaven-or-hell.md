---
id: 2098
title: Hosta Heaven (or Hell)
date: 2011-05-31T06:53:43+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2098
permalink: /2011/05/31/hosta-heaven-or-hell/
categories:
  - Garden
---
This is my <del>kitchen herb </del>hosta garden. I planted some lettuces, peas and radishes here before I understood the quirky personality of the hosta. Hostas are exuberrent, bossy and a trifle overbearing. They have decided that the planter is for them and them alone. They have completely taken over, drowning out any seedlings and flinging themselves over the sides so that anything beneath the planter wall ends up wearing a hosta shade &#8220;hat&#8221;. I feel as though I have inadvertedly raised a  herd of triffids!

<a rel="attachment wp-att-2100" href="http://quiltingthefarm.vius.ca/2011/05/hosta-heaven-or-hell/hostas/"><img class="alignnone size-full wp-image-2100" title="hostas" src="http://quiltingthefarm.plaidsheep.ca/files/2011/05/hostas.jpg" alt="hostas" width="412" height="309" /></a>

I have to admit (to myself mainly) they do look lovely, all green and lush especially after a gentle spring rain. I keep pulling them out and they keep coming back.

<a rel="attachment wp-att-2101" href="http://quiltingthefarm.vius.ca/2011/05/hosta-heaven-or-hell/hostas-in-rain/"><img class="alignnone size-full wp-image-2101" title="hostas-in-rain" src="http://quiltingthefarm.plaidsheep.ca/files/2011/05/hostas-in-rain.jpg" alt="hostas in the rain" width="412" height="309" /></a>

I hate myself for wanting rid of them, but that space is too valuable to give up to a shade loving plant like a hosta. Perhaps I can find a place in my yard _and in my heart_ for them next year.