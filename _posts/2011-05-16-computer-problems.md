---
id: 1949
title: Computer Problems
date: 2011-05-16T06:10:48+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=1949
permalink: /2011/05/16/computer-problems/
categories:
  - Uncategorized
---
I seem to be having problems with my computer. Unfortunately it appears to have caught a particularly nasty virus and until it has recovered I won&#8217;t be posting much. Hopefully I will be back up and running in the next couple of days. In the meantime why don&#8217;t you check out some of my older posts.