---
id: 2248
title: Land and Livestock
date: 2011-06-14T06:00:53+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2248
permalink: /2011/06/14/land-and-livestock/
categories:
  - Rural Living
---
Unfortunately we have the rain and chilly weather back. We were hoping to finish the fence around the raised beds, but that didn&#8217;t happen. At least I&#8217;m not having to water the gardens or planters so that&#8217;s a bonus, but that comes with the side effect of the grass and scrub growing too fast. Keeping the forest at bay seems to have become almost a full time job. We need a plan for this.

The last few days Hubby and I have been mulling over viable options for our smallhold going forward. The grass and scrub situation has us discussing the integratability of various types of livestock: cattle, sheep and goats. We have come to understand land management is as important as livestock management on a small farm. Without paying attention to the idiosyncrasies of our land, the benefits and disadvantages of what lies outside our backdoor, we can not maintain our farm sustainably or profitably.

Our land has a great deal of forest, so much forest that our &#8220;meadow&#8221; and &#8220;pasture&#8221; have tall dense scrub choking out much of the grass. We do have surface water, but it appears the creek was diverted in the past causing a few swampy areas in strategic places. We have several ponds that need to be maintained and we would enjoy them more if we could actually see them through the thicket. There are several rock walls on the property and we would like to integrate these as best we can&#8230;we sure aren&#8217;t moving them! We have rocky soil and an undulating landscape making fencing an &#8220;interesting&#8221; challenge.

Based on all of these variables we need to select livestock that will meet our needs, our land&#8217;s needs, and our budget. While we have some ideas, we find ourselves changing our mind at least once a day (cows are easy, goats are small, sheep are cute, cows are easy, goats are &#8230;). This has been (and still is) a very difficult decision, but we believe matching livestock to land is the key to success for a small farm like ours.