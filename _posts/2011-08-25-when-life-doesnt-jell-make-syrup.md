---
id: 2944
title: 'When Life Doesn&#8217;t Jell, Make Syrup'
date: 2011-08-25T07:00:38+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2944
permalink: /2011/08/25/when-life-doesnt-jell-make-syrup/
tinymce_signature_post_setting:
  - 'yes'
categories:
  - Kitchen
---
Life is a bowl of cherries or more to the point, a jar of cherry jelly, if only I could get it right! It would seem that my fortune is not to be made in the canning and preserving  business.

How is it that I could make marmalade last year while living in a small airless apartment, in downtown Halifax, with only my camping gear. I mean, I made marmalade in one of those nesting pots with the clip on handles, yet I am unable to get it right in my own kitchen with real canning supplies.

My second batch of chokecherry jelly was an utter failure again.  Actually my chokecherry jelly was really chokecherry and apple jelly. I thought for sure this would set&#8230;.but no 🙁

I added apples because the deer have been playing havoc with the apple tree closest to the house. Last year, when we first purchased this place, we noticed that this particular tree only had one apple on it, now we see why. The deer pull the branches down and pull the apples off onto the driveway, then they stand there and eat at their leisure&#8230;even during the day! A few days ago Hubby chased the same deer off about 5 times within 15 minutes! The nerve of it all.

<a rel="attachment wp-att-2948" href="http://quiltingthefarm.vius.ca/2011/08/when-life-doesnt-jell-make-syrup/apples-on-driveway/"><img class="alignnone size-full wp-image-2948" title="apples-on-driveway" src="http://quiltingthefarm.plaidsheep.ca/files/2011/08/apples-on-driveway.jpg" alt="" width="412" height="309" /></a>

So, I had lots of under-ripe apples, that were in perfectly good condition (the ones the deer didn&#8217;t take a chomp out of) scattered along the driveway. I had to pick them up anyway so we don&#8217;t have to keep driving over them and making applesauce. As I was collecting them, I thought to myself  &#8221; Apples are high in pectin, especially under-ripe ones, I&#8217;ll just add these to my jelly&#8221;.

I boiled up my chokecherries, I boiled up my cored and chopped apples. I strained them both, mixed them togther and carried on making jelly as normal. It wouldn&#8217;t jell. Perhaps I didn&#8217;t boil long enough? Would it have jelled if I had boiled it more? I actually thought it was jelling a bit when I did one of the jell tests. Next time I&#8217;ll use a thermometer just to be sure it reaches the correct temperature.

After doing a little research on apple pectin, I realise I should have boiled the apples AND the cores. I think the pectin is concentrated in the core itself. Perhaps that was my mistake.

I&#8217;ve been looking at the jars of &#8220;jelly&#8221; sitting on my counter, mulling over a re-do. I just don&#8217;t have the motivation to re-do this batch as well. I tried a little of the unjelled &#8220;jelly&#8221; and have decided it makes  the finest syrup!

<a rel="attachment wp-att-2949" href="http://quiltingthefarm.vius.ca/2011/08/when-life-doesnt-jell-make-syrup/chokecherry-syrup/"><img class="alignnone size-full wp-image-2949" title="chokecherry-syrup" src="http://quiltingthefarm.plaidsheep.ca/files/2011/08/chokecherry-syrup.jpg" alt="" width="412" height="309" /></a>

Who needs all those jars of chokecherry jelly anyway?