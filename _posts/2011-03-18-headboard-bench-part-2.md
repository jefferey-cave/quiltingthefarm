---
id: 1226
title: Headboard Bench Part 2
date: 2011-03-18T06:27:26+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=1226
permalink: /2011/03/18/headboard-bench-part-2/
image: /files/2011/03/black-bench2.jpg
categories:
  - Crafts
tags:
  - country life
  - crafts
  - decorating
  - designing
  - DIY
  - frugal
---
<span id="As_promised_I_have_part_2_of_the_headboard_bench.">

<h3>
  As promised, I have part 2 of the headboard bench.
</h3></span> 

If you missed it here is <a title="Headboard bench part 1" href="http://quiltingthefarm.vius.ca/2011/03/heardboard-bench-part-1/" target="_self">Headboard Bench Part 1</a>

Unfortunately I was so eager to get it finished I forgot to take any photos of this part of the process 🙁

<a rel="attachment wp-att-1231" href="http://quiltingthefarm.vius.ca/2011/03/headboard-bench-part-2/black-bench4/"><img class="alignnone size-full wp-image-1231" title="black-bench4" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/black-bench4.jpg" alt="headboard bench painted black" width="412" height="309" /></a>

I was going to paint with a brush, but figured that all those spindles would be awkward, not to mention annoying, so I used black gloss spray paint instead. I thought my finger was going to full off, it was pretty painful after a while of pushing down the nozzle of a spray paint can! The spray paint gives a wonderful smooth finish.

The cushions are two 18&#8243;x18&#8243; x3&#8243; pieces of foam (Walmart $5 each). The fabric also came from Walmart. I didn&#8217;t know they still sold fabric, but there was a clearance section on upholstery fabrics for $9 a piece. They were large pieces (I still have lots left over from this piece). I had wanted to use a red patterned fabric, but I wasn&#8217;t able to find any. Anyway, $9 was way too good a deal to pass up 🙂

_I also purchased 3 other pieces and I reupholstered Hubby&#8217;s rocking chair yesterday afternoon!_

I originally covered the foam as was. The seat of the bench is 17 inches deep and I thought that the extra inch wouldn&#8217;t be so bad. Well, I was wrong, it didn&#8217;t look good with the overhang so I had to unpick the cushions and cut the foam to the right size. _This is what I get for trying to save time!_ 

When I took this photo I still hadn&#8217;t finished sewing them&#8230;that&#8217;s why there&#8217;s wrinkles.

<a rel="attachment wp-att-1229" href="http://quiltingthefarm.vius.ca/2011/03/headboard-bench-part-2/black-bench2/"><img class="alignnone size-full wp-image-1229" title="black-bench2" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/black-bench2.jpg" alt="black headboard bench" width="412" height="309" /></a>

I like the bench with the baskets underneath. Can you see how Hubby staged the basket with the glove hanging out!

Here&#8217;s another look at what we started out with&#8230;

<a rel="attachment wp-att-1174" href="http://quiltingthefarm.vius.ca/2011/03/heardboard-bench-part-1/bench-back/"><img class="alignnone size-full wp-image-1174" title="bench-back" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/bench-back.jpg" alt="headboard and footboard" width="412" height="309" /></a>

I&#8217;m linking this post to <a title="Finding Fabulous" href="http://www.findingfabulousblog.com/2011/03/frugalicious-friday-giveawaybates.html" target="_blank">Finding Fabulous Frualicious Fridays</a> and

Miss Mustard Seed <a title="Miss Mustard Seed" href="http://missmustardseed.blogspot.com/2011/03/furniture-feature-friday-favorites-link_17.html" target="_blank">Furniture Feature Friday</a>

and <a title="Blue Cricket Design" href="http://www.bluecricketdesign.net/" target="_blank">Blue Cricket Design</a>

<p style="text-align: center">
  ******
</p>