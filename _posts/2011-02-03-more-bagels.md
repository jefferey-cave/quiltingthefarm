---
id: 467
title: More Bagels
date: 2011-02-03T13:28:06+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=467
permalink: /2011/02/03/more-bagels/
image: /files/2011/02/bagels.jpg
categories:
  - Kitchen
tags:
  - baking
  - breads
  - recipes
---
[<img class="alignleft size-medium wp-image-468" title="bagels" src="http://quiltingthefarm.plaidsheep.ca/files/2011/02/bagels-300x225.jpg" alt="" width="267" height="189" />](http://quiltingthefarm.plaidsheep.ca/files/2011/02/bagels.jpg)This is my second batch of bagels using the <a href="http://quiltingthefarm.vius.ca/2011/01/baking-bread/" target="_self">Grandmother&#8217;s Bread recipe</a>. You can find the original recipe <a href="http://chickensintheroad.com/cooking/grandmother-bread/" target="_blank">here</a>. They turned out wonderfully well, soft and chewy with a touch of crunchy sea salt sprinkled on top.

So far I have made regular bread, bagels, sprouted grain loaves and a sweet bread (I&#8217;ll share the other recipes with you in a few days) and all have turned out great. Having fresh, warm homemade bread is so easy. I&#8217;m loving it and so is <a href="http://vius.ca/2011/02/my-wife-doesnt-want-me-around-anymore/" target="_blank">Hubby!</a>