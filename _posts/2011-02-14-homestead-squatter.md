---
id: 673
title: Homestead Squatter
date: 2011-02-14T10:00:14+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=673
permalink: /2011/02/14/homestead-squatter/
categories:
  - Rural Living
---
I had no idea that when we purchased our homestead, we would be sharing it with another being who thinks we have absolutely no right to be here.

I was going to write about our grumpy, self absorbed <span style="text-decoration: line-through">squirrel</span> squatter, but Hubby tells the story of the <a title="Homestead Squatter" href="http://vius.ca/2011/02/evicting-squatters/" target="_blank">Homestead Squatter</a> so much better than I can.

<p style="text-align: center">
  ******
</p>