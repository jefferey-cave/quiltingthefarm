---
id: 1925
title: Homegrown Maple Syrup
date: 2011-05-14T07:01:04+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=1925
permalink: /2011/05/14/homegrown-maple-syrup/
categories:
  - Rural Living
---
<span id="Homegrown_maple_syrup">

<h2>
  Homegrown maple syrup!
</h2></span> 

Not from our trees, but from the <a title="sugaring off" href="http://quiltingthefarm.vius.ca/2011/03/sugaring-off/" target="_self">sap we collected</a> with our neighbour back in March. He did all the hard work of boiling it down and putting it in jars, and we got the easy job of enjoying some.

<a rel="attachment wp-att-1926" href="http://quiltingthefarm.vius.ca/2011/05/homegrown-maple-syrup/maple-syrup/"><img class="alignnone size-full wp-image-1926" title="maple-syrup" src="http://quiltingthefarm.plaidsheep.ca/files/2011/05/maple-syrup.jpg" alt="jar of maple syrup" width="412" height="309" /></a>

The jar was full! We just had to taste it before I took pictures 🙂 It was very tasty over our <a title="applesauce pancakes" href="http://quiltingthefarm.vius.ca/2011/01/applesauce-pancakes/" target="_self">applesauce pancakes</a>, a little thinner than usual maple syrup, but very sweet.

I really requested this for my Dad (Mom and Dad were visiting). He loves maple syrup but they can&#8217;t get it in England very easily. 

Next year we will tap a few of our own trees. This summer and fall we will be out tying ribbons to the ones we want to tap so they are easily identifiable in the leafless winter months.

<p style="text-align: center">
   
</p>

<p style="text-align: center">
  ******
</p>