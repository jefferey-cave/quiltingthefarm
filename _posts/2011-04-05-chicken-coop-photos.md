---
id: 1589
title: '&#8220;Chicken Coop&#8221; Photos'
date: 2011-04-05T06:32:00+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=1589
permalink: /2011/04/05/chicken-coop-photos/
categories:
  - Rural Living
tags:
  - chickens
  - country life
  - DIY
  - farm animals
---
First of all I&#8217;d like to say &#8220;<span style="color: #800080">Thank you</span>&#8221; to everyone for all the help so far. I have uploaded some photos of the **new chicken coop** (to be) for anyone who might have been holding out to see it before commenting (it was hard to explain without pictures).

I think, once it&#8217;s all sorted out and renovated, it will be a wonderful place for our chickens. I might even paint them a rooster mural on the wall. _Okay, that&#8217;s really for me but I&#8217;m sure they&#8217;ll appreciate it in their own way:)_

<a rel="attachment wp-att-1594" href="http://quiltingthefarm.vius.ca/2011/04/chicken-coop-photos/coop-with-yard/"><img class="alignnone size-full wp-image-1594" title="coop-with-yard" src="http://quiltingthefarm.plaidsheep.ca/files/2011/04/coop-with-yard.jpg" alt="chicken coop with yard" width="412" height="309" /></a>

Here is the new &#8220;coop&#8221;.  The door is facing south. I think a window here would be nice to capture all that sunshine. The coop is attached to the garage.

<a rel="attachment wp-att-1595" href="http://quiltingthefarm.vius.ca/2011/04/chicken-coop-photos/east-side-coop/"><img class="alignnone size-full wp-image-1595" title="east-side-coop" src="http://quiltingthefarm.plaidsheep.ca/files/2011/04/east-side-coop.jpg" alt="coop boarded up door and window" width="412" height="309" /></a>

Here is the east side with the boarded up window and door.

<a rel="attachment wp-att-1590" href="http://quiltingthefarm.vius.ca/2011/04/chicken-coop-photos/west-side-coop/"><img class="alignnone size-full wp-image-1590" title="west-side-coop" src="http://quiltingthefarm.plaidsheep.ca/files/2011/04/west-side-coop.jpg" alt="west side of coop" width="412" height="309" /></a>

This is the west side. There is no door on this side and one will need to cut out for the chickens to enter the yard.

<a rel="attachment wp-att-1596" href="http://quiltingthefarm.vius.ca/2011/04/chicken-coop-photos/inside-coop1/"><img class="alignnone size-full wp-image-1596" title="inside-coop1" src="http://quiltingthefarm.plaidsheep.ca/files/2011/04/inside-coop1.jpg" alt="inside coop" width="412" height="309" /></a>

Inside the coop facing south. The area to the right is just a hole in the drywall. The floor is concrete.

<a rel="attachment wp-att-1597" href="http://quiltingthefarm.vius.ca/2011/04/chicken-coop-photos/inside-coop2/"><img class="alignnone size-full wp-image-1597" title="inside-coop2" src="http://quiltingthefarm.plaidsheep.ca/files/2011/04/inside-coop2.jpg" alt="inside coop facing north" width="412" height="309" /></a>

Inside coop facing north. There is an area for ventilation at the top and there is electricity. This door leads into the garage.

<a rel="attachment wp-att-1591" href="http://quiltingthefarm.vius.ca/2011/04/chicken-coop-photos/coop-drywall/"><img class="alignnone size-full wp-image-1591" title="coop-drywall" src="http://quiltingthefarm.plaidsheep.ca/files/2011/04/coop-drywall.jpg" alt="drywall gaps" width="412" height="309" /></a>

The drywall hasn&#8217;t been finished at the edges by the doors.

<a rel="attachment wp-att-1593" href="http://quiltingthefarm.vius.ca/2011/04/chicken-coop-photos/coop-inbetween/"><img class="alignnone size-full wp-image-1593" title="coop-inbetween" src="http://quiltingthefarm.plaidsheep.ca/files/2011/04/coop-inbetween.jpg" alt="in between the coop and garage" width="412" height="309" /></a>

<a rel="attachment wp-att-1592" href="http://quiltingthefarm.vius.ca/2011/04/chicken-coop-photos/coop-inbetween2/"><img class="alignnone size-full wp-image-1592" title="coop-inbetween2" src="http://quiltingthefarm.plaidsheep.ca/files/2011/04/coop-inbetween2.jpg" alt="in between coop and garage" width="412" height="309" /></a>

This is the area where the coop is joined to the garage. We still have some wood stored in here. It is sealed, insulated and weather proof. This might be a good area to store feed.