---
id: 3099
title: On High Alert
date: 2011-09-10T07:00:00+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=3099
permalink: /2011/09/10/on-high-alert/
tinymce_signature_post_setting:
  - 'yes'
categories:
  - Rural Living
---
As I looked out of the upstairs window, to my horror I saw a huge bird sitting on the gate of the chicken yard. Actually, I thought at first, it was an animal because of its size. I yelled to Hubby (who was downstairs), he raced out with broom in hand, and watched as the bird flew away.

We checked on the chickens, there were still 12, but they have decided to stay in the coop today. Even when I collected the eggs this morning they didn&#8217;t scoot out the pop door, they just scattered around the coop. They obviously knew a predator was around.

Hubby said he thought it was a hawk, but he only saw it as it flew off, and didn&#8217;t get a very good look. I thought that it was an owl-like bird, huge and rather rotund looking, perhaps a barred owl.  Whatever it was, it was obviously up to no good, and has us a little on edge. We have had plans to build an enclosed chicken run for our birds for sometime now, but have been putting it off in favour of other chores. As of today, it&#8217;s right back at the top of the list.