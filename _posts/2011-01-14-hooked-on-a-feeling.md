---
id: 25
title: Hooked on a Feeling
date: 2011-01-14T23:16:10+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=25
permalink: /2011/01/14/hooked-on-a-feeling/
categories:
  - Crafts
tags:
  - crafts
  - decorating
  - designing
  - DIY
  - fabric
  - rugs
---
I want to make a rug.

I have made a t-shirt rug before but now I want to make a wool hooked rug. This is what I found at Value Village a couple of weeks ago…

<p style="text-align: center">
  <a href="http://quiltingthefarm.plaidsheep.ca/files/2011/01/hookingwool.jpg"><img class="size-medium wp-image-152 aligncenter" title="Rug wool" src="http://quiltingthefarm.plaidsheep.ca/files/2011/01/hookingwool-300x225.jpg" alt="" width="300" height="225" /></a>
</p>

a whole bag of ready to use wool for rug hooking. I can’t tell you how excited I am. This stuff isn’t cheap and I got it all for $7. I asked for a piece of burlap for Christmas (how sad is that) so all I need now is a hook and a cool design.

I’m going to be a hooker!