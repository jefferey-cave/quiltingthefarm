---
id: 1418
title: Green Shoots!
date: 2011-03-25T07:01:05+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=1418
permalink: /2011/03/25/green-shoots/
image: /files/2011/03/cucumber-sprouts.jpg
categories:
  - Garden
tags:
  - garden
  - gardening
  - seeds
  - sprouting
---
My seeds seem to like the spot I have chosen for them. When I peeked today this is what I saw&#8230;

<a rel="attachment wp-att-1423" href="http://quiltingthefarm.vius.ca/2011/03/green-shoots/basil-sprouts/"><img class="alignnone size-full wp-image-1423" title="basil-seedlings" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/basil-sprouts.jpg" alt="basil seedlings" width="412" height="309" /></a>

These are Sweet Basil seedlings. It looks like they will need some thinning out.

<a rel="attachment wp-att-1421" href="http://quiltingthefarm.vius.ca/2011/03/green-shoots/thyme-sprouts/"><img class="alignnone size-full wp-image-1421" title="thyme-seedlings" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/thyme-sprouts.jpg" alt="thyme-seedlings" width="412" height="297" /></a>

This is Thyme. They got off to a slightly later start and are smaller than the Basil.

<a rel="attachment wp-att-1419" href="http://quiltingthefarm.vius.ca/2011/03/green-shoots/chives-sprouts/"><img class="size-full wp-image-1419 alignnone" title="chives-seedlings" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/chives-sprouts.jpg" alt="chives seedlings" width="412" height="309" /></a>

Here are the chives, they are just peeking out from the soil.

<a rel="attachment wp-att-1422" href="http://quiltingthefarm.vius.ca/2011/03/green-shoots/tomato-sprouts/"><img class="alignnone size-full wp-image-1422" title="tomato-seedlings" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/tomato-sprouts.jpg" alt="tomato seedlings" width="412" height="309" /></a>

Tomato seedlings look happy to have found a place in the sunshine. I think at least one seedling has appeared in each pot.

<a rel="attachment wp-att-1420" href="http://quiltingthefarm.vius.ca/2011/03/green-shoots/cucumber-sprouts/"><img class="alignnone size-full wp-image-1420" title="cucumber-seedlings" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/cucumber-sprouts.jpg" alt="cucumber seedlings" width="412" height="309" /></a>

My cucumber seedlings are far and away the largest! Unfortunately Jen, one of my readers pointed out I had planted them to soon. I have to wait and plant them closer to last spring frost. A mistake on my part, but I guess that&#8217;s why they give you so many seeds in a package!

There you have my trials and errors for this first batch of seedlings. Neither the chili peppers nor Lavender have shown their faces yet, and I have my doubts that they will.