---
id: 2628
title: Celebrating The Small Farm
date: 2011-07-25T07:00:31+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2628
permalink: /2011/07/25/celebrating-the-small-farm/
tinymce_signature_post_setting:
  - 'yes'
categories:
  - Rural Living
---
Hubby and I are attending a conference today. &#8220;A Celebration of Small-Scale Farming&#8221; is being held at the Northville Farm Heritage Centre here in the Annapolis Valley.

We have decided that to make it more worthwhile, we will attend different courses throughout the day and meet up for a quick lunch and then again after the farm tour. The Agenda for the conference goes like this:

The first session choices are

  * **Season Extension in Vegetable Production** _(me attending)__﻿_
  * **Pastured Poultry Production** (_Hubby attending)_
  * **Dairy Goat Nutrition**

The second session choices are

  * **Compost Tea in Small Fruit Production** _(me)_
__

  * **Finishing Market Lambs** _(Hubby)_
  * **Women’s Panel: Finding Your Place in the Business**

There is then a lunch break with all sorts of things going on like bee keeping/bee products, used farm equipment, safe animal handling, bread making, wood shingle making, grafting fruit trees, blacksmith demo/ seed saving

The afternoon session choices are

  * **Animal Powered Farming**
  * **Marketing Farm** **Products** _(me)_
  * **Permaculture and** **Its Application on** **the Farm** _(Hubby)_

At the end of the conference there is a choice of vineyard, apple orchard or vegetable farm tour.

I have marked out which courses I will be taking, and which courses Hubby will be taking. Unfortunately there are only the 2 of us, but 3 different courses in a session. I really want to be in 2 places at one time! It&#8217;s hard to miss out of something that could be really useful.

The day seems full of interesting and exciting things to learn, and I hope we can bring some new knowledge back to our small farm that will help us as we move forward.