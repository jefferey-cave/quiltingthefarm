---
id: 2736
title: Dewberry Season
date: 2011-08-07T07:00:25+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2736
permalink: /2011/08/07/dewberry-season/
tinymce_signature_post_setting:
  - ""
categories:
  - Rural Living
---
The Blueberries have been wonderful. Hubby and I picked another 3 cups of them yesterday afternoon. Sadly it looks like they are coming to an end. Next year we will be able to uncover the rest of the bushes, pull out all the competing weeds, and have ourselves a great harvest. So, what&#8217;s next in the fruit department? **Blackberries!**

While picking the blueberries, we came across ripening blackberries here, <span><span>there,</span></span> and everywhere. I believe these are Dewberries, an early trailing species of blackberry. Our main blackberries aren&#8217;t ripe yet, although I don&#8217;t think it will be that long before we can start to enjoy them, but these dewberries are just about ready.

<a rel="attachment wp-att-2737" href="http://quiltingthefarm.vius.ca/2011/08/dewberry-season/colander-dewberries/"><img class="alignnone size-full wp-image-2737" title="colander-dewberries" src="http://quiltingthefarm.plaidsheep.ca/files/2011/08/colander-dewberries.jpg" alt="" width="412" height="309" /></a>

It is still a little early to be picking them, they are still red in parts, and not as sweet as they should be. I guess I&#8217;ll have to wait another week or so before I haul myself out to the field with berry basket in hand.

Here&#8217;s a conundrum for you&#8230;_What looks like a blueberry, tastes like a blueberry, grows and ripens with the blueberries, but isn&#8217;t a blueberry(the Lowbush variety) ?_ I&#8217;ll give you a clue&#8230;it&#8217;s NOT a Huckleberry either. All I can tell you is, the leaves are a darker green with red on them and the berries are a much darker blue/black and a little shiny. If you have any ideas please let me know, because the only thing we&#8217;ve come up with so far is Southern Blueberry, which seems to be hardy to zone 6 (we are between a 5b and a 6 here), but that&#8217;s pushing the limits quite a bit, so I doubt it&#8217;s that.