---
id: 2298
title: Garden Updates
date: 2011-06-21T06:38:24+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2298
permalink: /2011/06/21/garden-updates/
categories:
  - Garden
---
Today I picked some pineapple weed from the yard. I will dry this over the course of a few days, then grind it up for brewing into hot or iced tea. This is our summer staple drink&#8230;very refreshing.

<a rel="attachment wp-att-2299" href="http://quiltingthefarm.vius.ca/2011/06/garden-updates/pineapple-weed-2/"><img class="alignnone size-full wp-image-2299" title="pineapple-weed" src="http://quiltingthefarm.plaidsheep.ca/files/2011/06/pineapple-weed1.jpg" alt="drying pineapple weed" width="309" height="412" /></a>

The first tomtoes are beginning.

<a rel="attachment wp-att-2300" href="http://quiltingthefarm.vius.ca/2011/06/garden-updates/first-tomatoes/"><img class="alignnone size-full wp-image-2300" title="first-tomatoes" src="http://quiltingthefarm.plaidsheep.ca/files/2011/06/first-tomatoes.jpg" alt="first tomatoes" width="412" height="309" /></a>

The strawberries are ripening.

<a rel="attachment wp-att-2307" href="http://quiltingthefarm.vius.ca/2011/06/garden-updates/ripening-strawberries/"><img class="size-full wp-image-2307 alignnone" title="ripening-strawberries" src="http://quiltingthefarm.plaidsheep.ca/files/2011/06/ripening-strawberries.jpg" alt="ripening strawberries" width="412" height="309" /></a>

The very first sweet pepper.

<a rel="attachment wp-att-2301" href="http://quiltingthefarm.vius.ca/2011/06/garden-updates/peppers/"><img class="alignnone size-full wp-image-2301" title="peppers" src="http://quiltingthefarm.plaidsheep.ca/files/2011/06/peppers.jpg" alt="sweet peppers" width="412" height="309" /></a>

Two different kinds of potatoes.

<img class="alignnone size-full wp-image-2306" title="potatoes" src="http://quiltingthefarm.plaidsheep.ca/files/2011/06/potatoes.jpg" alt="potatoes" width="412" height="309" />

Tiny fuzzy peaches. Unfortunately it looks like the tree has leaf curl so the peaches won&#8217;t ripen. Next year we&#8217;ll try to take measures to correct that.

<a rel="attachment wp-att-2305" href="http://quiltingthefarm.vius.ca/2011/06/garden-updates/baby-peach/"><img class="alignnone size-full wp-image-2305" title="baby-peach" src="http://quiltingthefarm.plaidsheep.ca/files/2011/06/baby-peach.jpg" alt="tiny peaches" width="412" height="309" /></a>

Lots of apples! This was the first apple tree to blossom and it is heavy with tiny fruit.

<a rel="attachment wp-att-2303" href="http://quiltingthefarm.vius.ca/2011/06/garden-updates/apples/"><img class="alignnone size-full wp-image-2303" title="apples" src="http://quiltingthefarm.plaidsheep.ca/files/2011/06/apples.jpg" alt="lots of apples" width="412" height="309" /></a>

Huge hostas coming into flower. The leaves of these plants must be about 14 inches long. I had no idea hostas got this large.

<a rel="attachment wp-att-2302" href="http://quiltingthefarm.vius.ca/2011/06/garden-updates/hostas-flowering/"><img class="alignnone size-full wp-image-2302" title="hostas-flowering" src="http://quiltingthefarm.plaidsheep.ca/files/2011/06/hostas-flowering.jpg" alt="hostas coming into flower" width="412" height="309" /></a>

<p style="text-align: center">
  *****
</p>