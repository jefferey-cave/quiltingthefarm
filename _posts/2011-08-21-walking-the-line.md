---
id: 2890
title: Walking the Line
date: 2011-08-21T07:00:02+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2890
permalink: /2011/08/21/walking-the-line/
aktt_notify_twitter:
  - 'yes'
tinymce_signature_post_setting:
  - 'yes'
aktt_tweeted:
  - "1"
categories:
  - Rural Living
---
Yesterday we walked the fence line&#8230;the new fence that we haven&#8217;t installed yet, that is. It was a tough slog for Hubby, who went into the thick brush without me. He got stuck at one point, and I got a radio call when he fell in the ditch. It&#8217;s going to be a difficult fence to build. Perhaps we should procrastinate some more&#8230;NO, we just have to get this done! No fence equals no livestock. We really need livestock to clear the land, but to have livestock we need to clear some land. We keep going around in circles. It&#8217;s enough to make your head spin.

For the first pasture,  we have chosen the east side of the property because of the proximity to the large and small barns. The new pasture will start behind the barns, along the east property line to the road, along the road and back up to the barn following the east side of the driveway, but set back.

We have chatted with our neighbours, trying to pin point the property line. Our neighbours are so easy going they aren&#8217;t worried about it, but the chances are,  they won&#8217;t always be our neighbours and the new neighbours are likely to be less forgiving.

<a rel="attachment wp-att-2891" href="http://quiltingthefarm.vius.ca/2011/08/walking-the-line/pasture1/"><img class="alignnone size-full wp-image-2891" title="pasture1" src="http://quiltingthefarm.plaidsheep.ca/files/2011/08/pasture1.jpg" alt="" width="412" height="309" /></a>

Looking southeast across the new pasture. It will extend behind the barns somewhat.

<a rel="attachment wp-att-2895" href="http://quiltingthefarm.vius.ca/2011/08/walking-the-line/pasture2/"><img class="alignnone size-full wp-image-2895" title="pasture2" src="http://quiltingthefarm.plaidsheep.ca/files/2011/08/pasture2.jpg" alt="" width="412" height="309" /></a>

Looking northeast. The grasses, weeds and brush are quite deep. Most of this is well above my waist.

<a rel="attachment wp-att-2894" href="http://quiltingthefarm.vius.ca/2011/08/walking-the-line/pasture3/"><img class="alignnone size-full wp-image-2894" title="pasture3" src="http://quiltingthefarm.plaidsheep.ca/files/2011/08/pasture3.jpg" alt="" width="412" height="309" /></a>

Looking south. The pond, which will be incorporated into the pasture, is on the left. It&#8217;s overgrown and impossible to see right now.

<a rel="attachment wp-att-2892" href="http://quiltingthefarm.vius.ca/2011/08/walking-the-line/pasture5/"><img class="alignnone size-full wp-image-2892" title="pasture5" src="http://quiltingthefarm.plaidsheep.ca/files/2011/08/pasture5.jpg" alt="" width="412" height="309" /></a>

This should make a wonderful lush pasture and browse. We are thinking that highland cattle will be our first livestock, they&#8217;re pretty docile, wonderfully winter hardy and great brush browsers. Add to that the fencing requirements aren&#8217;t too bad with 3 strands of barbed wire being sufficient. 

As soon as we have this pasture done, we will start on the next one and we already have the perfect place picked out.