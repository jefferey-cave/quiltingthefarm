---
id: 2636
title: 'Next Year&#8217;s Seed Stash'
date: 2011-07-27T07:00:03+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2636
permalink: /2011/07/27/next-years-seed-stash/
tinymce_signature_post_setting:
  - 'yes'
categories:
  - Garden
---
At the conference the other day, there was a company selling seeds. <a href="http://www.hopeseed.com" target="_blank">Hope Seeds </a>is an organic seed company very close to where we live. The owner is new to the province, coming from New Brunswick last Fall. I have seen their seeds for sale at the market in Annapolis Royal (I think).

As I will be needing more seeds next year, I took the time to choose several packets from their selection. The best part was the price&#8230;all their seeds were on sale for $1 per package. These are all certified organic and sustainably-grown, heritage and open-pollinated garden seeds for $1! I bought as much as I had cash in my wallet (unfortunately not as much as I would have liked).

Here&#8217;s my stash:

<a rel="attachment wp-att-2637" href="http://quiltingthefarm.vius.ca/2011/07/next-years-seed-stash/hope-seeds/"><img class="alignnone size-full wp-image-2637" title="hope-seeds" src="http://quiltingthefarm.plaidsheep.ca/files/2011/07/hope-seeds.jpg" alt="" width="412" height="309" /></a>

I think I bought 17 packages of seeds (all different). It will give me a great start to my garden next year.