---
id: 2466
title: And the Wall Came Tumbling Down
date: 2011-07-06T07:07:18+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2466
permalink: /2011/07/06/and-the-wall-came-tumbling-down/
categories:
  - Rural Living
---
It didn&#8217;t exactly tumble, we hit it with a sledge hammer&#8230;on purpose.

I think I&#8217;ve said it before, but I&#8217;ll say it again, most of my house is clad in the most ugly pine tongue and groove. I know some people like this &#8220;cabiny&#8221; look, but I&#8217;m not one of them. It&#8217;s not that the pine walls are that bad really, it&#8217;s more the way it was put up with some vertical boards, some diaganol boards and some odds and ends fitted together like a jigsaw puzzles&#8230;with a few pieces missing. The walls are so thick that the panelling extents past the molding around the door&#8230;not exactly an elegant look.

I have been dying to know what&#8217;s under the pine, so yesterday morning we got out the pry bar and sledge hammer and went to work. We chose the wall of the stairs. This is what we found.

The original wall looked like this. I forgot to take a picture of the stair wall, but this is the one next to it. [<img class="alignnone size-full wp-image-2468" title="wall1" src="http://quiltingthefarm.plaidsheep.ca/files/2011/07/wall1.jpg" alt="" width="412" height="309" />](http://quiltingthefarm.plaidsheep.ca/files/2011/07/wall1.jpg)

Under the pine there is white plywood molding. We had already removed the lower piece when I remembered to take a picture.

<a rel="attachment wp-att-2469" href="http://quiltingthefarm.vius.ca/2011/07/and-the-wall-came-tumbling-down/wall2/"><img class="alignnone size-full wp-image-2469" title="wall2" src="http://quiltingthefarm.plaidsheep.ca/files/2011/07/wall2.jpg" alt="" width="412" height="309" /></a>

As you can see, there was &#8220;lovely&#8221; wallpaper underneath. Hubby is sure it was put on upside down. Under this wallpaper there was a layer of plain pink paper (shown) then a layer of pattern green and grey paper and below that hints of an older wallpaper that we could barely make out because it was stuck the the layer in front.

<a rel="attachment wp-att-2470" href="http://quiltingthefarm.vius.ca/2011/07/and-the-wall-came-tumbling-down/wall3/"><img class="alignnone size-full wp-image-2470" title="wall3" src="http://quiltingthefarm.plaidsheep.ca/files/2011/07/wall3.jpg" alt="" width="412" height="309" /></a>

And finally lath and plaster! We knocked all the plaster off, which was in surprisingly good condition and we are left staring at the lath. We are leaving the wall for now as we are going to put in a wood burning stove in this corner, and we need to find out what the best course of action is for this.

Until then I&#8217;m strangely enjoying looking at the lath, there&#8217;s character there and history, who cares about the mess!

<a rel="attachment wp-att-2467" href="http://quiltingthefarm.vius.ca/2011/07/and-the-wall-came-tumbling-down/wall4/"><img class="alignnone size-full wp-image-2467" title="wall4" src="http://quiltingthefarm.plaidsheep.ca/files/2011/07/wall4.jpg" alt="" width="412" height="309" /></a>