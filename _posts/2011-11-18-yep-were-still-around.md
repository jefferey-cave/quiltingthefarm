---
id: 3388
title: 'Yep, we&#8217;re still around.'
date: 2011-11-18T16:42:18+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=3388
permalink: /2011/11/18/yep-were-still-around/
tinymce_signature_post_setting:
  - ""
categories:
  - Rural Living
---
I know I said that my new farm website would be up and running now, but things got crazy around here. I hope you accept my apologies. I really hope to have the new site up in the next few weeks. It&#8217;s all designed and ready to go, but I&#8217;m waiting for hubby to have the time to put it all in to WordPress.

Anyway, I thought you might like an update on life here at the farm.

Hubby has found a permanent seasonal (long season) farm job that he really enjoys and he&#8217;s getting paid to do it! His job should run from about the middle of March until November with it being a mix of full time some months and part time other months. He has also signed up to become a volunteer firefighter for the town, and is waiting for a vacancy to open up. Also, Hubby and I have both been working on several graphic and website projects (paid). It&#8217;s crazy how everything comes all at once.

I have been working on many art projects, pastels being my favourite medium at the moment. I&#8217;ve had some good reviews and I am hoping to break into the Annapolis art circle one day 🙂 I have also been busy crafting for an upcoming Christmas craft sale. Unfortunately I didn&#8217;t know I had a table until last week leaving me little time to get ready, so now I am working night and day to make enough to sell.

The farm itself is coming along nicely. The chickens are still laying enough eggs for us _and_ to sell. The garden was still producing lettuce, swiss chard, kale, leeks, cabbages, rutabagas and carrots until we put it to bed a couple of days ago. We still have to till up a larger area for next year, but hopefully we&#8217;ll get to that next week. We have decided to concentrate our efforts on this farm in the cultivation of fruits. Our property is next door to, what once was, a thriving fruit farm a few years back, and the climate and soil seem really suited to a long season of tree and bush fruit with frosts coming slightly later than on the floor of the valley (in fact my last tomato plant survived until the first week on November!).

So, I apologize once again to those of you who are eagerly awaiting the new site, it&#8217;s coming soon, honest it is. Thanks for hanging in there and checking back. See you soon.