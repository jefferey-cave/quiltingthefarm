---
id: 23
title: And the North Wind Shall Blow
date: 2011-01-05T23:14:33+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=23
permalink: /2011/01/05/and-the-north-wind-shall-blow/
categories:
  - Rural Living
tags:
  - home
  - weather
  - winter
---
[<img class="alignleft size-thumbnail wp-image-111" title="winter_wonderland" src="http://quiltingthefarm.plaidsheep.ca/files/2011/01/winter_wonderland-150x150.jpg" alt="" width="150" height="150" />](http://quiltingthefarm.plaidsheep.ca/files/2011/01/winter_wonderland.jpg)And we shall have snow, lots of snow. But it’s not the snow that I take issue with, it’s those winds howling around, especially when they’re **in** my house.

Our old homestead is a tad drafty. I guess a tad, is really a bit of an understatement. We have been trying to track down the sources of these drafts since we moved in and today the kitchen window came under scrutiny. Yes, there was definitely a draft (more like a hurricane if you ask me) coming from the side and top of the window. I remember from years ago using that plastic film, that you heat up with a hair-dryer, over the window, so off I went to the store to pick some up.<!--more-->

Arriving home with plastic in hand I set out to seal the window. I’ve done this before, so it’ll be no problem, ha, not so fast. A mildly drafty window is one thing but, a window where the wind seems to be trying to make a break from the cold and squeeze itself through a tiny space into your living area is another. Anyway, I set to work. I put the sticky tape around the window. This part goes well. I then cut the film to size. This part also goes according to plan. I then tape the film up at the top of the window…okay so far, and then to the sides and bottom…Noooo! the plastic film took off like a nuclear powered torpedo, straight across my kitchen and landing on the dining room table like a limp condom!

Not to be beaten, I made another attempt, this time with hubby in tow. He held the plastic on one side; I held it on the other, and heated it up with the hairdryer. It worked…sort of. We sealed the last few gaps with duck tape and admired our handy work.

Now I can’t open the window, for any reason, until summer 🙁