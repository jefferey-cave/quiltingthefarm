---
id: 1153
title: A Walk on the Wild Side
date: 2011-03-14T08:09:59+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=1153
permalink: /2011/03/14/a-walk-on-the-wild-side/
image: /files/2011/03/wild-mushrooms2.jpg
categories:
  - Rural Living
tags:
  - breakfasts
  - country life
  - land
  - trees
  - weather
---
<span id="Sunday_started_this_way8230">

<h2>
  Sunday started this way&#8230;
</h2></span> 

<a rel="attachment wp-att-1154" href="http://quiltingthefarm.vius.ca/2011/03/a-walk-on-the-wild-side/breakfast/"><img class="alignnone size-full wp-image-1154" title="breakfast" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/breakfast.jpg" alt="Home grown breakfast" width="412" height="309" /></a>

Fresh free range eggs courtesy <a title="Downshift me" href="http://www.downshiftme.com/" target="_blank">Matt and AJ</a>, local cured bacon, and homemade (by me) oat rye bread.

<span id="Then_on_to_a_walk_on_the_wild_side">

<h2>
  Then on to a walk on the wild side
</h2></span> 

Our newly discovered &#8220;meadow&#8221;

<a rel="attachment wp-att-1158" href="http://quiltingthefarm.vius.ca/2011/03/a-walk-on-the-wild-side/meadow/"><img class="alignnone size-full wp-image-1158" title="meadow" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/meadow.jpg" alt="meadow" width="412" height="309" /></a>

Wild mushrooms

<a rel="attachment wp-att-1156" href="http://quiltingthefarm.vius.ca/2011/03/a-walk-on-the-wild-side/wild-mushrooms2/"><img class="alignnone size-full wp-image-1156" title="wild-mushrooms2" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/wild-mushrooms2.jpg" alt="wild mushrooms" width="412" height="309" /></a>

Fluorescent green moss!

<a rel="attachment wp-att-1155" href="http://quiltingthefarm.vius.ca/2011/03/a-walk-on-the-wild-side/moss/"><img class="alignnone size-full wp-image-1155" title="moss" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/moss.jpg" alt="moss" width="412" height="309" /></a>

More wild mushrooms

<a rel="attachment wp-att-1157" href="http://quiltingthefarm.vius.ca/2011/03/a-walk-on-the-wild-side/wild-mushrooms/"><img class="alignnone size-full wp-image-1157" title="wild-mushrooms" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/wild-mushrooms.jpg" alt="wild mushrooms on log" width="412" height="309" /></a>

Spring is everywhere!

\***\***