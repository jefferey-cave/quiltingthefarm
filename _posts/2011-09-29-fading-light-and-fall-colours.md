---
id: 3284
title: Fading Light and Fall Colours
date: 2011-09-29T07:13:11+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=3284
permalink: /2011/09/29/fading-light-and-fall-colours/
tinymce_signature_post_setting:
  - ""
categories:
  - Rural Living
---
The weather has been beautiful. If only it would last, but alas, winter is not far away. First, of course, we get to enjoy the wonderful colours that Fall affords us. The trees are already showing their Autumn hues, dressing themselves in leaves of scarlet and rust. This year is supposed to bring us a spectacular Fall colour show. We are lucky here in the East to have such a show. Back in Alberta, Fall lasts approximately half a day. The leaves start to turn, then a big wind comes and whips up a frenzy, knocking all the leaves to the ground. Everyone sighs as they get ready to rake up what was left of Autumn.

Back in Alberta the grass is now dead and brown, not so here. I have noticed that although we haven&#8217;t been able to mow our grass (broken tractor), it seems much shorter now it&#8217;s started to lay flat. I can see lots of new blueberry bushes poking their heads through to the catch the dying rays of the sun, now that the mat of cut grass has broken down. Next year should be quite the harvest.

The blackberries are all done now. I enjoyed the hiking and the discovery of new bushes.  Those with plumpest juiciest berries were almost always just out of reach, requiring a lack of good sense to retrieve. I retrieved many 😉 I have yet to put most of the berries to good use and other than a few pancake breakfasts and a pie here and there they sit in the freezer awaiting their fate. Some are destined for the canning pot, some to the carboy, and others will enjoy their last moments sprinkled on oatmeal. It is a fine life for a berry.

The hens are laying less eggs. They aren&#8217;t in molt yet as far as I know, so it&#8217;s probably the fading light. It will soon be time when they will have to spend more hours in their coop than out of it. Predators prowl more as their wild prey goes underground. It is not safe here after dusk.

The fading light in the evenings makes me want to hunker down with a crochet hook and a mountain of brightly coloured yarn. I am crocheting baby and toddler hats; pretty beanies with bows and flowers, antique lace and buttons adding playful charm. I will be ready for the Christmas craft sale blitz when it arrives in November.

<a rel="attachment wp-att-3288" href="http://quiltingthefarm.vius.ca/2011/09/fading-light-and-fall-colours/fading-light/"><img class="alignnone size-full wp-image-3288" title="fading-light" src="http://quiltingthefarm.plaidsheep.ca/files/2011/09/fading-light.jpg" alt="" width="412" height="309" /></a>