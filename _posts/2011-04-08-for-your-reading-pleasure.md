---
id: 1663
title: For Your Reading Pleasure
date: 2011-04-08T06:36:44+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=1663
permalink: /2011/04/08/for-your-reading-pleasure/
categories:
  - Rural Living
tags:
  - books
---
In our new rural life we are without TV. Well, not an actual television, but a subscription to satelite, cable or even the &#8220;peasant vision&#8221; freebies. We do watch some of the TV shows we have purchased and the (very) occasional movie on a Friday evening, but mostly we just read&#8230;a lot!

I thought I would share some of my favourite novels. I am a fan of Dystopian novels so they appear very frequently on my list.

  * Brave New World
  * Ape and Essence
  * Island
  * 1984
  * Animal Farm
  * Atlas Shrugged
  * On the Beach
  * A Town Like Alice
  * We
  * Slaughterhouse Five
  * To Kill a Mockingbird
  * The Road
  * The Handmaid&#8217;s Tale
  * A Clockwork Orange
  * The Prime of Miss Jean Brodie
  * The Sheltering Sky
  * White Noise
  * War of the Worlds
  * Day of the Trifids
  * The Chrysalids
  * Never let me go
  * Wuthering Heights
  * Tess of the d&#8217;Urbervilles
  * Far from the Madding Crowd
  * The Cider House Rules
  * <label for="ctl00_C_CList_GridViewLists_ctl38_CBDone">Memoirs of a Geisha</label>
  * <label for="ctl00_C_CList_GridViewLists_ctl38_CBDone"><label for="ctl00_C_CList_GridViewLists_ctl50_CBDone">Life of Pi</label></label>

<label for="ctl00_C_CList_GridViewLists_ctl38_CBDone"><label for="ctl00_C_CList_GridViewLists_ctl50_CBDone">Have you read any of these novels? What&#8217;s your favourite novel?</label></label>

<label for="ctl00_C_CList_GridViewLists_ctl38_CBDone"><label for="ctl00_C_CList_GridViewLists_ctl50_CBDone"></label></label>