---
id: 351
title: Homestead Bread Gets Seedy!
date: 2011-02-07T10:00:37+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=351
permalink: /2011/02/07/the-breads-getting-seedy/
image: /files/2011/01/sproutedgrain.jpg
categories:
  - Kitchen
  - Recipes
tags:
  - baking
  - breads
  - food
  - grains
  - recipes
  - sprouting
  - sprouts
---
I have been using Suzanne’s <a href="http://quiltingthefarm.vius.ca/2011/01/baking-bread/" target="_blank">Grandmother’s Bread</a> recipe for about 4 weeks now and I’m really having fun with this bread. Yesterday, I added sprouted wheat and sprouted quinoa to my regular bread recipe.

First off, I started some wheat berries and some quinoa seeds to sprout. The wheat needed to be soaked for 24 hours, I wasn’t sure about the quinoa so I soaked that too. After soaking I left them to sprout for a couple of days. The wheat had just started, but the quinoa had sprouted quite a lot (next time, I will probably not sprout the quinoa that long).

Once I was ready to bake my bread, I whipped out my trusty Magic Bullet (Christmas pressie from Hubby) and whizzed up the sprouts for a few seconds. I probably had about 3/4 cup of sprouts.

<p style="text-align: center">
  <a href="http://quiltingthefarm.plaidsheep.ca/files/2011/01/sproutedgrainblender.jpg"><img class="size-medium wp-image-297 aligncenter" title="sproutedgrainblender" src="http://quiltingthefarm.plaidsheep.ca/files/2011/01/sproutedgrainblender-300x225.jpg" alt="" width="300" height="225" /></a>
</p>

 

<p style="text-align: left">
  I used about 2 ½ cups of water instead of the usual 3.
</p>

<p style="text-align: center">
   <a href="http://quiltingthefarm.plaidsheep.ca/files/2011/01/sproutedgrain.jpg"><img class="size-medium wp-image-296 aligncenter" title="sproutedgrain" src="http://quiltingthefarm.plaidsheep.ca/files/2011/01/sproutedgrain-300x225.jpg" alt="" width="300" height="225" /></a>
</p>

<p style="text-align: left">
  I added the sprouts along with the flour. I had thought, the sprouts would add more moisture, but it turned out that I had to add more water while kneading, so next time I’ll use the usual amount.
</p>

<p style="text-align: center">
  <a href="http://quiltingthefarm.plaidsheep.ca/files/2011/01/sproutedgrainbread.jpg"><img class="size-medium wp-image-285 aligncenter" title="sproutedgrainbread" src="http://quiltingthefarm.plaidsheep.ca/files/2011/01/sproutedgrainbread-300x225.jpg" alt="" width="300" height="225" /></a>
</p>

<p style="text-align: center">
  <a href="http://quiltingthefarm.plaidsheep.ca/files/2011/01/sproutedgrain.jpg"></a>
</p>

 

The bread baked up nicely and it’s very tasty; a little bit nutty but still not too heavy.