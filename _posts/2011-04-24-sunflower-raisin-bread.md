---
id: 1868
title: Sunflower Raisin Bread
date: 2011-04-24T06:30:26+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=1868
permalink: /2011/04/24/sunflower-raisin-bread/
categories:
  - Kitchen
  - Recipes
---
<span id="Happy_Easter....">

<h1>
  <span style="color: #cc99ff">.<span style="color: #99ccff">.</span>.<span style="color: #ff99cc">.</span>.H<span style="color: #99ccff">a</span>p<span style="color: #ff99cc">p</span>y <span style="color: #99ccff">E</span>a<span style="color: #ff99cc">s</span>t<span style="color: #99ccff">e</span>r<span style="color: #99ccff">!</span>.<span style="color: #ff99cc">.</span>.<span style="color: #99ccff">.</span></span>
</h1></span> 

I wanted to bake a rustic, artisan type bread for Easter. I had seen a <a title="Chickens in the Road. Open crumb bread" href="http://chickensintheroad.com/cooking/in-search-of-open-crumb/" target="_blank">recipe</a> on Chickens in the Road on making a sponge first. I tried this with my bread recipe. I made mine with oat flour, sunflower seeds and raisins. It&#8217;s yummy!

<span id="Sunflower_Raisin_Bread">

<h2>
  Sunflower Raisin Bread
</h2></span> 

  * 4 cups warm milk
  * 1 1/2 tablespoons yeast
  * 1/4 cup honey
  * 1 1/2 tablespoons salt
  * 6-7  cups unbleached flour, divided
  * 1 1/2 tablespoons oil
  * 3 cup oat flour
  * ½ cup quick oats
  * ½ cup raisins
  * 1 tablespoon gluten flour

<span id="Makethe_sponge">

<h3>
  Make the sponge 
</h3></span> 

Place the warm milk in a large bowl. Add the yeast and honey. Let sit about 20 minutes (it should be nice and foamy).  Mix in 2 1/2 cups of the unbleached flour and the salt. Let sit for two (or more) hours, the longer the better. The sponge can sit up to 12 hours.

<span id="Make_the_bread">

<h3>
  Make the bread
</h3></span> 

Add the oil to the sponge and then gradually add the oat four and the rest of the unbleached flour, the gluten flour the raisins and the sunflower seeds.  Turn the dough out on to a floured surface and knead for 20 minutes (dough should be elastic and smooth).

Place dough in a floured bowl (no need to grease); cover with a lid and let rise in a warm, draft-free place until doubled in size.

Turn dough out onto floured surface again. Press dough gently to get rid of any large bubbles. Divide into 3 pieces and put in oiled loaf pans or make free form loaves. Dust with flour, slash the tops and sprinkle with some sunflower seeds (optional).

Cover the loaves with a damp tea towel and let them rise again until doubled.

Bake at 350-degrees for about 30 minutes or until golden brown. Loaves should sound somewhat hollow when tapped on the bottom.

Let cool on rack.

<a rel="attachment wp-att-1871" href="http://quiltingthefarm.vius.ca/2011/04/sunflower-raisin-bread/sunflower-raisin-bread-2/"><img class="alignnone size-full wp-image-1871" title="sunflower-raisin-bread" src="http://quiltingthefarm.plaidsheep.ca/files/2011/04/sunflower-raisin-bread.jpg" alt="sunflower raisin bread" width="412" height="309" /></a>