---
id: 3137
title: Painting the Porch
date: 2011-09-14T07:00:15+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=3137
permalink: /2011/09/14/painting-the-porch/
tinymce_signature_post_setting:
  - 'yes'
categories:
  - Crafts
---
I have spent the last few days busily painting the sun room. This was the sun room before, notice the &#8220;wonderful&#8221; (insert sarcasm here) pine tongue and groove that has been used absolutely everywhere in the house :0. Painting tongue and groove is hard work, all those nooks and crannies are a pain in the butt, and the pine just soaks up the paint like a sponge.

<a rel="attachment wp-att-228" href="http://quiltingthefarm.vius.ca/2011/02/too-much-of-a-good-thing/sunroom/"><img class="alignnone size-full wp-image-228" title="sunroom" src="http://quiltingthefarm.plaidsheep.ca/files/2011/01/sunroom.jpg" alt="" width="412" height="309" /></a>

I used a grey paint that I watered down. I painted one thin coat and let it dry. I&#8217;m pleased with the effect, a kind of silver weathered look that works well for the sun room turned porch. I painted the ceiling the same grey and the trim a nice bright white. The windows are, unfortunately, metal so they have to stay the same dark brown&#8230;I really wish I could paint them 🙁

<a rel="attachment wp-att-3138" href="http://quiltingthefarm.vius.ca/2011/09/painting-the-porch/porch/"><img class="alignnone size-full wp-image-3138" title="porch" src="http://quiltingthefarm.plaidsheep.ca/files/2011/09/porch.jpg" alt="" width="412" height="309" /></a>

I&#8217;m amazed at how bright the porch looks now, and how much brighter the place is looking out from the study/office. Next spring we are going to take out all the windows and put in just screen instead. We think an outdoor room will be more useful to us than an enclosed sun room.

Now I&#8217;m motivated to paint more stuff! I think I&#8217;m going to start on the study tomorrow.