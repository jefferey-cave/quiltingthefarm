---
id: 1911
title: New Readers and New Window.
date: 2011-05-11T06:48:52+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=1911
permalink: /2011/05/11/new-readers-and-new-window/
categories:
  - Kitchen
---
<span id="New_Readers">

<h2>
  <span style="color: #cc99ff">New Readers</span>
</h2></span> 

First of all, I would like to say &#8220;Hello&#8221; and &#8220;Welcome&#8221; to some of my new readers.

Laura, and her friends at Brooklands School in East Bergholt, England, have been reading my blog recently and I have heard that they have it bookmarked on one of the school computers! Thanks girls, I hope you enjoy your visits! Leave a comment sometime&#8230;I&#8217;d like that.

<span id="New_Window">

<h2>
  <span style="color: #339966">New Window</span>
</h2></span> 

On to my new window. I have a new kitchen window, one that doesn&#8217;t leak cold air! Pumpkin&#8217;s &#8220;Hubby&#8221; put the new one in last week&#8230;and what a big difference. I was a little concerned that the new window wasn&#8217;t wide enough, but it has worked out really well. The new window is taller than the old one and the white frame makes it look much brighter inside.

<a rel="attachment wp-att-1912" href="http://quiltingthefarm.vius.ca/2011/05/new-readers-and-new-window/new-kitchen-window/"><img class="alignnone size-full wp-image-1912" title="new-kitchen-window" src="http://quiltingthefarm.plaidsheep.ca/files/2011/05/new-kitchen-window.jpg" alt="new kitchen window" width="309" height="412" /></a>

All the windows have been caulked now (none had been caulked before) so we are hoping the house stays a little warmer.

We will be starting to revamp the kitchen quite soon now. we&#8217;ll have to pull off all the old beadboard, paneling, drywall layers to find out if the walls are insulated before we go any further.