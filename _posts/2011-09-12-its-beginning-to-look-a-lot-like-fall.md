---
id: 3114
title: 'It&#8217;s Beginning to Look a Lot Like Fall'
date: 2011-09-12T07:00:34+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=3114
permalink: /2011/09/12/its-beginning-to-look-a-lot-like-fall/
tinymce_signature_post_setting:
  - 'yes'
categories:
  - Rural Living
---
Fall is on its way. Saturday, although a  gorgeous sunny day, had that slight chill in the air that heralds the cooler season. The leaves are starting to turn.Fall in Nova Scotia is quite beautiful.

Hubby has finished stacking all the wood. That makes 9 cords altogether. We ordered 6 cords and still had 3 left over from last year. With our new wood stove in we hope to burn around 3 cords this coming winter.

We are still picking buckets full of blackberries, and I&#8217;m finding new areas that I haven&#8217;t picked before. We now have a freezer full of blackberries, blueberries, peaches and there&#8217;s still the apples to come. We have found someone who will press our apples for cider. He charges about 70 cents a litre plus the jug (under 50 cents for a 2 litre one). So 2 litres of our cider will cost us under $2. We are going to give it a try in a couple of weeks.

I have been painting the sunroom. The room used to be a porch, but has been enclosed sometime in the past. We will eventually open it back up , and screen the whole thing so we can enjoy summer evenings watlooking over the pond. For the time being I wanted to give it a fresher, more inviting look. I found an oops paint in grey at the hardware store. I watered it down to give the pine tongue and groove a whitewashed look. The grey works really nicely and gives the feel of old grey, weathered boards. It wasn&#8217;t what I expected, but I really like the effect. I still have to paint the ceiling&#8230;not something I&#8217;m looking forward to.

Last evening I was given a lobster. Hubby cooked it this morning, and now we have to come up with a nice dish to share the aforementioned lobster. I&#8217;m thinking creamed lobster with rice might be a tasty treat. It&#8217;s not everyday someone gives you a 2lb lobster.

Hubby has been painting the outside of the Inn I work at. The house is an old 4 story mansion. He is working up on scaffolding, and he&#8217;s not that keen on heights! There&#8217;s a lot to paint, I&#8217;m not sure how long he&#8217;ll be at it. It&#8217;s all good practice as I&#8217;m in the mood for a nice yellow house myself this Fall 😉

I&#8217;m still picking lots of tomatoes, and I&#8217;m actually giving some away now. I think I&#8217;ll have to make another batch of salsa and some more sauce. If all the green ones don&#8217;t ripen I&#8217;ll have more green tomatoes than I know what to do with. Does anybody have a tasty recipe for green tomaotes?

I picked, blanched and froze some of the swiss chard still growing in the garden&#8230;this stuff is never ending! Perhaps more pesto is in order.

I have re-arranged the furniture. I&#8217;m trying to get the best use out of the space we have, but there&#8217;s always something in the way of making it more efficient. I try moving stuff here, I try moving stuff there, with hopes of a better flow of traffic and not so much wasted space. I have turned the whole living room around and I think I have a winner&#8230;well sort of. There are still some details to work out, but I&#8217;m liking it.