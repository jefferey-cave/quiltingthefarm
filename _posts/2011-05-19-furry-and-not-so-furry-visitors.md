---
id: 1980
title: Furry (and Not So Furry) Visitors
date: 2011-05-19T07:26:59+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=1980
permalink: /2011/05/19/furry-and-not-so-furry-visitors/
image: /files/2011/05/turtle.jpg
categories:
  - Rural Living
---
We are starting to see many new faces around our farm. Just this past week there have been sightings of &#8230;

chipmunks

<a rel="attachment wp-att-1981" href="http://quiltingthefarm.vius.ca/2011/05/furry-and-not-so-furry-visitors/chipmunk2/"><img class="alignnone size-full wp-image-1981" title="chipmunk2" src="http://quiltingthefarm.plaidsheep.ca/files/2011/05/chipmunk2.jpg" alt="chipmunk in backyard" width="412" height="309" /></a>

wild rabbits

<a rel="attachment wp-att-1983" href="http://quiltingthefarm.vius.ca/2011/05/furry-and-not-so-furry-visitors/hare/"><img class="alignnone size-full wp-image-1983" title="hare" src="http://quiltingthefarm.plaidsheep.ca/files/2011/05/hare.jpg" alt="young Hares" width="412" height="337" /></a>

a snapping turtle

<a rel="attachment wp-att-1982" href="http://quiltingthefarm.vius.ca/2011/05/furry-and-not-so-furry-visitors/turtle/"><img class="alignnone size-full wp-image-1982" title="turtle" src="http://quiltingthefarm.plaidsheep.ca/files/2011/05/turtle.jpg" alt="snapping turtle" width="412" height="309" /></a>

a vole, and a snake or two (sorry no pics, they were just too fast).

The deer seem to have moved on now so perhaps I won&#8217;t need such a high fence for my garden, although I&#8217;ll definitely have to make sure the rabbits don&#8217;t eat my greens before I get a chance!