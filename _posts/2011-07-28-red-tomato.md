---
id: 2643
title: Red Tomato
date: 2011-07-28T06:50:08+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2643
permalink: /2011/07/28/red-tomato/
tinymce_signature_post_setting:
  - 'yes'
categories:
  - Garden
---
This is my very first ripe tomato of the year.

There were loads of little green tomatoes on the bushy stems, a few in the orange stage, and then this, strange alien, siamese twin, ripe tomato. I actually thought there were two tomatoes until I tried to pick it off the vine.  These are the tomato plants that have taken over my garden beds, there had better be a bumper harvest!

<a rel="attachment wp-att-2644" href="http://quiltingthefarm.vius.ca/2011/07/red-tomato/first-red-tomato/"><img class="alignnone size-full wp-image-2644" title="first-red-tomato" src="http://quiltingthefarm.plaidsheep.ca/files/2011/07/first-red-tomato.jpg" alt="" width="412" height="309" /></a>

It may have been odd looking but it sure tasted sweet and juicy. I can&#8217;t wait for the others now 🙂