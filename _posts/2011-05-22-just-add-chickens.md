---
id: 2002
title: Just Add Chickens
date: 2011-05-22T06:58:48+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2002
permalink: /2011/05/22/just-add-chickens/
categories:
  - Rural Living
---
We have been busy building the chicken coop.

We framed in just under half of the &#8220;shed&#8221; which is really a small barn of about 15 feet by 12 ft. It is fully insulated and has electricity.  

We covered the insulation in the &#8216;coop&#8217; part with OSB and framed in a &#8216;people&#8217; door along the long side

<a rel="attachment wp-att-2011" href="http://quiltingthefarm.vius.ca/2011/05/just-add-chickens/framing2/"><img class="alignnone size-full wp-image-2011" title="framing2" src="http://quiltingthefarm.plaidsheep.ca/files/2011/05/framing2.jpg" alt="framing the coop" width="412" height="309" /></a>

We built in the nesting boxes. That&#8217;s my Dad hiding behind the wall 😉

<a rel="attachment wp-att-2027" href="http://quiltingthefarm.vius.ca/2011/05/just-add-chickens/framing-boxes/"><img class="alignnone size-full wp-image-2027" title="framing-boxes" src="http://quiltingthefarm.plaidsheep.ca/files/2011/05/framing-boxes.jpg" alt="framing the nesting boxes" width="412" height="309" /></a>

We haven&#8217;t covered the insulation on the non-framed in side as yet.

<a rel="attachment wp-att-2010" href="http://quiltingthefarm.vius.ca/2011/05/just-add-chickens/framing1/"><img class="alignnone size-full wp-image-2010" title="framing1" src="http://quiltingthefarm.plaidsheep.ca/files/2011/05/framing1.jpg" alt="framing the coop" width="412" height="309" /></a>

<a rel="attachment wp-att-2011" href="http://quiltingthefarm.vius.ca/2011/05/just-add-chickens/framing2/"></a>

We decided on just 2 nesting boxes for now as our plan is to only have 6 chickens. We basically made one large box and put a removable divider in the middle.

<a rel="attachment wp-att-2016" href="http://quiltingthefarm.vius.ca/2011/05/just-add-chickens/nesting-boxes1/"><img class="alignnone size-full wp-image-2016" title="nesting-boxes1" src="http://quiltingthefarm.plaidsheep.ca/files/2011/05/nesting-boxes1.jpg" alt="nesting boxes" width="412" height="309" /></a>

The whole nesting box slides out so it is removable for cleaning

<a rel="attachment wp-att-2003" href="http://quiltingthefarm.vius.ca/2011/05/just-add-chickens/side-nesting-boxes/"><img class="alignnone size-full wp-image-2003" title="side-nesting-boxes" src="http://quiltingthefarm.plaidsheep.ca/files/2011/05/side-nesting-boxes.jpg" alt="nesting boxes" width="412" height="309" /></a>

The door on the back opens downwards. Although we did purchase door clasps, these wood ones were a better option.

<a rel="attachment wp-att-2015" href="http://quiltingthefarm.vius.ca/2011/05/just-add-chickens/nesting-box-door/"><img class="alignnone size-full wp-image-2015" title="nesting-box-door" src="http://quiltingthefarm.plaidsheep.ca/files/2011/05/nesting-box-door.jpg" alt="nesting box door" width="412" height="309" /></a>

Hubby is outside building the door. It folds down and has rungs across for chicken stairs.

<a rel="attachment wp-att-2014" href="http://quiltingthefarm.vius.ca/2011/05/just-add-chickens/making-the-door/"><img class="alignnone size-full wp-image-2014" title="making-the-door" src="http://quiltingthefarm.plaidsheep.ca/files/2011/05/making-the-door.jpg" alt="the chicken door" width="412" height="309" /></a>

The outside of the door. There is a frame built about an inch larger all around so that the door insets nicely and makes a good seal. We will be insulating the door before winter.

<a rel="attachment wp-att-2018" href="http://quiltingthefarm.vius.ca/2011/05/just-add-chickens/outside-chicken-door/"><img class="alignnone size-full wp-image-2018" title="outside-chicken-door" src="http://quiltingthefarm.plaidsheep.ca/files/2011/05/outside-chicken-door.jpg" alt="outside chicken door" width="412" height="309" /></a>

We added chicken wire to the door frame

[<img class="alignnone size-full wp-image-2008" title="finished-coop-door" src="http://quiltingthefarm.plaidsheep.ca/files/2011/05/finished-coop-door.jpg" alt="finished coop door" width="309" height="412" />](http://quiltingthefarm.plaidsheep.ca/files/2011/05/finished-coop-door.jpg)

This is my old kitchen window. We cut out this window on the south side of the coop. In the summer there will be mottled sunlight through the apple trees. It makes a perfect window for our new flock. We still have to add hardware cloth to the inside.

<a rel="attachment wp-att-2017" href="http://quiltingthefarm.vius.ca/2011/05/just-add-chickens/old-kitchen-window/"><img class="alignnone size-full wp-image-2017" title="old-kitchen-window" src="http://quiltingthefarm.plaidsheep.ca/files/2011/05/old-kitchen-window.jpg" alt="southside window" width="412" height="309" /></a>

This is the east facing window. We have covered it with hardware cloth and will make a removable or opening plexiglass window for the cooler weather

<a rel="attachment wp-att-2012" href="http://quiltingthefarm.vius.ca/2011/05/just-add-chickens/hardware-cloth-on-window/"><img class="alignnone size-full wp-image-2012" title="hardware-cloth-on-window" src="http://quiltingthefarm.plaidsheep.ca/files/2011/05/hardware-cloth-on-window.jpg" alt="window covered in hardware cloth" width="412" height="309" /></a>

A sloping roof was added to the nesting boxes to discourage roosting, giving us a nice little shelf for egg collecting.

<a rel="attachment wp-att-2009" href="http://quiltingthefarm.vius.ca/2011/05/just-add-chickens/finished-nest-box-back/"><img class="alignnone size-full wp-image-2009" title="finished-nest-box-back" src="http://quiltingthefarm.plaidsheep.ca/files/2011/05/finished-nest-box-back.jpg" alt="finished nest box back" width="309" height="412" /></a>

The finished coop. All the wire is stapled on. We used the hex wire as the shed itself is fully secure. We aren&#8217;t worried about predators in here.

<a rel="attachment wp-att-2006" href="http://quiltingthefarm.vius.ca/2011/05/just-add-chickens/finished-coop1/"><img class="alignnone size-full wp-image-2006" title="finished-coop1" src="http://quiltingthefarm.plaidsheep.ca/files/2011/05/finished-coop1.jpg" alt="finished coop" width="412" height="309" /></a>

All we need is bedding, a feeder and waterer and a couple of perches before we welcome our first guests to &#8220;Coop de ville&#8221;

<a rel="attachment wp-att-2007" href="http://quiltingthefarm.vius.ca/2011/05/just-add-chickens/finished-coop2/"><img class="alignnone size-full wp-image-2007" title="finished-coop2" src="http://quiltingthefarm.plaidsheep.ca/files/2011/05/finished-coop2.jpg" alt="finished coop" width="412" height="309" /></a>

The &#8220;we&#8221; in this post wasn&#8217;t just Hubby and me: thanks Dad and T-Man (Pumpkin&#8217;s hubby). We couldn&#8217;t have done it without you!

<p style="text-align: center">
  ******
</p>

<a rel="attachment wp-att-2005" href="http://quiltingthefarm.vius.ca/2011/05/just-add-chickens/coop-door-latch/"></a>