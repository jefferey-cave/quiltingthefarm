---
id: 640
title: Sunshine and Vitamin D
date: 2011-02-13T10:14:15+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=640
permalink: /2011/02/13/sunshine-and-vitamin-d/
image: /files/2011/02/knee_deep.jpg
categories:
  - Rural Living
tags:
  - land
  - ponds
  - snow
  - snowstorms
  - weather
  - winter
---
As we don’t have any animals as yet I have felt little need to venture outside. The snow is deep and the wind is cold, so I sit inside my drafty house all bundled up from the cold winter chill. But yesterday I read on <a href="http://homesteadgardenandpantry.com/agrarian-life/self-reliance/moving-towards-a-more-self-reliant-life/" target="_blank">Homestead Garden and Pantry</a> that every homesteader should get outside for at least a couple of hours a day…every day. Well, didn&#8217;t I feel like a homesteading failure. So today I figured I needed some photos of our pond, not to mention some badly needed vitamin D!

Hubby and I trekked down the driveway to the pond. While trying to find the pathway to the pond, Hubby managed to get himself stuck waist deep in snow. I had the good sense to put snow pants on…Hubby did not. With camera in hand, I laughed and laughed, and promptly forgot to take a photo (rolls eyes).

We didn’t make it to the pond.

Well, even if I didn’t get photos of the pond, I did get a small dose of vitamin D and a couple of pics of Hubby in the snow. I didn’t get my 2 hours outside but 20 minutes is better than nothing. I’m not a homesteader yet, but I’m getting there.

<div id="attachment_643" style="width: 310px" class="wp-caption aligncenter">
  <a rel="attachment wp-att-643" href="http://quiltingthefarm.vius.ca/2011/02/sunshine-and-vitamin-d/knee_deep/"><img class="size-medium wp-image-643 " title="Knee_deep" src="http://quiltingthefarm.plaidsheep.ca/files/2011/02/knee_deep-300x225.jpg" alt="" width="300" height="225" /></a>
  
  <p class="wp-caption-text">
    Huh.
  </p>
</div>

  

<div id="attachment_641" style="width: 310px" class="wp-caption aligncenter">
  <a rel="attachment wp-att-641" href="http://quiltingthefarm.vius.ca/2011/02/sunshine-and-vitamin-d/knee_deep2/"><img class="size-medium wp-image-641" title="knee_deep2" src="http://quiltingthefarm.plaidsheep.ca/files/2011/02/knee_deep2-300x225.jpg" alt="" width="300" height="225" /></a>
  
  <p class="wp-caption-text">
    Where are my boots?How do I get out of here?
  </p>
</div>

<div id="attachment_642" style="width: 310px" class="wp-caption aligncenter">
  <a href="http://quiltingthefarm.vius.ca/2011/02/sunshine-and-vitamin-d/knee_deep3/" rel="attachment wp-att-642"><img src="http://quiltingthefarm.plaidsheep.ca/files/2011/02/knee_deep3-300x225.jpg" alt="" title="knee_deep3" width="300" height="225" class="size-medium wp-image-642" /></a>
  
  <p class="wp-caption-text">
    How do I get out of here?
  </p>
</div>