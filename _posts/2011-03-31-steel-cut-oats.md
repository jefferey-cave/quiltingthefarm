---
id: 1375
title: Steel Cut Oats
date: 2011-03-31T06:53:30+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=1375
permalink: /2011/03/31/steel-cut-oats/
image: /files/2011/03/bowl-oatmeal1.jpg
categories:
  - Kitchen
  - Recipes
tags:
  - breakfasts
  - food
  - frugal
  - recipes
  - winter
---
**_Oatmeal. The stick to ribs kind of oatmeal that warms you up on a cold winter morning._**

There&#8217;s rolled oats, there&#8217;s quick oats, there&#8217;s even instant oatmeal (yuk) and our very favourite kind&#8230;**Steel Cut Oats**!

Have you ever had **steel cut oats**?

<div>
  <div>
    <strong>Steel-cut oats</strong> (sometimes called Irish Oats) are whole grain groats ( middle part of the oat kernel). These groats are cut up into two or three pieces using steel disks. <em>The upside</em>&#8230;they are very tasty and nutritious. <em>The downside</em>&#8230; they are not a quick breakfast; they take a while to cook. Fortunately you will be rewarded for your wait, with a hearty, nutty hot meal that will take you through to lunchtime.
  </div>
</div>

<a rel="attachment wp-att-1512" href="http://quiltingthefarm.vius.ca/2011/03/steel-cut-oats/steel-cut-oats1/"><img class="alignnone size-full wp-image-1512" title="steel-cut-oats1" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/steel-cut-oats1.jpg" alt="steel cut oats" width="412" height="309" /></a>

<a rel="attachment wp-att-1513" href="http://quiltingthefarm.vius.ca/2011/03/steel-cut-oats/steel-cut-oats2/"><img class="alignnone size-full wp-image-1513" title="steel-cut-oats2" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/steel-cut-oats2.jpg" alt="steel cut oats" width="412" height="309" /></a>

To make

  * 1 cup steel cut oats (you can purchase these in bulk at Bulk Barn)
  * 4 cups water
  * pinch salt

Put all ingredients in a large pot. Bring to the boil and then simmer for approximately 40 mins. Serve with your choice of milk, fruit, dried fruit like raisins or cranberries, brown sugar, maple syrup or honey&#8230;there are so many possibilities.

<a rel="attachment wp-att-1511" href="http://quiltingthefarm.vius.ca/2011/03/steel-cut-oats/bowl-oatmeal-2/"><img class="alignnone size-full wp-image-1511" title="bowl-oatmeal" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/bowl-oatmeal1.jpg" alt="oatmeal with pecans and cranberries" width="412" height="309" /></a>

One of my favourites&#8230;pecans, cranberries, brown sugar and a drizzle of evaporated milk.

<p style="text-align: center">
  ******
</p>