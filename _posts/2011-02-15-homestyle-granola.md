---
id: 678
title: Homestyle Granola
date: 2011-02-15T08:51:00+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=678
permalink: /2011/02/15/homestyle-granola/
image: /files/2011/02/granola.jpg
categories:
  - Kitchen
  - Recipes
tags:
  - baking
  - breakfasts
  - food
  - grains
  - meals
---
<span id="Crunchy_yummy_granola">

<h2>
  Crunchy, yummy granola
</h2></span> 

<span id="with_my_new_secret_ingredient8230cardamom">

<h3>
  with my new secret ingredient&#8230;cardamom!
</h3></span> 

<span id="Ingredients:">

<h4>
  Ingredients:
</h4></span> 

  * 3 cups quick oats
  * 1 cup flour
  * 1/2 cup nuts
  * 1/4 cup sesame seeds
  * 1/2 cup sunflower seeds
  * 1/4 cup dried milk powder
  * 1 cup brown sugar
  * 1/4 cup honey
  * 1/3 cup butter
  * 1/2 tsp cinnamon
  * 1/2 tsp fresh ground cardamom
  * 1 tsp vanilla extract

<div id="attachment_680" style="width: 422px" class="wp-caption aligncenter">
  <a rel="attachment wp-att-680" href="http://quiltingthefarm.vius.ca/2011/02/homestyle-granola/cardamom-seeds-2/"><img class="size-full wp-image-680" title="cardamom-seeds" src="http://quiltingthefarm.plaidsheep.ca/files/2011/02/cardamom-seeds1.jpg" alt="" width="412" height="309" /></a>
  
  <p class="wp-caption-text">
    cardamom seeds
  </p>
</div>

  1. Preheat oven t0 300F.
  2. In a large bowl, combine the oats, flour, sunflower seeds, sesame seeds, dry milk powder and nuts. Set aside.
  3. In a medium saucepan combine brown sugar, butter and water. Heat until the mixture is bubbly.
  4. Add honey and stir until dissolved. Stir in the salt (I forgot), cardamom, cinnamon and vanilla. Pour the mixture over the oats mixture and stir. Make sure all the mixture is well and truly coated.
  5. Spread the mixture out on a large baking sheet, Put it the middle shelf of the oven.
  6. Bake for approximately 35 – 45 minutes, checking and stiring mixture about every 15 mins (it burns easily) . The mixture should be dry.
  7. Remove from oven and allow to cool.
  8. Serve with milk or yogurt or whatever your heart (or stomach) desires. 
    <div id="attachment_679" style="width: 422px" class="wp-caption aligncenter">
      <a rel="attachment wp-att-679" href="http://quiltingthefarm.vius.ca/2011/02/homestyle-granola/granola/"><img class="size-full wp-image-679" title="granola" src="http://quiltingthefarm.plaidsheep.ca/files/2011/02/granola.jpg" alt="" width="412" height="309" /></a>
      
      <p class="wp-caption-text">
        Spicy, crunchy granola
      </p>
    </div></li> </ol>