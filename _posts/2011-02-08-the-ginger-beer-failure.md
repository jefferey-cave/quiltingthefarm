---
id: 565
title: The Ginger Beer Failure
date: 2011-02-08T09:30:37+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=565
permalink: /2011/02/08/the-ginger-beer-failure/
image: /files/2011/01/ginger-beer-bug.jpg
categories:
  - Kitchen
  - Recipes
tags:
  - DIY
  - food
  - recipes
  - snacks
---
I _so_ wanted to write about the success of my ginger beer…alas it is not to be.

Hubby and I always have &#8220;candy&#8221; night of Fridays. We drive to the corner store, pick up candy and pop and then sit down to watch a movie. Unfortunately (or fortunately) I&#8217;m not that fond of the store bought &#8216;pop&#8217;  flavours. I have recently gravitated toward cream soda, but even that&#8217;s wearing a bit thin now. I want something tasty and different. Homemade ginger beer sounds just the thing and all you need is sugar and fresh ginger.

I followed the directions for making the “bug” found <a href="http://my5acredream.blogspot.com/2009/11/meet-my-ginger-bug.html" target="_blank">here</a> and listed below.

_In a small bowl mix:_

**1 cup warm water
  
2 tsp sugar
  
2 tsp grated ginger**

_Keep in a warm place._ 

_Add 2 tsp sugar and2 tsp ginger every until it starts to bubble._ (Leigh had hers bubbly in a couple of days).

<p style="text-align: center">
  <a href="http://quiltingthefarm.plaidsheep.ca/files/2011/01/ginger-beer-bug.jpg"><img class="size-medium wp-image-277 aligncenter" title="ginger-beer-bug" src="http://quiltingthefarm.plaidsheep.ca/files/2011/01/ginger-beer-bug-300x225.jpg" alt="" width="300" height="225" /></a>
</p>

After several days nothing much had happened to mine. As I was having no luck, I found another <a href="http://down---to---earth.blogspot.com/2009/10/live-traditional-food-making-ginger.html" target="_blank">recipe</a> (almost the same as the first but it states to wait 7 days). Everyday I added 1 desert spoon of  sugar and <!--more-->1 tablespoon of grated ginger. I waited the recommended 7 days, I was sure it looked a little bubbly by then. I added the lemon juice and water, filtered the liquid into bottles and sealed them tight.

I waited and I waited, but there was no buildup of fizziness.

Hubby and I tried some of the “ginger beer”. It tasted nice and gingery (who’d of thought!) but had no effervescence at all.

I, knowing nothing, about most things, decided to add a little bit of champagne yeast to the bottles. After a couple of days,  the plastic bottles started to get hard and were obviously starting to fill with gas from the yeast and as I didn’t want exploded ginger beer everywhere, I put them in the fridge.

<p style="text-align: center">
  <a href="http://quiltingthefarm.plaidsheep.ca/files/2011/01/bottle-ginger-beer.jpg"><img class="size-medium wp-image-274 aligncenter" title="bottle-ginger-beer" src="http://quiltingthefarm.plaidsheep.ca/files/2011/01/bottle-ginger-beer-300x225.jpg" alt="" width="300" height="225" /></a>
</p>

Unfortunately, the fizziness was just an illusion. The bottles fizzed when opened but there was only just a hint of &#8216;flat pop&#8217; taste. I don’t know where I went wrong. Obviously, the “bug” didn’t grow like it should, but I don’t know why.

I’ll try again. The ginger beer tastes great and I’m enjoying it even though it’s not fizzy.