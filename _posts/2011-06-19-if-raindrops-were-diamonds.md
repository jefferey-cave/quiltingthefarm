---
id: 2275
title: 'If Raindrops Were Diamonds&#8230;'
date: 2011-06-19T06:25:03+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2275
permalink: /2011/06/19/if-raindrops-were-diamonds/
categories:
  - Rural Living
---
Lupins catch the rain drops in their leaves and hold them there like diamonds.  

Here is one of my diamonds in the rough. [](http://quiltingthefarm.plaidsheep.ca/files/2011/06/blog-photos-may-019-e1308430694707.jpg)

[<img class="alignnone size-large wp-image-2276" title="blog photos may 019" src="http://quiltingthefarm.vius.ca/wp-content/uploads/blog-photos-may-019-1024x768.jpg" alt="lupin with raindrop" width="480" height="353" />](http://quiltingthefarm.plaidsheep.ca/files/2011/06/blog-photos-may-019-e1308430694707.jpg)

We have had so much rain this spring, I feel like I should be celebrating some of the true wonders of nature.

<p style="text-align: center">
  ******
</p>

<a rel="attachment wp-att-2276" href="http://quiltingthefarm.vius.ca/2011/06/if-raindrops-were-diamonds/blog-photos-may-019/"></a>