---
id: 2769
title: Installing the Wood Stove
date: 2011-08-11T07:00:09+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2769
permalink: /2011/08/11/installing-the-wood-stove/
tinymce_signature_post_setting:
  - 'yes'
categories:
  - Rural Living
---
Tuesday it stopped raining long enough for our new chimney to be installed.

We were supposed to have the chimney installed several weeks ago, but every time it was planned&#8230;it rained. Under normal circumstances, working on the roof in the rain, dismantling an old chimney is unpleasant, on a metal roof it is treacherous. So we waited&#8230;and we waited. Monday came around and it rained again, postponing the chimney installation for a third time. Tuesday started off &#8220;iffy&#8221; but our heating guy took his chances and luckily it paid off. He got the chimney installed before the rain came again.

<a rel="attachment wp-att-2776" href="http://quiltingthefarm.vius.ca/2011/08/installing-the-wood-stove/new-chimney/"><img class="alignnone size-full wp-image-2776" title="new-chimney" src="http://quiltingthefarm.plaidsheep.ca/files/2011/08/new-chimney.jpg" alt="" width="412" height="309" /></a>
  
There are so many things we are finding out about our house, and most of them aren&#8217;t good. While installing the chimney, we found the wiring upstairs to be lacking (certainly the person who ran the wiring was lacking in good judgement and plain common sense) and a leaky roof. None of the wiring was grounded and it was all just a birds nest mess of live and dead wiring sticking in and out of an unattached junction box. Thankfully, our heating guy,also an electrician, noticed it right away and sorted it out. The roof had a leak where it hadn&#8217;t been installed properly in the first place. There was a large reservoir of putrid water just waiting to overflow into our house. Thankfully we had some left over roofing material in the barn and our guy fixed the mess. I tell you, it pays to hire the right person.

So, with the chimney in, our new Pacific Energy Vista Classic wood stove could be installed. A couple of weeks ago Hubby and I tiled a pad for the stove to sit on. Neither of us have ever tiled before, but our local Home Hardware <span>personnel</span> <span>were wonderful and made all the tile cuts for free. All we had to do was apply the <span>Thinset</span>,</span> and place the tiles. It doesn&#8217;t look too bad if I say so myself 🙂 The stove was released from all its packaging and this turned out to be one of the hardest jobs of the day. The wood frame was nailed and stapled in so many places none of us knew where to start. I had the distinct impression someone was messing with us&#8230;

<a rel="attachment wp-att-2774" href="http://quiltingthefarm.vius.ca/2011/08/installing-the-wood-stove/stove-packaging/"><img class="alignnone size-full wp-image-2774" title="stove-packaging" src="http://quiltingthefarm.plaidsheep.ca/files/2011/08/stove-packaging.jpg" alt="" width="412" height="309" /></a>

Hmmm&#8230;

<a rel="attachment wp-att-2775" href="http://quiltingthefarm.vius.ca/2011/08/installing-the-wood-stove/woodstove/"><img class="alignnone size-full wp-image-2775" title="woodstove" src="http://quiltingthefarm.plaidsheep.ca/files/2011/08/woodstove.jpg" alt="" width="412" height="309" /></a>

All ready for winter.