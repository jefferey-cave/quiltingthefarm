---
id: 2598
title: Deconstructing the House
date: 2011-07-21T07:00:50+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2598
permalink: /2011/07/21/deconstructing-the-house/
tinymce_signature_post_setting:
  - 'yes'
categories:
  - Rural Living
---
Our new wood stove has arrived and hubby and I are busily trying to finish the prep work before the installer comes. We have taken some walls down to the studs so that we can repair and drywall them before the stove goes in. There is an incredible amount of dust over every surface in the house even though we hung plastic at the doorways. The walls tell of a long history with their many layers. I exciting uncovering the lives of the previous owners, working back through the decades to the time the house was built. Each layer tells its own story, especially when there&#8217;s newspaper glued to the walls! One of the walls had pine tongue and groove. Underneath this was ugly 70&#8217;s type panelling (although it was real plywood). There were 2 layers of drywall in parts of the wall. Underneath the drywall and panelling we came across wallpaper&#8230;7 different layers of the stuff. And to end it all, lath and plaster. Our house has gained several inches of extra space to go along with the several inches of dust.!

<a rel="attachment wp-att-2601" href="http://quiltingthefarm.vius.ca/2011/07/deconstructing-the-house/corner-reno/"><img class="alignnone size-full wp-image-2601" title="corner-reno" src="http://quiltingthefarm.plaidsheep.ca/files/2011/07/corner-reno.jpg" alt="" width="412" height="309" /></a>

I took this photo Monday and by yesterday we had all the lath off as well. The chimney is being completely removed from the roof down. We have someone coming in to do that. The new stove pipe will run into the living room ceiling and up through the existing chinmey space oon the second floor. Without a chimney in there we get some extra room in  the kitchen area.

<a rel="attachment wp-att-2599" href="http://quiltingthefarm.vius.ca/2011/07/deconstructing-the-house/brick-wall/"><img class="alignnone size-full wp-image-2599" title="brick-wall" src="http://quiltingthefarm.plaidsheep.ca/files/2011/07/brick-wall.jpg" alt="" width="412" height="309" /></a>

This was the wall we had to take down in the kitchen. It was angled right out and blocked off the kitchen and the light. I am going to reuse the bricks to build a raised garden bed.

<a rel="attachment wp-att-2600" href="http://quiltingthefarm.vius.ca/2011/07/deconstructing-the-house/brickwall-demolish/"><img class="alignnone size-full wp-image-2600" title="brickwall-demolish" src="http://quiltingthefarm.plaidsheep.ca/files/2011/07/brickwall-demolish.jpg" alt="" width="412" height="309" /></a>

This was Monday&#8217;s work and the entire wall is now gone. It took us quite a while to chip out those bricks.

<a rel="attachment wp-att-2602" href="http://quiltingthefarm.vius.ca/2011/07/deconstructing-the-house/new-stove/"><img class="alignnone size-full wp-image-2602" title="new-stove" src="http://quiltingthefarm.plaidsheep.ca/files/2011/07/new-stove.jpg" alt="" width="412" height="309" /></a>

And last but not least&#8230;our brand new Pacific Energy Vista Classic wood stove. We were going to go with the Spectrum, which is the next size up, but it ended up being too much stove for our little house. The Vista heats up to 1500 sf, and our house is about 1300 sf. Because we had to order it in, we have a choice of colour. The green was very nice, the dark red&#8230;gorgeous, but the designer in me decided to be a little more practical and in the end we went with classic black. This stove should be here for our grandchildren to inherit, and we thought that basic black is unlikely to go out of style like the other colours will. Anyone remember Avocado appliances? 🙂