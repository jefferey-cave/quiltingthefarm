---
id: 2250
title: A Dreary Tuesday
date: 2011-06-15T06:00:41+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2250
permalink: /2011/06/15/a-dreary-tuesday/
categories:
  - Rural Living
---
The weather here has become the topic of choice. The rain is supposed to hold on all week and it&#8217;s really chilly. I have the furnace on again!

The Lee Valley catalogue arrived in the mail yesterday. It&#8217;s fun looking at all the new and innovative tools; ones I&#8217;ll never need and will never buy. Having said that there are several things I&#8217;d like, will get good use from, and aren&#8217;t crazy expensive. I have a birthday coming up at the end of this month, so maybe I&#8217;ll ask for something impractical, but fun like a hammock. Lying in a hammock, in the mottled sunshine, between 2 apple trees would be a very relaxing way to spend my seista hour in the heat of the summer.

Well it&#8217;s not warm and sunny now it&#8217;s cold and dreary and a perfect day to bake. We  are on our last loaf of bread anyway. I made three loaves of bread, but the kneading seemed extra hard and my arms got tired quickly (must have been all the post pounding I was doing). I didn&#8217;t get around to making the cookies I had planned. Oh well, I&#8217;d just have eaten the lot anyway! Sometimes things just work out for the best:) Instead I put on a big pot of beans and made a hot and spicy chili; perfect for a <del>chili</del> chilly day on the farm. I love chili over baked potatoes and hopefully I&#8217;ll be harvesting my own soon.

The 15 containers of potatoes are all to the top now so it&#8217;s sit back and wait time. I do hope it hasn&#8217;t been too wet for them. My Mom and Dad had problems with theirs this year as it has been so dry in England, the opposite problem to us, but a problem nevertheless. This is the first time I&#8217;ve ever attempted to grow potatoes. The garden itself is doing remarkably well with small green shoots popping up everywhere. The cucumbers have a new lease on life and are putting up lots of new leaves.

The rain caused the chickens to stay put inside the coop most of the day. I gave them some yogurt this morning but they certainly don&#8217;t like it. Perhaps they like the flavoured stuff better? They did enjoy the bread I gave them though. We added some more litter to the floor of the coop. We are hoping to use the deep litter method and have all that wonderful compost next year. To this end we have gone with a mixture of _Sphagnum Peat Moss_ and _wood shavings._ The chickens ended up finding the shelter of the spruce trees while we were in their coop, but not before they were soaked to the skin. Poor little bedraggled chickens.

I wonder what tomorrow will bring?