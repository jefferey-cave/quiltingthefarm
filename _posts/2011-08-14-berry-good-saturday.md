---
id: 2807
title: Berry Good Saturday
date: 2011-08-14T07:00:50+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2807
permalink: /2011/08/14/berry-good-saturday/
tinymce_signature_post_setting:
  - 'yes'
categories:
  - Rural Living
---
**_I spent the day picking berries_**.

The blueberries are coming to an end now. I picked one cup this morning and there are a few cups left on the bushes. I have frozen most of the berries for enjoying over the winter, but I&#8217;ve also made some syrup and numerous batches of blueberry pancakes.

The blackberries have started to ripen. Not just the dewberries, but also the upright bushes. I went out and picked a couple of cups this morning. I used to pick blackberries as a kid. We would take off for the day with several buckets and return dirty, scratched and bleeding, but happy with our haul. My Mom put them to good use, but for the life of me, I can&#8217;t remember what she made. After living in Alberta, and not seeing a blackberry bush in years, I had forgotten how how all the good berries are always the most difficult to pick!  I came home dirty, scratched and bleeding, but happy with my haul 🙂

Along with the blackberries, I found a ripe apple below one of the trees. It&#8217;s the first one and still a little tart. It won&#8217;t be long now.

<a rel="attachment wp-att-2809" href="http://quiltingthefarm.vius.ca/2011/08/berry-good-saturday/blackberries-and-apple/"><img class="alignnone size-full wp-image-2809" title="blackberries-and-apple" src="http://quiltingthefarm.plaidsheep.ca/files/2011/08/blackberries-and-apple.jpg" alt="" width="412" height="304" /></a>

The bushes in front of the house are full of chokecherries. I noticed them a few weeks, but the number of berries has definitely dwindled since then. Not all of them are  ripe, but I figured it was me or the bears/birds/deer&#8230;I vote me! I picked a full bowl of cherries for chokecherry jelly. I&#8217;m going to try a recipe without added pectin, as all those unripe fruits should help it set. If not I&#8217;ll add pectin at the end, or call it chokecherry syrup and enjoy it poured over pancakes. It&#8217;s a win-win situation 🙂

<a rel="attachment wp-att-2810" href="http://quiltingthefarm.vius.ca/2011/08/berry-good-saturday/chokecherries/"><img class="alignnone size-full wp-image-2810" title="chokecherries" src="http://quiltingthefarm.plaidsheep.ca/files/2011/08/chokecherries.jpg" alt="" width="412" height="309" /></a>

Gratuitous picture of bug&#8230;

<a rel="attachment wp-att-2808" href="http://quiltingthefarm.vius.ca/2011/08/berry-good-saturday/dragonfly2/"><img class="alignnone size-full wp-image-2808" title="dragonfly2" src="http://quiltingthefarm.plaidsheep.ca/files/2011/08/dragonfly2.jpg" alt="" width="412" height="296" /></a>

This little dragonfly (?) was hanging out in the garden. He didn&#8217;t seem to mind me getting in close for a couple of photos.

<span style="color: #993300">UPDATE: </span>Mom said she used to make bramble jelly!