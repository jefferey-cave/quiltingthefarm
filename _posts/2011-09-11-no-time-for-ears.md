---
id: 3103
title: No Time for Ears
date: 2011-09-11T07:00:25+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=3103
permalink: /2011/09/11/no-time-for-ears/
tinymce_signature_post_setting:
  - 'yes'
  - 'yes'
categories:
  - Garden
---
Could it be?

<a rel="attachment wp-att-3104" href="http://quiltingthefarm.vius.ca/2011/09/no-time-for-ears/corn-silks/"><img class="alignnone size-full wp-image-3104" title="corn-silks" src="http://quiltingthefarm.plaidsheep.ca/files/2011/09/corn-silks.jpg" alt="" width="412" height="309" /></a>

I&#8217;m just not sure, but I think there may be little tiny silks from the start of an ear of corn. Is this how corn ears start? I have no idea.

_**2 days later&#8230;** Yep, they&#8217;re corn silks!_

_<a rel="attachment wp-att-3109" href="http://quiltingthefarm.vius.ca/2011/09/no-time-for-ears/corn-silks-2/"><img class="alignnone size-full wp-image-3109" title="corn-silks-2" src="http://quiltingthefarm.plaidsheep.ca/files/2011/09/corn-silks-2.jpg" alt="" width="412" height="309" /></a>_

This is the largest plant in my small corn patch

<a rel="attachment wp-att-3106" href="http://quiltingthefarm.vius.ca/2011/09/no-time-for-ears/corn-stalks/"><img class="alignnone size-full wp-image-3106" title="corn-stalks" src="http://quiltingthefarm.plaidsheep.ca/files/2011/09/corn-stalks.jpg" alt="" width="309" height="412" /></a>

The entire patch consists of 9 plants (with a couple of strays).

<a rel="attachment wp-att-3105" href="http://quiltingthefarm.vius.ca/2011/09/no-time-for-ears/late-summer-corn-patch/"><img class="alignnone size-full wp-image-3105" title="late-summer-corn-patch" src="http://quiltingthefarm.plaidsheep.ca/files/2011/09/late-summer-corn-patch.jpg" alt="" width="412" height="309" /></a>

Before anybody bursts my bubble, I&#8217;ll do it myself 🙁 I know it&#8217;s far too late for corn. I planted this in July as an experiment to see if this area of the property would support any growth. The area itself is very sandy with lots of gravel. In it&#8217;s former life it may have been a riding ring or even a gravel pit, it&#8217;s very barren.

It looks like the experiment was a small success. I don&#8217;t know if they would have produced corn ears if I had planted them earlier, but it does show there is some nutrient in the soil &#8211; at least I think it does. I think it&#8217;s worthwhile planting corn and possibly amaranth here next year and seeing what happens.