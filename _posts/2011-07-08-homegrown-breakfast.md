---
id: 2482
title: Homegrown Breakfast
date: 2011-07-08T07:12:50+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2482
permalink: /2011/07/08/homegrown-breakfast/
tinymce_signature_post_setting:
  - 'yes'
categories:
  - Kitchen
---
<span id="Our_very_first_homegrown_breakfast.">

<h3>
  Our very first homegrown breakfast.
</h3></span> 

<span id="">

<h3>
  <a rel="attachment wp-att-2483" href="http://quiltingthefarm.vius.ca/2011/07/homegrown-breakfast/homegrown-breakfast/"><img class="alignnone size-full wp-image-2483" title="homegrown-breakfast" src="http://quiltingthefarm.plaidsheep.ca/files/2011/07/homegrown-breakfast.jpg" alt="" width="412" height="309" /></a>
</h3></span> 

French toast with fresh strawberries!

My homemade bread dipped in eggs laid by our own hens with a topping of freshly picked homegrown strawberries. Okay, I cheated a smidge, as the little bit of sugar and oil weren&#8217;t homegrown, but the rest is all ours. It sure was satisfying in more ways than one.