---
id: 2347
title: First Harvest!
date: 2011-06-26T06:54:32+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2347
permalink: /2011/06/26/first-harvest/
categories:
  - Garden
---
Our very first harvest. It may be small, but it&#8217;s very tasty.

Plump, ripe strawberries, straight from the garden.

<a rel="attachment wp-att-2348" href="http://quiltingthefarm.vius.ca/2011/06/first-harvest/first-strawberries/"><img class="alignnone size-full wp-image-2348" title="first-strawberries" src="http://quiltingthefarm.plaidsheep.ca/files/2011/06/first-strawberries.jpg" alt="ripe strawberries" width="412" height="309" /></a>