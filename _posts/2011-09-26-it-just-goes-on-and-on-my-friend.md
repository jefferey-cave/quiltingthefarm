---
id: 3245
title: 'It Just goes On and On My Friend&#8230;'
date: 2011-09-26T07:12:10+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=3245
permalink: /2011/09/26/it-just-goes-on-and-on-my-friend/
tinymce_signature_post_setting:
  - 'yes'
categories:
  - Garden
---
My garden that is 🙂

Yesterday I picked yet another large basket of ripe tomatoes. There are still more cucumbers, beans and squash growing away, there has not been a frost yet to take them out. I will be out again tomorrow filling this same basket with the tomatoes that were almost, but not quite ripe today. Yesterday I again sold  tomatoes to a neighbour, I also gave away a bag to a friend. I just can&#8217;t believe I&#8217;ve had this much produce from such a small area. I eagerly await next year, and a whole new year of crops and produce. I have my seeds ready to go.

<a rel="attachment wp-att-3246" href="http://quiltingthefarm.vius.ca/2011/09/it-just-goes-on-and-on-my-friend/garden-basket/"><img class="alignnone size-full wp-image-3246" title="garden-basket" src="http://quiltingthefarm.plaidsheep.ca/files/2011/09/garden-basket.jpg" alt="" width="412" height="311" /></a>

As we want a little bigger garden next year, Hubby built another 8&#215;4 raised bed. We are placing this one next to the other two; we&#8217;ll have to extend the fence a little, it held the deer (and everything else at bay).  We are also going to till another small area to plant in.

<a rel="attachment wp-att-3247" href="http://quiltingthefarm.vius.ca/2011/09/it-just-goes-on-and-on-my-friend/raised-bed3/"><img class="alignnone size-full wp-image-3247" title="raised-bed3" src="http://quiltingthefarm.plaidsheep.ca/files/2011/09/raised-bed3.jpg" alt="" width="412" height="309" /></a>

Moving on to the next project&#8230;Today we have to get out the huge tarps and go shake our apple trees! We need to collect a large banana box full to take to our cider guy by Saturday. Can&#8217;t you just taste that homegrown cider? I know I can 🙂

<a rel="attachment wp-att-3248" href="http://quiltingthefarm.vius.ca/2011/09/it-just-goes-on-and-on-my-friend/russet-apples/"><img class="alignnone size-full wp-image-3248" title="russet-apples" src="http://quiltingthefarm.plaidsheep.ca/files/2011/09/russet-apples.jpg" alt="" width="412" height="309" /></a>