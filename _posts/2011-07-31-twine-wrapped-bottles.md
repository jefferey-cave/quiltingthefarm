---
id: 2665
title: Twine Wrapped Bottles
date: 2011-07-31T07:02:03+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2665
permalink: /2011/07/31/twine-wrapped-bottles/
tinymce_signature_post_setting:
  - 'yes'
categories:
  - Crafts
---
The wildflowers (A.K.A weeds) look so pretty, and there&#8217;s so many of them, I thought that they could brighten up the inside of my house as well as the outside. Unfortunately, as I gave away most of my vases before we moved here, I&#8217;m basically left with canning jars, a couple of glass baby bottles (left here after daughter&#8217;s visit) and a handfull of empty wine bottles. I like the shape of the wine bottles, but the labels always seem to leave a residue, and they look like&#8230;well wine bottles. Here is an idea I found online somewhere during my globetrotting journey around the Internet.

<a rel="attachment wp-att-2667" href="http://quiltingthefarm.vius.ca/2011/07/twine-wrapped-bottles/twine-bottles/"><img class="alignnone size-full wp-image-2667" title="twine-bottles" src="http://quiltingthefarm.plaidsheep.ca/files/2011/07/twine-bottles.jpg" alt="" width="309" height="412" /></a>

(You have to excuse all that dust, we are in the middle of tearing down lath and plaster walls :0)

All you need is a bottle, twine and glue.

<a rel="attachment wp-att-2666" href="http://quiltingthefarm.vius.ca/2011/07/twine-wrapped-bottles/twine/"><img class="alignnone size-full wp-image-2666" title="twine" src="http://quiltingthefarm.plaidsheep.ca/files/2011/07/twine.jpg" alt="" width="412" height="309" /></a>

I purchased this large spool of twine cheap at the local hardware store. I had already wrapped the bottles when I took this photo, so there&#8217;s enough on this $6 spool to do many more (or use it for other stuff&#8230;string, useful stuff, string).

I used Aleene&#8217;s original Tacky Glue, which truly lived up to its name! The twine stayed on the bottle, and on my fingers when I wasn&#8217;t careful!

I still have to add some fancy touches to the bottles&#8230;perhaps rafia, a ribbon, or even an antique button or two. though I should probably wait until the dust dies down 😉