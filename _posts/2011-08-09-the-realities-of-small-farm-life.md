---
id: 2754
title: The Realities of Small Farm Life
date: 2011-08-09T07:00:38+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2754
permalink: /2011/08/09/the-realities-of-small-farm-life/
tinymce_signature_post_setting:
  - ""
categories:
  - Rural Living
---
There are no illusions. Small farm life is not all puppies and roses. Along with the maintenance of the property, the upkeep of the house and the need for farm equipment, it means that doing work outside of the farm is a inevitable&#8230;it&#8217;s just a reality.

When we purchased our farm, we were under no illusions that we would require money not supplied to us by our farm, especially in the early years. Owning a farm, and working it from scratch is the same as working any other business. You work hard for little or no short term pay-off, only long term, future dreams. Hubby and I have an at home business that helps pay our way. Hubby is a Software Engineer and I am a Graphic Designer. Together we build software applications, websites and a host of marketing material for our customers. One day, in the distant future, when and if our farm is profitable, this is something we would like to drop, but for now, we still need to eat, pay our bills, and buy what we need.

<a rel="attachment wp-att-2759" href="http://quiltingthefarm.vius.ca/2011/08/the-realities-of-small-farm-life/graphic-people/"><img class="alignnone size-full wp-image-2759" title="graphic-people" src="http://quiltingthefarm.plaidsheep.ca/files/2011/08/graphic-people.jpg" alt="" width="298" height="116" /></a>

Yesterday was spent writing a proposal for a client in Calgary, Alberta. Although we used to live there, this company doesn&#8217;t know us from our business there. It strange that you can move 5000 km across the country, and your prospective client comes from the place you just left behind. Writing a proposal, at least a good one, takes time and effort with absolutely no guarantee of work. You want to do the best job you can, but have to keep in mind that the time you are putting in may not pay off. It&#8217;s a fine balance.

Holding that fine balance while building the farm is precarious, sometimes dizzying. You can look down and see the big picture; something wonderful that was (and still is) just a dream. You find yourself ready to jump on every bandwagon that promises you a better life, but holding back because deep down you know that everything has its cost.  Investing time and resources into projects that may not pay off in the end is as stark a reality as it is anywhere else. It&#8217;s knowing how much to invest that can make or break the dream and hindsight is always 20-20.

This year was about keeping it small and letting the farm lead us in the direction it wants to go. I don&#8217;t mean that in a fluffy, frou-frou way, but more about the traits of the land itself and how we can capitalize on them. Starting small means that profits may not be huge, or even exist at this point. Learning about ourselves, our skills, talents and most importantly&#8230;likes and dislikes, is an important step in the process.  This is a life term dream for us. We have a long time to figure things out.

In the meantime we write proposals, relying on the skills we have already developed over the years. Skills that will be put on the backburner as new skills are developed.