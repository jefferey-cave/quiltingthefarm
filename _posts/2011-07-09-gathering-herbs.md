---
id: 2491
title: Gathering Herbs
date: 2011-07-09T07:15:28+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2491
permalink: /2011/07/09/gathering-herbs/
tinymce_signature_post_setting:
  - 'yes'
categories:
  - Rural Living
---
Recently I have been spending a great deal of time outside hunting for wild herbs that grow around the property. I have decided to make a binder of everything I find, with sections for each herb, their medicinal qualities, edible qualities, how to indentify and a pressing of the herb itself.

So far I have found:

  * Evening Primrose
  * Yarrow (pink and white)
  * Oxeye Daisy
  * St John&#8217;s Wort
  * Musk Mallow
  * Threadleaf Coreopsis
  * Pineapple Weed
  * Dock
  * Knapweed
  * Black-eyed Susan

Many years ago my daughter made me this flower press  in Kindergarten. I have never used it, but had it tucked away because I knew one day I would.

<div id="attachment_2495" style="width: 422px" class="wp-caption alignnone">
  <a rel="attachment wp-att-2495" href="http://quiltingthefarm.vius.ca/2011/07/gathering-herbs/flower-press/"><img class="size-full wp-image-2495" title="flower-press" src="http://quiltingthefarm.plaidsheep.ca/files/2011/07/flower-press.jpg" alt="" width="412" height="309" /></a>
  
  <p class="wp-caption-text">
    My gorgeous little flower press!
  </p>
</div>

<div class="mceTemp">
  I will be outside again today to try and identify another herb or two. Any guesses and what I might find next?
</div>