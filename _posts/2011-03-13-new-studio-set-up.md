---
id: 946
title: New Studio Set Up
date: 2011-03-13T08:52:16+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=946
permalink: /2011/03/13/new-studio-set-up/
image: /files/2011/03/studio.jpg
categories:
  - Crafts
tags:
  - crafts
  - decorating
  - designing
  - fabric
  - painting
  - sewing
---
We have a sunroom (I use this term loosely as it faces north) where I have decided to set up my arts and crafts studio.

[<img class="alignnone size-full wp-image-228" title="sunroom" src="http://quiltingthefarm.plaidsheep.ca/files/2011/01/sunroom.jpg" alt="" width="412" height="309" />](http://quiltingthefarm.plaidsheep.ca/files/2011/01/sunroom.jpg)

The view from the sunroom will be beautiful in the spring, summer and fall. From the window you can see the pond and the apple trees along the driveway.

_**It will be a perfect place to get inspiration.**_ 

I acquired a desk from a friend. It was supposed to be for hubby’s computer, but as he hadn’t taken ownership of it within 3 days, it defaulted to me. 🙂  

I have set the desk up with all my painting, drawing and various other supplies. It will also make a great sewing table so I’ll eventually be able to move my sewing machine off the dining room table!

<a rel="attachment wp-att-1140" href="http://quiltingthefarm.vius.ca/2011/03/new-studio-set-up/studio/"><img class="alignnone size-medium wp-image-1140" title="studio" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/studio-300x225.jpg" alt="studio/art room" width="300" height="225" /></a>

As you can see I&#8217;m not done setting up yet. There&#8217;s still a couple of boxes to unpack, but I&#8217;ll need some storage before I get to them.

<p style="text-align: center">
  ******
</p>