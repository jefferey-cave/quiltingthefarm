---
id: 2089
title: Raised Beds and Old Gardens
date: 2011-05-30T06:40:36+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2089
permalink: /2011/05/30/raised-beds-and-old-gardens/
categories:
  - Garden
---
Yesterday was a productive day.

The raised beds for this years garden had been half built back in March, but with snow still on the ground and no idea where to put them, they have sat in the garage waiting for us to get to them. Unfortunately with a chicken coop that needed to be built and the crazy rain that didn&#8217;t seem to stop, gardening had fallen by the wayside. Yesterday changed all that.

Hubby got the lawn tractor out and cut out an area of brush next the the orchard. _I would have really liked the garden in the front but that piece of land needs to be graded first_. The soil on own farm is sparse and the granite plentiful. Last time hubby dug holes it took him hours of digging rocks to make any headway. Below is the first hole we dug for the garden beds, there was barely a rock. It looks like we may have found the old garden, or at least some planting area from the past.

<a rel="attachment wp-att-2091" href="http://quiltingthefarm.vius.ca/2011/05/raised-beds-and-old-gardens/digging-the-post-holes/"><img class="alignnone size-full wp-image-2091" title="digging-the-post-holes" src="http://quiltingthefarm.plaidsheep.ca/files/2011/05/digging-the-post-holes.jpg" alt="digging post holes" width="412" height="309" /></a>

After digging three post holes these are all the rocks we found. The other post holes were as easy to dig. We ran into one huge granite boulder but it was quite deep and we were able to ignore it. It was a great and wonderous moment to realize we actually had soil under all that brush.

<a rel="attachment wp-att-2092" href="http://quiltingthefarm.vius.ca/2011/05/raised-beds-and-old-gardens/rocks-from-post-holes/"><img class="alignnone size-full wp-image-2092" title="rocks-from-post-holes" src="http://quiltingthefarm.plaidsheep.ca/files/2011/05/rocks-from-post-holes.jpg" alt="rocks from post holes" width="412" height="309" /></a>

Hubby decided to add the end pieces of the beds after seting the sides in the ground.  The original idea was  to leave them open so we could wheel in the wheelbarrow to dump the compost and soil more easily. It turned out to be difficult to keep the bed square, so we scrapped that idea for the second box.

We left a one foot post above the boards, these will allow us to add another board (for a deeper bed) at some point in the future.

<a rel="attachment wp-att-2090" href="http://quiltingthefarm.vius.ca/2011/05/raised-beds-and-old-gardens/start-of-raised-beds/"><img class="alignnone size-full wp-image-2090" title="start-of-raised-beds" src="http://quiltingthefarm.plaidsheep.ca/files/2011/05/start-of-raised-beds.jpg" alt="raised beds in progress" width="412" height="309" /></a>

While hubby dug holes, I raked up all the grass clippings, they will make good compostable material. I dumped them in over a layer of cardboard in the finished beds.

Our farm is beginning to come back to life. We have a long way to go, but some of our newly &#8220;found&#8221; areas bring a smile to my lips. It&#8217;s amazing what you find under years of  overgrowth.

<p style="text-align: center">
  ******
</p>