---
id: 2126
title: Nature Against Man
date: 2011-06-02T06:37:18+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2126
permalink: /2011/06/02/nature-against-man/
categories:
  - Rural Living
---
The winds here howl in the spring. My juvenile plants outside were trying to become acclimatized although nature was throwing them a curve ball. The  tomatoes, like withered old women, huddled against the elements; scarves tucked around their weathered faces, wishing only for the end. Cucumbers diving for any cover they could find and basil learning new yoga moves with hopes their little backs wouldn&#8217;t snap under the pressure. The sweet peppers looked so forlorn, and even gave a sigh of relief as I heaved them back  into the safety of the mudroom.  Ahhhh, Spring.

There is something afoot in our small barn. Strange noises eminated from the stall as I was <!--more-->shovelling out manure. I peered around the corner and there I was face to face with a raccoon. Now we have to find a strategy on relocating a raccoon and her young somewhere safer (for us, not her).

<div id="attachment_2127" style="width: 310px" class="wp-caption alignnone">
  <a rel="attachment wp-att-2127" href="http://quiltingthefarm.vius.ca/2011/06/nature-against-man/raccoon-2/"><img class="size-medium wp-image-2127" title="raccoon" src="http://quiltingthefarm.plaidsheep.ca/files/2011/06/raccoon1-300x208.jpg" alt="raccoon" width="300" height="208" /></a>
  
  <p class="wp-caption-text">
    Racoon hiding out in the barn
  </p>
</div>

The deer are back, I saw two this morning peering over some bushes. Like I told hubby, they could have been kangaroos, but I expect white tailed deer is a more likely choice. I have never actually seen a kangaroo in person, and I was surprised how like my expectation of a kangaroo the head of a white tailed deer looked. Added to the deer and raccoon, we have  two young cotton tails as frequent visitors and quite likely squatters on the land, a snapping turtle, several snakes, and a goundhog. I have to wonder if gardening is worth my while without a brick compound. If nothing else, the wildlife will have choice salad pickings this summer.

The smell of freshly washed laundry is always enticing, but none so much as the freshness from laundry dried outside on a clothes line. Tuesday was the first time I used my clothes line and the first time in about 12 years I have hung laundry outside to dry. Last night, while putting laundry away, I couldn&#8217;t help but bury my nose in it and reminisce about my childhood in England. It&#8217;s funny how smells bring back so many memories.

Hubby was out on his tractor again. It&#8217;s a fulltime job trying to keep the grass and forest at bay. The grass is tall and wet making cutting difficult and time consuming. The wild roses sprout up at a moments notice, their thorns catching and scratching anything that passes by.  The forest has encroached on our property over the last few years &#8211; the previous owner prefering to leave it au natural. Unfortunately that doesn&#8217;t bode well for farming. Even a tiny farm/homestead like ours needs more plan and less chaos.  Over time we will try and push back the forest just a little, although I admit keeping it from swallowing us whole is probably all we can do._ **It&#8217;s man against nature, and here in Nova Scotia nature won.**_