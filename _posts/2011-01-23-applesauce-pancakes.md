---
id: 213
title: Yummy Applesauce Pancakes
date: 2011-01-23T20:57:13+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=213
permalink: /2011/01/23/applesauce-pancakes/
categories:
  - Kitchen
  - Recipes
tags:
  - apples
  - breakfasts
  - fruit
  - recipes
---
Homemade applesauce. It’s chunky, it’s pink, it’s full of peel, it’s delicious and it’s wonderful in these fluffy pancakes.

**Pancakes**

  * 2 eggs
  * 2 c. all-purpose flour
  * 1 c. milk +
  * 1 c. applesauce
  * 1/4 c. vegetable oil
  * 1/2 tsp. cinnamon
  * 4 tsp. baking powder
  * 1 tsp. salt

  1. Beat eggs until fluffy; beat in remaining ingredients just until smooth.
  2. Spray a frying pan with a little oil and  heat over medium-high heat. 
  3. Pour in about 1/4 cup (slightly less) batter for each pancake.  Cook until bubbles appear on top. Flip the pancakes over and cook until golden,

Serve with applesauce syrup (see below)
  
****

**Applesauce Syrup**

  * ½ cup applesauce
  * ½ cup maple syrup

Blend together applesauce and maple syrup, heat in a small pan.