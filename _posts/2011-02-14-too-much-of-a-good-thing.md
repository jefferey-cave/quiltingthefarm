---
id: 227
title: Too Much of a Good Thing
date: 2011-02-14T14:56:19+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=227
permalink: /2011/02/14/too-much-of-a-good-thing/
categories:
  - Rural Living
tags:
  - decorating
  - DIY
  - house
  - painting
---
I know it must have been someone’s taste, but, good grief, know when to say “NO” I’m talking about the wood paneling in the house. There’s paneling on the walls, there’s paneling on the ceilings, there’s paneling in the mudroom, there’s paneling in the sunroom, there are even some wood paneled doors (and not the nice type).<!--more-->

 [<img class="size-medium wp-image-229 alignnone" title="bedroom" src="http://quiltingthefarm.plaidsheep.ca/files/2011/01/bedroom-300x225.jpg" alt="" width="300" height="225" />](http://quiltingthefarm.plaidsheep.ca/files/2011/01/bedroom.jpg)
  
[<img class="size-medium wp-image-228 alignnone" title="sunroom" src="http://quiltingthefarm.plaidsheep.ca/files/2011/01/sunroom-300x225.jpg" alt="" width="300" height="225" />](http://quiltingthefarm.plaidsheep.ca/files/2011/01/sunroom.jpg)

Now, I like wood paneling as much as the next guy (this is a downright lie, I detest it) but it just has to go…and soon. 

I’m not sure how prudent it is to pull it off the wall, who knows what I’ll find. Painting it seems reasonable except that most of it is knotty pine and it will be a devil to fill those knots and keep it from bleeding through. Has anybody had any luck painting this type of paneling? I’d love to see pics if you have some.