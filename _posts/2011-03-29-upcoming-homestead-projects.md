---
id: 1472
title: Upcoming Homestead projects
date: 2011-03-29T07:00:20+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=1472
permalink: /2011/03/29/upcoming-homestead-projects/
image: /files/2011/03/blocks.jpg
categories:
  - Rural Living
---
<span id="New_projects_coming_up">

<h3>
  <span style="color: #800080">New projects coming up!</span>
</h3></span> 

Can you guess what projects I have in mind&#8230;

<a rel="attachment wp-att-1473" href="http://quiltingthefarm.vius.ca/2011/03/upcoming-homestead-projects/large-spool/"><img class="alignnone size-full wp-image-1473" title="large-spool" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/large-spool.jpg" alt="large wooden spool" width="412" height="309" /></a>

For a wooden spool?

<a rel="attachment wp-att-1477" href="http://quiltingthefarm.vius.ca/2011/03/upcoming-homestead-projects/honey-tubs/"><img class="alignnone size-full wp-image-1477" title="honey-tubs" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/honey-tubs.jpg" alt="tubs of honey" width="412" height="309" /></a>

For two 30lb tubs of honey?

<a rel="attachment wp-att-1476" href="http://quiltingthefarm.vius.ca/2011/03/upcoming-homestead-projects/blocks/"><img class="alignnone size-full wp-image-1476" title="blocks" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/blocks.jpg" alt="wooden blocks" width="412" height="309" /></a>

For small pieces of wood off-cuts?