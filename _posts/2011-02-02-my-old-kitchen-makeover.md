---
id: 266
title: My Old Kitchen Makeover
date: 2011-02-02T03:22:02+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=266
permalink: /2011/02/02/my-old-kitchen-makeover/
image: /files/2011/01/kitchen-reno-004.jpg
categories:
  - Kitchen
tags:
  - crafts
  - decorating
  - designing
  - DIY
  - frugal
---
Here are some pictures from my old house. As you can see, the kitchen badly needed a makeover. Being on a tight budget meant that I had to do some major planning and budget shopping. My goal was to update this dreary kitchen for about $500.

[<img class="alignnone size-medium wp-image-390" title="old-kitchen" src="http://quiltingthefarm.plaidsheep.ca/files/2011/01/old-kitchen-300x219.jpg" alt="" width="300" height="219" />](http://quiltingthefarm.plaidsheep.ca/files/2011/01/old-kitchen.jpg)

I think I succeeded (with the help of hubby, of course) in making it a nicer place to cook, eat or just hang out.

[<img class="alignnone size-medium wp-image-394" title="kitchen-reno-004" src="http://quiltingthefarm.plaidsheep.ca/files/2011/01/kitchen-reno-004-300x225.jpg" alt="" width="300" height="225" />](http://quiltingthefarm.plaidsheep.ca/files/2011/01/kitchen-reno-004.jpg)

[<img class="alignnone size-thumbnail wp-image-393" title="kitchen-reno-002" src="http://quiltingthefarm.plaidsheep.ca/files/2011/01/kitchen-reno-002-150x150.jpg" alt="" width="115" height="105" />](http://quiltingthefarm.plaidsheep.ca/files/2011/01/kitchen-reno-002.jpg) [<img class="alignnone size-thumbnail wp-image-392" title="kitchen-reno-001" src="http://quiltingthefarm.plaidsheep.ca/files/2011/01/kitchen-reno-001-150x150.jpg" alt="" width="117" height="105" />](http://quiltingthefarm.plaidsheep.ca/files/2011/01/kitchen-reno-001.jpg) [<img class="alignnone size-thumbnail wp-image-391" title="kitchen-spicedrawer" src="http://quiltingthefarm.plaidsheep.ca/files/2011/01/kitchen-spicedrawer-150x150.jpg" alt="" width="105" height="105" />](http://quiltingthefarm.plaidsheep.ca/files/2011/01/kitchen-spicedrawer.jpg) [<img class="alignnone size-thumbnail wp-image-395" title="kitchen-reno-010" src="http://quiltingthefarm.plaidsheep.ca/files/2011/01/kitchen-reno-010-150x150.jpg" alt="" width="126" height="105" />](http://quiltingthefarm.plaidsheep.ca/files/2011/01/kitchen-reno-010.jpg) [<img class="alignnone size-thumbnail wp-image-396" title="kitchen-reno-011" src="http://quiltingthefarm.plaidsheep.ca/files/2011/01/kitchen-reno-011-150x150.jpg" alt="" width="114" height="106" />](http://quiltingthefarm.plaidsheep.ca/files/2011/01/kitchen-reno-011.jpg)[](http://quiltingthefarm.plaidsheep.ca/files/2011/01/kitchen-spicedrawer.jpg) 

(We put in new flooring, counter tops, hardware, backsplash and lighting, painted cabinets and walls, built island, spice drawer, wine shelf and cabinet feet).