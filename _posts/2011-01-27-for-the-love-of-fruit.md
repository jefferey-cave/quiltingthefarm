---
id: 237
title: For the Love of Fruit
date: 2011-01-27T00:39:25+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=237
permalink: /2011/01/27/for-the-love-of-fruit/
categories:
  - Garden
tags:
  - apples
  - fruit
  - gardening
  - trees
---
<div id="attachment_238" style="width: 310px" class="wp-caption alignnone">
  <a href="http://quiltingthefarm.plaidsheep.ca/files/2011/01/Old-Apple-Trees.jpg"><img class="size-medium wp-image-238 " title="Old-Apple-Trees" src="http://quiltingthefarm.plaidsheep.ca/files/2011/01/Old-Apple-Trees-300x225.jpg" alt="" width="300" height="225" /></a>
  
  <p class="wp-caption-text">
    The old apple trees in winter
  </p>
</div>

I love fruit, all sorts of fruit. We have some old apple trees on our property, a couple of blackberry patches (I’ll have to beat the bears to them this year!) and I think I saw a lone blueberry bush amongst the overgrown bushes (not too sure on this one). I would like to plant more this year. I have been thinking about a couple of new apple trees, a pear, cherry, plum and a few raspberries (I know, I’m a gluten for punishment). I’m not sure what will grow here so there’ll be a lot of trial and error.