---
id: 3044
title: Harvesting the Garden
date: 2011-09-04T07:03:54+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=3044
permalink: /2011/09/04/harvesting-the-garden/
tinymce_signature_post_setting:
  - 'yes'
categories:
  - Garden
---
Harvesting the garden is in full swing. More tomatoes are ripening everyday, and I&#8217;m excited because I have a new salsa recipe to try out.

The beans, however, are few and far between. The few that I have harvested have been tasty.

<a rel="attachment wp-att-3046" href="http://quiltingthefarm.vius.ca/2011/09/harvesting-the-garden/tomatoes-and-beans/"><img class="alignnone size-full wp-image-3046" title="tomatoes-and-beans" src="http://quiltingthefarm.plaidsheep.ca/files/2011/09/tomatoes-and-beans.jpg" alt="" width="412" height="309" /></a>

This is a tromboncino squash. It is an Italian summer squash, and easy to grow!  The squash can grow up to about 3 feet long and can be harvested anytime, from just a few inches through its full size (not sure what that is yet). It can be eaten sauted in butter and garlic (my favourite), grilled, or sliced raw in a salad. Some people say it has a slight artichoke flavour, although I can&#8217;t say I can taste it. I do like it&#8217;s buttery texture though.

<a rel="attachment wp-att-3045" href="http://quiltingthefarm.vius.ca/2011/09/harvesting-the-garden/tromboncino-squash/"><img class="alignnone size-full wp-image-3045" title="tromboncino-squash" src="http://quiltingthefarm.plaidsheep.ca/files/2011/09/tromboncino-squash.jpg" alt="" width="412" height="309" /></a>

<a rel="attachment wp-att-3049" href="http://quiltingthefarm.vius.ca/2011/09/harvesting-the-garden/sliced-squash/"><img class="alignnone size-full wp-image-3049" title="sliced-squash" src="http://quiltingthefarm.plaidsheep.ca/files/2011/09/sliced-squash.jpg" alt="" width="412" height="309" /></a>

The potato harvest brought in far fewer than expected. Next year I will have to plant way more seed potatoes. The seed potatoes I was given did much better than the ones I purchased, so I would take a guess that variety just did better with the soil and the weather this year. This is about half of the total harvest, I&#8217;ll definitely being buying some this winter!

<a rel="attachment wp-att-3047" href="http://quiltingthefarm.vius.ca/2011/09/harvesting-the-garden/potato-harvest/"><img class="alignnone size-full wp-image-3047" title="potato-harvest" src="http://quiltingthefarm.plaidsheep.ca/files/2011/09/potato-harvest.jpg" alt="" width="412" height="309" /></a>

Apart from that, I&#8217;ve picked up a large bucket of windfalls. These are from a different tree, with a different kind of apple. It will be interesting to see if I can see or taste any difference. I also have a couple of russet apple trees. The apples are much smaller (I believe they are late harvest) and the windfalls easily hide in the long grass. It looks like I&#8217;ll be making more apple butter, apple jelly, apple sauce, apple&#8230;

<a rel="attachment wp-att-3048" href="http://quiltingthefarm.vius.ca/2011/09/harvesting-the-garden/more-apples/"><img class="alignnone size-full wp-image-3048" title="more-apples" src="http://quiltingthefarm.plaidsheep.ca/files/2011/09/more-apples.jpg" alt="" width="412" height="309" /></a>