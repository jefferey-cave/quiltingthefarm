---
id: 2530
title: Porch Rocker
date: 2011-07-13T06:53:19+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2530
permalink: /2011/07/13/porch-rocker/
tinymce_signature_post_setting:
  - 'yes'
categories:
  - Rural Living
---
Remember our wonderful realtor I was telling you about? Well, while I was picking up all those free fruit bushes, I purchased this wicker rocking chairfrom her for $30.

I gave the rocker a new coat of paint, made a seat cushion and used a piece of embroidery I found in an antique store to make a throw pillow. The rocker will sit in the sunroom which we are returning to its former glory as a covered porch.

<div id="attachment_2531" style="width: 319px" class="wp-caption alignnone">
  <a rel="attachment wp-att-2531" href="http://quiltingthefarm.vius.ca/2011/07/porch-rocker/rocker/"><img class="size-full wp-image-2531" title="rocker" src="http://quiltingthefarm.plaidsheep.ca/files/2011/07/rocker.jpg" alt="" width="309" height="412" /></a>
  
  <p class="wp-caption-text">
    New porch rocker
  </p>
</div>

<div class="mceTemp">
  Dreaming of Summer evenings on the porch, with a nice glass of Raspberry wine 🙂
</div>