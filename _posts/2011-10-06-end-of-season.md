---
id: 3333
title: End of Season
date: 2011-10-06T07:02:38+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=3333
permalink: /2011/10/06/end-of-season/
tinymce_signature_post_setting:
  - 'yes'
categories:
  - Rural Living
---
Now it&#8217;s October, my part time, seasonal employment is almost over. The Inn has just one more wedding next weekend, and then it closes until May. It will feel strange not having to go to work in the evening, but more than that, it will be fiscally tough.

There are no other jobs on the horizon for me, and Hubby has applied for over 200 positions up and down the valley with nary a response. Don&#8217;t get me wrong, Hubby was offered a job in Truro with Hewlett Packard, but that&#8217;s a 3 1/2 hour drive form us&#8230;definitely not commuting material. There have also been job offers in Halifax, but that also too far at 2 1/2 hours away from home. These jobs are high powered careers with major companies, but that&#8217;s **_exactly_** what we moved away from! All we would like is a part time, minimum wage job close to home. How hard could that be??? Let me tell you&#8230;IT AIN&#8217;T EASY!

So with my work winding down, and more crappy, rainy weather in the forecast, I&#8217;m in the mood for making my house a home. Painting is cheap, easy (Hubby doesn&#8217;t agree), and (occassionally) fun. I have started the living room, the stairwell, and we have almost finished painting the upstairs bathroom. For the bathroom I chose grey for the bottom half of the walls and white for the tongue and groove above it. Painting tongue and groove is not a job I like. The going is so slow, and it&#8217;s hard to get in all the nooks and crannies. The pine boards also soak up a lot of paint, and after a coat of primer and 2 coats of paint, it&#8217;s still looking like another coat wouldn&#8217;t go amiss. Oh, did I mention the ceiling&#8217;s vaulted? If anyone out there loves to paint, I have the rest of an upper floor, complete with floor to (vaulted) ceiling tongue and groove, just waiting for a nice coat of paint! _I&#8217;ll <span style="color: #ff0000">love</span> you forever_&#8230;truly I will 🙂 _and_ I&#8217;ll supply homemade pizza and home brewed blackberry wine. How could anybody resist?