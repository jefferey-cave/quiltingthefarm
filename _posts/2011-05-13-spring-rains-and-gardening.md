---
id: 1900
title: Spring Rains and Gardening
date: 2011-05-13T06:39:56+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=1900
permalink: /2011/05/13/spring-rains-and-gardening/
categories:
  - Garden
---
It&#8217;s been raining the last few days and the homestead is greening up before our very eyes. All we need now is a whole lot of sun!

<a rel="attachment wp-att-1904" href="http://quiltingthefarm.vius.ca/2011/05/spring-rains-and-gardening/greening-up/"><img class="alignnone size-full wp-image-1904" title="greening-up" src="http://quiltingthefarm.plaidsheep.ca/files/2011/05/greening-up.jpg" alt="green grass and trees" width="412" height="309" /></a>

My parents helped me (or did I help them?) re-pot most of my seedlings. The tomatoes look very healthy and strong. I chose Cabot tomatoes, a bush tomato that is easy to grow.

The cucumbers, although planted too early, seem to be holding their own and I should be able to put them outside in the next week or two.

I<a rel="attachment wp-att-1901" href="http://quiltingthefarm.vius.ca/2011/05/spring-rains-and-gardening/mudroom-greenhouse2/"><img class="alignnone size-full wp-image-1901" title="mudroom-greenhouse2" src="http://quiltingthefarm.plaidsheep.ca/files/2011/05/mudroom-greenhouse2.jpg" alt="mudroom greenhouse" width="412" height="309" /></a>

I have purchased some seed potatoes (Kennebec) and they are being left to &#8220;chit&#8221; for a few days. Once the eye sprouts are about an inch long I will plant them in buckets.

<a rel="attachment wp-att-1906" href="http://quiltingthefarm.vius.ca/2011/05/spring-rains-and-gardening/tomatoes-and-potatoes/"><img class="alignnone size-full wp-image-1906" title="tomatoes-and-potatoes" src="http://quiltingthefarm.plaidsheep.ca/files/2011/05/tomatoes-and-potatoes.jpg" alt="tomatoes and potatoes" width="412" height="309" /></a>

My mudroom has become a greenhouse now. Next year I&#8217;ll put up a bunch of shelves and benches to house all my seedlings.

<a rel="attachment wp-att-1902" href="http://quiltingthefarm.vius.ca/2011/05/spring-rains-and-gardening/mudroom-greenhouse/"><img class="alignnone size-full wp-image-1902" title="mudroom-greenhouse" src="http://quiltingthefarm.plaidsheep.ca/files/2011/05/mudroom-greenhouse.jpg" alt="mudroom greenhouse with plants" width="412" height="309" /></a>

I planted my basil and thyme in large pots that I can keep on my deck. I purchased some sage and lemon balm as well.

<a rel="attachment wp-att-1905" href="http://quiltingthefarm.vius.ca/2011/05/spring-rains-and-gardening/herb-pots/"><img class="alignnone size-full wp-image-1905" title="herb-pots" src="http://quiltingthefarm.plaidsheep.ca/files/2011/05/herb-pots.jpg" alt="herb pots" width="412" height="309" /></a>

I re-potted the chives, hot peppers and lavender. They are small but healthy looking.

<a rel="attachment wp-att-1903" href="http://quiltingthefarm.vius.ca/2011/05/spring-rains-and-gardening/peppers-and-lavender/"><img class="alignnone size-full wp-image-1903" title="peppers-and-lavender" src="http://quiltingthefarm.plaidsheep.ca/files/2011/05/peppers-and-lavender.jpg" alt="peppers and lavender" width="309" height="412" /></a>

We dug up the outside planters and added compost from the barn. I am going to plant a few of my tomatoes in this planter, along with some herbs for a great kitchen garden. It&#8217;s a good are with it&#8217;s own little micro climate.