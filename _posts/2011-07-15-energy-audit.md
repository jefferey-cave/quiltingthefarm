---
id: 2541
title: Energy Audit
date: 2011-07-15T07:00:36+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2541
permalink: /2011/07/15/energy-audit/
tinymce_signature_post_setting:
  - 'yes'
categories:
  - Rural Living
---
Our house was very cold and drafty last winter and if you have been reading this blog you will remember I spent most of it in two pairs socks, two pairs pants, two or three top layers and a hat! We are trying to remedy the chilliness by adding a new wood stove in our living room, but that only go so far, what we need to do is seal the house better, but how and where? We had heard that an energy audit can help find the areas of concern, so yesterday we had an appointment for an Energy Audit to be performed on our house.

<!--more-->

The audit was done yesterday morning, it took several hours, but unfortunately only picked up the small problem areas we already aware of. If the small things were the only problem it would be an easy fix, but that&#8217;s not the case. I found the audit to be more about persuading us to switch to Energy Star appliances (which we already have) and low-flow toilets (which offer us no benefit as we are on well and septic), than to address our heat loss concerns. The focus of the audit was almost entirely on the government rebates rather than addressing the long term energy efficiency of my home. Unfortunately a government agency is about ticking check boxes and about short term measurable changes. Having a longer vision, we aren&#8217;t interested in a quick government payout but rather, small financial savings over the decades we intend live here.

If you are planning to get an energy audit my suggestion is to fore-go it and save your cash. Any astute homeowner will already know the obvious problems in their home and doesn&#8217;t need to spend $150 to have them pointed out.

On a happier note we spent the rest of the afternoon and evening at the beach, with a stop for a hot dog and icecream on the way home.