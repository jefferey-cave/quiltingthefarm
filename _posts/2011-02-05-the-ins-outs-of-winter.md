---
id: 526
title: 'The &#8220;Ins&#8221; &amp; &#8220;Outs&#8221; of Winter'
date: 2011-02-05T10:55:45+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=526
permalink: /2011/02/05/the-ins-outs-of-winter/
image: /files/2011/02/doctor-zhivago.jpg
categories:
  - Rural Living
tags:
  - snowstorms
  - tractor
  - weather
  - winter
---
The sun is shining, I dream of spring.

Reality is somewhat different though&#8230;

> the snow banks are slowly climbing their way up the walls and are encrouching on the windows.
> 
> The tractor that plows our driveway is broken.
> 
> The other tractor that plows our driveway is broken
> 
> My husband, sitting beside me, is wearing a woolly hat

 

This winter, my clothing has become a veritable Noah&#8217;s Ark. Everything I wear comes in twos&#8230;2 sweaters, 2 pairs socks, 2 pairs pants. I&#8217;m quickly running out of things to wear, so I live my life in flannel Pjs (my excuse and I&#8217;m sticking with it). I don&#8217;t think I&#8217;ve ever <!--more-->been this cold indoors, and I do mean EVER!

This spring we are going to get an energy audit.  I know it&#8217;s cold in here, I just can&#8217;t tell where it&#8217;s all coming from. We were told that the thick layer of snow halfway up the walls would be insulating, ha, ha, ha, too funny&#8230;and a complete and utter lie!

The ferret has commandeered my electric blanket. As soon he sees it, he pounces on it and I end up in the cold&#8230;and they say ferrets don&#8217;t have good eyesight.

The rabbit seems to be unperturbed by the cold. He keeps himself busy aranging and rearranging his cage and anything else in the house that&#8217;s not to his liking. He likes to rearrange Hubby&#8217;s pants&#8230;while Hubby is still wearing them (this causes &#8220;bad&#8221; words to come out of hubby&#8217;s mouth, rabbit teeth are sharp!).

<div class="mceTemp mceIEcenter">
  <dl id="attachment_533" class="wp-caption aligncenter" style="width: 442px">
    <dt class="wp-caption-dt">
      <a href="http://www.webomatica.com/wordpress/2010/10/03/movie-notes-doctor-zhivago/"><img class="size-full wp-image-533  " title="doctor-zhivago" src="http://quiltingthefarm.plaidsheep.ca/files/2011/02/doctor-zhivago.jpg" alt="" width="432" height="153" /></a>
    </dt>
    
    <dd class="wp-caption-dd">
      Okay, this is not my house and, maybe, just maybe, a little bit of an exaggeration on my part.
    </dd>
  </dl>
  
  <p>
    We have been told (now I&#8217;m beginning to wonder if this wasn&#8217;t the same person who told us snow would be insulating) that the snow will start to melt sometime in March, so until then I&#8217;m selling it at the discounted rate of $20 a load (U Pick) with the option of a free load next winter!
  </p>
</div>