---
id: 2452
title: Free Fruit Bushes and More!
date: 2011-07-05T06:59:56+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2452
permalink: /2011/07/05/free-fruit-bushes-and-more/
tinymce_signature_post_setting:
  - ""
categories:
  - Garden
---
Yesterday we were invited to supper with the realtor (Jen) who sold us our farm. We hadn&#8217;t seen Jen since she dropped off a house warming gift for us late last year. Jen&#8217;s husband is in the Canadian military, and we knew they were being posted to Vancouver Island this spring. Well, they are leaving next week for the long trek across the country. Before they left, Jen offered us a whole bunch of plants and bushes, so we dug some up. We have:

**3 horseradish plants,** 

**bunch of chives,** 

**one celeriac plant,** 

**2 red current bushes,** 

**2 thornless blackberry bushes** 

**2 rhubarb plants**

**and a couple of grapevine cuttings.**

Today we planted the blackberries and red currents over by the garden and orchard. The soil there is wonderful. Hubby dug down about a foot or so, and he didn&#8217;t hit clay or rocks, just wonderful loamy top soil.

<div id="attachment_2460" style="width: 422px" class="wp-caption alignnone">
  <a rel="attachment wp-att-2460" href="http://quiltingthefarm.vius.ca/2011/07/free-fruit-bushes-and-more/red-current/"><img class="size-full wp-image-2460" title="red-current" src="http://quiltingthefarm.plaidsheep.ca/files/2011/07/red-current.jpg" alt="" width="412" height="309" /></a>
  
  <p class="wp-caption-text">
    Red current
  </p>
</div>

<div id="attachment_2459" style="width: 422px" class="wp-caption alignnone">
  <a rel="attachment wp-att-2459" href="http://quiltingthefarm.vius.ca/2011/07/free-fruit-bushes-and-more/thornless-blackberry/"><img class="size-full wp-image-2459" title="thornless-blackberry" src="http://quiltingthefarm.plaidsheep.ca/files/2011/07/thornless-blackberry.jpg" alt="" width="412" height="309" /></a>
  
  <p class="wp-caption-text">
    Thornless blackberry
  </p>
</div>

We planted one rhubarb plant in front of the house and one out back behind the kitchen. We chose a spot on a slope that has been difficult to mow. Hopefully the rhubarb will spread and we won&#8217;t have to worry about that spot as much.

<div id="attachment_2458" style="width: 422px" class="wp-caption alignnone">
  <a rel="attachment wp-att-2458" href="http://quiltingthefarm.vius.ca/2011/07/free-fruit-bushes-and-more/rhubarb/"><img class="size-full wp-image-2458" title="rhubarb" src="http://quiltingthefarm.plaidsheep.ca/files/2011/07/rhubarb.jpg" alt="" width="412" height="309" /></a>
  
  <p class="wp-caption-text">
    Rhubarb
  </p>
</div>

I have put the grapevine cuttings in water for a few days until the roots start to sprout, then I&#8217;ll plant them in some soil.

The chives have found a home where my original chives were planted. I hope these ones survive the critter onslaught that robbed the others.

I still need homes for the celeriac and horseradish, but I have no idea where to put them.

Who knows if anything will take. _God knows I don&#8217;t have a green thumb yet, and although I&#8217;m working on it, it&#8217;s just dirty brown and muddy for now_.  A lot of this is reading, guesswork and trial and error, but I&#8217;m pleased with how things are going. If I can harvest just a little something this year, I&#8217;ll be ecstatic.

<p style="text-align: center">
  *******
</p>