---
id: 2033
title: Just Today.
date: 2011-05-23T06:39:44+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2033
permalink: /2011/05/23/just-today/
categories:
  - Rural Living
---
The day started off well. I got up around 7am and fed the birds &#8211; although I think the resident chipmunk has been stashing the seeds for his rainy day.  I picked a handful of dandelions, clover and grass for Stewie our pet rabbit.

Hubby left for his day at Hidden Meadow Farm and I hopped in the shower. I had written a list of things I wanted to get done before Hubby came home. I&#8217;m usually quite productive when he&#8217;s not here. Anyway, today I was going to&#8230;

  * Put up some roosts in coop.
  * Attach hardware cloth to inside of window
  * Bring topsoil down from the back forty
  * plant potatoes
  * clean out buckets
  * weed strawberry beds
  * weed planters next to house
  * Make rhubarb crisp

First on the agenda was to bring some topsoil down for the back forty. There used to be an old sawmill on our property many years ago and where this mill used to stand there is some wonderful soil from many years of sawdust and rotting wood. I got out the wheelbarrow and shovel and took off up the trail. I was only about halfway up when I heard and saw something black rushing towards me through the trees. We know we have bears and coyotes that visit us here, so I was terrified. Turns out it was two rather large and unfriendly looking dogs which unfortunately didn&#8217;t make me feel any less terrified. Packs of dogs are dangerous animals in the country, and with our plans for chickens any day now, very unwelcomed visitors.

I didn&#8217;t end up with any soil, just an empty wheelbarrow and my heavily beating heart cautiously making my way back to the house. A few minutes later the dogs came by the house and barn. I don&#8217;t know who owns them. None of my neighbours have dogs, so they had to have travelled some distance. I hope they weren&#8217;t dumped here by some idiot. I managed to get some photos in hope that someone will be able to identify them if need be, but I was a little nervous about being outside anymore.

My potatoes didn&#8217;t get planted.

Next on my list was weeding the strawberries. I did get one bed done but I felt a little vunerable. I&#8217;ll do the other one tomorrow.

Putting up the chicken roosts was an inside job. I like using the mitre saw, in fact I  have a thing for power tools in general. I measured and cut the boards, only to find out that I measured wrong, so had to cut again. Then I screwed the support to the wall in the wrong place four different times! I knew I had to quit; the dog encounter had really thrown me for a loop. There&#8217;s no point keep making the same mistakes. 

I scapped my to-do list and came in to write this post. I found a tick crawling up my neck. I stabbed it with my stitch ripper. I think I&#8217;m going to scream.

Tonight I am going to sit down, pick up my crochet (and the chocolate that Hubby brought home) and listen to the Spring Peepers chirping away in the pond. That&#8217;s what living here is all about.