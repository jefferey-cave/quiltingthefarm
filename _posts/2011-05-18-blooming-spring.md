---
id: 1964
title: Blooming Spring
date: 2011-05-18T07:03:39+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=1964
permalink: /2011/05/18/blooming-spring/
tinymce_signature_post_setting:
  - ""
image: /files/2011/05/blooming1.jpg
categories:
  - Garden
---
It may have been raining for what seems like forever but the trees and bushes are coming into bloom now.

I&#8217;m not sure what most of the trees are as yet. I&#8217;m hoping some are fruit bearing.

This one is more of a bush with many woody stems.

<a rel="attachment wp-att-1971" href="http://quiltingthefarm.vius.ca/2011/05/blooming-spring/blooming1/"><img class="alignnone size-full wp-image-1971" title="blooming1" src="http://quiltingthefarm.plaidsheep.ca/files/2011/05/blooming1.jpg" alt="blooming trees in spring" width="412" height="309" /></a>

Beautiful white blossoms on a taller tree.

<a rel="attachment wp-att-1972" href="http://quiltingthefarm.vius.ca/2011/05/blooming-spring/blooming2/"><img class="alignnone size-full wp-image-1972" title="blooming2" src="http://quiltingthefarm.plaidsheep.ca/files/2011/05/blooming2.jpg" alt="trees in blossom" width="412" height="309" /></a>

This one is an apple tree. The flowers haven&#8217;t opened yet but it wont be long now.

<a rel="attachment wp-att-1973" href="http://quiltingthefarm.vius.ca/2011/05/blooming-spring/blooming4/"><img class="alignnone size-full wp-image-1973" title="blooming4" src="http://quiltingthefarm.plaidsheep.ca/files/2011/05/blooming4.jpg" alt="apple tree ready to bloom" width="412" height="309" /></a>

Is this a peach? I have researched a little and it certainly looks like one. I don&#8217;t know if it&#8217;s warm enough where we live for it to bloom though.

<a rel="attachment wp-att-1965" href="http://quiltingthefarm.vius.ca/2011/05/blooming-spring/peach-tree-in-leaf3/"><img class="alignnone size-full wp-image-1965" title="peach-tree-in-leaf3" src="http://quiltingthefarm.plaidsheep.ca/files/2011/05/peach-tree-in-leaf3.jpg" alt="peach tree" width="412" height="309" /></a>

If you can identify any of these trees I&#8217;d certainly appreciate the help.

<p style="text-align: center">
  ******
</p>