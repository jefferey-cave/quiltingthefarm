---
id: 21
title: 'Mail Delivery &#8211; Maybe?'
date: 2011-01-04T23:10:47+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=21
permalink: /2011/01/04/mail-deliverymay-be/
categories:
  - Rural Living
tags:
  - country life
  - Nova Scotia
  - snow
  - snowstorms
  - weather
  - winter
---
[<img class="size-medium wp-image-104 alignleft" title="mailbox" src="http://quiltingthefarm.plaidsheep.ca/files/2011/01/mailbox-300x282.jpg" alt="" width="240" height="226" />](http://quiltingthefarm.plaidsheep.ca/files/2011/01/mailbox.jpg)We have gone over a week without a single piece of mail, not even a flyer. I know it’s been Christmas and all, but an entire 10 days without junk mail? Something’s up.

So we found out today what’s up, or to be more accurate, what’s down (on the ground). Apparently, in the country, you are supposed to shovel the snow and ice away from your mailbox, not just in front of the mail box, but enough for the mail delivery car to pull in and out. As you may have already guessed, we hadn’t. I suspect these are unwritten rules: you’re just supposed to know that’s how things work.

We have overcome this mild setback and today I hold, in my hand, a little bundle of  mail that’s been waiting for over a week for us to catch up with ourselves. Ahhh, life in the country.