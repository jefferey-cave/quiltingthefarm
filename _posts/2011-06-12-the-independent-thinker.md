---
id: 2236
title: The Independent Thinker
date: 2011-06-12T06:17:29+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2236
permalink: /2011/06/12/the-independent-thinker/
categories:
  - Rural Living
---
There&#8217;s one in every flock, and in ours it&#8217;s Amelia.

Amelia is an independent thinker. She&#8217;s not a follower of the crowd. She&#8217;s not the leader of the crowd. She is just&#8230; Amelia.

Amelia doesn&#8217;t care much for the advances Alexander the Great tries to make. He does his little song and dance, but Amelia just turns tail and trots off with her nose in the air. She&#8217;s having none of that nonsense.

Amelia does her own thing. When everyone is out, she is in. When everyone is in, she is out.  When everyone else is scratching and pecking, Amelia finds the best spot to sunbathe. When everyone else comes to sunbathe, off Amelia goes to scratch and peck.

Amelia is the adventurous one. She goes the furthest from the coop. Today we found her sauntering **_outside_** the chicken yard. We aren&#8217;t sure how she got out and she sure wasn&#8217;t going back without a fight.

<a rel="attachment wp-att-2237" href="http://quiltingthefarm.vius.ca/2011/06/the-independent-thinker/amelia/"><img class="alignnone size-full wp-image-2237" title="amelia" src="http://quiltingthefarm.plaidsheep.ca/files/2011/06/amelia.jpg" alt="Amelia the chicken" width="412" height="309" /></a>

Amelia has fast become the spirit of our small farm.

<p style="text-align: center">
  ******
</p>