---
id: 3206
title: (Crab) Apple Jelly
date: 2011-09-20T07:00:29+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=3206
permalink: /2011/09/20/more-jelly/
tinymce_signature_post_setting:
  - ""
categories:
  - Kitchen
---
Because I&#8217;m a glutton for punishment, I thought I should try my hand at another batch of jelly! I have blackberries in the freezer, blueberries in the freezer and apples ready to harvest, but guess what I choose??? crab apples! Yesterday I made crab apple jelly. Well it was really crab apple and apple jelly, but that&#8217;s quite a mouthful to say.

I have one small crab apple tree in my front yard. It has the tiniest red apples I&#8217;ve ever seen. Hubby thought I was crazy to pick them when we have apples trees everywhere, but I wanted to use the crab apples&#8230;&#8221;eat what you grow&#8221;.

I picked as many as I could, but most of the apples were at the top of the tree, and I had no way of getting them. Content with 1/2 bag, I rounded out my collection with a handful of windfall russets.

I made the juice. Boiled it up with the sugar, and this time I used my trusty candy thermometer. Wow, what a difference it made. No more guessing if it had jelled (&#8217;cause that&#8217;s my downfall). Just boil until temperature reaches 220 degrees&#8230;it couldn&#8217;t have been easier. I will definitely be using a thermometer in future.

<a rel="attachment wp-att-3207" href="http://quiltingthefarm.vius.ca/2011/09/more-jelly/crabapple-jelly/"><img class="alignnone size-full wp-image-3207" title="crabapple-jelly" src="http://quiltingthefarm.plaidsheep.ca/files/2011/09/crabapple-jelly.jpg" alt="" width="412" height="309" /></a>

Hubby thinks this is one of the tastiest jellies he&#8217;s eaten (this and dandelion jelly).