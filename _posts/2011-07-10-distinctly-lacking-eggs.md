---
id: 2502
title: Distinctly Lacking Eggs
date: 2011-07-10T07:15:11+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2502
permalink: /2011/07/10/distinctly-lacking-eggs/
tinymce_signature_post_setting:
  - 'yes'
categories:
  - Rural Living
---
It rained yesterday. A nice steady rain, perfect for watering my veggie gardens. The sun stayed behind the clouds all day long giving us a nice reprieve from the blistering heat that we have been enduring for the last little while.

The hens have decided to stop laying. That&#8217;s correct, not one of the _four_ hens we have has honoured us with a single, solitary egg for 3 days now. Two of them seem to have gone broody, one wasn&#8217;t well but is better now, and the last one? well who knows. It seems quite strange because we had called them our &#8220;two-a-day girls&#8221;. Perhaps it&#8217;s the weather, perhaps they are in molt (wrong time of year but there does seem to be an excess of feathers floating around) or perhaps it&#8217;s something I haven&#8217;t figured out yet. I do know they are well fed, get lots of exercise, have freedom to come and go as they please, and have lots of room in their coop. Any insight into their problem would be appreciated.

<div id="attachment_2503" style="width: 422px" class="wp-caption alignnone">
  <a rel="attachment wp-att-2503" href="http://quiltingthefarm.vius.ca/2011/07/distinctly-lacking-eggs/alexander-and-ladies/"><img class="size-full wp-image-2503" title="Alexander-and-ladies" src="http://quiltingthefarm.plaidsheep.ca/files/2011/07/Alexander-and-ladies.jpg" alt="" width="412" height="309" /></a>
  
  <p class="wp-caption-text">
    Alexander the Great with his ladies
  </p>
</div>

I have also been trying to &#8220;cure&#8221; the broodies&#8230;it&#8217;s not working. I have picked them up and put them outside, but they just come right back in. I have dipped their bottoms and underbellies into a tub of cold water; Annabelle actually enjoys this and Aubree just gets mad, but comes right back. I&#8217;ve moved the nesting boxes, but they just nest on the floor instead. They are certainly not going to be hatching out any chicks because nobody is laying any eggs!. Grr! We were planning on getting two or three more hens and now seems to be a perfect time to do it.

<div id="attachment_2504" style="width: 422px" class="wp-caption alignnone">
  <a href="http://quiltingthefarm.plaidsheep.ca/files/2011/07/broodies.jpg"><img class="size-full wp-image-2504" title="broodies" src="http://quiltingthefarm.plaidsheep.ca/files/2011/07/broodies.jpg" alt="" width="412" height="309" /></a>
  
  <p class="wp-caption-text">
    Annabelle and Aubree, the "broodies"
  </p>
</div>

 I hope the girls start laying again soon, I&#8217;m almost out of eggs 🙁