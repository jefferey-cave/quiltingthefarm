---
id: 2764
title: Pass the Peas Please
date: 2011-08-10T07:00:31+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2764
permalink: /2011/08/10/pass-the-peas-please/
tinymce_signature_post_setting:
  - 'yes'
categories:
  - Garden
---
Guess what I found hiding in my garden?

_**Peas!**_ oh so tasty peas. There&#8217;s nothing quite like snacking on freshly picked peas.

<a rel="attachment wp-att-2765" href="http://quiltingthefarm.vius.ca/2011/08/pass-the-peas-please/pea-pods/"><img class="alignnone size-full wp-image-2765" title="pea-pods" src="http://quiltingthefarm.plaidsheep.ca/files/2011/08/pea-pods.jpg" alt="" width="412" height="309" /></a>

_So far this year, my garden has produced:_

**Swiss Chard**&#8230;way too much for only a couple of plants (our pet rabbit is enjoying them).

**Beets**&#8230;small steamable beets and beet tops.

**Carrots**&#8230;just starting, small but tasty (thinnings). I won&#8217;t harvest these until the end of the season.

**Radishes**&#8230;some, but not as many as expected.

**Cucumbers**&#8230;We are on our third cucumber and there&#8217;s more ripening.

**Kale**&#8230;some, but not enough by far.

**Peas**&#8230;pods just starting to fatten out.

**Beans**&#8230;just starting.

**Lettuce**&#8230;less said about this the better.

**Tomatoes**&#8230;plants heavy with fruit, some red some still green.

**Leeks**&#8230;getting big now.

**Rutabagas**&#8230;tops are huge so hopefully something&#8217;s happening underground too.

**Squash**&#8230;flowering.

**Parsnips**&#8230;who knows? there&#8217;s tops (I think) so hopefully there will also be bottoms.

**Cabbage**&#8230;starting to make heads.

**Potatoes**&#8230;harvested one tub already. Lots of yummy small potatoes. The tops are dying back now, so it&#8217;s almost time to harvest the rest.

**Sweet Peppers**&#8230;lots and small green, but turning red peppers

**Hot Peppers**&#8230;just starting to flower

Not bad for two 4&#215;8 raised beds and a handful of plastic tubs 🙂 It won&#8217;t feed us for very long, but all in all it&#8217;s a positive first gardening experience so far.

It&#8217;s funny, when I first started this blog I was worried that I wouldn&#8217;t have enough gardening posts. Now I written so many of them, I worry that I&#8221;ll be boring everyone. I promise to write way less gardening posts in the winter 😉