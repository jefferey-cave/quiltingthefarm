---
id: 2325
title: Happiness is
date: 2011-06-22T14:10:16+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2325
permalink: /2011/06/22/happiness-is/
categories:
  - Rural Living
---
Happiness is&#8230;a big old shade tree on a warm sunny day.

<a rel="attachment wp-att-2326" href="http://quiltingthefarm.vius.ca/2011/06/happiness-is/chickens-shade-tree/"><img class="alignnone size-full wp-image-2326" title="chickens-shade-tree" src="http://quiltingthefarm.plaidsheep.ca/files/2011/06/chickens-shade-tree.jpg" alt="chickens under shade tree" width="412" height="309" /></a>

<a rel="attachment wp-att-2329" href="http://quiltingthefarm.vius.ca/2011/06/happiness-is/shade-tree-chairs/"><img class="alignnone size-full wp-image-2329" title="shade-tree-chairs" src="http://quiltingthefarm.plaidsheep.ca/files/2011/06/shade-tree-chairs.jpg" alt="chairs under apple trees" width="412" height="309" /></a>

<p style="text-align: center">
  ******
</p>