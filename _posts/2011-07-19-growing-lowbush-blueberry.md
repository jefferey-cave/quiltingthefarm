---
id: 2585
title: Growing Lowbush Blueberry
date: 2011-07-19T07:00:00+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2585
permalink: /2011/07/19/growing-lowbush-blueberry/
tinymce_signature_post_setting:
  - 'yes'
categories:
  - Garden
---
We picked more blueberries today. Although it&#8217;s a little early for harvest time, the berries are coming in very quickly. We are trying to pick them as they come so the bears/rabbits/deer? won&#8217;t get them before we do. We had blueberry pancakes for supper last night and I also got to freeze my first small bag of berries ready for a delicious treat during the long winter months. I chose to freeze the berries by spreading them out on a tray so that they stay individual berries and not a frozen mass. Doing it this way lets me use as much or as little as I want. Hubby has these berries ear marked for pancakes&#8230;again 🙂

<a rel="attachment wp-att-2586" href="http://quiltingthefarm.vius.ca/2011/07/growing-lowbush-blueberry/frozen-blueberries/"><img class="alignnone size-full wp-image-2586" title="frozen-blueberries" src="http://quiltingthefarm.plaidsheep.ca/files/2011/07/frozen-blueberries.jpg" alt="" width="412" height="309" /></a>

Wild blueberries are the No. 1 fruit crop in Nova Scotia. As these wild blueberries moved onto our land of their own accord and seem to be quite prolific, we are researching the idea of starting a small scale (and I do mean _small_) farming endeavor. The land the berries have chosen as their home is slightly rocky with light sandy soil. Not much grows in this area and some of it has been taken over by spreading dogbane. We are hoping we can clear some brush and forest to open up more areas for growing. As we have a pond uphill from the field and a small stream that runs beside it, irrigation seems do-able. I want to stress that this is preliminary research on the practicalities of farming blueberries, and is no way in depth at this point. It may all be just a dream in the end, but what a wonderful dream.