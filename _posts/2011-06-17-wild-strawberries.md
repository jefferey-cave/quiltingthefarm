---
id: 2261
title: Wild Strawberries
date: 2011-06-17T06:32:54+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2261
permalink: /2011/06/17/wild-strawberries/
categories:
  - Garden
---
Wild strawberries ( Fragaria virginiana) are one of nature&#8217;s tasty little treats&#8230;and I do mean little. These tiny strawberries look like miniature versions of cultivated strawberries, and are not much bigger than a pea. They grow in well-drained soil where there’s lots of sun like meadows and fields, along the edge of woods and forests, and on hillsides. I have several colonies of these strawberries scattered about the farm and they are now ripe for the picking.

<a rel="attachment wp-att-2263" href="http://quiltingthefarm.vius.ca/2011/06/wild-strawberries/wild-strawberries/"><img class="alignnone size-full wp-image-2263" title="wild-strawberries" src="http://quiltingthefarm.plaidsheep.ca/files/2011/06/wild-strawberries.jpg" alt="wild strawberries" width="412" height="309" /></a>

The flowers, leaves, and stems of the wild strawberry can also be eaten. Strawberry-leaf tea is a good Vitamin C supplement; these strawberries have more Vitamin C per gram than oranges. They also have various medicinal uses. The root is a strong astringent that can be used to treat digestive orders especially diarrhea.

Wild strawberry fruits are eaten by a number of birds and mammals including squirrels, skunks, rabbits, chipmunks, mice, turtles and deer (the only one of those not yet seen on our farm is the skunk). The animals eat the berries and later spread the seeds in their droppings.  I don&#8217;t suppose most farmers would appreciate these prolific berries in their fields. I expect they will be labeled a nuisance or worse. That aside, I&#8217;m not a farmer yet, and I have no real fields, so the wild strawberries get to stay on for a while.

Although I don&#8217;t think harvesting them would be worth the effort, I might give it a try if I can find a large enough colony. Hubby thinks I should make jam. I think Hubby should pick the berries! 

<a rel="attachment wp-att-2262" href="http://quiltingthefarm.vius.ca/2011/06/wild-strawberries/wild-strawberries-close-up/"><img class="alignnone size-full wp-image-2262" title="wild-strawberries-close-up" src="http://quiltingthefarm.plaidsheep.ca/files/2011/06/wild-strawberries-close-up.jpg" alt="wild strawberries close up" width="412" height="296" /></a>

Wild strawberries may never compete with cultivated strawberries, but they do pack a tart, tastebud fueling punch. I&#8217;m thinking a few scattered on top of granola and yogurt will make a tasty breakfast.

<p style="text-align: center">
  ******
</p>