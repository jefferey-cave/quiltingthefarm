---
id: 344
title: Planning for Spring
date: 2011-02-06T10:31:25+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=344
permalink: /2011/02/06/planning-for-spring/
image: /files/2011/01/garden-area.jpg
categories:
  - Garden
tags:
  - garden
  - gardening
  - land
---
One glance outside and I realize that there’s not going to be a post on gardening anytime soon.

The snow has buried, what will be my first garden here. But I’m sure spring will make its lazy way here eventually. I have to be patient.

There are, Of course, lots of things to do before spring planting. We have decided to go the Square Foot Gardening route for a number of reasons:

  1. There’s only the 2 of us, so we don’t want to end up with a ton of food that we can’t possibly eat
  2. We don’t have any large garden equipment on the homestead and in all likelihood won’t acquire any either.
  3. The smaller, well divided areas keep the garden from being too overwhelming to care for (this is especially important to us as first time homesteaders).
  4. Our soil is very thin and rocky and dumping store bought soil in 8&#215;4 boxes seems a whole lot easier. (we are composting and hope to amend our own soil over time).

 

When we purchased our homestead it came with lots of odds and ends of wood. Some of these pieces will make excellent boxes that won’t cost us a cent. We will need to construct at least two 8 ft x 4 ft boxes for our first year garden and that will give us some idea of what we’ll need going forward.

We have chosen a site near the house where, we hope, watering won’t be problem. We also, don’t know, but suspect that we will need some major fencing due to the over abundance of wild rabbit and deer that frequent the property.

 [<img class="aligncenter size-medium wp-image-345" title="garden-area" src="http://quiltingthefarm.plaidsheep.ca/files/2011/01/garden-area-300x225.jpg" alt="" width="300" height="225" />](http://quiltingthefarm.plaidsheep.ca/files/2011/01/garden-area.jpg)

All in all, it will be one of our first major undertakings towards a self reliant life. The next one will be chickens!