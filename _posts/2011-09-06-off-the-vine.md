---
id: 3075
title: Off the Vine
date: 2011-09-06T07:00:19+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=3075
permalink: /2011/09/06/off-the-vine/
tinymce_signature_post_setting:
  - 'yes'
categories:
  - Garden
---
Homemade salsa, using fresh off the vine tomatoes, homegrown by me!

<a rel="attachment wp-att-3076" href="http://quiltingthefarm.vius.ca/2011/09/off-the-vine/salsa/"><img class="alignnone size-full wp-image-3076" title="salsa" src="http://quiltingthefarm.plaidsheep.ca/files/2011/09/salsa.jpg" alt="" width="412" height="309" /></a>

It&#8217;s been a strange year for my tomatoes. What looked like it was going to be a promising harvest, almost came to an abrupt halt with the discovery of [blossom end rot](http://quiltingthefarm.vius.ca/2011/08/blossom-end-rot/). I quickly researched the problem and didn&#8217;t find many solutions for this year&#8217;s batch. I found out that blossom end rot is caused by fluctuating water levels in the soil; too much, then too little.  The only thing to do is to keep the moisture level constant. That is helpful for next year, when I&#8217;ll be way more dilligent. The one thing I tried to correct the problem, was to mulch my tomatoes. I know, I should have done this earlier on, but being a first time gardener, my knowledge base was minimal to non-existent.

Well I have to say, to my complete and utter surprise, the tomatoes recovered! I only lost about a dozen tomatoes, if that, and the plants have gone on to be really productive. I have frozen lots of sauce, and now canned some salsa. There&#8217;s way more green tomatoes on the vines, and they&#8217;re ripening everyday.

This has been a great tomato to grow. I purchased the seeds from Annapolis Seeds here in the Valley. The name, I believe is &#8216;Cabot&#8217; and it&#8217;s a bush type of tomato.  The fruit is sweet and delicious eaten right off the vine. This is a tomato that I recommend, and will grow again myself, but just be forwarned, it takes up a lot of space and is not really suitable for small bed square foot gardening. I found this out out the hard way :0