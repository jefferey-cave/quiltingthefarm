---
id: 2616
title: Catching a Bat
date: 2011-07-23T07:00:39+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2616
permalink: /2011/07/23/catching-a-bat/
tinymce_signature_post_setting:
  - 'yes'
categories:
  - Rural Living
---
It&#8217;s hot. The heat and humidity are getting to us. I have spent the day trying to get things done but find myself waning after a few minutes of physical work. Everyone and everything is hot.

Bats must like the heat. We saw one yesterday, under the eaves of the garage. Hubby had quite a long and involved conversation with this little guy. He hung out on the side of the building (the bat, not the Hubby) and listened to all our dreams and woes. He seem riveted. The bat seems to be living under the metal roof with its numerous offspring. I would have thought it would be a most uncomfortable place to spend a summer. Strange home or not, we welcome these little critters as they really do seem to keep the flying insect population at bay. Last week we had a bit of a fiasco when a bat followed Hubby through the mudroom into the house . Hubby was just chatting to me in the living room when he said &#8220;What was that?&#8221; Me, in my ever obversevent way, said &#8220;What? I didn&#8217;t see anything&#8221; Hubby was sure something had flown past his head, so he wandered upstairs. There in the rafters of the spare room was a very small bat, obviously terrified (it _and_ us). So how do you get a bat out? well, I have to say, I have found a new use for sheer curtains! I held one curtain up at the door while hubby threw another over the bat. We caught him unharmed and let him go outside. Be aware that this post makes it  sound much easier than it really was. There was lots of squealing (me, _not_ the bat) and a few choice words from Hubby.

Raccoons, snapping turtles, deer, coyotes, snakes and bats. What&#8217;s next? a bear? Yep, I know one&#8217;s out there somewhere. You would think we&#8217;d moved to the country or something 😉