---
id: 808
title: Misadventure Number One
date: 2011-02-24T08:08:58+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=808
permalink: /2011/02/24/misadventure-number-one/
image: /files/2011/02/digging_snow1.jpg
categories:
  - Rural Living
tags:
  - country life
  - home
  - snow
  - weather
  - winter
  - wood
---
Did you read the “<a href="http://quiltingthefarm.vius.ca/2011/01/how-to-heat-a-home-in-the-country/" target="_blank">How to Heat a Country Home</a>” post? In that post I explain our homestead heating debacle.

<span id="So_misadventure_number_one_goes_somethinglike_this">

<h3>
  So misadventure number one goes something like this…
</h3></span> 

Some twit (and I&#8217;m not <a title="Hubby" href="http://vius.ca/wp-content/uploads/ChristmasSuspenders.20101225.jpeg" target="_blank">naming names</a> here) thought that putting the wood in the barn was a great idea. Unfortunately, he hadn’t thought this through enough. The barn is quite far from the house (this is barely noticeable in summer when it&#8217;s warm and sunny, but in winter, under several feet of snow&#8230;not so much). Every morning hubby gets out there and digs a trail <!--more-->all the way from the barn to the basement…and it’s a long way. Some days, by the time the trail is dug, it is too late to haul any wood, and when the next morning arrives it is buried in snow again.

<div id="attachment_811" style="width: 422px" class="wp-caption aligncenter">
  <a rel="attachment wp-att-811" href="http://quiltingthefarm.vius.ca/2011/02/misadventure-number-one/digging_snow2/"><img class="size-full wp-image-811" title="digging_snow2" src="http://quiltingthefarm.plaidsheep.ca/files/2011/02/digging_snow2.jpg" alt="Digging to the Barn" width="412" height="309" /></a>
  
  <p class="wp-caption-text">
    Digging to the Barn
  </p>
</div>

<div id="attachment_813" style="width: 422px" class="wp-caption aligncenter">
  <a rel="attachment wp-att-813" href="http://quiltingthefarm.vius.ca/2011/02/misadventure-number-one/808-revision-2/"><img class="size-full wp-image-813" title="digging_snow1" src="http://quiltingthefarm.plaidsheep.ca/files/2011/02/digging_snow1.jpg" alt="Digging to the basement" width="412" height="309" /></a>
  
  <p class="wp-caption-text">
    Digging to the basement
  </p>
</div>

<div id="attachment_812" style="width: 422px" class="wp-caption aligncenter">
  <a href="http://quiltingthefarm.plaidsheep.ca/files/2011/02/its_a_long_way-e1298492827979.jpg"><img class="size-full wp-image-812" title="its_a_long_way" src="http://quiltingthefarm.plaidsheep.ca/files/2011/02/its_a_long_way-e1298492827979.jpg" alt="Wow, it's a long way to the house!" width="412" height="309" /></a>
  
  <p class="wp-caption-text">
    Wow, it's a long way to the house!
  </p>
</div>

Now we have this fresh and very irritating problem. The dry (est) wood is in the barn. The door to the barn is iced shut. Hubby has spent 3 days trying to chip ice away, but the door won’t budge. Now we are left with the wood in the shed, This wood is still wet.

Next year we are going to put lots of wood in the basement, and build a shed just outside the basement for the rest of it. So add another project to the growing list.

_**Live and learn. It&#8217;s the only way to do it!**_

<p style="text-align: center">
  <strong><em>******</em></strong>
</p>

<div class="mceTemp mceIEcenter">
  <dt class="wp-caption-dt">
  </dt>
</div>

[](http://quiltingthefarm.plaidsheep.ca/files/2011/02/digging_snow1.jpg)