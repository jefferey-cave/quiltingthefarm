---
id: 2366
title: My Birthday
date: 2011-06-28T07:22:03+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2366
permalink: /2011/06/28/my-birthday/
categories:
  - Uncategorized
---
Today is my birthday and I have decided to give myself the day off.

[<img class="alignnone size-full wp-image-2367" title="birthday" src="http://quiltingthefarm.plaidsheep.ca/files/2011/06/birthday.jpg" alt="" width="229" height="333" />](http://quiltingthefarm.plaidsheep.ca/files/2011/06/birthday.jpg)

See you tomorrow!

<p style="text-align: center">
  ******
</p>