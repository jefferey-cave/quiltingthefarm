---
id: 2081
title: Saturday and Market Day
date: 2011-05-29T06:27:36+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2081
permalink: /2011/05/29/saturday-and-market-day/
categories:
  - Rural Living
---
Today we stopped by the market. Annapolis Royal has a Farmers Market every Saturday throughout the summer season. Today was their second Saturday of opening.

The market offers everything from knitted dishcloths to wonderful fruit wines, from original paintings to funky jewelry, and of course the ever present &#8220;farm&#8221; portion (that means seedlings at this time and veggies later on).

The weather was very humid, 100% humidity in fact, but the skies were cloudy causing an oppressive stillness to hang in the air. The traders weren&#8217;t that many, and those who were there weren&#8217;t doing a roaring business; it&#8217;s still early in the season. We spent time meeting new people, networking and purchasing some used books.

While driving home we noticed a guy selling lawn trimmers in his front yard. We stopped to take a peek. Hubby got a good deal on a gas powered model. We left the front yard about 20 minutes later loaded up with the trimmer and a free bag of seed potatoes. More planting to do.

We stopped at the feed and seed on the way home and stocked up on food, bedding and waterers for our iminent arrivals. I had already constructed a feeding station out of a piece of plastic eavestrough and a couple of pieces of wood for the ends, so we didn&#8217;t have to purchase that. I&#8217;m also in the process of making my own waterer, but it&#8217;s not ready yet. We added The bedding to the new coop. We are planning to use the deep bedding method. I hope our chickens will be happy here. We are looking forward to having our own fresh eggs.

Hubby mowed the rather wet and heavy grass and bush down as much as he could. He cleared an area next to the orchard for our raised beds. This won&#8217;t be their permanent place as we need to grade that area before planting anything there. The two raised beds should give us some veggies for this year. It might be a small step, but it is a step towards self reliance.

<div id="attachment_2083" style="width: 422px" class="wp-caption alignnone">
  <a rel="attachment wp-att-2083" href="http://quiltingthefarm.vius.ca/2011/05/saturday-and-market-day/orchard/"><img class="size-full wp-image-2083" title="orchard" src="http://quiltingthefarm.plaidsheep.ca/files/2011/05/orchard.jpg" alt="apple orchard" width="412" height="309" /></a>
  
  <p class="wp-caption-text">
    The orchard looking "spiffy" after its first trim of the season.
  </p>
</div>