---
id: 37
title: No TV, No Problem!
date: 2011-01-17T23:56:34+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=37
permalink: /2011/01/17/no-tv-no-problem/
image: /files/2011/01/books3.jpg
categories:
  - Rural Living
tags:
  - books
  - entertainment
  - home
  - reading
---
What do you on the old homestead when you don&#8217;t have TV.

>  
> 
> &#8220;Books in boxes,
> 
>  Books on floor
> 
>  Books on shelves
> 
>  And still there’s more!&#8221;

[<img class="size-thumbnail wp-image-193 alignnone" title="books3" src="http://quiltingthefarm.plaidsheep.ca/files/2011/01/books3-150x150.jpg" alt="" width="106" height="99" />](http://quiltingthefarm.plaidsheep.ca/files/2011/01/books3.jpg)  [<img class="size-thumbnail wp-image-191 alignnone" title="bookboxes" src="http://quiltingthefarm.plaidsheep.ca/files/2011/01/bookboxes-150x150.jpg" alt="" width="108" height="98" />](http://quiltingthefarm.plaidsheep.ca/files/2011/01/bookboxes.jpg)  [<img class="size-thumbnail wp-image-190 alignnone" title="bookshelf" src="http://quiltingthefarm.plaidsheep.ca/files/2011/01/bookshelf-150x150.jpg" alt="" width="110" height="98" />](http://quiltingthefarm.plaidsheep.ca/files/2011/01/bookshelf.jpg)