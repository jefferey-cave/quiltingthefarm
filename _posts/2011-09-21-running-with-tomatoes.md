---
id: 3218
title: Running With Tomatoes
date: 2011-09-21T07:00:41+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=3218
permalink: /2011/09/21/running-with-tomatoes/
tinymce_signature_post_setting:
  - 'yes'
categories:
  - Rural Living
---
My chickens love tomatoes. Since I have been making lots of salsa, sauce, and stuff lately, I have been saving all the bits I can&#8217;t use for them. When the bowl of slop is full I take it outside and enjoy the spectacle&#8230;it&#8217;s a free-for-all! I watch as one chicken finds the biggest, juiciest piece. The others, always watchful, see her and give chase around the yard. She runs. She tries to hold on to it, but it&#8217;s sooo hard to eat when you&#8217;re running. She drops it. Another gleeful hen picks it up and is off and running in a new direction, many insistant followers vying for this one supreme piece.

This goes on  and on. Some hold on longer than others. Each hen grabbing what&#8217;s left until nothing much remains of the prize.

Meanwhile Alexander is calling his ladies over to the mass of squishy red tomato goo still on the ground at his feet. Do they listen to him? No. They all want that very special piece, _the one that somebody else has got!_

<div id="attachment_3222" style="width: 422px" class="wp-caption alignnone">
  <a rel="attachment wp-att-3222" href="http://quiltingthefarm.vius.ca/2011/09/running-with-tomatoes/tomato-scramble2/"><img class="size-full wp-image-3222" title="tomato-scramble2" src="http://quiltingthefarm.plaidsheep.ca/files/2011/09/tomato-scramble2.jpg" alt="" width="412" height="309" /></a>
  
  <p class="wp-caption-text">
    Hey, there's treats over here
  </p>
</div>

<div id="attachment_3219" style="width: 422px" class="wp-caption alignnone">
  <a rel="attachment wp-att-3219" href="http://quiltingthefarm.vius.ca/2011/09/running-with-tomatoes/tomato-scramble/"><img class="size-full wp-image-3219" title="tomato-scramble" src="http://quiltingthefarm.plaidsheep.ca/files/2011/09/tomato-scramble.jpg" alt="" width="412" height="309" /></a>
  
  <p class="wp-caption-text">
    It's a mad scramble
  </p>
</div>

<div id="attachment_3220" style="width: 422px" class="wp-caption alignnone">
  <a rel="attachment wp-att-3220" href="http://quiltingthefarm.vius.ca/2011/09/running-with-tomatoes/tomato-scramble4/"><img class="size-full wp-image-3220" title="tomato-scramble4" src="http://quiltingthefarm.plaidsheep.ca/files/2011/09/tomato-scramble4.jpg" alt="" width="412" height="294" /></a>
  
  <p class="wp-caption-text">
    There she goes!
  </p>
</div>