---
id: 475
title: On Metal Roofs
date: 2011-02-10T14:30:08+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=475
permalink: /2011/02/10/on-metal-roofs/
image: /files/2011/01/digging-out3.jpg
categories:
  - Rural Living
tags:
  - home
  - house
  - snow
  - snowstorm
  - weather
  - winter
---
<div id="attachment_413" style="width: 283px" class="wp-caption alignleft">
  <a href="http://quiltingthefarm.plaidsheep.ca/files/2011/01/digging-out3.jpg"><img class="size-medium wp-image-413" title="digging-out3" src="http://quiltingthefarm.plaidsheep.ca/files/2011/01/digging-out3-300x225.jpg" alt="" width="273" height="201" /></a>
  
  <p class="wp-caption-text">
    There's a metal roof under there somewhere!
  </p>
</div>

If you have ever wanted to know what it’s like to have a musket fired in your ear, try living with a metal roof. I tell you, when the ice melts off the top roof and falls onto the lower roof, it sounds like you are under attack. The first melt we had, hubby was outside in a flash and found…nothing. The second time we both went out and found, well, nothing. We stood outside for the sound again…nothing. We came inside and there it was again. Apparently ice falling on metal only sounds crazy from the inside!

I would love to say we are used to it now, but I suspect that will take a good while longer, perhaps even a lifetime because that’s when the warranty is up!