---
id: 1613
title: Wattle Fence Update
date: 2011-04-06T06:10:05+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=1613
permalink: /2011/04/06/wattle-fence-update/
image: /files/2011/04/wattling3.jpg
categories:
  - Rural Living
tags:
  - DIY
  - fencing
  - wattling
---
<a rel="attachment wp-att-1614" href="http://quiltingthefarm.vius.ca/2011/04/wattle-fence-update/wattling-closeup/"></a>I thought you might like an update on the <a title="wattling" href="http://quiltingthefarm.vius.ca/2011/03/wattling/" target="_blank">wattle fenced compost bin</a> Hubby is building.

Now that the snow has melted (again) hubby has been back in the garden area working on getting the walls for the first bin finished.

<a rel="attachment wp-att-1616" href="http://quiltingthefarm.vius.ca/2011/04/wattle-fence-update/wattling2/"><img class="alignnone size-full wp-image-1616" title="wattling2" src="http://quiltingthefarm.plaidsheep.ca/files/2011/04/wattling2.jpg" alt="wattle fencing for compost bin" width="412" height="309" /></a>

[<img class="alignnone size-full wp-image-1614" title="wattling-closeup" src="http://quiltingthefarm.plaidsheep.ca/files/2011/04/wattling-closeup.jpg" alt="wattle fence close up" width="412" height="309" />](http://quiltingthefarm.plaidsheep.ca/files/2011/04/wattling-closeup.jpg)

The corners are not the easiest thing to get right!

<a rel="attachment wp-att-1619" href="http://quiltingthefarm.vius.ca/2011/04/wattle-fence-update/wattling3/"><img class="alignnone size-full wp-image-1619" title="wattling3" src="http://quiltingthefarm.plaidsheep.ca/files/2011/04/wattling3.jpg" alt="wattle fence for compost bin" width="412" height="309" /></a>

After weaving some of the big branches; hubby filled in the area by weaving in all the little twigs left from trimming the branches.

The walls are over half completed now (hubby did more after I took these pics), and will be finished on the next nice day (raining today). We have a container full of compostables that need a new home, so we are keeping our fingers crossed for some nice weather 🙂

<p style="text-align: center">
  ******
</p>