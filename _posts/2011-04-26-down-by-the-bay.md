---
id: 1876
title: Down by the Bay
date: 2011-04-26T06:28:48+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=1876
permalink: /2011/04/26/down-by-the-bay/
categories:
  - Rural Living
---
As we have family here until the middle of May, I have decided to spend my time with them and post every _other_ day instead of every day, until they leave.  I&#8217;m sure to have lots of photos and news to share with you.

Today turned out to be the most beautiful day we&#8217;ve had so far this year. We took a short drive to see the Bay of Fundy. It was a good choice. The skies were blue, the company was great and the water was perfect for skipping rocks.

Tomorrow we are having our new kitchen window put in, and the old window will see its life renewed in the chicken coop. We found out that **_none_** of our windows had been caulked, so we assume that was part of why the house was so cold this last winter. I wish we would have known to look out for this, but, hey, you live and you learn.