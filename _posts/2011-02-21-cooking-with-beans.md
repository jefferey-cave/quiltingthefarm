---
id: 725
title: Cooking with Beans
date: 2011-02-21T08:30:43+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=725
permalink: /2011/02/21/cooking-with-beans/
image: /files/2011/02/beans_pinto.jpg
categories:
  - Kitchen
tags:
  - beans
  - food
  - frugal
  - meals
---
<span id="For_us_homesteading_means_being_frugal_wanting_less_and_eating_beans">

<h3>
  For us, homesteading means being frugal, wanting less and eating beans!
</h3></span> 

<address>
   Beans are nutritious, beans are tasty and beans are cheap.
</address>

I’m going to try to address some of the issues with cooking and eating beans.

<span id="Beans_take_work_and_forethought">

<h3>
  <em>Beans take work and forethought</em>
</h3></span> 

Beans do needed to be soaked overnight (unless you use canned). There are several ways to get around this, but I’ve found that soaking beans is still the best way. Just put beans out to soak before you go to bed. If you sprout them they will need at least a couple of days (see below for sprouting beans).

<div id="attachment_776" style="width: 422px" class="wp-caption aligncenter">
  <a rel="attachment wp-att-776" href="http://quiltingthefarm.vius.ca/2011/02/cooking-with-beans/beans_pinto/"><img class="size-full wp-image-776" title="beans_pinto" src="http://quiltingthefarm.plaidsheep.ca/files/2011/02/beans_pinto.jpg" alt="pinto beans" width="412" height="309" /></a>
  
  <p class="wp-caption-text">
    Pinto Beans are my favourite bean
  </p>
</div>

<span id="Beans_need_to_be_cooked_a_long_time">

<h3>
  <em>Beans need to be cooked a long time</em>
</h3></span> 

If you have a pressure cooker you may be able to cook beans much more quickly, although, unfortunately my pressure cooker/canner says not to. I use a crock pot, set it for 8 hours and forget about it. Easy peasy.

<span id="Beans_are_bland">

<h3>
  <em>Beans are bland</em>
</h3></span> 

Beans have little flavour, they need lots <!--more-->(way more than you think) of seasoning. Beans tend to neutralize spices very quickly so you’ll end up with bland boring beans if you don’t use enough. Make sure you follow a few bean recipes at first and 

_don&#8217;t_ second guess the amount of seasonings or spices it states.

<div id="attachment_777" style="width: 422px" class="wp-caption aligncenter">
  <a rel="attachment wp-att-777" href="http://quiltingthefarm.vius.ca/2011/02/cooking-with-beans/beans_black/"><img class="size-full wp-image-777 " style="border: black 1px solid" title="beans_black" src="http://quiltingthefarm.plaidsheep.ca/files/2011/02/beans_black.jpg" alt="black beans" width="412" height="309" /></a>
  
  <p class="wp-caption-text">
    Black beans are great for refried beans and chili
  </p>
</div>

<span id="Beans_are_hard_to_digest">

<h3>
  <em>Beans are hard to digest</em>
</h3></span> 

This can be a problem for many people. The side effects are just not worth it for some. Fortunately there is an answer; sprouting them first! I always sprout beans before use. Soak beans overnight as usual then rinse and drain and leave in a bowl on counter. Do this twice a day for a couple of days until you start to see tiny sprouts forming. They are now ready to cook. The taste is no different but they are much easier to digest.

So get those beans out and get soaking! Why not try <a href="http://quiltingthefarm.vius.ca/2011/02/tortillas-and-refried-beans/" target="_blank">Refried Beans</a>.