---
id: 1011
title: Brewing Mead
date: 2011-03-07T08:34:23+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=1011
permalink: /2011/03/07/brewing-mead/
categories:
  - Rural Living
tags:
  - brewing
  - mead
  - wine
---
One of the things hubby and I already do is make Mead. Mead is honey wine produced by fermenting a solution of honey and water.

<div id="attachment_1013" style="width: 415px" class="wp-caption alignnone">
  <a rel="attachment wp-att-1013" href="http://quiltingthefarm.vius.ca/2011/03/brewing-mead/450px-swedish_mead/"><img class="size-full wp-image-1013 " title="Mead" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/450px-Swedish_Mead.jpg" alt="mead" width="405" height="540" /></a>
  
  <p class="wp-caption-text">
    Mead (image from Wikipedia)
  </p>
</div>

There is a lifetime of variety in Mead itself: Melomel (add fruit like blackberries, raspeberries, strawberries), Methyglyn (add spices like nutmeg, cinnamon, cloves) and Acerglyn (add maple syrup). In times past, Mead nearly <!--more-->went extinct as honey was very expensive (compared to grapes) and even the kings of Europe couldn&#8217;t afford to purchase it.

The Mead we brew is a &#8220;Sack Mead&#8221; which is basically a sweet mead. Our brew usually turns out as more of a sipping wine, perhaps a sweet sherry type drink. It is very much like ice wine in texture, being thicker and more syrupy than regular wine. I almost always drink mine cut with club soda.

Our recipe takes 12 kilos of honey. Back in Alberta we found a beginner/hobby beekeeper who was only too glad to be able to sell 12 kilos at a great rate to us, but here in Nova Scotia we don&#8217;t yet have any contacts in the honey business (we will be tracking some down this week). IMPORTANT: your Mead is only as good as your honey! (our use of supermarket honey was a dismal failure). In the future we hope to supply ourselves by keeping our own bees.

When someone asks hubby what the alcohol quantity is, he simply answers &#8220;enough&#8221;.

If you would like to find out more about Mead,  <a title="Got Mead" href="http://www.gotmead.com/" target="_blank">Got Mead</a> is a good resource. Or stop by in a month or 2 and try some for yourself 🙂

<p style="text-align: center">
  ******
</p>