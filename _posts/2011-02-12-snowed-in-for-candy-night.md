---
id: 628
title: Snowed in for Candy Night!
date: 2011-02-12T09:18:04+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=628
permalink: /2011/02/12/snowed-in-for-candy-night/
image: /files/2011/02/chocolates_bowl.jpg
categories:
  - Kitchen
  - Recipes
---
We couldn’t get to the store last evening for our regular candy night extravaganza (snowed in again) so we had to make do with popcorn and these little morsels that taste somewhere between peanut butter fudge and Reese’s Peanut Butter Cups.

** **

<h3 style="text-align: center">
  Peanut Butter Blobs
</h3>

** _The innards_**

1 cup creamy peanut butter

1/8 cup butter or margerine

pinch  salt

1/4 teaspoon pure vanilla extract

Just under 2 cups icing sugar

Warm peanut butter, salt and margarine together in a saucepan (just so they melt and stir together easily)

Remove from heat and stir in the icing sugar. Add just one <!--more-->cup at first, adding more as needed to make a dough that is easy to roll into a ball and isn’t too sticky.

[<img class="aligncenter size-medium wp-image-630" title="chocolates_pan" src="http://quiltingthefarm.plaidsheep.ca/files/2011/02/chocolates_pan-300x225.jpg" alt="" width="300" height="225" />](http://quiltingthefarm.plaidsheep.ca/files/2011/02/chocolates_pan.jpg)

Roll the dough into small balls and place on a cookie sheet. Put in the fridge to cool until firm.

_**The outer layer**_

1 cup of semi sweet chocolate chips (milk chocolate if you have them)

1 tablespoon shortening

Melt the chocolate and shortening in a bowl over a pan of simmering water.

Dip the balls, one at a time, in the melted chocolate, making sure the entire ball is coated with chocolate.

Place the chocolate covered balls back on the cookie sheet and place back in the fridge to set.

<div id="attachment_629" style="width: 310px" class="wp-caption aligncenter">
  <a href="http://quiltingthefarm.plaidsheep.ca/files/2011/02/chocolates_bowl.jpg"><img class="size-medium wp-image-629" title="chocolates_bowl" src="http://quiltingthefarm.plaidsheep.ca/files/2011/02/chocolates_bowl-300x225.jpg" alt="" width="300" height="225" /></a>
  
  <p class="wp-caption-text">
    All ready for candy night
  </p>
</div>

 I think they would have been much better with milk chocolate, but I didn’t have that. Nevertheless, I gorged myself silly and paid for it later 🙁

\***\***\***\***