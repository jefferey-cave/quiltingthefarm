---
id: 35
title: New Homestead Arrival
date: 2011-01-10T00:00:57+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=35
permalink: /2011/01/10/new-homestead-arrival/
categories:
  - Rural Living
tags:
  - baking
  - breads
  - grains
---
In keeping with my homesteading and self reliant goals, I would like to announce the arrival of our new Country Living Grain Mill!

 [<img class="alignleft size-full wp-image-46" title="Country Living Grain Mill" src="http://quiltingthefarm.plaidsheep.ca/files/2011/01/country-living-mill.jpg" alt="Country Living Grain Mill" width="184" height="178" />](http://quiltingthefarm.plaidsheep.ca/files/2011/01/country-living-mill.jpg)

Arrived &#8211; Jan 10/11,

Weight – heavy 

Length – ?? 

Colour – white 

Location – TBD