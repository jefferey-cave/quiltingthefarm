---
id: 2207
title: Planting the Garden
date: 2011-06-09T06:09:30+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2207
permalink: /2011/06/09/planting-the-garden/
categories:
  - Garden
---
I know I had said planting day was Monday, but this and that happened, and before I knew it it was Wednesday.

_Wednesday was planting day._

We had been gifted some sheep manure and topsoil which we had already added to the finished beds earlier this week. Shovelling manure is just plain hard work. I hurt my back many years ago (when I was about 19 or 20), but I&#8217;m determined to get stronger. I try to share as much of the heavy work as possible. I sometimes wonder if I&#8217;m more hindrance than help, but hubby always gives me a turn (he&#8217;s probably humoring me) so I carry on.

<a rel="attachment wp-att-2210" href="http://quiltingthefarm.vius.ca/2011/06/planting-the-garden/trailer/"><img class="alignnone size-full wp-image-2210" title="trailer" src="http://quiltingthefarm.plaidsheep.ca/files/2011/06/trailer.jpg" alt="trailer full of topsoil and manure" width="412" height="309" /></a>

As we are trying to work with the square foot gardening method, Hubby made a grid (with string) of 1 foot blocks over each of the beds. We were all ready to go.

First thing in the morning we set up a spreadsheet on Google Docs. Google Docs is a great way to be able to share a document between you and work on it at the same time. We made a 4&#215;8 grid on the spreadsheet, took out the seeds and set about figuring out where best to plant each seedling or seed. We moved stuff around a lot, trying to find the perfect place for each set of seeds. Eventually we had each veggie assigned to a certain spot in a particular bed. We even colour coded all the blocks for easy reference. By using this plan I won&#8217;t have to put markers out in the beds, and next year we&#8217;ll know what was planted where, so we can rotate our crops easily. It was a fun project.

Fun projects aside, We had to get out and do the physical planting. Two 4&#215;8 beds aren&#8217;t much but it still took us a while. It was a beautifully sunny day, and we spent far too long watching our new chickens sunbathing and digging in the dirt. Fortunately we had mostly finished by noon and we took our cue from the chickens and went back inside to cool off. A shower and a glass of iced tea later and I felt human again.

The beds are watered and chicken wire has been placed over the seeds to help protect them from invading critters. We still need to build a fence around both beds, and hopefully we can get to that this weekend.

<a rel="attachment wp-att-2209" href="http://quiltingthefarm.vius.ca/2011/06/planting-the-garden/planted-garden/"><img class="alignnone size-full wp-image-2209" title="planted-garden" src="http://quiltingthefarm.plaidsheep.ca/files/2011/06/planted-garden.jpg" alt="planted raised beds" width="412" height="309" /></a>

With the garden planted and the new chickens in the coop, for the first time I&#8217;m beginning to feel a little like a farmer. It&#8217;s the little steps that make it feel good. No stress, no worries, just plain old satisfaction. So as I collapse on the couch tonight, dirt ground under my fingernails, cheeks, rosey from way to much sun and the ever present bug bites on my face and hands. I look at myself in a different light. I may not be that farmer yet, but I&#8217;m on my way. Can I ask for anything more?