---
id: 3291
title: Beadboard Signs
date: 2011-09-30T07:10:28+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=3291
permalink: /2011/09/30/beadboard-signs/
tinymce_signature_post_setting:
  - 'yes'
categories:
  - Crafts
---
When we bought our place all the walls (and ceilings) were covered in pine beadboard or pine tongue and groove. Slowly, we are taking it down and replacing it with drywall, but now I have a garage full of the stuff. What to do, What to do. Here&#8217;s <a href="http://redhenhome.blogspot.com/2011/07/beadboard-signs.html" target="_blank">something</a> I found a while ago, so I thought I&#8217;d give it a try.

And here it is. A beadboard &#8220;Fresh Farm Eggs&#8221; sign for our farm. That is our new (to us) sign post stand that we picked up at the ReStore in Halifax last week. You&#8217;ll have to take my word for it, but there really is the word &#8220;Eggs&#8221; painted on the end.  I was holding it up and trying to take a photo at the same time. I couldn&#8217;t get far enough away&#8230; my arms are only so long!

<a rel="attachment wp-att-3293" href="http://quiltingthefarm.vius.ca/2011/09/beadboard-signs/eggs-sign2/"><img class="alignnone size-full wp-image-3293" title="eggs-sign2" src="http://quiltingthefarm.plaidsheep.ca/files/2011/09/eggs-sign2.jpg" alt="" width="412" height="287" /></a>

Next up&#8230;Fresh Baked Bread!