---
id: 2341
title: One Thing and Another
date: 2011-06-25T06:12:23+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2341
permalink: /2011/06/25/one-thing-and-another/
categories:
  - Rural Living
---
<span id="If_it8217s_not_one_thing8230">

<h3>
  If it&#8217;s not one thing&#8230;
</h3></span> 

<div id="attachment_2127" style="width: 422px" class="wp-caption alignnone">
  <a rel="attachment wp-att-2127" href="http://quiltingthefarm.vius.ca/2011/06/nature-against-man/raccoon-2/"><img class="size-full wp-image-2127 " title="raccoon" src="http://quiltingthefarm.plaidsheep.ca/files/2011/06/raccoon1.jpg" alt="raccoon" width="412" height="286" /></a>
  
  <p class="wp-caption-text">
    Raccoon hiding out in the barn
  </p>
</div>

<span id="it8217s_another.">

<h3>
  it&#8217;s another.
</h3></span> 

<div id="attachment_2342" style="width: 422px" class="wp-caption alignnone">
  <a rel="attachment wp-att-2342" href="http://quiltingthefarm.vius.ca/2011/06/one-thing-and-another/snapping-turtle/"><img class="size-full wp-image-2342" title="snapping-turtle" src="http://quiltingthefarm.plaidsheep.ca/files/2011/06/snapping-turtle.jpg" alt="snapping turtle" width="412" height="309" /></a>
  
  <p class="wp-caption-text">
    Snapping Turtle in driveway!
  </p>
</div>

 

Did someone put an &#8216;Open House&#8217; sign at the end of my driveway???

_(no animals were harmed in the taking of these photos&#8230;or beyond 🙂 )_

The turtle showed up  last evening after supper. She was trying to dig a hole, many holes in fact, in our gravel driveway. As you can see, she is almost the same colour as the ground and camouflaged so well I almost tripped over her! Hubby tried to persuade her to move on, but mother turtle didn&#8217;t care, she was looking for a place to lay her eggs. She was here most of the day yesterday and then suddenly she was gone.

<a rel="attachment wp-att-2351" href="http://quiltingthefarm.vius.ca/2011/06/one-thing-and-another/hubby-and-turtle/"><img class="alignnone size-full wp-image-2351" title="hubby-and-turtle" src="http://quiltingthefarm.plaidsheep.ca/files/2011/06/hubby-and-turtle.jpg" alt="" width="412" height="309" /></a>

Ah, the things a city girl has to get used to. First it was the ticks, then it was the black-fly, the groundhog, the raccoon, the coyotes, the bear (haven&#8217;t seen it yet&#8230;but it&#8217;s around), and the snakes. Let me tell you about the snakes&#8230;on second thoughts, no, I&#8217;d rather not have to think about them too much. Of everything in the yard so far, they creep me out the most.

With all that&#8217;s underfoot, I step gently these days!