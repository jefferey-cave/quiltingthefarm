---
id: 2713
title: Crazy Days of Summer
date: 2011-08-05T07:00:42+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2713
permalink: /2011/08/05/crazy-days-of-summer/
tinymce_signature_post_setting:
  - 'yes'
categories:
  - Rural Living
---
The weather really throw us a curve ball this week. We were supposed to have our new [wood stove](http://quiltingthefarm.vius.ca/2011/07/deconstructing-the-house/) installed on Tuesday, but with the lightening and thunder that continued all day, it wasn&#8217;t to be. The stove needs a whole new chimney installed and that means someone (not us) has to go up on our metal roof and take the old one down. I can see why they didn&#8217;t want to do it, human lightening rod comes to mind!

The [new hens](http://quiltingthefarm.vius.ca/2011/08/comets-and-rhode-island-reds/) are doing well. They have been laying consistently since we got them on Monday. It&#8217;s good to see our egg production up, although considering that we were only getting one egg every other day, there was really nowhere to go but up. I&#8217;m back to exploring egg recipes, and the next time I make Huevos Rancheros, I&#8217;ll remember to put in the eggs! (funny story that).

Hubby has been busy building his work of art&#8230;the wood stack. I must admit, it&#8217;s looking pretty spiffy. The sides are supposed to be 7 ft tall, and according to Hubby he will be able to get the wood down when we need it. Now Hubby is 5ft 9 inches tall and at this point he hasn&#8217;t actually explained &#8216;how&#8217; that will happen.

<a rel="attachment wp-att-2714" href="http://quiltingthefarm.vius.ca/2011/08/crazy-days-of-summer/woodstack2-2/"><img class="alignnone size-full wp-image-2714" title="woodstack2" src="http://quiltingthefarm.plaidsheep.ca/files/2011/08/woodstack21.jpg" alt="" width="412" height="309" /></a>

My trail and error, experimental corn patch is growing. It&#8217;s planted in, what seems to be, really crappy sandy soil, so we&#8217;ll see what happens. I have seen deer over in that area several times now, so I expect if anything edible grows, _we_ won&#8217;t be the ones enjoying it.

<a rel="attachment wp-att-2720" href="http://quiltingthefarm.vius.ca/2011/08/crazy-days-of-summer/corn-patch/"><img class="alignnone size-full wp-image-2720" title="corn-patch" src="http://quiltingthefarm.plaidsheep.ca/files/2011/08/corn-patch.jpg" alt="" width="412" height="309" /></a>

On another wildlife note, we found bear scat in our backyard, and Hubby heard something snuffling around outside the other night. I&#8217;m seriously thinking that we should open a nature preserve and make our living selling tickets to the deer and bear show! On a bright note; the 4 foot chicken wire [fence around my garden](http://quiltingthefarm.vius.ca/2011/06/fencing-the-garden/) has, of this writing, held most of the critters at bay, leaving only the caterpillars, slugs and one vole as veggie predators. I would say, they are easier to deal with, but I&#8217;m still a bit freaked out over creepies and crawlies&#8230;I&#8217;m such a girl 🙂

<div id="_mcePaste" class="mcePaste" style="width: 1px;height: 1px;overflow: hidden">
  ﻿
</div>