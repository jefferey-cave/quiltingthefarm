---
id: 3091
title: Indentifying Apples
date: 2011-09-09T07:09:56+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=3091
permalink: /2011/09/09/indentifying-apples/
tinymce_signature_post_setting:
  - ""
categories:
  - Rural Living
---
<div>
  <span id="We_think_we_have_identified_one_of_our_apple_trees">
  
  <h3>
    We think we have identified one of our apple trees!
  </h3></span> 
  
  <p>
    This apple may be an Api Etoile or Star Apple as it&#8217;s known in English. I think it It looks like a small flattened pumpkin. It&#8217;s hard to tell from the photos but these apples have a definite star shape to them.
  </p>
  
  <p>
    &#8220;Star Lady is a rare apple tree introduced from Switzerland in the 1600s. Its flattened-end shape has the appearance of a rounded star. It is a small apple that ripens late in the season with red-blushed yellow color. It has an excellent taste that is sweet, slightly acidic and fine flavored. It is hardy in USDA Zones 5 to 9 and is grown as a sweet fresh-eating or dessert apple.&#8221;
  </p>
  
  <p>
    Hubby has tried one of the windfalls and says it&#8217;s the nicest apple he&#8217;s tasted from our trees. Unfortunately the tree is shedding it&#8217;s apples and is looking quite sparse now. The Api Etoile is one of the apples that is susceptible to the coddling moth, and it looks like that&#8217;s our problem. There&#8217;s not much to do about it this year, but we are going to look into the problem and find a solution for next year.
  </p>
  
  <p>
    <a rel="attachment wp-att-3093" href="http://quiltingthefarm.vius.ca/2011/09/indentifying-apples/star-lady-apple/"><img class="alignnone size-full wp-image-3093" title="star-lady-apple" src="http://quiltingthefarm.plaidsheep.ca/files/2011/09/star-lady-apple.jpg" alt="" width="412" height="309" /></a>
  </p>
  
  <span id="">
  
  <h2>
    <a rel="attachment wp-att-3092" href="http://quiltingthefarm.vius.ca/2011/09/indentifying-apples/more-star-apples/"><img class="alignnone size-full wp-image-3092" title="more-star-apples" src="http://quiltingthefarm.plaidsheep.ca/files/2011/09/more-star-apples.jpg" alt="" width="412" height="309" /></a>
  </h2></span> 
  
  <p>
    Don&#8217;t you think they look kind of cool?
  </p>
  
  <p>
    UPDATE: Now we think it could be a Calville Blanc D’Hiver. I need to get out there and do a little more research!
  </p>
</div>