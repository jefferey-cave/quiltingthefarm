---
id: 3118
title: Endings and Beginnings
date: 2011-09-13T07:00:46+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=3118
permalink: /2011/09/13/endings-and-beginnings/
tinymce_signature_post_setting:
  - 'yes'
categories:
  - Rural Living
---
We sold some tomatoes yesterday! Yes, us with the tiny garden, sold a bag full of juicy ripe tomatoes to our neighbour.  We also sold 2 dozen eggs yesterday morning and 1/2 dozen eggs yesterday afternoon. We aren&#8217;t in the business of selling eggs either. It&#8217;s strange how some things work.

It was another beautiful late summer day. The chickens watched, mesmerized, as I hung a load of laundry on the line. They seem fascinated by the soft movement of the linens gently blowing in the afternoon breeze. I don&#8217;t know how long I&#8217;ll be able to dry clothes outside, but I will miss the smell of fresh line dried laundry when the time comes to dry them back indoors. Summer just isn&#8217;t long enough.

The nights are getting cold. I am a little concerned with a possible frost coming early to these parts. There have been a couple of near misses. The forecast is for much milder evenings next week, so perhaps my garden can hold out a little longer. Yesterday I asked for recipes for green tomatoes, but I now suspect the tomatoes will all be ripe by next week. I picked an entire basket again today. Tomorrow I absolutely must process them somehow&#8230;anyhow (salsa, sauce, frozen). There is no room left in the kitchen or the fridge!

<a rel="attachment wp-att-3122" href="http://quiltingthefarm.vius.ca/2011/09/endings-and-beginnings/ripe-tomatoes/"><img class="alignnone size-full wp-image-3122" title="ripe-tomatoes" src="http://quiltingthefarm.plaidsheep.ca/files/2011/09/ripe-tomatoes.jpg" alt="" width="412" height="309" /></a>

I think they&#8217;re multiplying!

<a rel="attachment wp-att-3123" href="http://quiltingthefarm.vius.ca/2011/09/endings-and-beginnings/more-ripe-tomatoes/"><img class="alignnone size-full wp-image-3123" title="more-ripe-tomatoes" src="http://quiltingthefarm.plaidsheep.ca/files/2011/09/more-ripe-tomatoes.jpg" alt="" width="412" height="309" /></a>

So what do you do when the garden project is coming to an end, and the fruit is all picked? You paint the porch and&#8230;.

find a new project 🙂

Here is my new (to me) thrifted chair. It&#8217;s just screaming for a nice coat of paint and a new seat. What good&#8217;s a winter without a few projects in the line-up!

<a rel="attachment wp-att-3124" href="http://quiltingthefarm.vius.ca/2011/09/endings-and-beginnings/new-chair/"><img class="alignnone size-full wp-image-3124" title="new-chair" src="http://quiltingthefarm.plaidsheep.ca/files/2011/09/new-chair.jpg" alt="" width="309" height="412" /></a>

Linking to <a title="Homestead Barn Hop" href="http://homesteadrevival.blogspot.com/2011/09/barn-hop-27.html" target="_blank">Homestead Barn Hop</a>!