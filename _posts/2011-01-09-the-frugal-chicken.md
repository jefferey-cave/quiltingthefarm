---
id: 13
title: The Frugal Chicken
date: 2011-01-09T23:05:20+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=13
permalink: /2011/01/09/the-frugal-chicken/
categories:
  - Kitchen
  - Recipes
tags:
  - food
  - frugal
  - meals
---
My mother always said I was frugal. I don’t try to be; it just seems to come naturally. So you would think I would pass right by those supermarket, ready cooked chickens, wouldn’t you?

Anyway, the other day our rural supermarket had their chickens marked down by 50%. Usually they cost $9.99 (cdn) so I got one for about $5. Now you have to understand that in Canada it’s almost impossible to find a _frozen_ chicken under $10.

**Meal one:** Chicken breast with potatoes and veggies (could have made the chicken breast last for 2 meals if we hadn’t been so piggy!)
  
**Meal two:** Chicken pot pie
  
**Meal three:** Chicken legs, rice and salad
  
**Meal four:** Hearty chicken soup

Now, that’s one frugal chicken.