---
id: 1691
title: Baking Day, Oatmeal Date Muffins
date: 2011-04-12T06:40:06+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=1691
permalink: /2011/04/12/baking-day-oatmeal-date-muffins/
image: /files/2011/04/oatmeal-date-muffins.jpg
categories:
  - Kitchen
  - Recipes
---
With company coming in a couple of weeks I need to do some baking for the freezer.

Today was a breadmaking day.

<a rel="attachment wp-att-1692" href="http://quiltingthefarm.vius.ca/2011/04/baking-day-oatmeal-date-muffins/white-bread/"><img class="alignnone size-full wp-image-1692" title="white-bread" src="http://quiltingthefarm.plaidsheep.ca/files/2011/04/white-bread.jpg" alt="freshly baked bread" width="412" height="309" /></a>

I also added these wonderful <a title="oatmeal date muffins" href="http://sugarcrafter.net/2010/12/06/oatmeal-date-muffins/" target="_blank">Oatmeal Date Muffins</a> to my baking schedule. I doubled the original recipe, but I had to add more flour as the batter was far too runny.

  * 2 1/2 &#8211; 3 cups flour
  * 1 cup brown sugar
  * 3 tsp baking powder
  * 1/2 tsp salt
  * 1 tsp cinnamon
  * 1/4 tsp fresh ground cardamom seeds
  * 2 cups oats
  * 2 cups chopped dates
  * 1/2 cup butter, melted
  * 2 cup milk
  * 1 tsp pure vanilla extract
  * 2 eggs

  1. Preheat the oven to 400 degrees
  2. In one bowl mix together the first 8 ingredients.
  3. In another bowl, whisk together the eggs, vanilla, milk and butter.
  4. Add the dry ingredients to the wet ingredients and stir until just moistened.
  5. Spoon the batter into greased muffin tins.
  6. Bake for approximately 15 &#8211; 20 minutes, until the tops are golden brown.

<a rel="attachment wp-att-1693" href="http://quiltingthefarm.vius.ca/2011/04/baking-day-oatmeal-date-muffins/oatmeal-date-muffins/"><img class="alignnone size-full wp-image-1693" title="oatmeal-date-muffins" src="http://quiltingthefarm.plaidsheep.ca/files/2011/04/oatmeal-date-muffins.jpg" alt="oatmeal date muffins" width="412" height="309" /></a>

Here they are, all nicely package for the freezer. I just hope I can stay away from them for 2 weeks 🙂 

I wonder what I should bake next time.  Any suggestions?

I&#8217;m linking this post up with <a title="Homestead Barn Hop" href="http://homesteadrevival.blogspot.com/2011/04/barn-hop-8.html" target="_blank">Homestead Barn Hop</a>

<p style="text-align: center">
  ******
</p>