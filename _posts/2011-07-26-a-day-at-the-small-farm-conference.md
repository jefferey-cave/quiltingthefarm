---
id: 2631
title: A Day at the Small Farm Conference
date: 2011-07-26T07:00:28+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2631
permalink: /2011/07/26/a-day-at-the-small-farm-conference/
tinymce_signature_post_setting:
  - 'yes'
categories:
  - Rural Living
---
It was another beautiful day in the Valley, a little too hot if you ask me. Yesterday we spent the day at the &#8220;Celebration of Small-Scale Farming&#8221;. We set off early to drive the 1 hour 15 mins to Northville Farm Heritage Centre. Sign posts are a rare commodity in Nova Scotia, apparently you are only allowed 5 road signs in Kings County. If you live off the beaten track then 5 isn&#8217;t nearly enough, it leaves people stranded in the middle of nowhere, not knowing whether to turn or go straight. This happened to several people, but luckily not to us (which makes a change).

I had signed up for Vegetable Season Extension as my first course of the day. I must admit, although the presenter wasn&#8217;t great, I learned a whole lot that I am going to put into practice as soon as I can. All in all very informative. Hubby&#8217;s first course, Pastured Poultry Production also turned out to be very informative. Next for me was Compost Tea for Small Fruit Production. Sadly, nothing much useful was to be gained form this presentation, it was more about the fact that compost tea had helped the presenters strawberry growth and disease resistance. There was lots of techinical analysis but no real no useable info on compost tea itself. I actually left about 2/3 of the way through and dropped in on the Women&#8217;s Panel. Hubby&#8217;s Finishing Market Lambs course was much better, stuff he could actually put into practice&#8230;if we had actually had sheep!

One of the biggest disappointments was lunch, and the activities that were supposed to be happening. With our payment for the conference, we got a $5 food voucher. The food choices were&#8230;one vegeterian dish (basically assorted salads) or one meat dish (hot dogs). Now, I don&#8217;t know about you, but when I go to a conference for farmers I expected something more than hot dogs! At least the price of the hot dog was reasonable at $2&#8230;the veggie dish was $7! The lunchtime activities were hit and miss&#8230;mostly miss.

The afternoon course that I signed up for was Permaculture and Its Application on the Farm. (I changed my mind about the marketing course as it sounded more like an inspirational story and something I wasn&#8217;t interested in). Hubby went to the Animal Powered workshop instead. We are very interested in permaculture, it&#8217;s the way we would like to live on our farm. Our speaker set the wrong tone when he said he was excited about the upcoming economic collapse! Now I know some people think they&#8217;re doing everything right, and they&#8217;ll be saved from ruin and desperation, but anyone who wishes that on anyone else, is, in my mind, not worth the time of day. I did stay and listen for a while, hoping for better things, but the whole presentation was political grandstanding and nothing to do with permaculture on a small farm. I have my own thoughts, theories, and otherwise on Climate change, Peak Oil, etc. and I don&#8217;t like being presented with _theories_ spoken as fact. I left the presentation and went to watch the animal powered workshop instead.

Did we get anything out of the conference&#8230;yes, but not enough. Will we go again&#8230;probably not.