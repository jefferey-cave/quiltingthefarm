---
id: 1023
title: Designing Teddy Bears
date: 2011-03-08T08:14:01+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=1023
permalink: /2011/03/08/designing-teddy-bears/
image: /files/2011/03/bear-in-green.jpg
categories:
  - Crafts
tags:
  - crafts
  - designing
  - sewing
---
I have kept my teddy bears since childhood. A few years ago my mom gave me a beautiful mohair artist teddy bear and started me on the road to creating and designing my own original bears. I received my first award in 1999

I love designing and creating bears from mohair, cashmere and vintage coats. All my bears are hand stitched and each little face has been needle sculpted for that perfect expression.

The sizes of bears I create ranges from the teeny tiny 2 inch mini to the huggable 23&#8243; bear . Each bear has German hand blown glass eyes, embroidered nose and paws and is stuffed with glass beads or steel shot to give them a nice weight.

All my bears are designed for the collector and are not suitable for children.

Here are some of the bears I have kept for my collection. They always bring a smile to my face 🙂

<div id="attachment_1028" style="width: 310px" class="wp-caption alignnone">
  <a href="http://quiltingthefarm.plaidsheep.ca/files/2011/03/curly-bear.jpg"><img class="size-medium wp-image-1028  " title="curly-bear" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/curly-bear-300x225.jpg" alt="String cotton teddy bear" width="300" height="225" /></a>
  
  <p class="wp-caption-text">
    Curly Bear
  </p>
</div>

<div id="attachment_1028" style="width: 381px" class="wp-caption alignnone">
  <a href="http://quiltingthefarm.plaidsheep.ca/files/2011/03/bear-in-green.jpg"><img class="size-medium wp-image-1028  " title="Lola Bear" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/bear-in-green.jpg" alt="Mohair teddy bear" width="371" height="494" /></a>
  
  <p class="wp-caption-text">
    Lola Bear
  </p>
</div>

<div id="attachment_1028" style="width: 381px" class="wp-caption alignnone">
  <a href="http://quiltingthefarm.plaidsheep.ca/files/2011/03/papa-bear"><img class="size-medium wp-image-1028     " title="Old Timer Bear" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/papa-bear.jpg" alt="Mohair bear" width="371" height="494" /></a>
  
  <p class="wp-caption-text">
    Old Timer Bear
  </p>
</div>