---
id: 2607
title: Winter Wood Stacking
date: 2011-07-22T07:17:37+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2607
permalink: /2011/07/22/winter-wood-stacking/
tinymce_signature_post_setting:
  - ""
categories:
  - Rural Living
---
When I asked Hubby what I should write a post about, he said &#8220;my wood stack&#8221; So in honour of my hard working hubby, here&#8217;s a post and photos dedicated to his project.

<a rel="attachment wp-att-2610" href="http://quiltingthefarm.vius.ca/2011/07/winter-wood-stacking/wood-stack/"><img class="alignnone size-full wp-image-2610" title="wood-stack" src="http://quiltingthefarm.plaidsheep.ca/files/2011/07/wood-stack.jpg" alt="" width="412" height="309" /></a>

All our wood has been delivered for our winter heating. Altogether we ordered 6 cords of dried hardwood. Hubby has been busy building this stack. It&#8217;s all very neat and an organized 7ft. x 7ft. x 7ft. exactly. Did I ever tell you Hubby is an Analyst? Just to make sure that you don&#8217;t think that I am just the photo taker, I  worked on this too. See those  nicely stacked ends? they were done with my own fair hands. Hubby says I&#8217;m better at stacking wood than he is, I wonder if that&#8217;s just a gentle hint that I should be doing more 🙂

<a rel="attachment wp-att-2609" href="http://quiltingthefarm.vius.ca/2011/07/winter-wood-stacking/close-wood-pile/"><img class="alignnone size-full wp-image-2609" title="close-wood-pile" src="http://quiltingthefarm.plaidsheep.ca/files/2011/07/close-wood-pile.jpg" alt="" width="412" height="309" /></a>

We still have a long way to go., but there&#8217;s lots of time till winter. The weather here has been just too hot to maintain a pace for very long. We have plans for a trip to the South Shore for a day at the beach sometime this weekend to cool off.

<a rel="attachment wp-att-2608" href="http://quiltingthefarm.vius.ca/2011/07/winter-wood-stacking/more-wood/"><img class="alignnone size-full wp-image-2608" title="more-wood" src="http://quiltingthefarm.plaidsheep.ca/files/2011/07/more-wood.jpg" alt="" width="412" height="309" /></a>