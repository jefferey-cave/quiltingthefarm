---
id: 2931
title: Rolling Up the Fruit
date: 2011-08-24T07:00:28+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2931
permalink: /2011/08/24/rolling-up-the-fruit/
tinymce_signature_post_setting:
  - ""
categories:
  - Kitchen
---
When I first came over to Canada as a young woman, wide eyed with wonder in a foreign land, I was absolutely enthralled with Fruit Roll-Ups. I discovered them one day while I was travelling on a Greyhound Bus. The bus had stopped to pick up some people in a very small town, and because we were early, everyone had the opprtunity to get off and stretch their legs. The Bus depot was actually a convenience store, and the store obviously catered for the bus traveller plying them with individual snacks of cookies and candy. The store sold individual Fruit Roll Ups taken from a larger box. At $2-$3 dollars, the large box was out of my &#8220;snacking&#8221; price range, but one Fruit Roll Up, even I could afford. I took my Fruit Roll Up back to the bus. I wasn&#8217;t quite sure what to make of it. Was the paper it was stuck to edible? Was I to unroll it and pull the sticky fruit off the paper with my teeth? (this is actually what I did). This sweet sticky &#8220;fruity&#8221; stuff was tasty, but one devil of a problem to eat. Fortunately, It wasn&#8217;t long before I figured out the best way to consume them 🙂

I never buy Fruit Roll Ups now, although I still love the taste of them. When my daughter was small I would buy fruit leathers for her lunches. She enjoyed them, but I never did. Every attempt at making them has been a dismal failure&#8230;until now.

Yesterday, after boiling chokecherries for another batch of jelly, I had this notion to make jam instead. Rather than drain the juice and pulp through cheesecloth I worked it through a sieve. After discarding the pits, I had a large bowl of juice and pulp left. I let the mixture cool down on the counter top, but while it was cooling I, being the fickle person that I am, decided to go the jelly route again and promptly dumped the entire bowl of pulp and juice into a cheesecloth. Now, I had my juice for jelly, but I also had a bunch on wonderful pulp. The frugal in me just couldn&#8217;t thow it away, so I added a handful of sugar, mixed it together and poured it onto a oven tray covered in plastic wrap. Because it&#8217;s been so humid here I decided that drying it out in the oven would probably be best way. I set the oven for about 150 (my oven is not marked below 200) and left it all day.

This is what I got at supper time! 

<a rel="attachment wp-att-2935" href="http://quiltingthefarm.vius.ca/2011/08/rolling-up-the-fruit/chokecherry-leather/"><img class="alignnone size-full wp-image-2935" title="chokecherry-leather" src="http://quiltingthefarm.plaidsheep.ca/files/2011/08/chokecherry-leather.jpg" alt="" width="412" height="309" /></a>

All sticky and cling to your teeth sweet (not as sweet as it could be, but that&#8217;s an experiment for another time). And as I peeled the thin fruit away from the plastic backing with my teeth, I remembered my brief love affair with the Fruit Roll Up.

<a rel="attachment wp-att-2936" href="http://quiltingthefarm.vius.ca/2011/08/rolling-up-the-fruit/chokecherry-roll-up/"><img class="alignnone size-full wp-image-2936" title="chokecherry-roll-up" src="http://quiltingthefarm.plaidsheep.ca/files/2011/08/chokecherry-roll-up.jpg" alt="" width="412" height="309" /></a>