---
id: 1186
title: Oat Rye Bread
date: 2011-03-17T07:18:19+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=1186
permalink: /2011/03/17/oat-rye-bread/
image: /files/2011/03/oat-rye-bread.jpg
categories:
  - Kitchen
  - Recipes
tags:
  - baking
  - breads
  - country life
  - food
  - grains
  - recipes
---
I have had a request from Mary, one of my readers for_ ****_the_ **<a title="Walk on the wild side" href="http://quiltingthefarm.vius.ca/2011/03/a-walk-on-the-wild-side/" target="_blank">Oat Rye Bread</a>**_.

_<span style="text-decoration: underline"><strong>Mary, sorry I took so long to get it posted, but here it is now 🙂</strong></span>_

I&#8217;ve been playing around with recipes and ingredients quite a bit now that I seem to have mastered bread baking _(I just hope I haven&#8217;t jinxed myself now)._

One of the things I&#8217;ve found helpful is to raise the dough in a large plastic bowl with a tight fitting lid. A bowl like this<!--more--> is a great size; the dough just touches the top when it has doubled in size. No more guess work.

<a rel="attachment wp-att-1207" href="http://quiltingthefarm.vius.ca/2011/03/oat-rye-bread/bowl/"><img class="alignnone size-full wp-image-1207" title="bowl" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/bowl.jpg" alt="bowl for raising bread" width="412" height="309" /></a>

<span id="Here8217s_the_list_of_ingredients_for_Oat_Rye_Bread:">

<h3>
  Here&#8217;s the list of ingredients for Oat Rye Bread:
</h3></span> 

  * 4 cups milk, slightly warmed. ( I use powder milk for this)
  * 1 1/2 tablespoons yeast
  * 2 tablespoons sugar
  * 2 tablespoons of oil
  * 4 cups unbleached or all purpose flour
  * 1 tablespoon gluten flour
  * 4 &#8211; 6 cups of a combination of oat flour and rye flour
  * 1 1/2 tablespoon salt

<span id="Here8217s_the_How_To:">

<h3>
  Here&#8217;s the How To:
</h3></span> 

  1. Place warmed milk in a large bowl and add the yeast and sugar. Let sit for a few minutes until it starts to turn a little frothy
  2. Stir in oil
  3. Add  all 4 cups of all purpose/unbleached flour, the gluten flour and salt to the liquid. stir to combine.
  4. Add about a cup of oat flour and a cup of rye flour   (you may need to add more or less depending on constistency). The dough should just be coming together in a rather ragged mess.
  5. Cover bowl with damp tea towel and let sit for 20 minutes to allow dough to rest.
  6. Turn dough out onto floured surface and knead in one cup of oat flour and one cup of rye flour. Knead for 20 minutes adding as much of the remaining flours as you can. I never quite manage to knead in all of it.
  7. Flour the sides and bottom of a large bowl  (no need to grease bowl). Place dough in the bowl and leave dough to double in size.
  8. Tip dough out onto floured surface and just press out a little. This will deflate any large bubbles and give your bread an even texture. There is no need to knead again.
  9. Divide dough into 3 and place in greased loaf pans. Dust tops with flour.
 10. Bake at 350 degrees for about 30 minutes.
 11. Remove from pans and let cool for at least 30 minutes before slicing. Yeah right! I&#8217;m lucky if we hold off for 5 minutes. Isn&#8217;t that why we bake bread so we can eat it hot from the oven???

<a rel="attachment wp-att-1209" href="http://quiltingthefarm.vius.ca/2011/03/oat-rye-bread/oat-rye-bread/"><img class="alignnone size-full wp-image-1209" title="oat-rye-bread" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/oat-rye-bread.jpg" alt="Oat rye bread" width="412" height="309" /></a>

This bread makes awesome cinnamon raisin bread too. When I baked this last week I took 1/3 dough and pressed it out into a rectangle with my hands. I spread a  layer of butter over the rectangle, sprinkled it with brown sugar and cinnamon, topped it with a handful of raisins and rolled it up tightly. I baked this along with the other loaves (same temperature, same time). I didn&#8217;t manage to take a photo as it went too quickly!

I&#8217;m linking up to FFF. Check it out and visit the other great blogs
  
<a href="http://www.verdefarm.com" target="_blank"><img src="http://i1091.photobucket.com/albums/i390/VerdeFarm/farmgirlfridaybuttonforamycopy150.jpg" alt="" width="129" height="118" /></a>

<p style="text-align: center">
  ******
</p>