---
id: 3181
title: Just Chickens
date: 2011-09-18T07:00:31+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=3181
permalink: /2011/09/18/just-chickens/
tinymce_signature_post_setting:
  - 'yes'
categories:
  - Rural Living
---
Our chickens enjoying a beautiful late summer afternoon.

<a rel="attachment wp-att-3182" href="http://quiltingthefarm.vius.ca/2011/09/just-chickens/chickens-outside/"><img class="alignnone size-full wp-image-3182" title="chickens-outside" src="http://quiltingthefarm.plaidsheep.ca/files/2011/09/chickens-outside.jpg" alt="" width="412" height="309" /></a>

<a rel="attachment wp-att-3188" href="http://quiltingthefarm.vius.ca/2011/09/just-chickens/chickens-outside3/"><img class="alignnone size-full wp-image-3188" title="chickens-outside3" src="http://quiltingthefarm.plaidsheep.ca/files/2011/09/chickens-outside3.jpg" alt="" width="412" height="310" /></a>

<a rel="attachment wp-att-3187" href="http://quiltingthefarm.vius.ca/2011/09/just-chickens/chickens-outside4/"><img class="alignnone size-full wp-image-3187" title="chickens-outside4" src="http://quiltingthefarm.plaidsheep.ca/files/2011/09/chickens-outside4.jpg" alt="" width="412" height="309" /></a>

<a rel="attachment wp-att-3189" href="http://quiltingthefarm.vius.ca/2011/09/just-chickens/chickens-outside2/"><img class="alignnone size-full wp-image-3189" title="chickens-outside2" src="http://quiltingthefarm.plaidsheep.ca/files/2011/09/chickens-outside2.jpg" alt="" width="412" height="309" /></a><a rel="attachment wp-att-3188" href="http://quiltingthefarm.vius.ca/2011/09/just-chickens/chickens-outside3/"></a>

<a rel="attachment wp-att-3186" href="http://quiltingthefarm.vius.ca/2011/09/just-chickens/chickens-outside5/"><img class="alignnone size-full wp-image-3186" title="chickens-outside5" src="http://quiltingthefarm.plaidsheep.ca/files/2011/09/chickens-outside5.jpg" alt="" width="412" height="309" /></a>

<a rel="attachment wp-att-3185" href="http://quiltingthefarm.vius.ca/2011/09/just-chickens/chickens-outside6/"><img class="alignnone size-full wp-image-3185" title="chickens-outside6" src="http://quiltingthefarm.plaidsheep.ca/files/2011/09/chickens-outside6.jpg" alt="" width="412" height="309" /></a>

<a rel="attachment wp-att-3184" href="http://quiltingthefarm.vius.ca/2011/09/just-chickens/chickens-outside7/"><img class="alignnone size-full wp-image-3184" title="chickens-outside7" src="http://quiltingthefarm.plaidsheep.ca/files/2011/09/chickens-outside7.jpg" alt="" width="412" height="309" /></a>

<a rel="attachment wp-att-3183" href="http://quiltingthefarm.vius.ca/2011/09/just-chickens/chickens-outside8/"><img class="alignnone size-full wp-image-3183" title="chickens-outside8" src="http://quiltingthefarm.plaidsheep.ca/files/2011/09/chickens-outside8.jpg" alt="" width="412" height="309" /></a>