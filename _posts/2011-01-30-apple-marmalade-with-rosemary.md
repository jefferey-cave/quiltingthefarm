---
id: 207
title: Apple Marmalade with Rosemary
date: 2011-01-30T20:45:27+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=207
permalink: /2011/01/30/apple-marmalade-with-rosemary/
categories:
  - Kitchen
  - Recipes
tags:
  - apples
  - canning
  - fruit
  - preserving
---
[<img class="alignleft size-medium wp-image-209" title="marmalade" src="http://quiltingthefarm.plaidsheep.ca/files/2011/01/marmalade-300x225.jpg" alt="" width="300" height="225" />](http://quiltingthefarm.plaidsheep.ca/files/2011/01/marmalade.jpg)When we purchased our homestead, the apple season was just coming to an end, but the  trees were still producing. At the beginning, we only spent weekends out here at the farm, so most of the apples made their way into any deer or bear passing by, but, being kind and generous animals, they left a few for me!

Our apples are gnarly things; the trees are old and haven’t been sprayed. But the apples taste great.

I collected all that I could and set out to make anything that I could. Here’s a recipe for Apple Marmalade from <a href="http://tigressinajam.blogspot.com/2010/11/apple-marmalade-with-rosemary.html" target="_blank">Tigress in a Jam</a>.I tweaked it a little for my purpose (working with what I had on hand)

  * 2 (or so) pounds apples
  * 2 lemons
  * 3 1/2 cups sugar
  * Rosemary (I used a large pinch of dried but use fresh if you have it)
  * Canning jars, bands and lids

  1. To prepare jars, boil for 5 minutes in a large pot. Also simmer the bands and lids. Keep sterilized jars in warm oven until needed (this helps to eliminate breakages from putting hot liquid into cold jars)
  2. Wash and dry the lemons. Cut in quarters and slice as thinly as possible, remove and discard the seeds. Place lemon slices in a non-reactive (stainless or enameled iron) pot with 3 cups cold water.
  3. Peel, core and chop apples, add to the pot submerging the apples to stop them from discolouring.
  4. Bring to the boil, reduce heat and simmer for 30-45 minutes. The lemon slices should be soft.
  5. Add sugar and stir until dissolved. A rosemary and then bring back to the boil Cook on medium high for approximately 45 minutes to 1 hour.
  6. Test for doneness by back of spoon or plate/freezer method.
  7. Fill hot jars to 1/4 inch from top and process in a hot water bath for 10 minutes. (Make sure your jars are covered by at least one inch of water)
  8. Remove from bath and let marmalade stand, undisturbed, for 24 hours.
  9. Check all seals before storing in pantry. Any jars that didn’t seal can be kept in the fridge to be consumed relatively quickly.
 10.  Enjoy on buttered toast or use as a wonderful glaze for chicken or pork