---
id: 2215
title: Rhubarb Salsa
date: 2011-06-10T07:11:34+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2215
permalink: /2011/06/10/rhubarb-salsa/
categories:
  - Kitchen
  - Recipes
---
Last week I was given a bag full of rhubarb. Hubby and I both love rhubarb, and I usually make rhubarb crisp or muffins&#8230;something sweet.

When we were at the market last week, we tried some rhubarb salsa at one of the stalls. The tang of the rhubarb combined with the heat of the jalapeno made for interesting yet delicious taste sensation. My tongue tingled.

I searched online for a recipe and found one here at <a title="rhubarb salsa" href="http://www.sbcanning.com/2011/05/canning-rhubarb-now-for-something.html" target="_blank">sbcanning.com</a>. I changed it up a little, using a little more rhubarb and simmering all the ingredients together. Here&#8217;s my version of rhubarb salsa&#8230;

7 cups chopped rhubarb

1 cup sugar

1/4  cup water

2 tablespoons finely grated orange peel

½ cup red bell pepper, diced

¼ cup onion, finely chopped

⅓ cup red onion, finely chopped

1 jalapeno pepper, seeded and finely chopped

2 tablespoons honey

2 tablespoons lemon juice

1 teaspoon fresh ginger, grated

Combine the sugar, water and orange peel. Bring to a boil over high heat. Add the rhubarb slices and reduce the heat to medium.

Simmer gently until the Rhubarb is tender, about 20 minutes.

Add remaining ingredients and simmer for 5 more minutes.

Ladle into sterilized jars and process 20 minutes in water bath canner at a full boil. Makes between 5-6 cups.

<a rel="attachment wp-att-2216" href="http://quiltingthefarm.vius.ca/2011/06/rhubarb-salsa/rhubarb-salsa/"><img class="alignnone size-full wp-image-2216" title="rhubarb-salsa" src="http://quiltingthefarm.plaidsheep.ca/files/2011/06/rhubarb-salsa.jpg" alt="rhubarb salsa" width="412" height="309" /></a>

We have already polished off one jar with a bag of taco chips. It&#8217;s delicious. If you&#8217;re overrun with rhubarb and want to try something a little different,  give it a try.

Linking up to <a title="Farmgirl Friday Blog Hop" href="http://www.deborahjeansdandelionhouse.com/2011/06/farmgirl-friday-blog-hop-11.html" target="_blank">Farmgirl Friday Blog Hop</a>