---
id: 1838
title: Signs of Spring Around the Homestead
date: 2011-04-21T06:30:07+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=1838
permalink: /2011/04/21/signs-of-spring/
tinymce_signature_post_setting:
  - ""
categories:
  - Garden
---
Now it&#8217;s Spring, it&#8217;s fun to discover all sorts of things about our new homestead. As we only moved in two days before the first snows&#8230;and there was a lot of snow! we are now able to see the lay of the land, uncover secret spots, glimpse the small streams and observe the buds of new life.

Yesterday, I noticed some daffodils outside the front window. When I went to investigate, I found several clumps planted in a semi circle. They are already quite large. I can&#8217;t believe I didn&#8217;t see them before.

<a rel="attachment wp-att-1839" href="http://quiltingthefarm.vius.ca/2011/04/signs-of-spring/daffodils/"><img class="alignnone size-full wp-image-1839" title="daffodils" src="http://quiltingthefarm.plaidsheep.ca/files/2011/04/daffodils.jpg" alt="daffodils" width="412" height="309" /></a>  

The trees are all in bud. I don&#8217;t know what most of them are yet. We do have several apple trees, maples and a crab apple. The previous owner told us recently that he thinks there may be a peach tree (he wasn&#8217;t the owner who planted it), but it has never produced. Peach trees are &#8220;iffy&#8221; here in Nova Scotia, some produce only once every 5 years or so.

I found these little blue flowers peeking through the debris in the planters next to the house. If you know what they are please let me know. (I suppose they could be weeds).

<a rel="attachment wp-att-1840" href="http://quiltingthefarm.vius.ca/2011/04/signs-of-spring/blue-flowers/"><img class="alignnone size-full wp-image-1840" title="blue-flowers" src="http://quiltingthefarm.plaidsheep.ca/files/2011/04/blue-flowers.jpg" alt="little blue flowers" width="412" height="293" /></a>

After finding the flowers, I pulled out all the old dead hostas and these are what I found underneath. Perhaps they are new hostas. It does seem to be a waste to have shade tolerant/loving plants in a sunny south facing spot. I&#8217;ll be removing them anyway as this is where I&#8217;ll be having a kitchen garden.

<a rel="attachment wp-att-1842" href="http://quiltingthefarm.vius.ca/2011/04/signs-of-spring/bulbs/"><img class="alignnone size-full wp-image-1842" title="bulbs" src="http://quiltingthefarm.plaidsheep.ca/files/2011/04/bulbs.jpg" alt="unknown plants" width="412" height="309" /></a>

The signs of Spring are everywhere now. It won&#8217;t be long before I can get out in the garden and discover the delights of black fly season!

<p style="text-align: center">
  ******
</p>