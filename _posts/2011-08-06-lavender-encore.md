---
id: 2724
title: Lavender Encore
date: 2011-08-06T07:00:02+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2724
permalink: /2011/08/06/lavender-encore/
tinymce_signature_post_setting:
  - ""
categories:
  - Garden
---
In a previous post I wrote about wanting to grow <span style="color: #666699"><strong>lavender</strong></span>. I purchased seeds early in the spring, planted them in clear plastic tubs and placed them on a sunny windowsill. To my amazement the seeds grew and before long I had a few healthy looking seedlings. I re-potted those seedlings, and eventually, as soon as the weather was good I planted them outside. The next day I went to check on them and&#8230;.[they were gone](http://quiltingthefarm.vius.ca/2011/06/stolen/)! Something had been by in the night and dug all of them up :(.

I had planted many lavender seeds in that tub, and after leaving them (because I forgot) for many more weeks, some of the other seeds started to sprout. I planted them in March, but they didn&#8217;t pop up until July! That&#8217;s quite a while to wait and a wonderful surprise.  Anyway, I re-potted those seedlings and they seem even healthier than the first lot. They already smell divine.

<a rel="attachment wp-att-2726" href="http://quiltingthefarm.vius.ca/2011/08/lavender-encore/lavender-pots-2/"><img class="alignnone size-full wp-image-2726" title="lavender-pots" src="http://quiltingthefarm.plaidsheep.ca/files/2011/08/lavender-pots1.jpg" alt="" width="412" height="309" /></a>

I don&#8217;t know whether to risk putting them outside again, or to keep them in pots until they are much larger.

Here is a lavender plant I received for my birthday back at the end of June.

<a rel="attachment wp-att-2725" href="http://quiltingthefarm.vius.ca/2011/08/lavender-encore/birthday-lavender-2/"><img class="alignnone size-full wp-image-2725" title="birthday-lavender" src="http://quiltingthefarm.plaidsheep.ca/files/2011/08/birthday-lavender1.jpg" alt="" width="412" height="309" /></a>

It has grown a great deal since then and seems happy with the sandy soil along my driveway. I have pinched it back a couple of times to let it grow in more bushy, it seems to be working. I will take cuttings from this one over the weekend. I already have some rooting hormone powder, so I may as well see if I have any luck with it.

My future goal is to have a small field of lavender that I can use for culinary and cosmetic products. That future is still a long way off, but a few small seedlings are a good beginning.