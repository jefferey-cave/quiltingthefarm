---
id: 691
title: Tortillas and Refried Beans
date: 2011-02-17T09:15:55+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=691
permalink: /2011/02/17/tortillas-and-refried-beans/
image: /files/2011/02/tortilla_cooking.jpg
categories:
  - Kitchen
  - Recipes
tags:
  - beans
  - breads
  - food
  - frugal
  - recipes
---
Here on our old homestead we eat lots of beans. Bean soup, bean stew, bean patties and now&#8230;

<span id="Tortillas_and_Refried_Beans">

<h2>
  Tortillas and Refried Beans
</h2></span> 

<span id="The_Tortillas">

<h3>
  The Tortillas
</h3></span> 

  * <span id="cups_all-purpose_flour">
    
    <h4>
      2-½ cups all-purpose flour
    </h4></span> 

  * <span id="teaspoons_baking_powder">
    
    <h4>
      2-½ teaspoons baking powder
    </h4></span> 

  * <span id="teaspoon_salt">
    
    <h4>
      1 teaspoon salt
    </h4></span> 

  * <span id="cup_2_tbspvegetable_shortening">
    
    <h4>
      ½ cup +2 tbspvegetable shortening
    </h4></span> 

  * <span id="cup_hot_water">
    
    <h4>
      1 cup hot water
    </h4></span> 

  1. In a large bowl combine flour, baking powder and salt.
  2. Add shortening. Cut mixture until it resembles coarse crumbs (I use my hands).
  3. Pour in hot water and mix together. Lightly knead dough until it comes together in a dough, Cover with a tea towel and let dough to rest for about an hour.
  4. Roll into small balls, cover with a tea towel, and let rest for another 20 minutes or so.
  5. Heat a heavy frying pan over medium to high heat. Roll out a ball of dough until very, very <!--more-->thin. Put tortilla in pan and cook on each side for 20 to 30 seconds, They should start to slightly brown in spots.

  6. Remove from pan, and cover with a towel to keep warm. Continue until all dough balls are cooked.

<div id="attachment_694" style="width: 422px" class="wp-caption aligncenter">
  <a rel="attachment wp-att-694" href="http://quiltingthefarm.vius.ca/2011/02/tortillas-and-refried-beans/tortilla_balls/"><img class="size-full wp-image-694" title="tortilla_balls" src="http://quiltingthefarm.plaidsheep.ca/files/2011/02/tortilla_balls.jpg" alt="" width="412" height="309" /></a>
  
  <p class="wp-caption-text">
    Divide dough into golf ball sized balls
  </p>
</div>

<div class="mceTemp mceIEcenter">
  <dt class="wp-caption-dt">
    <div id="attachment_696" style="width: 422px" class="wp-caption aligncenter">
      <a rel="attachment wp-att-696" href="http://quiltingthefarm.vius.ca/2011/02/tortillas-and-refried-beans/tortilla_rolling/"><img class="size-full wp-image-696" title="tortilla_rolling" src="http://quiltingthefarm.plaidsheep.ca/files/2011/02/tortilla_rolling.jpg" alt="" width="412" height="309" /></a>
      
      <p class="wp-caption-text">
        Roll out your tortillas really thin
      </p>
    </div>
    
    <p>
      <a rel="attachment wp-att-696" href="http://quiltingthefarm.vius.ca/2011/02/tortillas-and-refried-beans/tortilla_rolling/"></a>
    </p>
  </dt>
</div>

 <a rel="attachment wp-att-694" href="http://quiltingthefarm.vius.ca/2011/02/tortillas-and-refried-beans/tortilla_balls/"></a><a rel="attachment wp-att-694" href="http://quiltingthefarm.vius.ca/2011/02/tortillas-and-refried-beans/tortilla_balls/"></a>

<div class="mceTemp mceIEcenter">
  <dt class="wp-caption-dt">
  </dt>
  
  <dd class="wp-caption-dd">
  </dd>
</div>

<div class="mceTemp mceIEcenter">
  <div id="attachment_695" style="width: 422px" class="wp-caption aligncenter">
    <a rel="attachment wp-att-695" href="http://quiltingthefarm.vius.ca/2011/02/tortillas-and-refried-beans/tortilla_cooking/"><img class="size-full wp-image-695" title="tortilla_cooking" src="http://quiltingthefarm.plaidsheep.ca/files/2011/02/tortilla_cooking.jpg" alt="" width="412" height="309" /></a>
    
    <p class="wp-caption-text">
      It's an alien!
    </p>
  </div>
</div>

Serve warm with refried beans

<span id="The_Beans">

<h3>
  The Beans
</h3></span> 

  * <span id="onion_finely_chopped">
    
    <h4>
      1 onion, finely chopped
    </h4></span> 

  * <span id="cup_dry_pinto_beans_rinsed">
    
    <h4>
      1 cup dry pinto beans, rinsed
    </h4></span> 

  * <span id="cups_dry_black_beans_rinsed">
    
    <h4>
      2 cups dry black beans, rinsed
    </h4></span> 

  * <span id="cloves_garlic_minced">
    
    <h4>
      4 cloves garlic, minced
    </h4></span> 

  * <span id="teaspoons_salt">
    
    <h4>
      3 teaspoons salt
    </h4></span> 

  * <span id="tablespoon_chili_powder">
    
    <h4>
      1 tablespoon chili powder
    </h4></span> 

  * <span id="teaspoons_fresh_ground_black_pepper">
    
    <h4>
      1 1/2 teaspoons fresh ground black pepper
    </h4></span> 

  * <span id="pinch_ground_cumin">
    
    <h4>
      pinch ground cumin
    </h4></span> 

  * <span id="cups_water">
    
    <h4>
      9 cups water
    </h4></span> 

  1. Put the chopped onion,  beans, garlic, salt, pepper, chili powder and cumin into a slow cooker. Pour in the water and stir.
  2. Cook on High for 2 hours, turn to low and cook for 6 more hours.
  3. Once beans have finished cooking, remove the beans with a slotted spoon and place in a bowl large enough to mash them. Mash with a potato masher (or whatever you have) adding a little of the left behind liquid as needed for a smooth (ish) constistency.

 

<div id="attachment_697" style="width: 422px" class="wp-caption aligncenter">
  <a rel="attachment wp-att-697" href="http://quiltingthefarm.vius.ca/2011/02/tortillas-and-refried-beans/refried_beans/"><img class="size-full wp-image-697" title="refried_beans" src="http://quiltingthefarm.plaidsheep.ca/files/2011/02/refried_beans.jpg" alt="" width="412" height="309" /></a>
  
  <p class="wp-caption-text">
    Beans in the crockpot
  </p>
</div>

<div id="attachment_698" style="width: 422px" class="wp-caption aligncenter">
  <a rel="attachment wp-att-698" href="http://quiltingthefarm.vius.ca/2011/02/tortillas-and-refried-beans/mashing_beans/"><img class="size-full wp-image-698" title="mashing_beans" src="http://quiltingthefarm.plaidsheep.ca/files/2011/02/mashing_beans.jpg" alt="" width="412" height="309" /></a>
  
  <p class="wp-caption-text">
    Nasty looking but oh so tasty!
  </p>
</div>

<span id="Serve">

<h3>
  Serve
</h3></span> 

Spread as much refried bean mixture as you like on a warm tortilla. Add grated cheese, chopped tomato  and a spoon of salsa. Roll the tortilla up (if you still can!) and enjoy.

<div id="attachment_693" style="width: 422px" class="wp-caption aligncenter">
  <a rel="attachment wp-att-693" href="http://quiltingthefarm.vius.ca/2011/02/tortillas-and-refried-beans/tortillas/"><img class="size-full wp-image-693" title="tortillas" src="http://quiltingthefarm.plaidsheep.ca/files/2011/02/tortillas.jpg" alt="" width="412" height="309" /></a>
  
  <p class="wp-caption-text">
    Mmmmm
  </p>
</div>