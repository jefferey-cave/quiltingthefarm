---
id: 400
title: Every Homestead Needs a Quilt
date: 2011-03-14T16:28:56+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=400
permalink: /2011/03/14/every-homestead-needs-a-quilt/
image: /files/2011/01/quiltblocksjpg.jpg
categories:
  - Crafts
tags:
  - crafts
  - decorating
  - hobbies
  - home
  - homemaking
  - quilting
  - quilts
  - sewing
---
Every homestead needs a quilt.
  
[<img class="alignnone size-thumbnail wp-image-287" title="kidquilt2" src="http://quiltingthefarm.plaidsheep.ca/files/2011/01/kidquilt2-150x150.jpg" alt="" width="150" height="150" />](http://quiltingthefarm.plaidsheep.ca/files/2011/01/kidquilt2.jpg) [<img class="alignnone size-thumbnail wp-image-291" title="quiltblocksjpg" src="http://quiltingthefarm.plaidsheep.ca/files/2011/01/quiltblocksjpg-150x150.jpg" alt="" width="150" height="150" />](http://quiltingthefarm.plaidsheep.ca/files/2011/01/quiltblocksjpg.jpg)
  
 [<img class="alignnone size-thumbnail wp-image-290" title="quilt" src="http://quiltingthefarm.plaidsheep.ca/files/2011/01/quilt-150x150.jpg" alt="" width="150" height="150" />](http://quiltingthefarm.plaidsheep.ca/files/2011/01/quilt.jpg) [<img class="alignnone size-thumbnail wp-image-401" title="christmas_quilt" src="http://quiltingthefarm.plaidsheep.ca/files/2011/01/christmas_quilt-150x150.jpg" alt="" width="150" height="150" />](http://quiltingthefarm.plaidsheep.ca/files/2011/01/christmas_quilt.jpg)

What&#8217;s a homestead without quilts? especially unfinished ones 🙂