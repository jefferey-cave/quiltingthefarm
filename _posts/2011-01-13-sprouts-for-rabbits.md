---
id: 40
title: Sprouts for Rabbits
date: 2011-01-13T00:03:33+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=40
permalink: /2011/01/13/sprouts-for-rabbits/
categories:
  - Rural Living
tags:
  - food
  - gardening
  - grains
  - pets
  - rabbits
  - sprouting
  - sprouts
  - winter
---
[<img class="alignleft size-medium wp-image-109" title="stewie" src="http://quiltingthefarm.plaidsheep.ca/files/2011/01/stewie-300x225.jpg" alt="" width="300" height="225" />](http://quiltingthefarm.plaidsheep.ca/files/2011/01/stewie.jpg)Our rabbit, Stew, loves greens. This is all fine and dandy in the summer when dandelions are a plenty but what to do when there’s 2 feet of snow on the ground?

I have been buying greens from the store but at $3 a pop for green or red leaf lettuce, curly endive, collard greens or escarole, it gets very expensive, very quickly. So, I’m thinking _sprouts_.

Sprouts are a great food for humans, so why not rabbits? They’re green, they’re yummy and they’re easy to grow.<!--more-->

Normally I sprout in jars, but this won’t work for Stew as he only eats the tops and not the seeds. I need them to grow upright. I took a disposable plastic tub, punched holes in the bottom for water drainage and put a layer of paper towel in the bottom. I sprinkled the seeds on top and added water…oops, we have a problem; the tiny seeds started floating to the edges…back to the drawing board. I took the lid of the tub and punched holes all over the top so that the water could be poured over the lid and drip slowly into the bottom tub. It worked! But was it a success?

Stew is trying them, he’s not sure if he likes them yet. Picky Rabbit.