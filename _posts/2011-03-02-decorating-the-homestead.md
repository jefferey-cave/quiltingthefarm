---
id: 909
title: Decorating the Homestead
date: 2011-03-02T08:44:29+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=909
permalink: /2011/03/02/decorating-the-homestead/
categories:
  - Rural Living
tags:
  - country life
  - decorating
  - DIY
  - home
  - house
---
Yesterday I started to prime the walls of our dining area. If you missed this <a href="http://quiltingthefarm.vius.ca/2011/02/too-much-of-a-good-thing/" target="_blank">post</a> you won&#8217;t know that almost our entire house is paneled in pine tongue and groove or bead board, and I mean walls and ceilings (interestingly, the floors are mostly laminate). We don&#8217;t want to pull off the paneling yet as we aren&#8217;t quite ready to find the interesting surprises lurking beneath, so for now we will brighten the place up, bit by bit.

I purchased some sort of latex primer that supposed to be great for sealing stains and sticking to glossy surfaces. I don&#8217;t want to go with oil primer unless I really have no choice. So far I have only primed part of one wall, but I can already see the knots in the pine bleeding through. I hope a second coat stops it.

It&#8217;s strange the things you notice when you&#8217;re up close and personal with your walls. First, for some inexplicable reason there are no real baseboards, just flimsy 1/4 round molding (this makes it difficult to keep paint of the floors). Second, the door framing has been done with any leftover, mismatched boards and even these haven&#8217;t been mitred in some places. We&#8217;re talking &#8220;handyman special&#8221; here. Thirdly, mismatched paneling has been used to fill in any gaps, and is barely attached to the walls in places. And to top it all, the paneling is crooked. The &#8220;renovator&#8221; obviously had never heard of a level or a plumb line.

<div id="attachment_911" style="width: 422px" class="wp-caption alignleft">
  <a rel="attachment wp-att-911" href="http://quiltingthefarm.vius.ca/2011/03/decorating-the-homestead/dining4/"><img class="size-full wp-image-911 " title="dining4" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/dining4.jpg" alt="" width="412" height="309" /></a>
  
  <p class="wp-caption-text">
    Vertical and diagonal paneling. Why? Check out the door frame too.
  </p>
</div>

<div id="attachment_914" style="width: 422px" class="wp-caption alignleft">
  <a rel="attachment wp-att-914" href="http://quiltingthefarm.vius.ca/2011/03/decorating-the-homestead/dining3/"><img class="size-full wp-image-914" title="dining3" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/dining3.jpg" alt="Baseboards" width="412" height="309" /></a>
  
  <p class="wp-caption-text">
    Flimsy 1/4 round baseboards
  </p>
</div>

<div class="mceTemp">
  <dl id="attachment_913" class="wp-caption alignleft" style="width: 422px">
    <dt class="wp-caption-dt">
      <a rel="attachment wp-att-913" href="http://quiltingthefarm.vius.ca/2011/03/decorating-the-homestead/dining2/"><img class="size-full wp-image-913" title="dining2" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/dining2.jpg" alt="" width="412" height="309" /></a>
    </dt>
    
    <dd class="wp-caption-dd">
      pieces of board masquerading as door frame molding
    </dd>
  </dl>
  
  <p>
    Ah, the joys of discovering the &#8220;homegrown&#8221; reno
  </p>
</div>

 

<div class="mceTemp">
   
</div>

<div class="mceTemp" style="text-align: center">
  ******
</div>