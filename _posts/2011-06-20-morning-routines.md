---
id: 2291
title: Morning Routines
date: 2011-06-20T06:15:22+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2291
permalink: /2011/06/20/morning-routines/
categories:
  - Rural Living
---
It&#8217;s always light when I get up now. 6:30 in the morning is a isn&#8217;t really that early on a farm, but for me it&#8217;s the perfect time to drag my pyjama clad butt out the door in the vain hope of finding a few stray dandelions or clover for the rabbit&#8217;s breakfast. <span>Hubby has been doing his due diligence with the lawn tractor, leaving few obvious signs of rabbit fodder. It takes me a while to find a handful of</span> green leaves and with the morning dew still glistening on them I make my way back inside. I drop of the greens and pick up the tub of wild bird seed.

<span>I have been feeding the birds (and the chipmunks) every morning for the last 6 weeks. They have become used to the free breakfast fly-through and sit in the trees waiting and squawking. </span> They still see me as a terrible threat, yet happily eat anything I give them. The chipmunk, unfortunately doesn&#8217;t see me as the ultimate evil. He eyes me carefully as he stuffs his chubby little cheeks, but he&#8217;s not going anywhere until he&#8217;s good and ready. I leave the birds to their feast and move on to the chickens.

<span>I wander around the side of the coop to the &#8220;people&#8221; door. I can hear their muffled sounds of excitement&#8230;&#8221;she&#8217;s coming, she&#8217;s coming&#8221;, they say in their own fowl language. As I enter the coop, all the birds are piled up in the doorway of the pop</span> hole. They can&#8217;t wait another minute to get outside. I put some pellets in the feeders and check the waterers.  I don&#8217;t have to collect eggs in the morning as our girls are tardy layers, usually waiting until close to noon to do us the honour. Sometimes I have a treat for them, but in the time it takes the others to process that I have tossed in a snack or two, Aubree has zipped around the coop and eaten every last one. I expect Aubree&#8217;s a pudgy chicken under all those fluffy feathers. By this time Hubby, who stayed behind to put the coffee on,  is in the chicken yard opening their door for the day. The door opens and there&#8217;s stampede. The chicken in the front never gets out first. Two chickens try to squeeze through at the same time. Neither goes anywhere until one backs down. If it&#8217;s raining the first ones want back in before the last ones get out. This is chicken TV at its finest.

With the chickens taken care of it&#8217;s back to the house, while quietly mulling over how grateful I am that I don&#8217;t have a cow or a goat to milk.  

<div class="mceTemp" style="text-align: center">
  <dl id="attachment_2292" class="wp-caption alignnone" style="width: 422px">
    <dt class="wp-caption-dt">
      <img class="size-full wp-image-2292" title="aubree1" src="http://quiltingthefarm.plaidsheep.ca/files/2011/06/aubree1.jpg" alt="Aubree the chicken" width="412" height="277" />
    </dt>
    
    <dd class="wp-caption-dd">
      Aubree&#8230;one pudgy chicken?
    </dd>
  </dl>
</div>

<div class="mceTemp" style="text-align: center">
  ******
</div>