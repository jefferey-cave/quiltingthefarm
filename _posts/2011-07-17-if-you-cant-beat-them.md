---
id: 2574
title: 'If You Can&#8217;t Beat them'
date: 2011-07-17T07:01:22+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2574
permalink: /2011/07/17/if-you-cant-beat-them/
tinymce_signature_post_setting:
  - ""
categories:
  - Rural Living
---
If you can&#8217;t beat them&#8230;give up trying!

After trying almost everything to stop our 2 broody hens, we have thrown in the towel&#8230;well almost.

Aubree and Annabelle have been broody for a while. We have tried all the tricks to &#8220;cure&#8221; them, but to no avail, so today we took drastic measures. We placed several eggs under Aubree and removed Annabelle  to a pen without bedding, but with lots of airflow. We are hoping to stop Annabelle&#8217;s broodiness. We decided to let only one broody hen hatch the eggs as the 2 hens won&#8217;t sit in separate boxes, but on top of each other, squished in a corner!

[<img class="size-full wp-image-2575 alignnone" title="annabelle-in-pen" src="http://quiltingthefarm.plaidsheep.ca/files/2011/07/annabelle-in-pen.jpg" alt="" width="412" height="309" />](http://quiltingthefarm.plaidsheep.ca/files/2011/07/annabelle-in-pen.jpg)

Poor Annabelle, she looks so forlorn without her buddy. It&#8217;s hard to see her, but she&#8217;s in there.

[<img class="alignnone size-full wp-image-2576" title="aubree-broody" src="http://quiltingthefarm.plaidsheep.ca/files/2011/07/aubree-broody.jpg" alt="" width="412" height="309" />](http://quiltingthefarm.plaidsheep.ca/files/2011/07/aubree-broody.jpg)

Aubree now has her eggs and is nesting in an old drawer (she won&#8217;t use the nesting boxes). Now to wait the 21 days to see if any of the eggs hatch. Perhaps we&#8217;ll have some those little balls of fluff we so didn&#8217;t want this year&#8230;Oh well.