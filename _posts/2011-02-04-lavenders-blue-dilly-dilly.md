---
id: 485
title: Lavender’s Blue Dilly Dilly
date: 2011-02-04T09:30:18+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=485
permalink: /2011/02/04/lavenders-blue-dilly-dilly/
image: /files/2011/02/lavender.jpg
categories:
  - Garden
tags:
  - flowers
  - garden
  - gardening
---
[<img class="alignleft size-medium wp-image-486" title="lavender" src="http://quiltingthefarm.plaidsheep.ca/files/2011/02/lavender-300x200.jpg" alt="" width="178" height="144" />](http://quiltingthefarm.plaidsheep.ca/files/2011/02/lavender.jpg)Lavender&#8217;s blue, dilly dilly,
  
lavender&#8217;s green,
  
When I am king, dilly, dilly,
  
you shall be queen.
  
Who told you so, dilly, dilly,
  
who told you so?
  
&#8216;Twas my own heart, dilly, dilly,
  
that told me so. 

This year I am going to plant a (small) field of lavender&#8230;well&#8230;okay, more like a small patch :). Lavender seems to grow reasonably well in Nova Scotia and it likes slopes and tolerates poor but well drained soil so it may be perfect for our place.

Bees also love lavender and they are part of the long term strategy for our homestead.<!--more-->

I have been researching the types of lavender that grow best in our climate. So far I have come up with, **Munstead** and **Hidcote** and these interesting ones, **Lavender Lady**, **Lavender Vera** and **Potpourri White Lavender**. All these are hardy to zone 5 (my zone here in Nova Scotia).

If you&#8217;re interested in Lavender and its many uses, visit <a href="http://www.lavendercanada.com/" target="_blank">Seafoam Lavender Farm</a> (we visited this farm when we first arrived in Nova Scotia ). They have everything you could possibly want in the way of lavender.

Also, in about 3 years I should have lavender products for sale. _I’ll keep you posted!_