---
id: 3323
title: Last of the Summer Veggies
date: 2011-10-05T07:04:16+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=3323
permalink: /2011/10/05/last-of-the-summer-veggies/
tinymce_signature_post_setting:
  - 'yes'
categories:
  - Garden
---
The season has definitely turned Fall. A trip to Digby yesterday, gave us our first real glimpse of the forests just starting to show their Fall finery.

Fall has finally hit my garden, but luckily there has been no frost yet. It is still producing, but the tomatoes are pretty much spent. I harvested what was left of the ripe tomatoes and the green ones.

<a rel="attachment wp-att-3324" href="http://quiltingthefarm.vius.ca/2011/10/last-of-the-summer-veggies/green-tomatoes2/"><img class="alignnone size-full wp-image-3324" title="green-tomatoes2" src="http://quiltingthefarm.plaidsheep.ca/files/2011/10/green-tomatoes2.jpg" alt="" width="449" height="337" /></a>

The green ones I&#8217;ll use for fried green tomatoes (which I&#8217;ve always wanted to try) as there isn&#8217;t enough to process into chutney or mincemeat. I suppose having only about a dozen small unripe tomatoes is pretty much a success story, the ripe tomatoes still take up the bulk of room in the fridge and I&#8217;ve been processing them as fast as I can. I will make one more large batch of salsa, this time using my own hot peppers, and that should take care of them.

<a rel="attachment wp-att-3327" href="http://quiltingthefarm.vius.ca/2011/10/last-of-the-summer-veggies/hotpeppers3/"><img class="alignnone size-full wp-image-3327" title="hotpeppers3" src="http://quiltingthefarm.plaidsheep.ca/files/2011/10/hotpeppers3.jpg" alt="" width="449" height="337" /></a><a rel="attachment wp-att-3324" href="http://quiltingthefarm.vius.ca/2011/10/last-of-the-summer-veggies/green-tomatoes2/"></a>

The squash are still producing (but more slowly), I have 3 cabbages that need harvesting (does anyone have any great recipes for cabbage?), the carrots are now quite large, and the rutabagas are huge!

There were a few disappointments&#8230;

<a rel="attachment wp-att-3325" href="http://quiltingthefarm.vius.ca/2011/10/last-of-the-summer-veggies/parsnips/"><img class="alignnone size-full wp-image-3325" title="parsnips" src="http://quiltingthefarm.plaidsheep.ca/files/2011/10/parsnips.jpg" alt="" width="449" height="338" /></a>

<a rel="attachment wp-att-3326" href="http://quiltingthefarm.vius.ca/2011/10/last-of-the-summer-veggies/parsnips-and-carrot/"><img class="alignnone size-full wp-image-3326" title="parsnips-and-carrot" src="http://quiltingthefarm.plaidsheep.ca/files/2011/10/parsnips-and-carrot.jpg" alt="" width="448" height="330" /></a>

Poor little parsnips. So tiny. They suffered early from being planted next to fast growing, humongous rutabaga leaves. I&#8217;m not sure it&#8217;s even worth putting them in a stew.

I now have to get out to the garden and pull all the dead and dying stuff. I noticed lots of slugs out there, so it&#8217;s not going to be pleasant work. I hate slugs.