---
id: 1752
title: Lumber Delivery for the New Coop
date: 2011-04-15T06:29:17+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=1752
permalink: /2011/04/15/lumber-delivery-for-the-new-coop/
categories:
  - Rural Living
---
We are having the lumber for our **brand new chicken coop** being delivered today.

Last week Hubby and I went to our local hardware store and noticed 2&#215;4 studs on sale. Needing some to frame an inner coop in the shed (among other projects) We decided to buy as many as we could fit in the car. Hubby asked the cashier if he could load up the car first to see how many will fit and then pay for them. Now Hubby does tend to believe his car is a small pick up truck, and it usually works out well for him, but this time his Hyundai Accent, although quite roomy in the hatchback,  just wasn&#8217;t suitable for 8 foot boards! Disappointed, Hubby opted for delivery instead, but not before he managed to cram and wedge 2 pieces between the front seats leaving little room for the passenger &#8211; me &#8211; to get in and sit comfortably.  I&#8217;m glad we weren&#8217;t far from home!

As the wood was going to be delivered we ended up buying 70 2&#215;4&#8217;s (a purely random number), some OSB and chicken wire&#8230;that should be good for a few upcoming homestead projects possibly a small greenhouse, but that&#8217;s not in the works until next year.

Hubby has the coop all sketched out now, so we know exactly what we&#8217;re doing&#8230;famous last words LOL.