---
id: 2062
title: Profit and (Hopefully Not) Loss
date: 2011-05-26T06:35:34+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2062
permalink: /2011/05/26/profit-and-loss/
categories:
  - Rural Living
---
The weather this week has not cooperated for the outdoor life, so instead today has become finances day. We have let our finances go unchecked for the last little while, but with the imminent arrival of our first livestock (chickens), we really need to be able to account for all our income and expenses.

Starting a small farm requires a bit of a leap of faith, but without a plan moving forward, it is likely to be a chaotic money pit. We need our farm to sustain us, so we will have to investigate and research everything before we commit.

After chatting with several people in the last week, we have come to the conclusion that raising pigs is not a viable option financially for us. According to these sources finishing a pig costs more than it’s worth at market rate (at this time). Now, this doesn’t mean we can’t raise a pig for personal consumption, in fact that does seem financially viable.

Our chickens will certainly be an expense. In fact, our chickens will be expensive in the short term. We invested quite a bit of time and resources into building the chicken coop and we’re not finished yet. Will our hens be able to justify the cost? Maybe…likely not. We have decided to peg this expense as ‘research and development’.

How do we keep ourselves from taking a loss? Better yet how do we make a profit off of our farm?

Unfortunately &#8216;profit&#8217; has become a ‘dirty’ word in this day and age. But all profit really means is that you have something left over after expenses. If you’re not making a profit, you’re taking a loss. It may be a small loss, but that doesn’t mean that it won’t bleed you dry over time. If you do any small farm activity as a business, then you owe it to yourself _and_ your customer to make a profit, after all it’s the only way you’ll stay in business. Your customer has already invested time and effort into choosing you, he’d probably like to know that you’ll be there next time he needs something. With profit you can afford goodwill. That loyal customer deserves a little something extra once in a while. Profit affords you this luxury.

What can we do to make a profit? Now that’s the million dollar (or perhaps five dollar) question isn’t it? We have many thoughts and ideas, some have already been scrapped and some are still waiting in the wings.

So for now we crunch the numbers and start with a small (R&D) flock of chickens. This farm maybe a dream come true, but it’s not a far step in the opposite direction, to financial nightmare territory.