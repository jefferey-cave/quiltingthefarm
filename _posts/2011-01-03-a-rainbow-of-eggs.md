---
id: 17
title: A Rainbow (of Eggs)
date: 2011-01-03T23:58:49+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=17
permalink: /2011/01/03/a-rainbow-of-eggs/
categories:
  - Rural Living
tags:
  - breakfasts
  - food
  - friends
  - meals
---
[<img class="alignleft fltlft size-medium wp-image-100" title="eggs" src="http://quiltingthefarm.plaidsheep.ca/files/2011/01/eggs-300x205.jpg" alt="" width="300" height="205" />](http://quiltingthefarm.plaidsheep.ca/files/2011/01/eggs.jpg)Red and yellow and pink and blue&#8230;okay, not quite a rainbow, but some of them were blue!

We have been here just over a week and we have been given our first farm fresh, free range, eggs. No, they aren’t from our chickens; our chickens are still just a dream until spring, these came from our friends <a href="http://www.downshiftme.com/" target="_blank">Matt and AJ</a>. Apparently their chickens started laying again (unexpectedly) and now they have more eggs than they know what to do with.

<!--more-->Two dozen eggs now sit in my fridge, I almost don’t want to eat them; they look so pretty (my favourite are the blue ones from the Ameraucanas). Tonight I made a frittata, a puffy omelete of sorts, with eggs (of course), potatoes, sweet yellow peppers, mushrooms, ham, onions and garlic. Yummy. Tomorrow will be french toast for breakfast.

Now I need my own homestead chickens!