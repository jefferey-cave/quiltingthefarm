---
id: 2902
title: Overcoming Adversity
date: 2011-08-22T07:03:54+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2902
permalink: /2011/08/22/overcoming-adversity/
aktt_notify_twitter:
  - 'yes'
tinymce_signature_post_setting:
  - ""
aktt_tweeted:
  - "1"
categories:
  - Rural Living
---
This is the story of a chicken. This the story of Agatha.

Agatha came to us with our first batch of chickens; the Cochins. Back then she didn&#8217;t have a name, but she was part of the &#8220;A&#8217; Team and became Agatha.

When we first saw Agatha she was a haggered looking creature. She was small and thinner than the others and was missing most of her head feathers. Unfortunately the other hens were always picking on her and driving her away from the food dishes. Agatha was bottom of the pecking order.  .

Maybe Agatha needed some space to get away. We hoped Agatha would thrive in the outdoors, and like the others she was pretty gungho about getting out and eating the grass and weeds.  Being new chicken owners, we had been watching them closely, and I started seeing an odd behaviour in Agatha. She had a strange lump in her crop area and was doing weird actions with her neck. Agatha had developed sour crop.

Agatha was isolated from the others while we tried to make her better. Three times a day we massaged her crop, turned her upside down to drain it and gave her oil to help things along. She was apoor little thing and we expected to lose her. After about a week of seeing no improvement we came to the decision to put her back with the flock and let her take her chances. We waited another week in hopes that she would at least put on a little weight. Agatha seemed to be ok on her own.

We put Agatha back with her flock. The others picked on her again, but no worse than before. I would take treats like strawberries and hold them out in my hand. All of them would keep a safe distance, except Agatha. Agatha was the only one who would walk right over and take them from me. Thanks to all that handling while she was sick Agatha gained a sense of confidence around me that the others didn&#8217;t have, this mean&#8217;t she always got some of the good treats. For much of the time life went on as usual. Then came the &#8220;Reds&#8221;.

The Reds lived apart from the Cochins for a while. They stared at each other through the wire, but the Cochins kept their distance. The Reds were more aggressive creatures; friendly, a bit flighty, but without that docile streak the Cochins possess. We integrated the Cochins and the Reds, and immediately the Reds became top dogs. Poor little Agatha was now bottom of a much larger pecking order, so many more beaks to avoid.

The strangest thing has happened. Not only did Agatha survive the onslaught she actually thrived. She is still bottom of the pecking order, but the Cochins now take care of her. Aubree calls her over and tries to feed her. They all hang out together now. It seems that the Cochins have realised that there is strength in numbers and Agatha is one of their &#8220;numbers&#8221; She has allies at last. Agatha still gets picked on, but she&#8217;s not scared to venture into the fray. She respects the Reds, but is the only one that isn&#8217;t terrified of them. She knows that when I put my hand out with food, she&#8217;ll be the first, and only one to take it from me. She is growing chubbier and fluffier everyday and her comb is a wonderful bright red. You can almost see her smiling.

<div id="attachment_2907" style="width: 422px" class="wp-caption alignnone">
  <a rel="attachment wp-att-2907" href="http://quiltingthefarm.vius.ca/2011/08/overcoming-adversity/agatha-improved/"><img class="size-full wp-image-2907" title="agatha-improved" src="http://quiltingthefarm.plaidsheep.ca/files/2011/08/agatha-improved.jpg" alt="" width="412" height="309" /></a>
  
  <p class="wp-caption-text">
    Not camera shy!
  </p>
</div>

<div id="attachment_2908" style="width: 422px" class="wp-caption alignnone">
  <a rel="attachment wp-att-2908" href="http://quiltingthefarm.vius.ca/2011/08/overcoming-adversity/agatha-improved-2/"><img class="size-full wp-image-2908" title="agatha-improved-2" src="http://quiltingthefarm.plaidsheep.ca/files/2011/08/agatha-improved-2.jpg" alt="" width="412" height="305" /></a>
  
  <p class="wp-caption-text">
    Looking good
  </p>
</div>

<div id="attachment_2909" style="width: 422px" class="wp-caption alignnone">
  <a rel="attachment wp-att-2909" href="http://quiltingthefarm.vius.ca/2011/08/overcoming-adversity/intrepid-cochins/"><img class="size-full wp-image-2909" title="intrepid-cochins" src="http://quiltingthefarm.plaidsheep.ca/files/2011/08/intrepid-cochins.jpg" alt="" width="412" height="305" /></a>
  
  <p class="wp-caption-text">
    One of the flock now (Agatha in middle)
  </p>
</div>

 Linking to <a href="http://homesteadrevival.blogspot.com/2011/08/barn-hop-25.html" target="_blank">Homestead Barn Hop</a>. Check out the other blogs and have fun.

<a rel="attachment wp-att-1317" href="http://quiltingthefarm.vius.ca/2011/03/starting-seeds/barn-hop/"><img class="alignnone size-full wp-image-1317" title="Barn-Hop" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/Barn-Hop.jpg" alt="" width="200" height="200" /></a>