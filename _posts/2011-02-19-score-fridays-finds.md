---
id: 753
title: 'Score! Friday&#8217;s Finds'
date: 2011-02-19T09:32:22+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=753
permalink: /2011/02/19/score-fridays-finds/
image: /files/2011/02/pressure_canner.jpg
categories:
  - Rural Living
tags:
  - appliances
  - canning
  - DIY
  - food
  - kitchen
---
Yesterday, our local hardware store had a &#8216;blow-out&#8217; sale at the fire hall. There were lots of odds and ends to rummage through, and I found a wooden headboard and footboard for $15 that I&#8217;ll make into a bench like this (someday).

<p style="text-align: center">
  <div id="attachment_757" style="width: 250px" class="wp-caption aligncenter">
    <a href="http://firstadream.blogspot.com/2010/11/headboard-bench-gets-makeover.html"><img class="size-medium wp-image-757  " title="5000_Bench1" src="http://quiltingthefarm.plaidsheep.ca/files/2011/02/5000_Bench1-300x300.jpg" alt="Headboard bench" width="240" height="240" /></a>
    
    <p class="wp-caption-text">
      Headboard bench (courtesy of firstadream.blogspot.com)
    </p>
  </div>
  
  <p>
    <em><strong>and this is for $25&#8230;</strong></em>
  </p>
  
  <div class="mceTemp mceIEcenter" style="text-align: left">
    <dl id="attachment_754" class="wp-caption aligncenter" style="text-align: left;width: 310px">
      <dt class="wp-caption-dt">
        <a href="http://www.amazon.com/gp/product/B0002803O8?ie=UTF8&tag=cw-pressure-cooker-1000-20&linkCode=as2&camp=1789&creative=390957&creativeASIN=B0002803O8"><img class="size-full wp-image-754" title="pressure_canner" src="http://quiltingthefarm.plaidsheep.ca/files/2011/02/pressure_canner.jpg" alt="All American Pressure Canner" width="300" height="300" /></a>
      </dt>
      
      <dd class="wp-caption-dd">
        All American Pressure Canner
      </dd>
    </dl>
  </div>
  
  <span id="Yes_25_for_a_200_pressure_canner">
  
  <h3>
    Yes $25 for a $200 pressure canner! 
  </h3></span> 
  
  <p>
    It&#8217;s only a small canner, holding 7 pint jars or 4 quart jars, but that doesn&#8217;t matter too much, there&#8217;s only the 2 of us here. 
  </p>
  
  <p>
    This summer, if my garden does well, I&#8217;ll be able to can my own veggies for next winter. I can&#8217;t tell you how excited I am!
  </p>