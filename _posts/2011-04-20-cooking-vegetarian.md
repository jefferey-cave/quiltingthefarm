---
id: 1766
title: Cooking Vegetarian
date: 2011-04-20T06:30:20+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=1766
permalink: /2011/04/20/cooking-vegetarian/
categories:
  - Kitchen
---
I have mentioned before that my family are coming to visit pretty soon. Well my daughter, her hubby and their brand new little guy are arriving this weekend and my Mom and Dad are coming from England next week. _**There will be a house full!**_

I have been baking and stocking the freezer and coming up with different main meal items. All this is easy except for the fact that my Mom is vegetarian. I do have a few vegetarian meals normally in my menu, but I really want to try and feed the meat eaters &#8220;_meat_&#8221; and my Mom a &#8220;_non-meat_&#8221; version of the same meal. I&#8217;m having difficulties with this.

_**Some of my ideas are&#8230;**_

making big batches of lentil veggie or bean soups, giving appetizer size to the meat eater and meal sized to my Mom.

Making individual pizzas with toppings of your choice

<a title="Mexi bean burgers" href="http://quiltingthefarm.vius.ca/2011/04/mexi-bean-burgers/" target="_self">Bean burgers</a> (I don&#8217;t think anyone will miss the meat in these)

Fish cakes (Mom does eat fish and seafood)

Bean chili (again, I don&#8217;t think the lack of meat will be noticed)

Salmon and chick pea patties

Those are my ideas so far. I would love any advice you might be able to offer. If you have a recipe you would like to share, I&#8217;m all ears!

<p style="text-align: center">
  ******
</p>