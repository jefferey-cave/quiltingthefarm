---
id: 359
title: Homesteading Goals for 2011
date: 2011-02-01T15:47:59+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=359
permalink: /2011/02/01/homesteading-goals-for-2011/
image: /files/2011/01/pond.jpg
categories:
  - Rural Living
---
**These are our homesteading goals for 2011**

Build chicken coop

Have our first chickens

Build at least 2 8&#215;4 raised beds

Plant a garden

Prepare the pond for fish

[<img class="alignnone size-thumbnail wp-image-385" title="pond" src="http://quiltingthefarm.plaidsheep.ca/files/2011/01/pond-150x150.jpg" alt="" width="150" height="150" />](http://quiltingthefarm.plaidsheep.ca/files/2011/01/pond.jpg)

Can enough applesauce for a year

Make jam (blackberry, apple) for one year

Learn to make yogurt and cheese

Paint living room, study and kitchen walls

Repair and Paint sideboard

Repair and paint dresser

Plant some lavender (start a small field)

Start ammending our soil