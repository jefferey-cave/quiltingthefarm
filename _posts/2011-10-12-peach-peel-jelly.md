---
id: 3375
title: Peach Peel Jelly
date: 2011-10-12T07:03:42+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=3375
permalink: /2011/10/12/peach-peel-jelly/
tinymce_signature_post_setting:
  - 'yes'
categories:
  - Kitchen
---
Remember my peach tree? When I processed all those peaches this summer, I saved all the peels. It seemed such a waste, so I boiled them up and made juice. The juice has been in my freezer ever since. Yesterday I thawed the juice and made peach jelly!

<a rel="attachment wp-att-3376" href="http://quiltingthefarm.vius.ca/2011/10/peach-peel-jelly/peach-jelly/"><img class="alignnone size-full wp-image-3376" title="peach-jelly" src="http://quiltingthefarm.plaidsheep.ca/files/2011/10/peach-jelly.jpg" alt="" width="337" height="449" /></a>

Isn&#8217;t it a wonderful colour? Two large jars and two small jars of delicious peach jelly, and I use the word &#8220;jelly&#8221; loosely, very loosely. Jelly has been tough for me to master. Last time I used a thermometer, and it went well. This recipe called for pectin and boil for one minute. After one minute it didn&#8217;t look anything like jell, so I boiled a little longer. You _can_ spread the &#8220;jelly&#8221; on toast, but it won&#8217;t stay on if you tip the bread. I say &#8220;Don&#8217;t tip the bread!&#8221;