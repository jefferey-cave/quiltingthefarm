---
id: 441
title: Dinner in Paradise
date: 2011-01-31T16:17:07+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=441
permalink: /2011/01/31/dinner-in-paradise/
categories:
  - Rural Living
tags:
  - food
  - friends
---
Saturday evening we were invited for dinner with our new friends in the Valley. Matt and Adrian moved from the UK about 5 years ago and now have a homestead in <a href="http://en.wikipedia.org/wiki/West_Paradise,_Nova_Scotia" target="_blank">West Paradise</a>.

Adrian is a great chef and treated us to handmade samosas with a spicy chutney and a delightful vegetarian curry. We were introduced to another couple (who also had moved from Alberta) who are doing the homestead thing. They moved here to Nova Scotia about 5 years ago and have been raising chickens for eggs and meat ever since. A short while ago they purchased a young Jersey cow. Like us they knew nothing about raising animals and it was wonderful to hear that they are doing so well.

We sat around chatting for hours and tried not to outstay our welcome. In typical Matt and Adrian style, we left with more than we arrived with; a dozen free range eggs and a jar of homemade orange grapefruit marmalade.