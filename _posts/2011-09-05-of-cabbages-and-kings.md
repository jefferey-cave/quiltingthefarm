---
id: 3065
title: Of Cabbages and Kings
date: 2011-09-05T07:00:06+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=3065
permalink: /2011/09/05/of-cabbages-and-kings/
tinymce_signature_post_setting:
  - 'yes'
categories:
  - Rural Living
---
**Of Cabbages&#8230;**

The cabbages did really well (Thank you for the seedlings, Farmer). I have several large heads of purple cabbage waiting to be harvested.

<a rel="attachment wp-att-3071" href="http://quiltingthefarm.vius.ca/2011/09/of-cabbages-and-kings/cabbage-2/"><img class="alignnone size-full wp-image-3071" title="cabbage" src="http://quiltingthefarm.plaidsheep.ca/files/2011/09/cabbage1.jpg" alt="" width="412" height="309" /></a>

What I&#8217;m really looking forward to is&#8230;

Pickled cabbage 🙂

As a kid, my mom used to make pickled cabbage. We would always have a jar or two around. I love pickled cabbage (and pickled onions), and no, it&#8217;s not sauerkraut, but a crunchy, spicy pickle with a kick.

Today I picked the biggest cabbage, shredded it up, and covered it in pickling salt. Tomorrow I will get to the rest of the process.

<a rel="attachment wp-att-3071" href="http://quiltingthefarm.vius.ca/2011/09/of-cabbages-and-kings/cabbage-2/"></a>

<a rel="attachment wp-att-3069" href="http://quiltingthefarm.vius.ca/2011/09/of-cabbages-and-kings/sliced-red-cabbage/"><img class="alignnone size-full wp-image-3069" title="sliced-red-cabbage" src="http://quiltingthefarm.plaidsheep.ca/files/2011/09/sliced-red-cabbage.jpg" alt="" width="412" height="309" /></a>

<a rel="attachment wp-att-3068" href="http://quiltingthefarm.vius.ca/2011/09/of-cabbages-and-kings/salted-cabbage/"><img class="alignnone size-full wp-image-3068" title="salted-cabbage" src="http://quiltingthefarm.plaidsheep.ca/files/2011/09/salted-cabbage.jpg" alt="" width="412" height="309" /></a>

I have never made pickled cabbage before, so wish me luck.

**Of Kings&#8230;**

Well a drink fit for a king anyway. Wine is a sophisticated drink nowadays, but did you know that the kings of old switched from _mead_ to wine when they were unable to afford honey.

Well, we already have a batch of mead on the go, so now it&#8217;s time to make some wine&#8230;blackberry wine to be precise.

Hubby and I went out this morning and picked two bowls of blackberries. We needed 18 cups, but ended up with 20.

<a rel="attachment wp-att-3067" href="http://quiltingthefarm.vius.ca/2011/09/of-cabbages-and-kings/bowls-blackberries/"><img class="alignnone size-full wp-image-3067" title="bowls-blackberries" src="http://quiltingthefarm.plaidsheep.ca/files/2011/09/bowls-blackberries.jpg" alt="" width="412" height="309" /></a>

On to the fun part, mushing and squishing the berries with your hands. A word of caution though, If you&#8217;ve just been picking fruit from painfully prickly, thorn ladened bushes, it may not be the best time to shove your hands in an acidic solution&#8230;ask me how I know :0

<a rel="attachment wp-att-3066" href="http://quiltingthefarm.vius.ca/2011/09/of-cabbages-and-kings/crushed-blackberries/"><img class="alignnone size-full wp-image-3066" title="crushed-blackberries" src="http://quiltingthefarm.plaidsheep.ca/files/2011/09/crushed-blackberries.jpg" alt="" width="412" height="309" /></a>

We have added the yeast, and have to leave it to ferment for about 4 days before we strain the juice out.

We have never made wine before, so wish us luck.