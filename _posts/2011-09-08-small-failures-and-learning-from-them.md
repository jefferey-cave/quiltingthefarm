---
id: 3084
title: Small Failures and Learning From Them
date: 2011-09-08T07:11:58+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=3084
permalink: /2011/09/08/small-failures-and-learning-from-them/
tinymce_signature_post_setting:
  - 'yes'
categories:
  - Garden
---
Yesterday I wrote about the successes from my garden. This time I want to share the failures, and what I have learned so far.

**Failures**

With all the pro&#8217;s there has to be some con&#8217;s along for the ride. Some of our seeds just didn&#8217;t produce at all, lettuce being the main one. I planted all the seeds in the package and the best I could do was 5 puny plants that bolted as soon as I looked at them. I have heard it wasn&#8217;t a good year for lettuce, it certainly wasn&#8217;t a good year for salad suppers! 

The peas I planted withered on the vine, I have no idea why. I think it may have been the rutabagas blocking out the sun to the soil, but it could have been a number of different things. The beans also took a bit of a hit. The weather??? perhaps. I did get a small harvest, but nothing to write home about.

The kale, like the lettuces has done poorly. Although it is still going, the leaves have been small and thin, and barely worth picking. Stewie, our rabbit has enjoyed the few that I&#8217;ve picked. Kale is one of his favourite foods.

**Things I&#8217;ve learned**

I was lazy and unprepared and didn&#8217;t mulch when I really knew I should have. I got in the mindset of &#8220;I&#8217;ll do it tomorrow&#8221;, but of course tomorrow came and went, and it didn&#8217;t get done. It wasn&#8217;t until the blossom end rot took hold of my tomatoes, that I took note. I am thankful that I wasn&#8217;t too late. I know now that next year, I will have to get out and mulch as soon as the ground is nice and warm. I won&#8217;t risk another tomato crop (famous last words, I know).

The veggies that needed a trellis went too long without one and I struggled to make them go where I wanted them to go. Next year I will make sure the trellis is ready to go as soon as the seeds are planted.

If you were to ask me the one thing I take away for this year it would be  &#8216;Everything needed way more room than we gave it&#8217; . I guess, if you are planting long rows, the spacing we used might work, but using blocks (as in the square foot gardening method) caused the plants to overhang each other on all sides. This cause some smaller plants to became so sheltered by their neighbours, they lost access to the sun completely and never produced. Next year we have decided to till a piece of land and plant some vegetables in rows. I think the squash will be happier with the new arrangements. Perhaps, just perhaps, they will stay where I plant them and not escape over the fence!