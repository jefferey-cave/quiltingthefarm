---
id: 3415
title: How to Heat a Country Home
date: 2011-01-11T20:41:14+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=7
permalink: /2011/01/11/how-to-heat-a-country_home/
categories:
  - Rural Living
tags:
  - barn
  - heating
  - winter
---
<p style="text-align: center">
  <a href="http://quiltingthefarm.plaidsheep.ca/files/2011/01/woodpile.jpg"><img class="size-medium wp-image-114   aligncenter" title="woodpile" src="http://quiltingthefarm.plaidsheep.ca/files/2011/01/woodpile-300x225.jpg" alt="" width="270" height="203" /></a>
</p>

  1. Phone all known wood delivery guys and find someone…anyone to deliver
  2. Eventually find 2 different guys who will deliver 3 cords each (6 cords should be enough for the winter)
  3. First guy delivers wood (only it doesn’t seem to be, to your place)
  4. Call first guy and ask where wood is. He says “I dropped it off earlier”. You say “But it’s not here”. He says “I delivered it to the house with the blue roof” You say” Which town….you can see where this is going. Wood guy asks if you will drive to the place he mistakenly dropped it off and explain why they have 3 cords of wood in the middle of their driveway…uh No!
  5. Eventually your have first 3 cords of cut and split wood in a big pile outside the barn ready for stacking.
  6. Cover wood pile with large tarp to keep it dry.
  7. Stack your wood: haul the wood piece by piece into the barn. A wheelbarrow makes this job go much faster (unfortunately we didn’t have one)
  8. Second wood delivery gets lost en route even though you gave awesome directions.
  9. You now have 3 more cords to stack **and** you have a wheelbarrow this time!
 10. Wood is all stacked and ready to go
 11. Time to start the wood burning furnace up. You light a fire and wait for the furnace fan to kick in. You wait. You wait some more. Something is wrong.
 12. You call your furnace guy and say “Why is our house not warming up?” He says “Sounds like you received _wet_ wood” Arrrgg!

Wood heat is wonderful; or so they say.