---
id: 730
title: Using Less
date: 2011-03-03T09:30:00+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=730
permalink: /2011/03/03/using-less/
image: /files/2011/02/clothes-rack.jpg
categories:
  - Rural Living
tags:
  - country life
  - frugal
  - home
---
<span id="Using_Less8211_Electricity">

<h2>
  Using Less &#8211; Electricity
</h2></span> 

When we decided to do the homesteading, self reliant thing, we knew there were areas of our budget that we would need to cut back on. One of these areas was electricity. Now don&#8217;t get me wrong, I love my electricity, when you add it all up (lights, TV, computers, stove, etc) electricity is a good deal, but without a regular income to pay this bill AND the power outages that are oh, so, common in the country, cutting down and learning to reduce our dependency on electricity, seems prudent. 

<span id="Clothes_Dryer">

<h3>
  Clothes Dryer
</h3></span> 

When we lived in the city, I dried our clothes in a dryer (as most people do). But here on the farm, I have decided to air <!--more-->dry if I can. As we moved here in winter, we assumed that a dryer would be needed (we did purchase one) until we could use the outside clothes line, but surprisingly, although in a much damper climate than we have been used to, I can dry all my clothes inside on a cheap wooden drying rack. 

<div id="attachment_731" style="width: 310px" class="wp-caption aligncenter">
  <a href="http://www.amazon.com/Indoor-Clothes-Accordion-Drying-42x30x15/dp/B0010XMUOU"><img class="size-full wp-image-731" title="clothes rack" src="http://quiltingthefarm.plaidsheep.ca/files/2011/02/clothes-rack.jpg" alt="" width="300" height="300" /></a>
  
  <p class="wp-caption-text">
    Wooden clothes rack from Amazon
  </p>
</div>

 

Now, I could be tempted to use the dryer more often, but it’s way down in the basement and those stairs scare me! 

<span id="Washing_Machine">

<h3>
  Washing Machine
</h3></span> 

We purchased a <a title="Washing machine" href="http://quiltingthefarm.vius.ca/2011/01/the-new-washing-machine/" target="_blank">high efficiency washing machine</a> when we moved in (we didn&#8217;t actually order this but it&#8217;s the one we ended up with). I&#8217;m not sure that it can use less electricity as the cycle takes way longer, but it does use less water and laundry soap. I can&#8217;t be sure it&#8217;s a win, but it&#8217;s what we have. 

<span id="Furnace">

<h3>
  Furnace
</h3></span> 

Our furnace is a combination wood/oil furnace. We try to use wood as much as we can, only using oil first thing in the morning. The upstairs of our house has electric baseboard heating that we have never used. 

<span id="Stove">

<h3>
  Stove
</h3></span> 

At the present time, I have an electric stove. This spring or summer we will be installing a wood cook stove in our kitchen. The wood stove will be used for cooking and heating during the winter, saving the electric one for summer use. 

<span id="Fridge">

<h3>
  Fridge
</h3></span> 

I have heard that old fridges are electricity hogs. Our fridge came with the place so I have no idea how old it is. Here&#8217;s my dilema; new fridges are expensive, not to mention poorly made, and what does one do with the old fridge&#8230;throw it out? This just doesn&#8217;t seem the best use of resources; mine or anybody else&#8217;s. So, for now I stick with the old fridge until it decides not to play nice anymore. 

<span id="Television">

<h3>
  Television
</h3></span> 

We gave up cable  TV several years ago now. There just didn&#8217;t seem to be anything worth watching and with the advent of &#8220;reality TV&#8221; I was just no longer interested. Having said that, we do actually own a TV, but we only watch a movie or one of the TV shows we already own, on Friday evenings  for our weekly candy night &#8220;extravaganza&#8221;.

<span id="Computers">

<h3>
   Computers
</h3></span> 

Our computers are on a lot but, that&#8217;s how we earn our living at the moment, so they have to stay. Hopefully, our dependency on computers will fade in the future. In the meantime I will  be swapping out my desktop for a more efficient laptop. 

<p style="text-align: center">
  ******
</p>