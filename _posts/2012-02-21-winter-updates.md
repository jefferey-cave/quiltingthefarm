---
id: 3396
title: Winter Updates
date: 2012-02-21T18:24:58+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=3396
permalink: /2012/02/21/winter-updates/
tinymce_signature_post_setting:
  - 'yes'
categories:
  - Rural Living
---
Spring is almost upon us, and I have to admit, I&#8217;m no closer to having our farm website up and running than I was last time I wrote. I know I will have to get a move on if it&#8217;s going to be up in time for the gardening season.

This winter is just flying by, perhaps it&#8217;s the milder weather, or the fact that our house is wonderfully cosy thanks to our new wood stove (more about this in a moment), or perhaps it&#8217;s that we have been busy renovating the kitchen. Maybe, time is just going by faster as I get older!

It&#8217;s been a mild winter. The garden hasn&#8217;t been under much snow this winter. Every Time I look out of the upstairs window I see the raised beds and I believe Spring is around the corner. I can&#8217;t get too excited&#8230;it&#8217;s still February, and there could be a lot of winter still left to come. Last year we had 8 foot piles of snow from the driveway plowing, and the rest of the property was under a snow blanket about waist deep. This year there&#8217;s nothing much. Oh, it does snow here and there, but it mostly melts away before the next storm comes along.

Our chickens are not enjoying the little snow we do have. I tried to chase them out of the coop a couple of days ago, but they weren&#8217;t budging. I went inside and shoved them out the pop door. All this did was cause a commotion in the coop and a pile of 6 chickens hanging on to the door for dear life. I&#8217;ll try again in a few days. I _would_ like to thank our hens for the pleasant surprise of fresh eggs all winter, we actually had enough to sell! As of about a week ago they also ramped up the egg production, so I guess I&#8217;ll be trying a whole lot more egg recipes soon.

Let&#8217;s get into more of the happenings so far this winter. First of all the Pacific Energy wood stove we purchased back in the summer was a dud. The stove wouldn&#8217;t even heat our tiny living room. The dealers sent several people by to see what the problem was, but to no avail. In the end they gave us 100% credit towards another stove (Kudos to Home Hardware in Digby, NS). We decided to upgrade to the Alderlea T5 also by Pacific Energy. Wow, what a stove! Our house is amazingly warm on both floors now and I can&#8217;t begin to tell you how wonderful it is. The wood stove is stated to have a 12 hour burn, and the literature didn&#8217;t lie. This stove will go until noon the next day and still have enough hot coals to easily light back up. Just throw on a couple of big logs and away it goes. The difference between the 2 stoves is incredible. The Alderlea stove has a cook-top and swing out warming shelves, which I must admit I&#8217;ve only used to proof bread and boil water. One thing this mild winter has brought us is a distinct lack of power outages!

We have been going through the wonderful experience of renovating the kitchen&#8230;oh the joy (insert sarcastic tone here). We (our wonderful neighbour along with my amazing Hubby did most of the work, with me as the helper) removed the plaster/lath/acoustic tile ceilings in the dining area and kitchen (the dust was awful), and replaced them with plaster board. I had wanted to open up the ceiling and show the beams, but, alas, the beams were far to close together and made the room look dark. I must say, I&#8217;m truly surprised at the difference a nice flat, matte ceiling makes to a room. Painting the ceiling wasn&#8217;t fun, but I&#8217;m almost over it now that my neck has recovered ;). We  also removed all the paneling/drywall/piaster/lath on the walls, re-insulated where needed, and drywalled that too. We moved the stove and fridge, but left the sink alone.  We took down all the top cabinetry and put up open shelving instead. We are adding a large kitchen island for an extra prep and seating area. I even have a new laundry room &#8211; moving the washer and dryer from the basement &#8211; making laundry a much more pleasant experience. I will post some pics when we are a bit more organized. Oh, one last thing&#8230; a new back door. We have ordered it and it should be here in the next few days.

On to the farm itself. Hubby has ordered some bees. We are going to start off with just the one hive this year and see how it goes. We do have to figure out some electric fencing though, as the bears already frequent our property for the apples, and I imagine the bees will extra appealing to them.

A couple of days ago our shipment of spiles arrived. It won&#8217;t be long now until it will be time to get out to the maple stand we found last summer and tap our trees! We purchased 100 spiles so far, though I suspect we won&#8217;t be using all of them this year. We don&#8217;t actually have a plan for boiling the sap yet, so we don&#8217;t want too much on our hands wondering how we&#8217;ll process it :0

We purchased a truck. We needed a truck to haul wood and get into the back forty. We kept putting it off, and putting it off. A few weeks ago, someone in town was selling there truck with dump bed AND snow plow, for a price that seemed reasonable. The dump bed is great for hauling logs, and the plow&#8230;well, we haven&#8217;t had to pay anyone to clear our drive all winter (not that there&#8217;s been a lot of snow, but that&#8217;s not the point :))

Hubby is out cutting down trees as I am typing this. I can hear the chainsaw in the distance. He is cutting some of the standing dead trees and a few that have blown over in the many windstorms we have had. Up until now, we purchased all our wood for fuel, but based on the fact we have only just started on the wood we ordered this winter, we should have enough for one more year, after which we&#8217;ll use our own. Our new wood stove is much more efficient than our old wood furnace, so that lowers the cost/amount used. We haven&#8217;t used our oil furnace at all this year so there are big savings fuel-wise for us .

Hubby is now a firefighter with the local volunteer fire department. His very first call was a funny one&#8230;  an overturned truck carrying cases of lobster (driver not hurt). Apparently there were lobsters and scallops all over the road. The birds had a heyday and so did a few drivers who saw fit to stop and nab a case or two. As well as being a volunteer firefighter, Hubby starts back at his job next month. he&#8217;s looking forward to learning about all the ins and outs of running a fruit farm. He&#8217;s going to be a busy guy.

I have been keeping myself busy with projects. I have been offered a place at a gallery in Annapolis Royal this Spring and I&#8217;ll need several paintings to hang there. The kitchen reno project has taken over my time at the moment (painting cabinets, building shelves, refurbishing dressers into kitchen storage) and I really need to get motivated and sit and paint! I have most of my seeds for this year&#8217;s garden. I only need to order some pinto beans, sunflowers, purple coneflowers and a couple of good cold weather greens for the new cold frames I will be building from the windows I&#8217;ve collected over the winter.

We have found out more about our property, and the history is quite fascinating. Our place was originally settled by the Acadians back in the 1600&#8217;s. There is a rock wall along our land that borders an old road (now an ATV trail) that was a well travelled route during the 1700&#8217;s. There is a gap in that wall that leads to a flat spot that we suspect there was once a small home. The original homes were built along this old road, not along the newer road that is in use now. We also found the recorded history of our house and land from 1831, when the land was purchased by someone named James Robinson, and this house was constructed between then and 1876 when it was first documented. The property was an orchard until about 20-30 years ago.

Well, that&#8217;s about all for the update so far.

P.S. The blackberry wine we made was delicious!

&nbsp;

&nbsp;