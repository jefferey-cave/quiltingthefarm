---
id: 242
title: I Can Go Outside Again!
date: 2011-01-16T00:43:40+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=242
permalink: /2011/01/16/i-can-go-outside-again/
categories:
  - Rural Living
tags:
  - land
  - snow
  - snowstorms
  - wearables
  - weather
  - winter
---
[<img class="alignleft size-medium wp-image-244" title="boots" src="http://quiltingthefarm.plaidsheep.ca/files/2011/01/boots-300x225.jpg" alt="" width="300" height="225" />](http://quiltingthefarm.plaidsheep.ca/files/2011/01/boots.jpg)I have a pair of rain boots!

I have been somewhat of a hermit lately, content to stay inside. One of the reasons is; I am just too lazy to put on AND tie up my hiking boots each and every single time I venture outdoors.

My new rain boots will make life so much easier; just slip on and away I go. Deep snow…no problem; my boots come up to my knees. Yes, it will be great.

Now, if I could only get motivated to unpack them!