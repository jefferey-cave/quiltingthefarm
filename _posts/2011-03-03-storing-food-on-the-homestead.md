---
id: 737
title: Storing Food on the Homestead
date: 2011-03-03T10:26:01+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=737
permalink: /2011/03/03/storing-food-on-the-homestead/
categories:
  - Rural Living
tags:
  - food
  - home
  - meals
---
There’s a lot of talk going around regarding food storage.

Based on my research, there are 2 ways of looking at this&#8230;

  1. Buy, seal and package food away for an emergency situation, or
  2. Buy food that you eat every day and rotate, rotate, rotate

 

After a lot of research I have decided (don’t tell Hubby) to go with the second way. I can still work in a lot of storage with this method but it gets rotated constantly and gets used up on a regular basis.

While the first method is great if you plan on storing food and leaving it alone for a time of need. But my need is now. I want to eat what I have. For me, there’s little point sealing a large amount of rice in an expensive bucket with an oxygen pack, if I’m going to be opening it in a couple of months or so. Then it will sit open for several months while we use what’s in it. I hate using things that are all sealed up anyway, so I’d probably run to the store and buy another package rather than open one we already have…that’s probably just my quirk 🙂 So for now, I’ll try and keep what I buy in jars or in its original packaging and store, if necessary, in a Tupperware or Rubbermaid tub (rodent and insect control).

P.S. I’m pretty sure in my other life, the one before homesteading (**<span style="color: #800000">L</span>**ife <span style="color: #800000"><strong>B</strong></span>efore **<span style="color: #800000">H</span>**omesteading), I ate rice, pasta and who knows what that was well over a year old. Spring cleaning always brings up some pleasant (and unpleasant) surprises!