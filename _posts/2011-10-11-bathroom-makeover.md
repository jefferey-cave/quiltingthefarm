---
id: 3357
title: Bathroom Makeover
date: 2011-10-11T07:09:18+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=3357
permalink: /2011/10/11/bathroom-makeover/
tinymce_signature_post_setting:
  - ""
categories:
  - Rural Living
---
<a rel="attachment wp-att-3358" href="http://quiltingthefarm.vius.ca/2011/10/bathroom-makeover/toilet-wall/"></a>_The bathroom before&#8230;_

_<a rel="attachment wp-att-3301" href="http://quiltingthefarm.vius.ca/2011/10/3296/spabath1-2/"><img class="alignnone size-full wp-image-3301" title="spabath1" src="http://quiltingthefarm.plaidsheep.ca/files/2011/09/spabath11.jpg" alt="" width="443" height="329" /></a>_

I had already tried to give the bathroom a &#8220;spa&#8221; feel&#8230;not too sucessfully mind you.

<a rel="attachment wp-att-3300" href="http://quiltingthefarm.vius.ca/2011/10/3296/spabath2-2/"><img class="alignnone size-full wp-image-3300" title="spabath2" src="http://quiltingthefarm.plaidsheep.ca/files/2011/09/spabath21.jpg" alt="" width="446" height="321" /></a>

The walls were plain white at the bottom with unfinished tongue and groove above.

<a rel="attachment wp-att-3302" href="http://quiltingthefarm.vius.ca/2011/10/3296/spabath3-2/"><img class="alignnone size-full wp-image-3302" title="spabath3" src="http://quiltingthefarm.plaidsheep.ca/files/2011/09/spabath31.jpg" alt="" width="448" height="564" /></a>

An old pine cabinet I resecued for the downstairs bathroom.

And now, I can&#8217;t wait to show you&#8230;

the After shots!

<a rel="attachment wp-att-3363" href="http://quiltingthefarm.vius.ca/2011/10/bathroom-makeover/chair-corner/"><img class="alignnone size-full wp-image-3363" title="chair-corner" src="http://quiltingthefarm.plaidsheep.ca/files/2011/10/chair-corner.jpg" alt="" width="449" height="337" /></a>

The paint is an &#8220;oops&#8221; grey, and the trim and tongue and groove, a bright white. I have a thing for white trim 🙂 The picture above the chair is a coloured pencil piece that my daughter made for me when she was in Junior High.

<a rel="attachment wp-att-3359" href="http://quiltingthefarm.vius.ca/2011/10/bathroom-makeover/antique-chair/"><img class="alignnone size-full wp-image-3359" title="antique-chair" src="http://quiltingthefarm.plaidsheep.ca/files/2011/10/antique-chair.jpg" alt="" width="337" height="449" /></a>

This little antique chair was purchased for $20 at an end of season sale from Summerhouse Antiques here in the Valley. The shadow box has sand dollars I found at Cape Scott Provincial Park on Vancouver Island.

<a rel="attachment wp-att-3367" href="http://quiltingthefarm.vius.ca/2011/10/bathroom-makeover/sink-corner/"><img class="alignnone size-full wp-image-3367" title="sink-corner" src="http://quiltingthefarm.plaidsheep.ca/files/2011/10/sink-corner.jpg" alt="" width="337" height="449" /></a>

I took the doors off of the cabinet and sewed this skirt instead. The baskets to the side are just a few from my ever growing collection.

<a rel="attachment wp-att-3369" href="http://quiltingthefarm.vius.ca/2011/10/bathroom-makeover/skirt-close-2/"><img class="alignnone size-full wp-image-3369" title="skirt-close" src="http://quiltingthefarm.plaidsheep.ca/files/2011/10/skirt-close1.jpg" alt="" width="449" height="312" /></a>

The skirt I sewed from a piece of decorator fabric I bought a while ago at Walmart. I added a border print and some wood buttons.

<a rel="attachment wp-att-3360" href="http://quiltingthefarm.vius.ca/2011/10/bathroom-makeover/bath-corner/"></a>

<a rel="attachment wp-att-3361" href="http://quiltingthefarm.vius.ca/2011/10/bathroom-makeover/bathtub/"><img class="alignnone size-full wp-image-3361" title="bathtub" src="http://quiltingthefarm.plaidsheep.ca/files/2011/10/bathtub.jpg" alt="" width="449" height="337" /></a>

<a rel="attachment wp-att-3360" href="http://quiltingthefarm.vius.ca/2011/10/bathroom-makeover/bath-corner/"><img class="alignnone size-full wp-image-3360" title="bath-corner" src="http://quiltingthefarm.plaidsheep.ca/files/2011/10/bath-corner.jpg" alt="" width="449" height="337" /></a>

The basket was a gift from my neighbour. The towels and rug were already part of the old bathroom. The twisted willow branches (behind the basket of towels, I didn&#8217;t get a good photo) are from my front yard. The shower curtain is a regular curtain that was originally hanging in the bedroom. We don&#8217;t use this bathroom for showering so the curtain is purely decorative.

<a rel="attachment wp-att-3364" href="http://quiltingthefarm.vius.ca/2011/10/bathroom-makeover/decoration/"><img class="alignnone size-full wp-image-3364" title="decoration" src="http://quiltingthefarm.plaidsheep.ca/files/2011/10/decoration.jpg" alt="" width="449" height="337" /></a><a rel="attachment wp-att-3360" href="http://quiltingthefarm.vius.ca/2011/10/bathroom-makeover/bath-corner/"></a>

<a rel="attachment wp-att-3358" href="http://quiltingthefarm.vius.ca/2011/10/bathroom-makeover/toilet-wall/"><img class="alignnone size-full wp-image-3358" title="toilet-wall" src="http://quiltingthefarm.plaidsheep.ca/files/2011/10/toilet-wall.jpg" alt="" width="337" height="449" /></a>

The mirror was one I picked up last year at Value Village in Halifax. It has been waiting for a good home 🙂 The shutters behind the toilet are the old cabinet doors! I made the wreath out of twigs I collected the day before yesterday. As you can see, I still need new light fixture&#8230;or at least bulb covers!

<a rel="attachment wp-att-3362" href="http://quiltingthefarm.vius.ca/2011/10/bathroom-makeover/beams/"><img class="alignnone size-full wp-image-3362" title="beams" src="http://quiltingthefarm.plaidsheep.ca/files/2011/10/beams.jpg" alt="" width="337" height="449" /></a>

It wasn&#8217;t fun painting all that tongue and groove on the ceiling&#8230;just ask Hubby 😉 One day I would like to stain the beams a little darker&#8230;one day.

<a rel="attachment wp-att-3365" href="http://quiltingthefarm.vius.ca/2011/10/bathroom-makeover/old-pine-shelf/"><img class="alignnone size-full wp-image-3365" title="old-pine-shelf" src="http://quiltingthefarm.plaidsheep.ca/files/2011/10/old-pine-shelf.jpg" alt="" width="337" height="449" /></a>

And lastly the old pine shelf with a coat of dark walnut stain. Not perfect, but so much better.

I still have to do a few things like add new baseboards and make a curtain or blind for the window.

The breakdown

  * Oops grey paint $11 from Rona
  * White paint $25 (have over 1/2 can left)
  * Primer (already had)
  * Antique chair $20
  * Curtain, rug, towels, baskets (already had)
  * Stain (already purchased to stain stairs)
  * Curtain rod $12.99
  * Fabric (for sink curtain) $9

Well, there you have it, a fresh and bright bathroom for next to nothing. I&#8217;m really pleased with how it turned out. I smile everytime I go into the room now, and sometimes I just stop by to take a quick look 🙂

I wonder what room we should tackle next?