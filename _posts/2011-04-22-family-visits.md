---
id: 1849
title: Family Visits
date: 2011-04-22T06:34:19+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=1849
permalink: /2011/04/22/family-visits/
categories:
  - Rural Living
---
It was cold and wet yesterday, I&#8217;m still wearing all my winter clothes but it&#8217;s Spring, it really is!  I&#8217;m counting on the weather to perk up in a day or so as our daughter, her &#8220;hubby&#8221; and their newborn little guy will be visiting us for a couple of weeks. We hope to be able to show them all the wonderful sights that Nova Scotia has to offer. Our daughter (&#8220;Pumpkin&#8221;) wants to go to the beach. I told her not to hold her breath!

Yesterday, we picked up our rented mini van. We need seating for 7 people as my Mom and Dad will be joining us from England next week. Mom doesn&#8217;t know what clothes to pack, and I&#8217;m no help as this is my first Spring here.

We have been framing the inside of the chicken coop. Limey from <a title="Little Homestead in the Valley" href="http://www.littlehomesteadinthevalley.blogspot.com/" target="_blank">Little Homestead in the Valley</a> had already been nice enough to help us remove the drywall a couple of weeks ago (thanks again Limey). We would really like to get some chickens right after everyone leaves (I&#8217;m consoling myself with feathered friends). We have an end wall framed and half of the main wall down the middle of the shed. Once we accepted that there&#8217;s not a single square wall on our entire farm, things went more smoothly&#8230;just accept and move on. 

We purchased a mitre saw and a reciprocating saw at Home Depot as Pumpkin&#8217;s &#8220;hubby&#8221; is going to put in a new kitchen window for us (remember the kitchen window&#8230;the one that may as well be missing the entire glass for the amount of drafts it lets in). We purchased a nice new window. I hope it works as it&#8217;s narrower than the last, but quite a bit taller. I&#8217;m now wondering if we should have got a bigger one.

Well, I&#8217;ve gotta go, there&#8217;s cupcakes to ice, cushions to finish, laundry to wash,  a dozen or two free range eggs to track down and&#8230;