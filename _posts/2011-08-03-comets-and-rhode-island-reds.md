---
id: 2685
title: Comets and Rhode Island Reds
date: 2011-08-03T07:00:53+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2685
permalink: /2011/08/03/comets-and-rhode-island-reds/
tinymce_signature_post_setting:
  - ""
categories:
  - Rural Living
---
Our new hens are here. We picked up 7 brown layers to supplement our flock. We now have 12 birds altogether; one rooster and 11 hens.

Our new hens are 1 year old Comets and Rhode Island Reds. They seem pretty friendly, and will come right up to me.

<a rel="attachment wp-att-2688" href="http://quiltingthefarm.vius.ca/2011/08/comets-and-rhode-island-reds/comets/"><img class="alignnone size-full wp-image-2688" title="comets" src="http://quiltingthefarm.plaidsheep.ca/files/2011/08/comets.jpg" alt="" width="412" height="309" /></a>

They are in their temporary quarters, and will stay there for the next couple of weeks until everybody gets used to everybody else.

Comets and RIR are supposed to be strong layers and living up to their hype they have already given us <del>2</del> <del>4</del> 5 eggs. The Cochins don&#8217;t quite know what to make of them. Our rooster, Alexander, has been trying to entice them to his coop (we have them in side by side coops) by pretending he has food. Last night, when I poked my head into the coop after dark, the Cochins were all piled up on top of Annabelle (our broody) in the nest box&#8230;hillarious. I did feel sorry for Annabelle, I hope they weren&#8217;t there all night long. Poor things, they just don&#8217;t know what to make of these odd brown strangers.