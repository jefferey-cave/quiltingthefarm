---
id: 2830
title: Integration Day
date: 2011-08-16T07:00:06+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2830
permalink: /2011/08/16/integration-day/
tinymce_signature_post_setting:
  - 'yes'
categories:
  - Rural Living
---
Saturday was integration day for our two flocks.

The broody cochins had not hatched any chicks, the eggs were duds (which didn&#8217;t surprise us) but they needed to get over their broodiness. We decided it was a good time to shake things up a bit.

It had been two weeks since we acquired our new hens. They have been living next to, but apart from, our original flock, the cochins. Saturday was the day we choose to hold our collective breaths and open the door between the two coops.

The Reds didn&#8217;t hesitate to scoot right in on the cochins&#8217; territory. The cochins, on the other hand, were not impressed. We watched for a while, a few fights broke out but nothing major. The broodies (Annabelle and Aubree) are no longer at the top of the pecking order, whether that will change, only time will tell. At the moment it&#8217;s difficult to see any kind of ranking among the hens, although our little Agatha still seems to be the lowest in the pecking order&#8230;perhaps she&#8217;s just settled in to her lot in life by now.

Alexander the Great, is having the time of his life! He has a whole new harem, and he&#8217;s as pleased as punch. He loves to protect his hens, and the Reds are really enamoured with him. He stands tall and proud.

<a rel="attachment wp-att-2831" href="http://quiltingthefarm.vius.ca/2011/08/integration-day/integration/"><img class="alignnone size-full wp-image-2831" title="integration" src="http://quiltingthefarm.plaidsheep.ca/files/2011/08/integration.jpg" alt="" width="412" height="307" /></a>