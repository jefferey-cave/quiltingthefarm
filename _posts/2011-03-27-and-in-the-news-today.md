---
id: 1450
title: 'And in the News Today&#8230;'
date: 2011-03-27T07:23:20+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=1450
permalink: /2011/03/27/and-in-the-news-today/
image: /files/2011/03/doily.jpg
categories:
  - Crafts
tags:
  - crafts
  - crochet
  - DIY
  - gardening
---
**_I&#8217;m very excited this morning because my <span style="color: #cc99ff"><span style="color: #666699">Lavender</span> </span>has sprouted!_** 

Admittedly, there are not that many shoots yet, but I can see tiny little bits of <span style="color: #008000">green</span> poking through. I have heard that it is difficult to grow lavender from seed, but germination is a step in the right direction.

AND&#8230;

In more exciting (for me) news, I have been practicing my crochet skills&#8230;

 <a rel="attachment wp-att-1451" href="http://quiltingthefarm.vius.ca/2011/03/and-in-the-news-today/doily/"><img class="alignnone size-full wp-image-1451" title="doily" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/doily.jpg" alt="doily crocheted in white cotton" width="412" height="309" /></a>

I crocheted this from some cotton I found in my sewing box. As you can see the tension isn&#8217;t that great (cotton is unforgiving to the beginner), but I think I&#8217;m getting the hang of it.

I have since improved my tension and have some exciting projects in the works 😉 __

_You&#8217;ll have to check back in a week or 2 to see what I&#8217;m up to._

_ _

<p style="text-align: center">
  <em>******</em>
</p>