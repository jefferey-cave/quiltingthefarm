---
id: 2441
title: Orchard Updates
date: 2011-07-04T06:03:08+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2441
permalink: /2011/07/04/orchard-updates/
categories:
  - Garden
---
Now it is July I thought a quick update on my fruit gardens and orchards would be in order.

The apple trees doing well. Every branch is heavy with fruit. I don&#8217;t know what kind of apples we have, but there are many different varieties.

<a rel="attachment wp-att-2442" href="http://quiltingthefarm.vius.ca/2011/07/orchard-updates/apples-growing-2/"><img class="alignnone size-full wp-image-2442" title="apples-growing" src="http://quiltingthefarm.plaidsheep.ca/files/2011/07/apples-growing1.jpg" alt="" width="412" height="309" /></a>

I have discovered blueberries everywhere! I thought we only had a small patch, but I was wrong.

<a rel="attachment wp-att-2445" href="http://quiltingthefarm.vius.ca/2011/07/orchard-updates/blueberries2-2/"><img class="alignnone size-full wp-image-2445" title="blueberries2" src="http://quiltingthefarm.plaidsheep.ca/files/2011/07/blueberries21.jpg" alt="" width="412" height="309" /></a>

These are the black berries. We are not yet sure if they are actually blackberries or black raspberries. Either way they will be delicious if we get to them before the bears do.

<a rel="attachment wp-att-2444" href="http://quiltingthefarm.vius.ca/2011/07/orchard-updates/blackberries2-2/"><img class="alignnone size-full wp-image-2444" title="blackberries2" src="http://quiltingthefarm.plaidsheep.ca/files/2011/07/blackberries21.jpg" alt="" width="412" height="309" /></a>

My most exciting discovery this spring was the peach tree. It has never produced fruit before this year. This is just one of several fuzzy little fruits. <a rel="attachment wp-att-2443" href="http://quiltingthefarm.vius.ca/2011/07/orchard-updates/peaches-2/"><img class="alignnone size-full wp-image-2443" title="peaches" src="http://quiltingthefarm.plaidsheep.ca/files/2011/07/peaches1.jpg" alt="" width="412" height="309" /></a>

I didn&#8217;t take any pictures of the strawberries. We have been enjoying them for a week now (so have the chickens :)). Next year I will plant a few more.

The tree that we thought was a plum, then a pear, seems to be some kind of cherry. The Blue Jays have decided to pick all the fruit of the tree, so whatever it is, we won&#8217;t be enjoying the fruits this year 🙁