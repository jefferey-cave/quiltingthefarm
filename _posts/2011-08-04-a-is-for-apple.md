---
id: 2700
title: A is for Apple.
date: 2011-08-04T07:00:43+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2700
permalink: /2011/08/04/a-is-for-apple/
tinymce_signature_post_setting:
  - 'yes'
categories:
  - Rural Living
---
If all else fails there will still be lots of apples!

After we purchased our place late last year, there were still apples on the trees. I picked as many as I could and made a huge batch of apple sauce, apple rosehip syrup and some apple marmalade. This year there be lots more. I&#8217;ll need to find all sorts of ways to use them up. I&#8217;m also looking into the best way to keep the apples fresh over the winter.

Keeping apples over winter (or a good portion of it) means keeping them cool. We don&#8217;t have a root cellar or cold room, but we do have a basement. Last year we used the wood furnace in the basement to heat our house. This kept the temperature just above freezing down there. This year we are putting in a wood stove upstairs meaning the basement won&#8217;t be heated at all. I&#8217;m not sure if the temperature will stay above freezing, so I don&#8217;t know if it will work as a cold room. We also have an unheated mudroom at the side of the house. Last year we had a bucket of water in there and it did freeze over, just not very deep. I might give both of them a try.

<a rel="attachment wp-att-2705" href="http://quiltingthefarm.vius.ca/2011/08/a-is-for-apple/bumper-harvest/"><img class="alignnone size-full wp-image-2705" title="bumper-harvest" src="http://quiltingthefarm.plaidsheep.ca/files/2011/08/bumper-harvest.jpg" alt="" width="412" height="309" /></a>

<a rel="attachment wp-att-2702" href="http://quiltingthefarm.vius.ca/2011/08/a-is-for-apple/many-apples/"><img class="alignnone size-full wp-image-2702" title="many-apples" src="http://quiltingthefarm.plaidsheep.ca/files/2011/08/many-apples.jpg" alt="" width="412" height="309" /></a>

I&#8217;m not sure when the harvest for our apples start. We purchased our house in September last year and some of the varieties were coming to an end, and some were just starting to ripen. A couple of the trees were still producing into December.

With all those apples, it seems I&#8217;ll also have lots of blackberries. I like that apples and blackberries go well together&#8230;I have an over abundance of both! The trailing blackberries (dewberries, I think) are starting to ripen, but the main event won&#8217;t happen until early Fall. Last year the bears??? deer??? got to most of them before we did. This year I&#8217;ll be keeping vigil over them. I can taste the apple and blackberry pie already.

<a rel="attachment wp-att-2703" href="http://quiltingthefarm.vius.ca/2011/08/a-is-for-apple/dewberries/"><img class="alignnone size-full wp-image-2703" title="dewberries" src="http://quiltingthefarm.plaidsheep.ca/files/2011/08/dewberries.jpg" alt="" width="412" height="309" /></a>

Let me know if you have managed to keep apples for any length of time, and please, please send me all those apple recipes you have tucked away&#8230;I&#8217;ll be needing them 🙂