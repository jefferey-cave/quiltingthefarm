---
id: 3318
title: Rain and Renovating
date: 2011-10-03T07:12:39+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=3318
permalink: /2011/10/03/rain-and-renovating/
tinymce_signature_post_setting:
  - 'yes'
categories:
  - Kitchen
---
Well, it has been a rainy, rainy weekend. It was a weekend to spend as much time as possible indoors. Even the chickens didn&#8217;t venture outside.

We have lots of indoor projects to do. We have someone coming to install a new ceiling in our kitchen/dining area in a couple of weeks. We are hoping to take done the old plaster ceiling and lath and open up the beams. Our house has very low ceilings so it will give us a little more height and add a sense of airy-ness to the room.

Here is a picture of what we would like.

<div class="mceTemp">
  <dl id="attachment_3319" class="wp-caption alignnone" style="width: 459px">
    <dt class="wp-caption-dt">
      <a href="http://cooper-grey.com/category/lighting/page/2/"><img class="size-full wp-image-3319" title="open beams" src="http://quiltingthefarm.plaidsheep.ca/files/2011/10/open-beams.png" alt="" width="449" height="535" /></a>
    </dt>
    
    <dd class="wp-caption-dd">
      Open beams (source: cooper-grey.com)
    </dd>
  </dl>
  
  <p>
    First we have top remove all the upper cabinets in the kitchen and we will not be putting them back up. but will go with open shelves instead. I&#8217;m looking forward to having a prettier kitchen with a much better layout <em>and</em> more counter space.</div>