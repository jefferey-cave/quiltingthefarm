---
id: 948
title: Little Chair Love
date: 2011-03-04T08:59:16+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=948
permalink: /2011/03/04/little-chair-love/
image: /files/2011/03/chair_cushion.jpg
categories:
  - Crafts
tags:
  - crafts
  - decorating
  - designing
  - DIY
  - fabric
  - home
---
This is the little chair I purchased in a antiquey/junk store a couple of weeks ago. I wasn’t sure about it at first, but I desperately needed another chair (I only have 3). The chair was priced at $35 although when I went to pay it turned out to be just $25.

<div id="attachment_949" style="width: 422px" class="wp-caption alignleft">
  <a rel="attachment wp-att-949" href="http://quiltingthefarm.vius.ca/2011/03/little-chair-love/chair2/"><img class="size-full wp-image-949" title="little chair" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/chair2.jpg" alt="little chair" width="412" height="453" /></a>
  
  <p class="wp-caption-text">
    little chair
  </p>
</div>

 I stood the chair against my (ugly) dining room walls and it was love at second sight!

I made a little reversible quilted cushion out of cheerful fabrics so I can change it up depending on the mood of the day.

<div id="attachment_950" style="width: 422px" class="wp-caption alignleft">
  <a href="http://quiltingthefarm.plaidsheep.ca/files/2011/03/chair_cushion.jpg"><img class="size-full wp-image-950" title="chair_cushion" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/chair_cushion.jpg" alt="chair" width="412" height="493" /></a>
  
  <p class="wp-caption-text">
    with blue side showing
  </p>
</div>

<div class="mceTemp">
  <dl id="attachment_951" class="wp-caption alignleft" style="width: 422px">
    <dt class="wp-caption-dt">
      <a href="http://quiltingthefarm.plaidsheep.ca/files/2011/03/chair_cushion2.jpg"><img class="size-full wp-image-951" title="chair_cushion2" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/chair_cushion2.jpg" alt="chair" width="412" height="507" /></a>
    </dt>
    
    <dd class="wp-caption-dd">
      with red/yellow cushion side
    </dd>
  </dl>
  
  <p>
    Now it really looks at home.
  </p>
  
  <p style="text-align: center">
    ******
  </p>
</div>