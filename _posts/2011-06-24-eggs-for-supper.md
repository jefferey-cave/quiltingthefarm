---
id: 2336
title: Eggs for Supper
date: 2011-06-24T06:29:36+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2336
permalink: /2011/06/24/eggs-for-supper/
tinymce_signature_post_setting:
  - ""
categories:
  - Kitchen
  - Recipes
---
With hens comes eggs, and if you&#8217;re lucky&#8230;lots of eggs.

Eggs are an excellent source of protein. A hard-boiled egg in that mid-afternoon slump really works wonders. I sometimes boil some up in the morning, then we we get hungry there&#8217;s a healthy snack ready to go. For meals we have been having fried eggs, scrambled eggs, french toast, frittatas and omletttes. I haven&#8217;t made a quiche yet, but it&#8217;s on my list of to-do&#8217;s. The other night, I was in the mood for something different, something spicy&#8230;curried eggs maybe? Well I did find a curried eggs recipe online so I tried it out with a few alterations.

<span id="What_you_need8230">

<h3>
  What you need&#8230;
</h3></span> 
:   3 tbsp vegetable oil 
:   2 tsp  mustard seeds
:   1 tsp ground cumin 
:   2 onions, thinly sliced 
:   1 jalapeno, seeded and chopped finely
:   3 cloves garlic, minced 
:   2 tsp gingerroot, grated
:   2-1/2 tsp curry powder 
:   1 tsp  turmeric 
:   3/4 tsp salt 
:   1/4 to 1/2 tsp of cayenne pepper flakes
:   1-1/2 cups diced canned tomatoes 
:   1/2 cup plain yogurt 
:   pinch black pepper 
:   4 hard-boiled eggs, halved 
:   1/4 tsp ground corianda
:    

<span id="How_to8230">

<h3>
  How to&#8230;
</h3></span> 

<div>
  <p>
    Heat oil in a large skillet or pan. Fry mustard seeds and cumin for 3-4 minutes . Add onions and cook gently until golden, about 7 or 8 minutes. 
  </p>
  
  <p>
    Add the jalapeno, garlic and ginger and cook until fragrant, 1 to 2 minutes.
  </p>
  
  <p>
    Stir in the curry powder, turmeric, salt, cayenne pepper flakes and corianda and  cook for 1 minute. 
  </p>
  
  <p>
    Add the tomatoes with their juice and simmer, covered, for 5 minutes.
  </p>
  
  <p>
    Stir in yogurt and black pepper then add the hard-boiled eggs. Bring just to a simmer. Remove from heat, cover and let stand until the eggs are warmed through.
  </p>
  
  <p>
    Serve with rice.
  </p>
  
  <p>
    Makes 2 large and delicious servings.
  </p>
  
  <p>
    <em>No photo&#8230;sorry we ate it too quickly 😉</em>
  </p>
  
  <p>
    original <a href="http://www.homemakers.com/food-and-recipes/curried-eggs/r/8485" target="_blank">recipe</a>
  </p>
  
  <p style="text-align: center">
    ******
  </p>
</div>