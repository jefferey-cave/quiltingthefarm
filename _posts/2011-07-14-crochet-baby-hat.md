---
id: 2537
title: Crochet Baby Hat
date: 2011-07-14T07:07:06+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2537
permalink: /2011/07/14/crochet-baby-hat/
tinymce_signature_post_setting:
  - 'yes'
image: /files/2011/07/baby-hat.jpg
categories:
  - Crafts
---
A couple of weeks ago, I was invited to a baby shower for my neighbour. When I get invited to something like this I really like to make something unique. I made these [cute washcloths](http://quiltingthefarm.vius.ca/2011/06/cute-handmade-washcloths/ "Cute Baby Washcloths") and wrote a post about making them a week or so ago(they were a big hit 🙂 at the shower). I also wanted to use my newly acquired skills in crochet and although still very much a beginner, I came up with this baby hat.

<a rel="attachment wp-att-2538" href="http://quiltingthefarm.vius.ca/2011/07/crochet-baby-hat/baby-hat/"><img class="alignnone size-full wp-image-2538" title="baby-hat" src="http://quiltingthefarm.plaidsheep.ca/files/2011/07/baby-hat.jpg" alt="" width="412" height="309" /></a>

The hat took me a couple of evenings to make. I designed the pattern from scratch. I originally made pompoms for the top, but switched them out for tassels later as I prefered the look.

I first crocheted the main part of the hat. I started with a row of plain yarn and fancy yarn (used together) to make the &#8220;fluffy&#8221; band then I worked in the plain yellow yarn. This part is made in one piece and joined at the back. I picked up stitches along the band and crocheted the ear flaps. I had to unpick these several times before I got it right! I crocheted the fancy yarn across the top once the hat was put together, then I added the tassels.

My neighbour was thrilled with the hat, and a couple of guests even asked if I was interested in selling them!