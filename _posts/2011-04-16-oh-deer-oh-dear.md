---
id: 1755
title: Oh Deer, Oh Dear
date: 2011-04-16T06:45:56+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=1755
permalink: /2011/04/16/oh-deer-oh-dear/
categories:
  - Rural Living
---
As much as I wanted to hope that my first garden was going to be safe from critters, I&#8217;m afraid I have to face the reality that a family of 5 deer have set up shop in my backyard!

Every evening for the past couple of weeks, we&#8217;ve had constant visits from our <span style="text-decoration: line-through">dear </span>deer friends. Last night I tried to take some photos of these cunning little marauders.

<a rel="attachment wp-att-1757" href="http://quiltingthefarm.vius.ca/2011/04/oh-deer-oh-dear/deer2/"><img class="alignnone size-full wp-image-1757" title="deer2" src="http://quiltingthefarm.plaidsheep.ca/files/2011/04/deer2.jpg" alt="deer grazing" width="412" height="309" /></a>

These two were on their way to the pond. See that one giving me the &#8220;evil eye&#8221; !

<a rel="attachment wp-att-1758" href="http://quiltingthefarm.vius.ca/2011/04/oh-deer-oh-dear/deer3/"><img class="alignnone size-full wp-image-1758" title="deer3" src="http://quiltingthefarm.plaidsheep.ca/files/2011/04/deer3.jpg" alt="deer behind tree" width="412" height="309" /></a>

This one is just outside the dining room window (sorry about the glare)

<a rel="attachment wp-att-1756" href="http://quiltingthefarm.vius.ca/2011/04/oh-deer-oh-dear/deer/"><img class="alignnone size-full wp-image-1756" title="deer" src="http://quiltingthefarm.plaidsheep.ca/files/2011/04/deer.jpg" alt="deer in yard" width="412" height="309" /></a>

Here&#8217;s another one going by. They don&#8217;t seem to care that we live here. They aren&#8217;t that bothered by our presence.

I had thought that I only needed to protect the garden from rabbits and other _**small**_ critters. Deer require a different strategy.

As we have only built two 8 foot beds this year, fencing them off wont be that costly. I&#8217;m not sure if we&#8217;ll need a double fence to keep the deer at bay, or if the fenced garden area will be small enough to discourage the deer from jumping in.

Have you had deer problems? What worked best for you?