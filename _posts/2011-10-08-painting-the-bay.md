---
id: 3342
title: Painting the Bay
date: 2011-10-08T07:05:08+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=3342
permalink: /2011/10/08/painting-the-bay/
tinymce_signature_post_setting:
  - 'yes'
categories:
  - Crafts
---
I was going to write about our cider making today, but it all went very wrong. Hubby and I are both ticked off right now, so I&#8217;m going to write about something different.

Here&#8217;s the painting I did. I finished it yesterday.

<div id="attachment_3343" style="width: 459px" class="wp-caption alignnone">
  <a rel="attachment wp-att-3343" href="http://quiltingthefarm.vius.ca/2011/10/painting-the-bay/painting/"><img class="size-full wp-image-3343" title="painting" src="http://quiltingthefarm.plaidsheep.ca/files/2011/10/painting.jpg" alt="" width="449" height="337" /></a>
  
  <p class="wp-caption-text">
    Storm over Fundy
  </p>
</div>

My watercolour painting represents a large storm over the Bay of Fundy. It seamed a fitting subject with all the wild weather and storm surges Nova Scotia has been experiencing these last few days. I wonder if the painting will look good in my bathroom?

With a bit of luck the weatherman will be correct, and we&#8217;ll be back to 24 celsius on Sunday. I hope he&#8217;s right because I&#8217;m planning on foraging for pinecones and leaves to decorate my house and give it a cozy Fall look and feel.