---
id: 1680
title: 100th Post and a Bench
date: 2011-04-11T09:59:18+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=1680
permalink: /2011/04/11/100th-post-and-a-bench/
categories:
  - Crafts
---
**<span style="color: #800080"><span style="color: #000000">My apologies, our server was down this morning, so I have been unable to update the blog until now.</span> I was getting a little concerned as I wanted to celebrate my</span>**

<h1 style="text-align: center">
  <span style="color: #800080">100th post!</span>
</h1>

**<span style="color: #800080"> </span>**On with the bench. I promised you a photo of the bench I built and here it is&#8230;

<a rel="attachment wp-att-1681" href="http://quiltingthefarm.vius.ca/2011/04/100th-post-and-a-bench/bedroom-bench/"><img class="alignnone size-full wp-image-1681" title="bedroom-bench" src="http://quiltingthefarm.plaidsheep.ca/files/2011/04/bedroom-bench.jpg" alt="bedroom bench" width="412" height="309" /></a>

While hubby was hauling drywall to the dump the other day, I thought I would surprise him. I built this bench, to put at the end of our bed, out of scraps of wood in the garage. The basic bench took about 2 hours to cut the wood and put together. It&#8217;s not perfect, it is not as deep is it should be (I had to work with what I had)  and it was the first time I had ever used a table saw so I had &#8220;wing&#8221; it a little 🙂 All that&#8217;s left now is a coat of primer and paint. I might use the yellow from the <a title="Ugly dresser makeover" href="http://quiltingthefarm.vius.ca/2011/03/the-ugly-dresser-makeover/" target="_blank">dresser revamp</a> or go with white&#8230;not sure yet.

The bench is a pattern from <a title="Ana white spa bench" href="http://ana-white.com/2010/03/plans-simple-spa-bench-inpsired-by.html" target="_blank">Ana White&#8217;s website</a>. If you&#8217;ve never visited her site it&#8217;s a must see. Ana builds just about everything with very simple plans that anyone can follow (and you know I can&#8217;t follow a knitting pattern!). I built my bench a little longer as I had two matching boards and no helper to cut the pieces down. I added the middle support because of the extra length.

I&#8217;m quite pleased with the way it turned out. I can even sit on it! I&#8217;ll be using it to store extra pillows and blankets at the end of the bed.

My next mission is <a title="potting bench" href="http://ana-white.com/2011/03/simple-potting-bench" target="_blank">this potting bench</a>. Wish me luck 😉

<p style="text-align: center">
  ******
</p>