---
id: 2844
title: Keeping My Perspective
date: 2011-08-18T07:00:35+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2844
permalink: /2011/08/18/keeping-my-perspective/
tinymce_signature_post_setting:
  - ""
categories:
  - Rural Living
---
To become self reliant is a long journey full of successes, failures, and make-do&#8217;s. Finding the starting point has been a struggle for us. We know where we want to be, but we have no idea how to start the process of getting there. It&#8217;s not the fork in the road, it&#8217;s the complete lack of road that&#8217;s the problem.

It&#8217;s been hard to see what and where we have accomplished anything. People ask us what we have done. When this question comes up both Hubby and I are stuck.  We know we have been busy, we know we have been &#8216;doing&#8217;, but there&#8217;s been little we can pinpoint in the way of accomplishment that people would get. Even we didn&#8217;t get it.

Slowly, I&#8217;m beginning to discover that self-reliance is more a series of small journeys than it is any kind of destination. Some  journeys end along the trail somewhere, others end far away from where they begin, and still others end very close to where they started. Some journeys are so short that you don&#8217;t realize you have travelled them. These journeys add up over time.

While picking berries the other day I was thinking about reliance on our own fruit. We have had a harvest (albeit small) of strawberries. The blueberries, which should only get better with time and care are just finishing up. the blackberries are ripe for the picking and the apple harvest won&#8217;t be far behind. After apples there are rose hips. Early next spring, I hope to harvest my first rhubarb. That&#8217;s a lot of fruit for two people. If I freeze and can and jelly and juice, we should be able to keep ourselves in fruit year round . What&#8217;s strange about this is I hadn&#8217;t seen this as self reliance; I just saw it as having our own fruit. Today I realized that this is one of those small journeys. To keep ourselves in fruit is a major accomplishment, I was seeing it as just a small, fun hobby.

Since we moved here in December, I have baked all the bread that our family has eaten. Over these eight months, I have vastly improved, and now Hubby won&#8217;t eat store purchased bread. Am I self reliant? Yes and no. The bread I bake is made with flour from the local grocery store. I don&#8217;t supply the milk, oil or sugar. To be truly self sufficient, I would have to grow and mill my own wheat, grow some kind of oil producing plant and press my own oil, milk my own cow, goat or sheep, and collect and process honey from my own hives. The destination for producing this is far further away than I am ever likely to travel, perhaps my journey will end at the milling of purchased grain. Yes, I will try and grow my own wheat, but there is always someone more efficient at doing this than I am. There&#8217;s something to be said about specialization. I doesn&#8217;t mean I have lost the plot, it means my journey to self reliance is a shorter one.

My garden could be classed as a bit of a disappointment, but it really has been much more of a learning experience than I had imagined. I am not a gardener, I am a student learning the ropes. I will have many failures, some will be my fault, others will be dumb luck.  Did you know There are 30 common tomato diseases? This year I have blossom end rot,so I only have 29 more to go through before I&#8217;m a master at raising tomatoes! I feel it will be many years before the successes outway the failures, but that&#8217;s ok. Self reliance in food doesn&#8217;t come quickly. This year I planted potatoes in tubs. There hasn&#8217;t been an abundant harvest, but what we&#8217;ve pulled from the earth has been tasty and disease free. Next year, I will try and plant enough potatoes to keep us through the winter. Perhaps I&#8217;ll even work on all the root veggies and winter squash. I will still plant a regular lettuce, greens, peas etc garden, but it will be just for eating fresh. It will be another year before I start thinking about self reliance in that area.

We now have our own chickens. They supply us with eggs. Our chickens need feed. They forage all day, but the weeds and bugs aren&#8217;t enough to keep them alive, they need grain that we don&#8217;t grow. We have discussed growing chicken feed, but without the cleared land or equipment, the expense is not worth it. Being self reliant in eggs, but relying on someone else for chicken feed is OK, and perhaps it&#8217;s the best way for us.

Today when I look back,  I see that we have taken many small journeys towards looking after ourselves. Will we ever be self sufficient? No, but we **_will_** be more self reliant.