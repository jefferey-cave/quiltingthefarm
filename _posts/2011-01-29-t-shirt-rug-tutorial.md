---
id: 261
title: T-Shirt Rug Tutorial
date: 2011-01-29T00:56:54+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=261
permalink: /2011/01/29/t-shirt-rug-tutorial/
image: /files/2011/01/tshirt_rug.jpg
categories:
  - Crafts
tags:
  - crafts
  - DIY
  - home
  - sewing
  - tutorial
---
[](http://quiltingthefarm.plaidsheep.ca/files/2011/01/tshirt_rug.jpg)

[<img class="alignnone size-medium wp-image-262" title="tshirt_rug" src="http://quiltingthefarm.plaidsheep.ca/files/2011/01/tshirt_rug-300x225.jpg" alt="" width="300" height="225" />](http://quiltingthefarm.plaidsheep.ca/files/2011/01/tshirt_rug.jpg)

This is the rug that lies beside my bed. It’s cozy on my toes and makes getting out of bed on a cold winter’s morning, just a little bit more bearable.

Hubby and I made this rug. It’s made from old t-shirts and such (I used some old pjs). Below are the instructions. I wouldn’t recommend making this in the summer as the rug is thick and heavy and gets way too warm on your lap!<!--more-->

**You will need:**

  * a piece of burlap the size you would like your rug plus 2 inches on all sides (the sides will get turned under when finishing)
  * lots of old t-shirts, or any cotton jersey fabric
  * cutting board and rotary cutter (makes life easy) or scissors and ruler
  * an implement for poking the fabric strips through the burlap

** Instructions**

  1. Tape all the edges of your burlap with masking tape (this stops the edges fraying)
  2. Draw a line around the burlap (on the reverse side) for the size of your rug.
  3. Cut lots and lots of strips of fabric approximately 7 in x 1.5 in.
[<img class="size-thumbnail wp-image-282 alignnone" title="rug_strip" src="http://quiltingthefarm.plaidsheep.ca/files/2011/01/rug_strip-150x150.jpg" alt="" width="150" height="150" />](http://quiltingthefarm.plaidsheep.ca/files/2011/01/rug_strip.jpg)

  4. Starting along one of the edges and with the back facing up, poke a fabric strip down into the burlap along the line you drew. Pull it about half way through. Take the end that’s sticking up and poke that through the burlap about 1/8 in (or about 4 threads) away. Grab the 2 ends underneath the rug and pull to tighten making sure the 2 ends are even.
  5. Continue adding strips along this line, spacing them about ¾ inch apart (the closer together the strips the thicker the rug will be).
<div id="attachment_273" style="width: 160px" class="wp-caption alignnone">
  <a href="http://quiltingthefarm.plaidsheep.ca/files/2011/01/back-rug.jpg"><img class="size-thumbnail wp-image-273" title="back-rug" src="http://quiltingthefarm.plaidsheep.ca/files/2011/01/back-rug-150x150.jpg" alt="" width="150" height="150" /></a>
  
  <p class="wp-caption-text">
    Oops, don't look at all that dust!
  </p>
</div>

  6. As the pieces that form the edge need to be secure, once you have finished one entire row; flip the rug over and make a half knot on each strip.
[<img class="size-thumbnail wp-image-272 alignnone" title="rug-knots" src="http://quiltingthefarm.plaidsheep.ca/files/2011/01/rug-knots-150x150.jpg" alt="" width="150" height="150" />](http://quiltingthefarm.plaidsheep.ca/files/2011/01/rug-knots.jpg)

**NOTE:** You only need to do this for the outside strips.

  7. Start the second row with one knotted piece and then continue adding strips as in step #4. **DO NOT KNOT THESE STRIPS**. End the row with a knotted strip.
  8. Continue in this fashion until the entire piece is filled in. End your rug with a row of knotted strips.
  9. To finish the rug, fold under the extra burlap on all sides and sew or tape in place. Add a backing fabric if you desire.
[<img class="alignnone size-thumbnail wp-image-281" title="rug-edges" src="http://quiltingthefarm.plaidsheep.ca/files/2011/01/rug-edges-150x150.jpg" alt="" width="150" height="150" />](http://quiltingthefarm.plaidsheep.ca/files/2011/01/rug-edges.jpg)</ol> 

The strips that aren’t knotted stay in place and really cinch up after the rug is gently washed.

I&#8217;m linking to <a href="http://debbie-debbiedoos.blogspot.com/" target="_blank">Debbiedoos Newbies Party</a>