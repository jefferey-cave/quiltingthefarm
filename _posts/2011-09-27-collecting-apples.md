---
id: 3253
title: Collecting Apples
date: 2011-09-27T07:04:01+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=3253
permalink: /2011/09/27/collecting-apples/
tinymce_signature_post_setting:
  - 'yes'
categories:
  - Rural Living
---
With Fall comes apples. With apples comes cider.

Harvesting the apples for our first batch of cider proved more difficult and painstaking than we had anticipated. I suppose we should have known, we&#8217;ve yet to try something that wasn&#8217;t!

Yesterday, tarp in hand, we wandered out to one of the trees that still has apples on it.  Our russet apples won&#8217;t be ready for another three or four weeks, but some of the other, as yet unidentified, apples are ripe.

<a rel="attachment wp-att-3255" href="http://quiltingthefarm.vius.ca/2011/09/collecting-apples/apples-2/"><img class="alignnone size-full wp-image-3255" title="apples" src="http://quiltingthefarm.plaidsheep.ca/files/2011/09/apples1.jpg" alt="" width="412" height="309" /></a>

Unfortunately lots of apples have been eaten by the deer and bear so all the good apples are way at the top of the trees. These apple trees are very large and very old.

We spread the tarp under the tree.

<a rel="attachment wp-att-3254" href="http://quiltingthefarm.vius.ca/2011/09/collecting-apples/tarp/"><img class="alignnone size-full wp-image-3254" title="tarp" src="http://quiltingthefarm.plaidsheep.ca/files/2011/09/tarp.jpg" alt="" width="412" height="309" /></a>

Hubby went up the ladder and shook the tree. Unfortunately not much happened. We got a few, but not nearly enough for the effort.

<a rel="attachment wp-att-3260" href="http://quiltingthefarm.vius.ca/2011/09/collecting-apples/shaking-tree/"><img class="alignnone size-full wp-image-3260" title="shaking-tree" src="http://quiltingthefarm.plaidsheep.ca/files/2011/09/shaking-tree.jpg" alt="" width="412" height="309" /></a>

Hubby climbed into the tree and went out on a limb&#8230;literally!

<a rel="attachment wp-att-3259" href="http://quiltingthefarm.vius.ca/2011/09/collecting-apples/hub-in-tree/"><img class="alignnone size-full wp-image-3259" title="hub-in-tree" src="http://quiltingthefarm.plaidsheep.ca/files/2011/09/hub-in-tree.jpg" alt="" width="412" height="309" /></a>

That wasn&#8217;t very successful either. We did get some, but again, not enough. We ended up just picking them off the tree one at a time. We will go out again, but this time we&#8217;ll take the big ladder so we can reach the fruit at the top.

<a rel="attachment wp-att-3258" href="http://quiltingthefarm.vius.ca/2011/09/collecting-apples/fallen-apples/"><img class="alignnone size-full wp-image-3258" title="fallen-apples" src="http://quiltingthefarm.plaidsheep.ca/files/2011/09/fallen-apples.jpg" alt="" width="412" height="309" /></a>

Apparently we have had a very recent visitor! A Bear, it would seem, has been helping himself to our apples. No wonder there weren&#8217;t any on the lower branches. This does not make me want to go outside at night.

<a rel="attachment wp-att-3256" href="http://quiltingthefarm.vius.ca/2011/09/collecting-apples/bear-scat/"><img class="alignnone size-full wp-image-3256" title="bear-scat" src="http://quiltingthefarm.plaidsheep.ca/files/2011/09/bear-scat.jpg" alt="" width="412" height="309" /></a>

Our haul for today wasn&#8217;t overly impressive. We&#8217;ll need a lot more than that for a decent amount of cider.

<a rel="attachment wp-att-3257" href="http://quiltingthefarm.vius.ca/2011/09/collecting-apples/box-apples/"><img class="alignnone size-full wp-image-3257" title="box-apples" src="http://quiltingthefarm.plaidsheep.ca/files/2011/09/box-apples.jpg" alt="" width="412" height="309" /></a>

It was hard work and we really did have to drag our sorry butts back to the house when we were done.

Even though we didn&#8217;t get as many apples as we would have liked, we did get to spend the afternoon soaking up the last of the late summer rays (In fact it was a little too hot), and Hubby got to enjoy his second childhood climbing trees 🙂  <a rel="attachment wp-att-3255" href="http://quiltingthefarm.vius.ca/2011/09/collecting-apples/apples-2/"></a>