---
id: 1804
title: Baby Blocks
date: 2011-04-25T06:44:01+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=1804
permalink: /2011/04/25/baby-blocks/
image: /files/2011/04/baby-blocks-close.jpg
categories:
  - Crafts
---
Remember these scraps of wood? They were leftovers from Hubby&#8217;s bench building escapade.

<a rel="attachment wp-att-1806" href="http://quiltingthefarm.vius.ca/2011/04/baby-blocks/blocks-2/"><img class="alignnone size-full wp-image-1806" title="blocks" src="http://quiltingthefarm.plaidsheep.ca/files/2011/04/blocks1.jpg" alt="wooden blocks" width="412" height="309" /></a>

I sanded them. I painted them

<a rel="attachment wp-att-1809" href="http://quiltingthefarm.vius.ca/2011/04/baby-blocks/painted-blocks/"><img class="alignnone size-full wp-image-1809" title="painted-blocks" src="http://quiltingthefarm.plaidsheep.ca/files/2011/04/painted-blocks.jpg" alt="painting wooden blocks" width="412" height="309" /></a>

I put these cute stickers on them

<a rel="attachment wp-att-1808" href="http://quiltingthefarm.vius.ca/2011/04/baby-blocks/stickers/"><img class="alignnone size-full wp-image-1808" title="stickers" src="http://quiltingthefarm.plaidsheep.ca/files/2011/04/stickers.jpg" alt="alphabet stickers" width="412" height="309" /></a>

I glazed them.

<a rel="attachment wp-att-1805" href="http://quiltingthefarm.vius.ca/2011/04/baby-blocks/baby-blocks-close/"><img class="alignnone size-full wp-image-1805" title="baby-blocks-close" src="http://quiltingthefarm.plaidsheep.ca/files/2011/04/baby-blocks-close.jpg" alt="wooden baby blocks" width="412" height="309" /></a>

And this is how they look now.

<a rel="attachment wp-att-1807" href="http://quiltingthefarm.vius.ca/2011/04/baby-blocks/baby-blocks/"><img class="alignnone size-full wp-image-1807" title="baby-blocks" src="http://quiltingthefarm.plaidsheep.ca/files/2011/04/baby-blocks.jpg" alt="baby blocks with letters and animals" width="412" height="309" /></a>

<p style="text-align: center">
  ******
</p>