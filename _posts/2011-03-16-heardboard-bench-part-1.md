---
id: 1171
title: Heardboard Bench Part 1
date: 2011-03-16T07:01:09+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=1171
permalink: /2011/03/16/heardboard-bench-part-1/
image: /files/2011/03/bench-finished.jpg
categories:
  - Crafts
tags:
  - crafts
  - decorating
  - designing
  - DIY
  - frugal
  - home
---
<span id="Hubby_has_been_helping_me_build_building_a_bench_out_of_a_headboard.">

<h3>
  Hubby has been <span style="text-decoration: line-through">helping me build</span> building a bench out of a headboard.
</h3></span> 

I picked the headboard up at a hardware sale a few weeks ago for $15. Hubby looked at me as if I was crazy, but just smiled and hauled it to the car for me anyway.

**1.** Here is the headboard and the (already cut in half) footboard. The footboard sides were trimed down a little more to make a good depth for the bench.

<a rel="attachment wp-att-1174" href="http://quiltingthefarm.vius.ca/2011/03/heardboard-bench-part-1/bench-back/"><img class="alignnone size-full wp-image-1174" title="bench-back" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/bench-back.jpg" alt="headboard and footboard" width="412" height="309" /></a>

<a rel="attachment wp-att-1177" href="http://quiltingthefarm.vius.ca/2011/03/heardboard-bench-part-1/bench-side/"><img class="alignnone size-full wp-image-1177" title="bench-side" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/bench-side.jpg" alt="foot board cut in half" width="412" height="309" /></a><!--more-->

**2.** Hubby has the arms attached and is making the inner frame for the seat.

<a rel="attachment wp-att-1173" href="http://quiltingthefarm.vius.ca/2011/03/heardboard-bench-part-1/bench/"><img class="alignnone size-full wp-image-1173" title="bench" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/bench.jpg" alt="making the bench frame" width="412" height="309" /></a>

Excuse the mess 🙂

**3.** The legs of the headboard were cut down as they were a little too long. Hubby cut them down by about 4 inches. <a rel="attachment wp-att-1172" href="http://quiltingthefarm.vius.ca/2011/03/heardboard-bench-part-1/bench-sideview/"><img class="alignnone size-full wp-image-1172" title="bench-sideview" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/bench-sideview.jpg" alt="bench sideveiw" width="412" height="309" /></a>

**4.** The frame is complete. All it needs now is a seat. The seat will be inset so the top will be flush.

<a rel="attachment wp-att-1176" href="http://quiltingthefarm.vius.ca/2011/03/heardboard-bench-part-1/bench-frame/"><img class="alignnone size-full wp-image-1176" title="bench-frame" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/bench-frame.jpg" alt="completed frame" width="412" height="309" /></a>

**5.** The seat boards are in and the bench has been sanded (see all that dust on the floor) and is ready for the next part of the project&#8230;a new paint job and a snazzy seat cushion. Stay tuned for part 2 to see the finished product.

<a rel="attachment wp-att-1175" href="http://quiltingthefarm.vius.ca/2011/03/heardboard-bench-part-1/bench-finished/"><img class="alignnone size-full wp-image-1175" title="bench-finished" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/bench-finished.jpg" alt="finished heardboard bench" width="412" height="309" /></a>

I&#8217;m linking up with <a title="Blue Cricket Design" href="http://www.bluecricketdesign.net/" target="_blank">Blue Cricket Design</a> for their Show and Tell Wednesdays

UPDATE: Part 2 <a title="Headboard bench part 2" href="http://quiltingthefarm.vius.ca/2011/03/headboard-bench-part-2/" target="_self">&#8220;The finished Bench&#8221;</a>

<p style="text-align: center">
  ******
</p>