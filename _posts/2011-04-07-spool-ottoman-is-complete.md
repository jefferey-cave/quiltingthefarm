---
id: 1630
title: Spool Ottoman is Complete
date: 2011-04-07T06:45:45+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=1630
permalink: /2011/04/07/spool-ottoman-is-complete/
image: /files/2011/04/finished-ottoman.jpg
categories:
  - Crafts
tags:
  - crafts
  - decorating
  - designing
  - DIY
  - fabric
  - frugal
  - sewing
---
I have been working on my spool ottoman these last few days (and building a bench but that&#8217;s for another post) I don&#8217;t know if you saw the <a title="Upcoming homestead projects" href="http://quiltingthefarm.vius.ca/2011/03/upcoming-homestead-projects/" target="_blank">post with the spools</a> I acquired, but **the big one is now an ottoman**!

Here&#8217;s the original spool.

<a rel="attachment wp-att-1473" href="http://quiltingthefarm.vius.ca/2011/03/upcoming-homestead-projects/large-spool/"><img class="alignnone size-full wp-image-1473" title="large-spool" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/large-spool.jpg" alt="large wooden spool" width="412" height="309" /></a>

This is the fabric I found for the ottoman.

<a rel="attachment wp-att-1278" href="http://quiltingthefarm.vius.ca/2011/03/hubbys-rocking-chair/pillow-fabric/"><img class="alignnone size-full wp-image-1278" title="pillow-fabric" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/pillow-fabric.jpg" alt="fabric for new pillows" width="412" height="309" /></a>

**HOW TO MAKE A SPOOL OTTOMAN**

**For the top.** Cut a piece of 2 inch foam to fit the top and glue in place. Cover foam with a layer of batting <!--more-->(this helps smooth the edges and gives it a more finished look when covered).

<a rel="attachment wp-att-1631" href="http://quiltingthefarm.vius.ca/2011/04/spool-ottoman-is-complete/spool-with-foam-top/"><img class="alignnone size-full wp-image-1631" title="spool-with-foam-top" src="http://quiltingthefarm.plaidsheep.ca/files/2011/04/spool-with-foam-top.jpg" alt="spool with foam and batting" width="412" height="309" /></a>

Cut out a piece of fabric to fit the top and side with enough to pull to the under side. Pull snugly and staple in place.

<a rel="attachment wp-att-1639" href="http://quiltingthefarm.vius.ca/2011/04/spool-ottoman-is-complete/spool-with-fabric-attached-top/"><img class="alignnone size-full wp-image-1639" title="spool-with-fabric-attached-top" src="http://quiltingthefarm.plaidsheep.ca/files/2011/04/spool-with-fabric-attached-top.jpg" alt="fabric attached to spool top" width="412" height="309" /></a>

<a rel="attachment wp-att-1641" href="http://quiltingthefarm.vius.ca/2011/04/spool-ottoman-is-complete/spool-with-fabric-attached/"><img class="alignnone size-full wp-image-1641" title="spool-with-fabric-attached" src="http://quiltingthefarm.plaidsheep.ca/files/2011/04/spool-with-fabric-attached.jpg" alt="staple the fabric to the top" width="412" height="309" /></a>

**For the skirt.** Cut a piece of fabric 3 inches deeper than the height of the ottoman and 2 times the circumference. _This gives you extra for hemming and making pleats._

<a rel="attachment wp-att-1638" href="http://quiltingthefarm.vius.ca/2011/04/spool-ottoman-is-complete/spool-skirt-pieces/"><img class="alignnone size-full wp-image-1638" title="spool-skirt-pieces" src="http://quiltingthefarm.plaidsheep.ca/files/2011/04/spool-skirt-pieces.jpg" alt="ottoman skirt pieces" width="412" height="309" /></a>

Turn up 2 inches at bottom and turn edge under to make hem. _I used fusible web as I was way too lazy to sew!_

<a rel="attachment wp-att-1640" href="http://quiltingthefarm.vius.ca/2011/04/spool-ottoman-is-complete/spool-skirt/"><img class="alignnone size-full wp-image-1640" title="spool-skirt" src="http://quiltingthefarm.plaidsheep.ca/files/2011/04/spool-skirt.jpg" alt="hemming the ottoman skirt" width="412" height="309" /></a>

Turn under a small amount at the top and iron.

<a rel="attachment wp-att-1636" href="http://quiltingthefarm.vius.ca/2011/04/spool-ottoman-is-complete/spool-skirt-top-hem/"><img class="alignnone size-full wp-image-1636" title="spool-skirt-top-hem" src="http://quiltingthefarm.plaidsheep.ca/files/2011/04/spool-skirt-top-hem.jpg" alt="skirt top" width="412" height="309" /></a>

Pin skirt to spool, making pleats as you go around. Once you have all the pleats evenly spaced, staple skirt to spool.

<a rel="attachment wp-att-1635" href="http://quiltingthefarm.vius.ca/2011/04/spool-ottoman-is-complete/skirt-stapled-on-spool/"><img class="alignnone size-full wp-image-1635" title="skirt-stapled-on-spool" src="http://quiltingthefarm.plaidsheep.ca/files/2011/04/skirt-stapled-on-spool.jpg" alt="" width="412" height="309" /></a>

<a rel="attachment wp-att-1634" href="http://quiltingthefarm.vius.ca/2011/04/spool-ottoman-is-complete/stapled-skirt-of-ottoman/"><img class="alignnone size-full wp-image-1634" title="stapled-skirt-of-ottoman" src="http://quiltingthefarm.plaidsheep.ca/files/2011/04/stapled-skirt-of-ottoman.jpg" alt="skirt stapled to ottoman" width="412" height="309" /></a>

Make a fabric tube by cutting a 3 inch wide strip of fabric on the bias. You will need a piece the circumference of the ottoman plus 2 inches. (you may need to join pieces). Fold piece in half lengthwise and sew to make a long tube. Leave both ends open for stuffing.

Stuff with fibre fill. Test fit on ottoman top. Join ends to make a circle and slip stitch closed.

<a rel="attachment wp-att-1633" href="http://quiltingthefarm.vius.ca/2011/04/spool-ottoman-is-complete/fabric-tube/"><img class="alignnone size-full wp-image-1633" title="fabric-tube" src="http://quiltingthefarm.plaidsheep.ca/files/2011/04/fabric-tube.jpg" alt="fabric tube" width="412" height="309" /></a>

Pull tube over the top and hot glue in place to secure.

Here is the finished ottoman. To give the ottoman a more finished look, I added a grosgrain ribbon beneath the fabric tube.

<a rel="attachment wp-att-1632" href="http://quiltingthefarm.vius.ca/2011/04/spool-ottoman-is-complete/finished-ottoman/"><img class="alignnone size-full wp-image-1632" title="finished-ottoman" src="http://quiltingthefarm.plaidsheep.ca/files/2011/04/finished-ottoman.jpg" alt="finished spool ottoman" width="412" height="309" /></a>

I forgot to mention I turned to spool so the bolts were at the bottom. As the bolts are not flush with the bottom, I added tiny adhesive rubber feet I found at the hardware store.

<p style="text-align: center">
  ******
</p>