---
id: 1495
title: Mexi Bean Burgers
date: 2011-04-04T06:20:57+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=1495
permalink: /2011/04/04/mexi-bean-burgers/
image: /files/2011/03/burgers-in-pan.jpg
categories:
  - Kitchen
  - Recipes
tags:
  - beans
  - breads
  - food
  - frugal
  - recipes
---
I found this tasty recipe for <a title="Black Bean Burgers" href="http://chickensintheroad.com/farm-bell-recipes/making-black-bean-burgers/" target="_blank"><strong>Black Bean Burgers</strong></a> over at Chickens in the Road.

I had beans already soaking for my chili last night, so I stole 2 cups from that for this recipe. I changed things up a little as I used a variety of beans: red kidney, black and navy.

_**These burgers are very tasty and very large!**_ I recommend making at least 8 burgers (not 6) from this recipe as they are really filling.

_This recipe is suitable for vegans (which we are not) if <!--more-->you leave off the cheese._

<span id="MexiBean_Burgers">

<h3>
  Mexi Bean Burgers
</h3></span> 

  * 2 cups of various soaked and cooked beans, drained
  * 1/2 cup Bisquick
  * 1/2 cup fresh bread crumbs
  * 1 teaspoon chili powder
  * 1 teaspoon garlic powder
  * 1 teaspoon cumin
  * 1 teaspoon crushed red pepper flakes
  * 1 teaspoon sea salt
  * 1 teaspoon onion powder
  * 1 tablespoon ketchup
  * 1/4 cup water
  * vegetable oil for frying

Mash beans as well as you can. Mine had been in the fridge since yesterday and were pretty stiff! (I added a little warm water to make it easier). Add all the dry ingredients and mix well. Stir in ketchup and water if needed.

<a rel="attachment wp-att-1502" href="http://quiltingthefarm.vius.ca/2011/04/mexi-bean-burgers/mash-beans/"><img class="alignnone size-full wp-image-1502" title="mash-beans" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/mash-beans.jpg" alt="mashing drained beans" width="412" height="309" /></a>

Divide the mixture into 6 balls. Floured hands and counter make life easier 😉

Heat the oil in a frying pan. Add the balls and squish them a little to flatten into a burger shape . Fry for 7-8 minutes on one side, flip and do the same for the other side.

<a rel="attachment wp-att-1503" href="http://quiltingthefarm.vius.ca/2011/04/mexi-bean-burgers/fry-burgers/"><img class="alignnone size-full wp-image-1503" title="fry-burgers" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/fry-burgers.jpg" alt="frying burgers" width="412" height="283" /></a>

Add some cheese to the tops of the burgers.

<a rel="attachment wp-att-1506" href="http://quiltingthefarm.vius.ca/2011/04/mexi-bean-burgers/burgers-in-pan/"><img class="alignnone size-full wp-image-1506" title="burgers-in-pan" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/burgers-in-pan.jpg" alt="burgers with melting cheese" width="412" height="309" /></a>

Place burgers on homemade bread (I used oat rye bread).

<img class="alignnone size-full wp-image-1501" title="kneading-bread" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/kneading-bread.jpg" alt="Oat rye burger bread" width="412" height="309" />

<a rel="attachment wp-att-1504" href="http://quiltingthefarm.vius.ca/2011/04/mexi-bean-burgers/burger-bread/"><img class="alignnone size-full wp-image-1504" title="burger-bread" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/burger-bread.jpg" alt="sliced bread for burgers" width="412" height="309" /></a>

Top with lettuce, tomato, onions, salsa or any trimmings you desire. Serve with a heaping side of homemade potato salad, sit back in your lawn chair and dream of summer 🙂

<a rel="attachment wp-att-1505" href="http://quiltingthefarm.vius.ca/2011/04/mexi-bean-burgers/finished-burger/"><img class="alignnone size-full wp-image-1505" title="finished-burger" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/finished-burger.jpg" alt="Bean burger with all the toppings" width="412" height="309" /></a>

Linking this post up with <a title="Barn Hop" href="http://homesteadrevival.blogspot.com/2011/04/barn-hop-7.html" target="_blank">Barn Hop</a> at Homestead Revival and <a title="Tasty Tuesday Parade of Food" href="http://beautyandbedlam.com/tasty-tuesday-parade-of-foods-6/" target="_blank">Tasty Tuesday Parade of Foods </a>at Balancing Beauty and Bedlam

<p style="text-align: center">
  ******
</p>