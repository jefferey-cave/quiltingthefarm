---
id: 994
title: Lentil and Sausage Soup
date: 2011-03-06T08:50:56+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=994
permalink: /2011/03/06/lentil-and-sausage-soup/
tinymce_signature_post_setting:
  - ""
categories:
  - Kitchen
  - Recipes
tags:
  - beans
  - food
  - frugal
  - recipes
---
This soup is a wonderfully flavourful soup and a meal unto itself. Serve with a thick slice of homemade bread.

  * 4-6 hot Italian sausages
  * 1 large onion, chopped
  * 2 cloves garlic, minced
  * 1 (16 ounce) package dry lentils, rinsed
  * 1 cup grated carrot
  * 8 cups water
  * 3 cups chicken broth
  * 1 large can diced tomatoes
  * 3 bay leaves
  * 1/2 teaspoon dried oregano
  * 1/4 teaspoon dried thyme
  * 1/4 teaspoon dried basil
  * 1 tablespoon salt, or to taste
  * 1/2 teaspoon black pepper
<a rel="attachment wp-att-103" href="http://quiltingthefarm.vius.ca/2011/03/lentil-and-sausage-soup/lentilsoup/"><img class="size-full wp-image-103 alignnone" title="lentilsoup" src="http://quiltingthefarm.plaidsheep.ca/files/2011/01/lentilsoup.jpg" alt="lentil sausage soup ingredients" width="403" height="302" /></a>

<span id="Directions">

<h3>
  Directions
</h3></span> 

  1. Slice sausages and place  in a large pot. fry over medium heat until browned. Add onion and  garlic, and saute until tender and translucent. 
    <div id="attachment_102" style="width: 413px" class="wp-caption alignnone">
      <a rel="attachment wp-att-102" href="http://quiltingthefarm.vius.ca/2011/03/lentil-and-sausage-soup/lentilsoup2/"><img class="size-full wp-image-102 " title="lentilsoup2" src="http://quiltingthefarm.plaidsheep.ca/files/2011/01/lentilsoup2.jpg" alt="sausages for lentil soup" width="403" height="302" /></a>
      
      <p class="wp-caption-text">
        Slice the sausages first (I forgot this time!)
      </p>
    </div></li> 
    
      * Stir in lentils, carrot, chicken broth tomatoes and water, Season with parsley, bay leaves, oregano, thyme, basil, salt and pepper.
      * Bring to a boil, then reduce heat. Cover, and simmer for 2 1/2 to 3 hours, or until lentils are tender.</ol> 
    
    You can slice the sausages after you have browned them, but take my word for it, burnt fingers aren&#8217;t that pleasant!
    
    This post is I&#8217;m linking up with FFF and <a title="Balancing beauty and bedlam" href="http://beautyandbedlam.com/following-recipes/" target="_blank">Balancing Beauty and Bedlam</a>!
  
    <a href="http://www.verdefarm.com" target="_blank"><img class="alignnone" src="http://i1091.photobucket.com/albums/i390/VerdeFarm/farmgirlfridaybuttonforamycopy150.jpg" alt="" /></a>
    
    <p style="text-align: center">
      ******
    </p>