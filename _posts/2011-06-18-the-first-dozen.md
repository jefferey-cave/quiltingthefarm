---
id: 2268
title: The First Dozen
date: 2011-06-18T06:16:16+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2268
permalink: /2011/06/18/the-first-dozen/
categories:
  - Rural Living
---
We have our first <del>dozen</del> baker&#8217;s dozen (one late arrival) eggs from our new chickens. Our 12th egg was a bit of a fiasco. As we were about to leave this morning, Hubby  noticed one of the chickens (Annabelle) had what looked like an egg sticking out from her rear end. Annabelle was wandering around in the chicken yard so it was kind of hard to tell. I checked from over the fence and it certainly was an egg. We, being newbies were a little alarmed. Both of us dropped everything and came in the house to do some quick research. Hubby was concerned the chicken was egg bound (of course we have no idea what this actually looks like) and checking out ways to deal with this. Luckily, I came across a photo of a chicken walking around with an egg stuck to her butt feathers. Could that be it? We went back outside armed with a little more knowledge, vaseline (for a bound egg) and scissors (for a stuck egg). We sneaked into the chicken yard, Hubby on one side of the tree, me on the other trying to corner Annabelle. She froze, looked at us with disdain, then took off like a bat out of hell, leaving behind a perfect, but slightly feathered egg and a couple of giggling would be farmers. Crazy chicken.

So anyway, our Cochins are laying 2 eggs a day now (today we got 3) that gives us over a dozen each week, and it&#8217;s just about perfect for us. There are just the 2 of us, and a dozen eggs should take care of our eating and baking needs for a week. We have no plans on selling any, so any extras we&#8217;ll give away to a neighbour or two.

<a rel="attachment wp-att-2269" href="http://quiltingthefarm.vius.ca/2011/06/the-first-dozen/eggs-in-carton/"><img class="alignnone size-full wp-image-2269" title="eggs-in-carton" src="http://quiltingthefarm.plaidsheep.ca/files/2011/06/eggs-in-carton.jpg" alt="eggs in carton" width="412" height="309" /></a>

The total cost so far for our chickens has been $91.18 (this does not include the cost of materials for the coop itself as we including that as a property improvement). So far we have had 13 eggs therefore the cost per egg is $7.01. Assuming store bought eggs cost on average 22 cents per egg, our chicken project breaks even at 415 eggs (only 402 to go), that is almost 7 months from now. These costs do not take into account any feed, bedding, vaccines or otherwise the chickens will need during that time. As you can see, even starting small, keeping chickens isn&#8217;t <del>cheep</del> cheap 😉