---
id: 2408
title: First Eats of the Season
date: 2011-06-30T18:26:46+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2408
permalink: /2011/06/30/first-eats-of-the-season/
categories:
  - Garden
---
Today&#8230;

Sharing our fresh garden strawberries with the chickens, and eating salad from our own garden.

<a rel="attachment wp-att-2409" href="http://quiltingthefarm.vius.ca/2011/06/first-eats-of-the-season/lettuce/"><img class="alignnone size-full wp-image-2409" title="lettuce" src="http://quiltingthefarm.plaidsheep.ca/files/2011/06/lettuce.jpg" alt="lettuce" width="412" height="309" /></a>

This is what it&#8217;s all about.