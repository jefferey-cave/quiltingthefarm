---
id: 3164
title: 'It&#8217;s All In The Details'
date: 2011-09-17T07:00:57+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=3164
permalink: /2011/09/17/its-all-in-the-details/
tinymce_signature_post_setting:
  - 'yes'
categories:
  - Crafts
---
It&#8217;s all in the details, and those details are happening on a shoestring (budget).

To decorate the porch I hung some quilts over a ladder I made out of scraps of 2&#215;4 leftover from building the chicken coop. I sprayed painted it with the leftovers from the rocking chair. It&#8217;s definitely &#8220;rustic&#8221; looking, but it works well in the porch.

<a rel="attachment wp-att-3166" href="http://quiltingthefarm.vius.ca/2011/09/its-all-in-the-details/decorative-ladder/"><img class="alignnone size-full wp-image-3166" title="decorative-ladder" src="http://quiltingthefarm.plaidsheep.ca/files/2011/09/decorative-ladder.jpg" alt="" width="309" height="412" /></a>

I found this tin rooster in the garage when we moved in. Originally it was a rainbow of washed out and rusty colours. Now after a coat of spray paint it looks kind of cute. I&#8217;m not saying this would be something I would purchase &#8217;cause it&#8217;s definitely not, but for the &#8216;nothing&#8217; it cost it was well worth the effort of a quick spray paint!

<a rel="attachment wp-att-3169" href="http://quiltingthefarm.vius.ca/2011/09/its-all-in-the-details/blue-rooster/"><img class="alignnone size-full wp-image-3169" title="blue-rooster" src="http://quiltingthefarm.plaidsheep.ca/files/2011/09/blue-rooster.jpg" alt="" width="309" height="412" /></a>

Next up, a piece of wall art. All I needed for this was a sheet of water colour paper, a staghorn sumac leaf and some spray paint. I love the way it turned out, the movement of the leaves during the spraying gives the impression of the leaf blowing in a soft breeze. I will be painting some different leaves in different colours to go with this one.

<a rel="attachment wp-att-3168" href="http://quiltingthefarm.vius.ca/2011/09/its-all-in-the-details/leaf-picture/"><img class="alignnone size-full wp-image-3168" title="leaf-picture" src="http://quiltingthefarm.plaidsheep.ca/files/2011/09/leaf-picture.jpg" alt="" width="412" height="309" /></a>

Here is my little decorative grouping. I added a sea sponge I found at Mavillette Beach (here on the Bay of Fundy), and a apothecary jar filled with found shells and beach glass.

I still need to make or find a nice little shelf for everything to sit on. I should go sort through the wood scraps.

<a rel="attachment wp-att-3167" href="http://quiltingthefarm.vius.ca/2011/09/its-all-in-the-details/rooster-group/"><img class="alignnone size-full wp-image-3167" title="rooster-group" src="http://quiltingthefarm.plaidsheep.ca/files/2011/09/rooster-group.jpg" alt="" width="309" height="412" /></a>

As we are on the subject of shoestring budgets&#8230;do you remember that nasty looking dresser I picked up a couple of months ago?

<a rel="attachment wp-att-2822" href="http://quiltingthefarm.vius.ca/2011/08/project-day/old-dresser/"><img class="alignnone size-full wp-image-2822" title="old-dresser" src="http://quiltingthefarm.plaidsheep.ca/files/2011/08/old-dresser.jpg" alt="" width="412" height="309" /></a>

I have been working on it for the last couple of days, and it&#8217;s starting to look quite spiffy. I have painted the body of the dresser in a lovely &#8220;oops&#8221; cream colour. The top I stained with a stain colour I found in the basement. I don&#8217;t much care for the colour, so I will have to purchase something darker and go over it again ( I&#8217;m sure I can use dark stain on lots of other projects!)

<div id="attachment_3171" style="width: 422px" class="wp-caption alignnone">
  <a rel="attachment wp-att-3171" href="http://quiltingthefarm.vius.ca/2011/09/its-all-in-the-details/cream-dresser-2/"><img class="size-full wp-image-3171" title="cream-dresser-2" src="http://quiltingthefarm.plaidsheep.ca/files/2011/09/cream-dresser-2.jpg" alt="" width="412" height="309" /></a>
  
  <p class="wp-caption-text">
    still have to put a new piece on the back
  </p>
</div>

At this point, I&#8217;m not sure that the drawers are salvageable. Hubby syas he can cut some plywood to put in as shelves. I would love wicker baskets on the shelves, but that&#8217;s going to be out of my price range, so I&#8217;m thinking of making some burlap covered boxes with sisal handles. What do you think?

<a rel="attachment wp-att-3170" href="http://quiltingthefarm.vius.ca/2011/09/its-all-in-the-details/cream-dresser/"><img class="alignnone size-full wp-image-3170" title="cream-dresser" src="http://quiltingthefarm.plaidsheep.ca/files/2011/09/cream-dresser.jpg" alt="" width="412" height="309" /></a>

Wanna see my upcoming projects?

<a rel="attachment wp-att-3172" href="http://quiltingthefarm.vius.ca/2011/09/its-all-in-the-details/sewing-table/"><img class="alignnone size-full wp-image-3172" title="sewing-table" src="http://quiltingthefarm.plaidsheep.ca/files/2011/09/sewing-table.jpg" alt="" width="412" height="309" /></a>

<a rel="attachment wp-att-3173" href="http://quiltingthefarm.vius.ca/2011/09/its-all-in-the-details/blue-dresser/"><img class="alignnone size-full wp-image-3173" title="blue-dresser" src="http://quiltingthefarm.plaidsheep.ca/files/2011/09/blue-dresser.jpg" alt="" width="412" height="309" /></a>

Hubby says I have to start keeping my &#8220;junk&#8221; in the barn 🙂