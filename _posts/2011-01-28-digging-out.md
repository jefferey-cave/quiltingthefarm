---
id: 409
title: Digging Out!
date: 2011-01-28T18:36:38+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=409
permalink: /2011/01/28/digging-out/
tinymce_signature_post_setting:
  - 'yes'
image: /files/2011/01/digging-out9.jpg
categories:
  - Rural Living
tags:
  - snowstorms
  - tractor
  - weather
  - winter
---
The aftermath of the snowstorm

<div id='gallery-1' class='gallery galleryid-409 gallery-columns-2 gallery-size-thumbnail'>
  <dl class='gallery-item'>
    <dt class='gallery-icon landscape'>
      <a href='http://quiltingthefarm.plaidsheep.ca/2011/01/28/digging-out/digging-out9/'><img width="150" height="113" src="http://quiltingthefarm.plaidsheep.ca/files/2011/01/digging-out9.jpg" class="attachment-thumbnail size-thumbnail" alt="" aria-describedby="gallery-1-410" /></a>
    </dt>
    
    <dd class='wp-caption-text gallery-caption' id='gallery-1-410'>
      Hubby Staring wistfully at the big toys
    </dd>
  </dl>
  
  <dl class='gallery-item'>
    <dt class='gallery-icon landscape'>
      <a href='http://quiltingthefarm.plaidsheep.ca/2011/01/28/digging-out/digging-out1/'><img width="150" height="113" src="http://quiltingthefarm.plaidsheep.ca/files/2011/01/digging-out1.jpg" class="attachment-thumbnail size-thumbnail" alt="" aria-describedby="gallery-1-411" /></a>
    </dt>
    
    <dd class='wp-caption-text gallery-caption' id='gallery-1-411'>
      Are we getting out of the garage?
    </dd>
  </dl>
  
  <br style="clear: both" />
  
  <dl class='gallery-item'>
    <dt class='gallery-icon landscape'>
      <a href='http://quiltingthefarm.plaidsheep.ca/2011/01/28/digging-out/digging-out2/'><img width="150" height="113" src="http://quiltingthefarm.plaidsheep.ca/files/2011/01/digging-out2.jpg" class="attachment-thumbnail size-thumbnail" alt="" aria-describedby="gallery-1-412" /></a>
    </dt>
    
    <dd class='wp-caption-text gallery-caption' id='gallery-1-412'>
      That&#8217;s a whole lot of snow
    </dd>
  </dl>
  
  <dl class='gallery-item'>
    <dt class='gallery-icon landscape'>
      <a href='http://quiltingthefarm.plaidsheep.ca/2011/01/28/digging-out/digging-out3/'><img width="150" height="113" src="http://quiltingthefarm.plaidsheep.ca/files/2011/01/digging-out3.jpg" class="attachment-thumbnail size-thumbnail" alt="" aria-describedby="gallery-1-413" /></a>
    </dt>
    
    <dd class='wp-caption-text gallery-caption' id='gallery-1-413'>
      There&#8217;s a metal roof under there somewhere!
    </dd>
  </dl>
  
  <br style="clear: both" />
  
  <dl class='gallery-item'>
    <dt class='gallery-icon landscape'>
      <a href='http://quiltingthefarm.plaidsheep.ca/2011/01/28/digging-out/digging-out4/'><img width="150" height="113" src="http://quiltingthefarm.plaidsheep.ca/files/2011/01/digging-out4.jpg" class="attachment-thumbnail size-thumbnail" alt="" aria-describedby="gallery-1-414" /></a>
    </dt>
    
    <dd class='wp-caption-text gallery-caption' id='gallery-1-414'>
      Phew! That&#8217;s a long way down the driveway
    </dd>
  </dl>
  
  <dl class='gallery-item'>
    <dt class='gallery-icon landscape'>
      <a href='http://quiltingthefarm.plaidsheep.ca/2011/01/28/digging-out/digging-out6/'><img width="150" height="113" src="http://quiltingthefarm.plaidsheep.ca/files/2011/01/digging-out6.jpg" class="attachment-thumbnail size-thumbnail" alt="" aria-describedby="gallery-1-416" /></a>
    </dt>
    
    <dd class='wp-caption-text gallery-caption' id='gallery-1-416'>
      We won&#8217;t be using the front door anytime soon!
    </dd>
  </dl>
  
  <br style="clear: both" />
  
  <dl class='gallery-item'>
    <dt class='gallery-icon landscape'>
      <a href='http://quiltingthefarm.plaidsheep.ca/2011/01/28/digging-out/digging-out7/'><img width="150" height="113" src="http://quiltingthefarm.plaidsheep.ca/files/2011/01/digging-out7.jpg" class="attachment-thumbnail size-thumbnail" alt="" aria-describedby="gallery-1-417" /></a>
    </dt>
    
    <dd class='wp-caption-text gallery-caption' id='gallery-1-417'>
      Matching snow drifts on window and ground
    </dd>
  </dl>
  
  <dl class='gallery-item'>
    <dt class='gallery-icon landscape'>
      <a href='http://quiltingthefarm.plaidsheep.ca/2011/01/28/digging-out/digging-out8/'><img width="150" height="113" src="http://quiltingthefarm.plaidsheep.ca/files/2011/01/digging-out8.jpg" class="attachment-thumbnail size-thumbnail" alt="" aria-describedby="gallery-1-418" /></a>
    </dt>
    
    <dd class='wp-caption-text gallery-caption' id='gallery-1-418'>
      I&#8217;m glad no critters live in the barn yet
    </dd>
  </dl>
  
  <br style="clear: both" />
</div>