---
id: 1733
title: 'I&#8217;m Ticked Off!'
date: 2011-04-14T06:35:01+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=1733
permalink: /2011/04/14/im-ticked-off/
categories:
  - Rural Living
---
If you go out in the woods today you&#8217;re sure of a big surprise&#8230;yes, apparently it&#8217;s tick season already.

Hubby and I went for a walk on Tuesday and somehow brought home a couple of hitchhikers! Neither of us noticed them, neither of us thought to check ourselves. I&#8217;ve never even seen a wood tick before, so I&#8217;m not sure I would have thought it more than a small spider to be quickly eradicated with a brush of my hand.

On Wednesday morning, hubby opened up his laptop and there, on the screen, was a wood tick. That wood tick is now safely in a jar. A little while later I found, what I thought was a spider crawling on my hand, yep, you&#8217;ve guessed it, another wood tick. This too is safely in the jar on Hubby&#8217;s desk. Hubby has named his new found friends &#8220;Igg&#8221; and &#8220;Ook&#8221;.

<a rel="attachment wp-att-1734" href="http://quiltingthefarm.vius.ca/2011/04/im-ticked-off/ticks/"><img class="alignnone size-full wp-image-1734" title="ticks" src="http://quiltingthefarm.plaidsheep.ca/files/2011/04/ticks.jpg" alt="nasty wood ticks" width="412" height="309" /></a>

I can&#8217;t stop myself from itching now.

Ticks abound in areas of tall grass and brush of which we have lots.  Avoiding these areas is not really an option so we will have to take precautions like using DEET, wearing long pants, long sleeved shirts and hats and checking each other before we come in the house. Light-coloured clothing is also a good idea as ticks will be easier to spot if they do get on us. This is a unfortunate as Hubby&#8217;s Carhartts are dark brown (tick coloured)!

The government of Nova Scotia has some information on wood ticks and deer ticks <a title="wood ticks" href="http://www.gov.ns.ca/natr/forestprotection/foresthealth/sheets/tick.asp" target="_blank">here</a>

We are now beginning to rethink having guineas. We have heard that a couple of guineas can keep up to 2 acres clear of ticks. One of the previous owners of our farm kept a number of guineas&#8230;I can see why.

<p style="text-align: center">
  ******
</p>