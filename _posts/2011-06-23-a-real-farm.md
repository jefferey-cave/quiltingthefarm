---
id: 2312
title: A Real Farm
date: 2011-06-23T06:15:38+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2312
permalink: /2011/06/23/a-real-farm/
categories:
  - Rural Living
---
> &#8220;_Once upon a time there was a farm. It was a beautiful farm, a real farm. On this farm there roamed cows and sheep. Rolling pasture land and golden hay fields filled the view. Productive land worked at leisurely pace by a team of oxen. Today the fields are hidden from view, where there once were hay fields, there is now forest. Today this is our farm_.&#8221;

Our neighbours came over for a visit the other evening. The wife grew up right across the road, at the now abandoned and decaying homestead. She told us all about our farm and what it used to be like many years ago. It was motivating to know that this overgrown land once (not that long ago) was a vibrant and productive farm. beneath the forest canopy lies a majestic landscape, just waiting to be unearthed and re-fenced.

<a rel="attachment wp-att-2320" href="http://quiltingthefarm.vius.ca/2011/06/a-real-farm/freshcut-grass-2/"><img class="alignnone size-full wp-image-2320" title="freshcut-grass" src="http://quiltingthefarm.plaidsheep.ca/files/2011/06/freshcut-grass1.jpg" alt="our farm" width="412" height="309" /></a>

A lot of our questions have been answered now. We knew very little historyof this land, just the past 5 years or so and even that has  been piece-mealed together. Someone else&#8217;s memories sure bring things into focus. We have been wondering about raising cattle or sheep, and now we know that both thrived here in the past. A team of oxen worked this land back in the 40&#8217;s and 50&#8217;s. The great soil we keep finding was once pasture and hayland. When I close my eyes, I can see it all.

<a rel="attachment wp-att-2321" href="http://quiltingthefarm.vius.ca/2011/06/a-real-farm/blog-photos-may-092/"><img class="alignnone size-large wp-image-2321" title="our farm" src="http://quiltingthefarm.plaidsheep.ca/files/2011/06/blog-photos-may-092-1024x768.jpg" alt="our farm" width="412" height="309" /></a>

We have a lifetime of work ahead of us, but knowing that as late as 50 years ago this farm was what we have envisioned in our future. We thought we were building anew, but instead, we will work backwards in time, ending up at a place long ago where giant oxen roamed this small piece of green earth.

<p style="text-align: center">
  ******
</p>