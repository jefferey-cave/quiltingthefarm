---
id: 1304
title: Learning to Crochet
date: 2011-03-22T07:16:12+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=1304
permalink: /2011/03/22/learning-to-crochet/
image: /files/2011/03/crochet-piece-2.jpg
categories:
  - Crafts
tags:
  - crafts
  - DIY
---
<span id="I_have_never_up_until_this_point_in_my_life_never_mastered_the_fine_art_of_crochet.">

<h3>
  I have never, up until this point in my life, never mastered the fine art of crochet.
</h3></span> 

Over the last couple of days I&#8217;ve been feeling a little under the weather, so I thought I would put my _sitting-on-the-couch_ time to good use and tidy up my sewing box. While going through all the junk that had collected in there, I found these. I&#8217;m not sure how I acquired them. I certainly haven&#8217;t used them before.

<a rel="attachment wp-att-1308" href="http://quiltingthefarm.vius.ca/2011/03/learning-to-crochet/crochet-hooks/"><img class="alignnone size-full wp-image-1308" title="crochet-hooks" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/crochet-hooks.jpg" alt="crochet hooks" width="412" height="309" /></a>

Adrian, a friend of ours, lent me this beginners crochet book couple of weeks ago, so I thought I&#8217;d give this <!--more-->crochet &#8216;thing&#8217; a whirl.

<a rel="attachment wp-att-1306" href="http://quiltingthefarm.vius.ca/2011/03/learning-to-crochet/crochet-book/"><img class="alignnone size-full wp-image-1306" title="crochet-book" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/crochet-book.jpg" alt="Beginner's crochet book" width="412" height="309" /></a>

I&#8217;m totally surprised that _I_ could learn this from a book&#8230;but I did&#8230;well sort of 🙂

<a rel="attachment wp-att-1309" href="http://quiltingthefarm.vius.ca/2011/03/learning-to-crochet/crochet-piece-1/"><img class="alignnone size-full wp-image-1309" title="crochet-piece-1" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/crochet-piece-1.jpg" alt="first crochet piece" width="412" height="309" /></a>

This is my first piece. Yes, it was supposed to be square; not sure what happened there! But, as you can see, I&#8217;ve mastered _**some**_ of the stitches.

<a rel="attachment wp-att-1305" href="http://quiltingthefarm.vius.ca/2011/03/learning-to-crochet/crochet-piece-2/"><img class="alignnone size-full wp-image-1305" title="crochet-piece-2" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/crochet-piece-2.jpg" alt="second crochet piece" width="412" height="309" /></a>

I even tried a cluster/shell? stitch around the outside of this piece. Now, I do understand that this is no master piece, but I think I&#8217;m almost ready to crochet a granny square (or possibly a dish cloth). What do you think?

<a rel="attachment wp-att-1307" href="http://quiltingthefarm.vius.ca/2011/03/learning-to-crochet/crochet-hands/"><img class="alignnone size-full wp-image-1307" title="crochet-hands" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/crochet-hands.jpg" alt="crochet tension" width="412" height="309" /></a>

I&#8217;m still working on keeping the tension! This is the most difficult part for me, I even have trouble when I&#8217;m knitting. Maybe there&#8217;s a knack to this that I&#8217;m not aware of.