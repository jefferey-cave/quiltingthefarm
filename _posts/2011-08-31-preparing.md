---
id: 2999
title: Preparing
date: 2011-08-31T07:12:56+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2999
permalink: /2011/08/31/preparing/
tinymce_signature_post_setting:
  - 'yes'
categories:
  - Kitchen
---
My feet hurt 🙁

I have been standing in the kitchen all day trying to put up all the fruit we&#8217;ve picked. I have ripe tomatoes, juicy peaches, and windfall apples today, they are taking up way to much room in my fridge/counter and need to find a better home.

I peeled and chopped the last of the peaches then bagged them, with a light sugar syrup, in ziplock freezer bags. Peaches are just about the messiest fruit to work with. I wanted nicely cut peach slices, instead I got jagged, ragged bits and pieces _and_ I got soaked to the skin in the process (peaches are juicy little devils). I&#8217;m not sure I&#8217;d serve these peaches at a dinner party, but seeing as how guests are unusual occurance around here, it won&#8217;t matter. If I bake them in a pie no one will be any the wiser anyway.

<a rel="attachment wp-att-3000" href="http://quiltingthefarm.vius.ca/2011/08/preparing/prepared-peaches/"><img class="alignnone size-full wp-image-3000" title="prepared-peaches" src="http://quiltingthefarm.plaidsheep.ca/files/2011/08/prepared-peaches.jpg" alt="" /></a>

Next came the tomatoes. I had wanted to can some sauce, but this time, feeling to lazy to get out the canning equipment, I decided to whizz them up in my Magic Bullet and freeze them in baggies. I never even thought about doing this way until I read about it yesterday. It seems like a great idea, and so very easy.

<a rel="attachment wp-att-3002" href="http://quiltingthefarm.vius.ca/2011/08/preparing/chopping-tomatoes/"><img class="alignnone size-full wp-image-3002" title="chopping-tomatoes" src="http://quiltingthefarm.plaidsheep.ca/files/2011/08/chopping-tomatoes.jpg" alt="" width="309" height="412" /></a>

It looks like it worked well too, but I&#8217;ll have to wait for the taste test another day.

<a rel="attachment wp-att-3001" href="http://quiltingthefarm.vius.ca/2011/08/preparing/tomato-sauce/"><img class="alignnone size-full wp-image-3001" title="tomato-sauce" src="http://quiltingthefarm.plaidsheep.ca/files/2011/08/tomato-sauce.jpg" alt="" width="412" height="309" /></a>

And lastly, the apples. There are so many of them, I haven&#8217;t even got through the first bag yet! I made some juice, which is sitting in the freezer for a rainy day. Hopefully it will become apple jelly, but as you know, my foray into the &#8216; jelly&#8217; world hasn&#8217;t been that successful thus far. If I make it in the winter, I&#8217;ll be half a year older and much, much wiser in the ways of the world&#8230;won&#8217;t I?

I peeled and I cored and I cut&#8230;ok, I just got out my trusty little apple corer gadget and pushed down hard. Voila! cut and cored and ready for the pot. I didn&#8217;t bother peeling them; I think I would have gone crazy with all those fiddly little apples 😉

<a rel="attachment wp-att-3005" href="http://quiltingthefarm.vius.ca/2011/08/preparing/apple-corer/"><img class="alignnone size-full wp-image-3005" title="apple-corer" src="http://quiltingthefarm.plaidsheep.ca/files/2011/08/apple-corer.jpg" alt="" width="412" height="309" /></a>

Here is one bag I can&#8217;t belive there&#8217;s more and we aren&#8217;t even in apple season yet) of prepared apples. I added a teeny bit of water, boiled and simmered the apples until soft and squishy. I dumped the whole lot into a colander and used my potato masher to push it through. I ended up with a nice smooth apple puree sans skin. So much easier than peeling!

<a rel="attachment wp-att-3004" href="http://quiltingthefarm.vius.ca/2011/08/preparing/apples-cooking/"><img class="alignnone size-full wp-image-3004" title="apples-cooking" src="http://quiltingthefarm.plaidsheep.ca/files/2011/08/apples-cooking.jpg" alt="" width="412" height="309" /></a>

Once the apples were all pureed down I dumped the whole shabang in the crookpot, added a few spices (cinnamon, nutmeg, ginger, allspice) and some brown sugar. I&#8217;m leaving it to cook on low for 12 hours and hopefully at that time I&#8217;ll have apple butter ready to can for winter.