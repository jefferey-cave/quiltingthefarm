---
id: 765
title: Have Rose Hips, Make Syrup.
date: 2011-02-20T09:00:27+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=765
permalink: /2011/02/20/have-rose-hips-make-syrup/
image: /files/2011/02/rosehip_apple-syrup.jpg
categories:
  - Kitchen
  - Recipes
tags:
  - canning
  - country life
  - fruit
  - garden
  - preserving
  - recipes
---
If you grew up in England in the 60s and 70s you’ll probably remember Rosehip Syrup.

Rosehip Syrup was given, by the health authority, to all babies at that time. It is full of vitamin C, but unfortunately, like most syrups, it was seen as having too much sugar, and contributing to tooth decay in children.

Our homestead has an abundance of rose hips, they grow wild everywhere.

<div id="attachment_766" style="width: 310px" class="wp-caption aligncenter">
  <a rel="attachment wp-att-766" href="http://quiltingthefarm.vius.ca/2011/02/have-rose-hips-make-syrup/rose_hips/"><img class="size-medium wp-image-766" title="rose_hips" src="http://quiltingthefarm.plaidsheep.ca/files/2011/02/rose_hips-300x227.jpg" alt="" width="300" height="227" /></a>
  
  <p class="wp-caption-text">
    Fat juicy rose hips
  </p>
</div>

As we didn’t purchase our home until late last year, there wasn’t much to <!--more-->harvest, apart from a few apples and the aforementioned rose hips (rose hips are much tastier after the first frosts of Fall). So what do you do when you have rose hips? You make rose hip syrup!

I don’t actually have a specific recipe for this. I take a guess at the quantities, by tasting and hoping for the best!

  * <address>
      Rosehips – take of stalks
    </address>

  * <address>
      Sugar
    </address>

  * <address>
      Water
    </address>

 

  1. Boil the rose hips in water for about 20 minutes, mashing then down as necessary, until the water is nice and red (I told you there was no specific recipe for this!) 
  2. Drain the rose hips through a piece of cheesecloth into a bowl. Don’t squeeze the cheesecloth (although I got impatient and did!) You should now have a bowl of beautiful red, mostly clear, liquid. 
  3. Put the liquid in a pan, add the sugar and boil until syrupy and slightly thickened (about 10 &#8211; 15 minutes).
  4. Put into hot, sterilized jars and process in a water bath for 15 minutes.

<div id="attachment_767" style="width: 422px" class="wp-caption aligncenter">
  <a href="http://quiltingthefarm.plaidsheep.ca/files/2011/02/rosehip_apple-syrup.jpg"><img class="size-full wp-image-767 " title="rosehip_apple-syrup" src="http://quiltingthefarm.plaidsheep.ca/files/2011/02/rosehip_apple-syrup.jpg" alt="rose hip syrup" width="412" height="309" /></a>
  
  <p class="wp-caption-text">
    This is actually rosehip and apple syrup that I also canned. We have already used up all our rose hip syrup!
  </p>
</div>

 (I found this site that gives you lots of information on <a href="http://www.gardenguides.com/317-growing-harvesting-rose-hips.html" target="_blank">growing and harvesting rose hips</a> )

Rose hip syrup is great used on ice cream or pancakes.  My favourite use is to put a tablespoonful in a mug and top up with boiling water. It makes a sweet alternative to the ubiquitous chicken soup when you&#8217;re feeling a little under the weather.