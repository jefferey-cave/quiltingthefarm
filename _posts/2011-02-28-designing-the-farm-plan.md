---
id: 872
title: Designing the Farm Plan
date: 2011-02-28T07:43:38+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=872
permalink: /2011/02/28/designing-the-farm-plan/
image: /files/2011/02/map.gif
categories:
  - Rural Living
tags:
  - country life
  - designing
  - farm animals
  - food
  - fruit
  - garden
  - home
  - house
  - Nova Scotia
---
This is a map I have drawn of our homestead 4(?) acres as we envision it in the not so distant future.

<div class="mceTemp mceIEcenter" style="text-align: left">
  <dl id="attachment_873" class="wp-caption aligncenter" style="text-align: left;width: 438px">
    <dt class="wp-caption-dt">
      <a href="http://quiltingthefarm.plaidsheep.ca/files/2011/02/map.gif"><img class="size-full wp-image-873  " title="map of our home 4 acres" src="http://quiltingthefarm.plaidsheep.ca/files/2011/02/map.gif" alt="map of our home 4 acres" width="428" height="554" /></a>
    </dt>
    
    <dd class="wp-caption-dd">
      Map of our home 4 acres
    </dd>
  </dl>
  
  <p>
    I&#8217;m guessing at the actual acreage around the homestead; it could be more an 4 acres but probably no more than 6. The hundred acre woods is still a mystery to us. We have been unable to get back into the forest due to weather related adversities. I joke with hubby that there&#8217;s probably a condo development back there!
  </p>
</div>

<div class="mceTemp mceIEcenter" style="text-align: left">
  You can click on the map to see a slightly larger version, you will then be able to read the signs. Please let me know if you see any flaws in the farm design. We appreciate all the help we can get.
</div>