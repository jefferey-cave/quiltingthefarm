---
id: 1277
title: 'Hubby&#8217;s Rocking Chair'
date: 2011-03-21T06:40:40+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=1277
permalink: /2011/03/21/hubbys-rocking-chair/
image: /files/2011/03/rocking-chair-new.jpg
categories:
  - Crafts
tags:
  - crafts
  - designing
  - DIY
  - fabric
  - frugal
---
This is the Rocking Chair I bought Hubby for his birthday last year.

<a rel="attachment wp-att-1281" href="http://quiltingthefarm.vius.ca/2011/03/hubbys-rocking-chair/rocking-chair-old-2/"><img class="alignnone size-full wp-image-1281" title="rocking-chair-old" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/rocking-chair-old1.jpg" alt="rocking chair" width="412" height="309" /></a>

I found it on Kijiji, and knew right away he&#8217;d love it. It&#8217;s quarter sawn oak, was made in the 1920&#8217;s and badly needs a reupholstered seat cushion.

When I went to Walmart last week I picked up some upholstery fabric for $9. I didn&#8217;t particularly choose this green piece for the chair, but it worked so well.

I removed the old fabric cover to find an even older leather cover underneath. This also had to be removed&#8230;**_it was nasty!_** I had to wear gloves and a mask because of all the dust.

Anyway, with both covers removed, hubby pulled out all the old tacks and fixed the broken springs _(nobody wants wires poking them in the bottom_!). I stuffed the cushiony part with some heavy duty batting from a pillow I no longer needed, and stapled the new fabric on to the frame.

<a rel="attachment wp-att-1279" href="http://quiltingthefarm.vius.ca/2011/03/hubbys-rocking-chair/rocking-chair-new/"><img class="alignnone size-full wp-image-1279" title="rocking-chair-new" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/rocking-chair-new.jpg" alt="newly upholstered rocking chair " width="412" height="549" /></a>

What a difference! This was supposed to be a short and sweet project but ended up taking the better part of the afternoon with all the &#8216;fixing&#8217; we had to do, but we are thrilled with the results.

Next up are some new throw pillows or an ottoman from this stripey fabric (another one of my $9 pieces) that matches the colours in the area rug beneath the chair. This was PURE DUMB LUCK 🙂

<a rel="attachment wp-att-1278" href="http://quiltingthefarm.vius.ca/2011/03/hubbys-rocking-chair/pillow-fabric/"><img class="alignnone size-full wp-image-1278" title="pillow-fabric" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/pillow-fabric.jpg" alt="fabric for new pillows" width="412" height="309" /></a>

It&#8217;s all starting to come together.

<p style="text-align: center">
  ******
</p>