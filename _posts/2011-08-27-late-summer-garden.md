---
id: 2966
title: Late Summer Garden
date: 2011-08-27T07:00:06+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2966
permalink: /2011/08/27/late-summer-garden/
tinymce_signature_post_setting:
  - 'yes'
categories:
  - Garden
---
 Is it really late summer? It seems winter lasted a whole lot longer than this! Anyway, without further ado, here is my _late summer_ garden in pictures. Just opne note to add&#8230;my tomatoes seemed to have recovered from their blossom end rot 🙂

<a rel="attachment wp-att-2967" href="http://quiltingthefarm.vius.ca/2011/08/late-summer-garden/garden/"><img class="alignnone size-full wp-image-2967" title="garden" src="http://quiltingthefarm.plaidsheep.ca/files/2011/08/garden.jpg" alt="" width="412" height="309" /></a>

<a rel="attachment wp-att-2969" href="http://quiltingthefarm.vius.ca/2011/08/late-summer-garden/cabbage/"><img class="alignnone size-full wp-image-2969" title="cabbage" src="http://quiltingthefarm.plaidsheep.ca/files/2011/08/cabbage.jpg" alt="" width="412" height="309" /></a>

<a rel="attachment wp-att-2971" href="http://quiltingthefarm.vius.ca/2011/08/late-summer-garden/red-tomatoes/"><img class="alignnone size-full wp-image-2971" title="red-tomatoes" src="http://quiltingthefarm.plaidsheep.ca/files/2011/08/red-tomatoes.jpg" alt="" width="412" height="309" /></a>

<a rel="attachment wp-att-2970" href="http://quiltingthefarm.vius.ca/2011/08/late-summer-garden/green-tomatoes/"><img class="alignnone size-full wp-image-2970" title="green-tomatoes" src="http://quiltingthefarm.plaidsheep.ca/files/2011/08/green-tomatoes.jpg" alt="" width="412" height="309" /></a>

<a rel="attachment wp-att-2972" href="http://quiltingthefarm.vius.ca/2011/08/late-summer-garden/beans2/"><img class="alignnone size-full wp-image-2972" title="beans2" src="http://quiltingthefarm.plaidsheep.ca/files/2011/08/beans2.jpg" alt="" width="412" height="309" /></a>

<a rel="attachment wp-att-2973" href="http://quiltingthefarm.vius.ca/2011/08/late-summer-garden/rutabagas2/"><img class="alignnone size-full wp-image-2973" title="rutabagas2" src="http://quiltingthefarm.plaidsheep.ca/files/2011/08/rutabagas2.jpg" alt="" width="412" height="309" /></a>

<a rel="attachment wp-att-2976" href="http://quiltingthefarm.vius.ca/2011/08/late-summer-garden/squash2/"><img class="alignnone size-full wp-image-2976" title="squash2" src="http://quiltingthefarm.plaidsheep.ca/files/2011/08/squash2.jpg" alt="" width="412" height="309" /></a>

<a rel="attachment wp-att-2977" href="http://quiltingthefarm.vius.ca/2011/08/late-summer-garden/leeks2/"><img class="alignnone size-full wp-image-2977" title="leeks2" src="http://quiltingthefarm.plaidsheep.ca/files/2011/08/leeks2.jpg" alt="" width="412" height="309" /></a>

<a rel="attachment wp-att-2974" href="http://quiltingthefarm.vius.ca/2011/08/late-summer-garden/cucumbers/"><img class="alignnone size-full wp-image-2974" title="cucumbers" src="http://quiltingthefarm.plaidsheep.ca/files/2011/08/cucumbers.jpg" alt="" width="412" height="309" /></a>

<a rel="attachment wp-att-2975" href="http://quiltingthefarm.vius.ca/2011/08/late-summer-garden/carrots/"><img class="alignnone size-full wp-image-2975" title="carrots" src="http://quiltingthefarm.plaidsheep.ca/files/2011/08/carrots.jpg" alt="" width="412" height="309" /></a>