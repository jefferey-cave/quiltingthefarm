---
id: 2516
title: Every Cloud
date: 2011-07-12T06:58:12+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2516
permalink: /2011/07/12/every-cloud/
tinymce_signature_post_setting:
  - 'yes'
categories:
  - Rural Living
---
The day started like this.

<a rel="attachment wp-att-2521" href="http://quiltingthefarm.vius.ca/2011/07/every-cloud/tractor-and-brush-hog/"><img class="alignnone size-full wp-image-2521" title="tractor-and-brush-hog" src="http://quiltingthefarm.plaidsheep.ca/files/2011/07/tractor-and-brush-hog.jpg" alt="" width="412" height="309" /></a>

We have a field full of spreading dogbane. It has started to flower and is quite pretty, but behind that soft fragrance lies a toxic and invasive weed. We called in the &#8220;big guns&#8221; to &#8220;brush hog&#8221; our problem away&#8230;temporarily at least.

Now our land has some rocks, there&#8217;s no doubt about that. The guys came by the evening before and identified the main rocks and told us that it would be a slow and expensive job. _We needed it done_.

The tractor and driver arrived this morning and started work. He went at such a frantic pace, that both Hubby and I raised our eyebrows a few times&#8230;after all he was getting paid by the hour, so why the hurry?  We watched and cringed every time the mower hit a rock. After about 20 minutes our field was partly done,  unfortunately, so was the mower!

<a rel="attachment wp-att-2520" href="http://quiltingthefarm.vius.ca/2011/07/every-cloud/field-dogbane/"><img class="alignnone size-full wp-image-2520" title="field-dogbane" src="http://quiltingthefarm.plaidsheep.ca/files/2011/07/field-dogbane.jpg" alt="" width="412" height="309" /></a>

So, now what? A broken brush hog mower and a field full of dogbane. Hubby certainly wasn&#8217;t going to manage with our little garden tractor; that dogbane is over 3 feet tall!

<div id="attachment_2517" style="width: 422px" class="wp-caption alignnone">
  <a rel="attachment wp-att-2517" href="http://quiltingthefarm.vius.ca/2011/07/every-cloud/our-mower/"><img class="size-full wp-image-2517" title="our-mower" src="http://quiltingthefarm.plaidsheep.ca/files/2011/07/our-mower.jpg" alt="" width="412" height="309" /></a>
  
  <p class="wp-caption-text">
    Our little tractor is no match for the Dogbane
  </p>
</div>

 Hubby turns to this&#8230;

<a rel="attachment wp-att-2519" href="http://quiltingthefarm.vius.ca/2011/07/every-cloud/strimmer/"><img class="alignnone size-full wp-image-2519" title="strimmer" src="http://quiltingthefarm.plaidsheep.ca/files/2011/07/strimmer.jpg" alt="" width="412" height="309" /></a>

a hand held trimmer. It&#8217;s going to be a long and labourious job and not one that we expected when we got up this morning. But as they say, _**Every cloud&#8230;**_

_**&#8230;has a silver lining**_

**Blueberries!** Hundreds of blueberries bushes hidden in the overgrown and weedy field. And not just bushes, but fully ripe and delicious berries tucked under some of the tiny leaves. I&#8217;m in heaven. I think this may have been a blueberry field at one time and, had the brush hog mower not broken, it would have mowed down all the bushes I didn&#8217;t even know were there.

<div class="mceTemp">
  <dl id="attachment_2518" class="wp-caption alignnone" style="width: 422px">
    <dt class="wp-caption-dt">
      <a rel="attachment wp-att-2518" href="http://quiltingthefarm.vius.ca/2011/07/every-cloud/bowl-blueberries/"><img class="size-full wp-image-2518" title="bowl-blueberries" src="http://quiltingthefarm.plaidsheep.ca/files/2011/07/bowl-blueberries.jpg" alt="" width="412" height="309" /></a>
    </dt>
    
    <dd class="wp-caption-dd">
      Delicious blueberries
    </dd>
  </dl>
  
  <p>
    How&#8217;s that for a silver lining 🙂
  </p>
</div>