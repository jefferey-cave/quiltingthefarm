---
id: 1079
title: Seeds Arrived Today
date: 2011-03-11T08:00:32+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=1079
permalink: /2011/03/11/seeds-arrived-today/
image: /files/2011/03/seeds.jpg
categories:
  - Garden
tags:
  - food
  - garden
  - gardening
  - land
  - seeds
---
<span id="My_seeds_have_arrived">

<h2>
  My seeds have arrived!
</h2></span> 

[<img class="alignnone size-medium wp-image-1080" title="vegetable seeds" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/seeds-300x225.jpg" alt="vegetable seed packages from Annapolis Seeds" width="300" height="225" />](http://quiltingthefarm.plaidsheep.ca/files/2011/03/seeds.jpg)

Today all the seeds I ordered from <a title="Annapolis Seeds" href="http://www.annapolisseeds.com/" target="_blank">Annapolis Seeds</a> arrived in the mail. There seems to be a lot more than I had thought we had ordered, but on checking the list it seems everything is correct and accounted for. We are short just one batch; the Purple top Milan Turnips. They are all sold out for this year.

[<img class="alignnone size-medium wp-image-1081" title="vegetable-seeds" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/vegetable-seeds-300x225.jpg" alt="Vegetable seeds" width="300" height="225" />](http://quiltingthefarm.plaidsheep.ca/files/2011/03/vegetable-seeds.jpg)

Add to these seeds the ones I purchased from the local store and some herb seeds that a friend has offered me, I have a feeling my garden is going to be a little larger than I had anticipated.