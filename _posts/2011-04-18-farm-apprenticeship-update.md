---
id: 1784
title: Farm Apprenticeship Update
date: 2011-04-18T06:31:12+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=1784
permalink: /2011/04/18/farm-apprenticeship-update/
categories:
  - Rural Living
---
I just realized that I haven&#8217;t updated you on <a title="The Farm Apprentice" href="http://quiltingthefarm.vius.ca/2011/03/the-farm-apprentice/" target="_blank">Hubby&#8217;s farm apprenticeship</a>.

He did get the apprenticeship and will be starting at <a title="Hidden Meadow Farm" href="http://enjoyingthegoodlifeathiddenmeadowfarm.blogspot.com/" target="_blank">Hidden Meadow Farm</a> next month (after our family leaves). He will be learning many aspects of farming; from looking after livestock to building fences and working with farm machinery.  We would like to say &#8220;Thank you Stacey&#8221;. for giving Hubby this opportunity.

People have asked why Hubby is doing this. The answer is, we want to learn to do things the right way (not the hard way). As we have very little experience, one of the ways to get it is to learn from someone who has. Working with someone who operates a viable farm is a huge opportunity for us and Hubby is such a conscientious worker that he will be a great asset to anyone needing help. It&#8217;s a win, win situation. Stacey gets a person to help with farm chores and we get the ability to move forward, with confidence, in our homesteading life and not flounder about in the dark!

If you have a moment you need to head over to <a title="Hidden Meadow Farm" href="http://enjoyingthegoodlifeathiddenmeadowfarm.blogspot.com/" target="_blank">Hidden Meadow Farm</a> to see what they are up to. They have cool Highland cattle and new babies too!