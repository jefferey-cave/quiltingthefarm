---
id: 3029
title: Apple Butter
date: 2011-09-02T07:00:26+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=3029
permalink: /2011/09/02/apple-butter/
tinymce_signature_post_setting:
  - ""
categories:
  - Kitchen
  - Recipes
---
I have had a couple of requests for the apple butter recipe. I really don&#8217;t have a recipe per se, just some guidelines. Here&#8217;s what I did.

I cooked a grocery bag full of apples down to mush. Some of them I cooked whole, some I peeled, cored and cut. I don&#8217;t think it matters either way, just put them in a pot with a little bit of water and boil until soft. After the apples are mushsoft, drain them through a colander or sieve. After the juice drains through, I take a potato masher and mash as much pulp as I can through the sieve. I was left with a large bowl and smooth apple sauce.

Place the apple sauce in a crockpot. Add 1 cup brown sugar, 1 1/2 cups white sugar, 2 tsp cinnamon, 1 tsp allspice, 1 tsp nutmeg and 1/2 tsp of ginger. Cook on high for 1 hour, stiring occassionally so it doesn&#8217;t stick and burn. Turn crockpot to low and cook for 12 hours. You&#8217;ll want to prop the lid of the crockpot up a little so the steam can escape and the sauce can cook down to about 1/2 the volume. UPDATE: I forgot to say I also added a splash of apple cider vinegar 🙂

<a rel="attachment wp-att-3031" href="http://quiltingthefarm.vius.ca/2011/09/apple-butter/applebutter/"><img class="alignnone size-full wp-image-3031" title="applebutter" src="http://quiltingthefarm.plaidsheep.ca/files/2011/09/applebutter.jpg" alt="" width="412" height="309" /></a>

<a rel="attachment wp-att-3032" href="http://quiltingthefarm.vius.ca/2011/09/apple-butter/applesauce-canned/"><img class="alignnone size-full wp-image-3032" title="applesauce-canned" src="http://quiltingthefarm.plaidsheep.ca/files/2011/09/applesauce-canned.jpg" alt="" width="412" height="309" /></a>

Pour into sterized jars, seal and give them 10 minutes in a waterbath, then if you&#8217;re like me, open one and enjoy on toast!

P.S. The spices are only a guideline. I tasted mine as I went, and added more of this, and more of that until it tasted how I wanted.

I also have this recipe from Rosanne, one of my readers. I haven&#8217;t tried it yet because I need to go into town and find some of the cinnamon candies. If I find those candies, this is going to be my next venture. It sounds delicious.

** Apple butter recipe:**
  
8 cups applesauce
  
5 cups sugar
  
1/4 cup vinegar (can substitute lemon juice)
  
1 3/4 cup cinnamon drops candy (for flavor and coloring)
  
Boil 20 minutes, stirring constantly.
  
Makes 6 pints (may vary)
  
Place in jars, pressure 5 lb for 5 minutes