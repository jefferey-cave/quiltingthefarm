---
id: 3266
title: Roasted Tomato Sauce
date: 2011-09-28T07:12:36+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=3266
permalink: /2011/09/28/roasted-tomato-sauce/
tinymce_signature_post_setting:
  - 'yes'
categories:
  - Kitchen
  - Recipes
---
They&#8217;re multiplying like rabbits! My tomatoes that is 🙂

Having picked basket after basket after basket of ripe tomatoes there&#8217;s still no end in sight. I gave away 2 bags to some of our neighbours today, made tomato sauce, and there&#8217;s still a fridge full!

Todays tomato sauce was a recipe (kind of) from Flora at <a href="http://floramary.wordpress.com/2011/09/08/roasting-vegetables-baking-bread-and-drying-garlic/" target="_blank">Our Bear River Adventure</a>. It&#8217;s a roasted tomato sauce that uses many veggies from the garden; you can really put in whatever you like.

**Roasted Tomato Sauce**

In a large pan put:

  * as many cut up tomatoes as will fit (leave tomatoes in big chunks&#8230;I quartered some and halved the small ones)
  * 2 medium-sized tromboncino squash
  * 2 medium-sized onions
  * 4 cloves of garlic
  * 1 green pepper
  * 2 yellow peppers
  * 1/2 cup of loosely packed chopped fresh basil
  * 1/2 teaspoon of ground black pepper

<a rel="attachment wp-att-3267" href="http://quiltingthefarm.vius.ca/2011/09/roasted-tomato-sauce/sauce-ingredients/"><img class="alignnone size-full wp-image-3267" title="sauce-ingredients" src="http://quiltingthefarm.plaidsheep.ca/files/2011/09/sauce-ingredients.jpg" alt="" width="412" height="309" /></a>

1. Toss the ingredients with a generous amount of olive oil (1/4 cup)

2. Cover with foil and bake/roast for about an hour at 350 F

3. After an hour, remove the foil and continue roasting for another 20 or 30 min.

<a rel="attachment wp-att-3269" href="http://quiltingthefarm.vius.ca/2011/09/roasted-tomato-sauce/roasted-veg/"><img class="alignnone size-full wp-image-3269" title="roasted-veg" src="http://quiltingthefarm.plaidsheep.ca/files/2011/09/roasted-veg.jpg" alt="" width="412" height="309" /></a>

4. When the veggies are cool, run them through a food processor (or in my case my trusty Magic Bullet) and blend.

5. Divide into ziplock baggies (I put about 1 1/2 &#8211; 2 cups in each) and put in the freezer.

<a rel="attachment wp-att-3269" href="http://quiltingthefarm.vius.ca/2011/09/roasted-tomato-sauce/roasted-veg/"></a>

To be honest, the veggies looked so good straight out of the oven that they almost didn&#8217;t make it to the sauce stage. I did manage to scoop out a cup or so, add a little more seasoning and serve it over pasta for supper. It was rather delicious (and incredibly easy). This is one recipe I&#8217;ll be making again&#8230;probably tomorrow by the look of the garden again.

<a rel="attachment wp-att-3268" href="http://quiltingthefarm.vius.ca/2011/09/roasted-tomato-sauce/roasted-veg2/"><img class="alignnone size-full wp-image-3268" title="roasted-veg2" src="http://quiltingthefarm.plaidsheep.ca/files/2011/09/roasted-veg2.jpg" alt="" width="412" height="309" /></a>