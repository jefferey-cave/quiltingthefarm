---
id: 1314
title: Company for Dinner
date: 2011-05-07T07:00:54+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=1314
permalink: /2011/05/07/company-for-dinner/
categories:
  - Kitchen
  - Recipes
---
A few weekends ago we had some friends over for dinner. This was the first time I&#8217;ve really had an opportunity to cook for someone other than hubby in quite some time.

Our friends are vegetarian (we are not) so I had to come up with something suitable. A few weeks ago I made tortilla&#8217;s and re fried beans. They went over really well with Hubby I made those for the main course. For the appetizer I made a avocado and peach salad. For dessert, baked egg custard.

<span id="Avocado_and_Peach_Salad">

<h3>
  Avocado and Peach Salad
</h3></span> 

  * 1 avocado
  * 1 peach (I had to use canned as fresh are way out of season)
  * juice of 1 lime
  * freshly ground black pepper

 

<span id="Egg_Custard">

<h3>
  Egg Custard
</h3></span> 

I used this <a title="Lil egg custards" href="http://chickensintheroad.com/farm-bell-recipes/lil-egg-custards/" target="_blank">recipe</a> from Farm Bell Recipes at Chickens in the Road Blog

  * 5 large eggs
  * 1/3 cup sugar
  * 1 tsp. vanilla
  * 1 large can of evaporated milk (12 ounces)
  * Additional milk 1-1/5 cups
  * 8 Tbsp. brown sugar

<span id="Directions">

<h3>
  Directions
</h3></span> 

  1. Preheat oven to 350 degrees.
  2. Place 6 ramekins in a pan large enough to space them out so they don’t touch.
  3. Press 1 1/2 Tablespoons of brown sugar in the bottom of each ramekin.
  4. In a large measuring cup that holds at least 4 cups, crack all 5 eggs and add sugar and vanilla. Whisk for a minute. Add evaporated milk. Top up with additional milk until you have 4 cups of mixture.
  5. Pour gently into each ramekin until almost to the rim.
  6. Pour enough hot water into the pan until the water comes halfway up the sides of the ramekins.
  7. Bake until the centers don’t jiggle, about 20-30 minutes (this took quite a bit longer for me). Let cool on a rack, then refrigerate.

<p style="text-align: center">
  ******
</p>