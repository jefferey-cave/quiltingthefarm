---
id: 609
title: 'Country Men like the Colour &#8220;Wood&#8221;'
date: 2011-02-16T09:18:35+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=609
permalink: /2011/02/16/country-men-like-wood/
image: /files/2011/02/old-sideboard2.jpg
categories:
  - Crafts
tags:
  - decorating
  - designing
  - DIY
  - home
---
In real life, my job is graphic design. I design logos, business cards, websites, catalogues. You name it, I design it. I love playing with colour and I have a passion for orange. Before I met my hubby, I even arranged my bookcase based on colour (I am no longer allowed to do that as it drives hubby crazy – he’s an analyst).

Now that we have our homestead, I&#8217;m trying to revamp some antique (read: junk) furniture I&#8217;ve collected in <!--more-->the last few months. My passion for colour wants me to paint this old sideboard.

[<img class="size-thumbnail wp-image-616 alignnone" title="old-sideboard1" src="http://quiltingthefarm.plaidsheep.ca/files/2011/02/old-sideboard1-150x150.jpg" alt="" width="150" height="150" />](http://quiltingthefarm.plaidsheep.ca/files/2011/02/old-sideboard1.jpg)   [<img class="size-thumbnail wp-image-615 alignnone" title="old-sideboard2" src="http://quiltingthefarm.plaidsheep.ca/files/2011/02/old-sideboard2-150x150.jpg" alt="" width="150" height="150" />](http://quiltingthefarm.plaidsheep.ca/files/2011/02/old-sideboard2.jpg)

I’m debating whether to paint it red or turquoise.

I’m leaning toward red at the moment. What do you think? I nice dark red with a black undercoat and a little aging here and there?

[<img class="size-thumbnail wp-image-614  alignnone" title="side board 1" src="http://quiltingthefarm.plaidsheep.ca/files/2011/02/side-board-1-150x150.jpg" alt="" width="150" height="150" />](http://rusticwoods.blogspot.com/)   [<img class="size-thumbnail wp-image-612  alignnone" title="red sideboard" src="http://quiltingthefarm.plaidsheep.ca/files/2011/02/red-sideboard-150x150.jpg" alt="" width="150" height="150" />](http://quiltingthefarm.plaidsheep.ca/files/2011/02/red-sideboard.jpg)

[](http://quiltingthefarm.plaidsheep.ca/files/2011/02/red-sideboard.jpg)

or maybe turquoise? These are very pretty.

[<img class="size-thumbnail wp-image-610  alignnone" title="turquoise sideboard" src="http://quiltingthefarm.plaidsheep.ca/files/2011/02/turquoise-sideboard-150x150.jpg" alt="" width="150" height="150" />](http://clustres.blogspot.com/2010_06_01_archive.html)   [<img class="size-thumbnail wp-image-617  alignnone" title="bluesideboard" src="http://quiltingthefarm.plaidsheep.ca/files/2011/02/bluesideboard-150x150.jpg" alt="" width="150" height="150" />](http://afamiliarpath.com/2009/07/diy-blue-sideboard/)

Can&#8217;t ask Hubby because, like most men, Hubby thinks &#8216;wood&#8217; is the very best colour for it!