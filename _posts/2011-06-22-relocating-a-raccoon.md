---
id: 2314
title: Relocating a Raccoon
date: 2011-06-22T06:21:08+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2314
permalink: /2011/06/22/relocating-a-raccoon/
categories:
  - Rural Living
---
For the last few weeks there has been a raccoon living in our small barn.

It was our own fault. We had not got around to cleaning out all the hay and straw bedding that had been left behind when we moved in, making it an ideal place for wildlife to get comfortable. A female raccoon seized this opportunity, moved in and had her litter of four in one of the small stalls. Neither hubby nor I knew she was there until I went to clean out some manure one day and I came face to face with mother raccoon. She wasn&#8217;t happy. Nor was I.

<a rel="attachment wp-att-2099" href="http://quiltingthefarm.vius.ca/2011/05/hosta-heaven-or-hell/raccoon/"><img class="alignnone size-full wp-image-2099" title="raccoon" src="http://quiltingthefarm.plaidsheep.ca/files/2011/05/raccoon.jpg" alt="raccoon on side of barn" width="412" height="286" /></a>

<span>What to do? As we had no livestock at the time we decided to wait it out as long as we could or at least until the babies had a good chance of survival. I&#8217;m a softy when it comes to all animals and I didn&#8217;t want the babies to die. We lived (tentatively) side by side for a couple of weeks&#8230;that was until we got our chickens.</span>

Now the chicken coop is definitely predator safe, no raccoon is getting in there. As raccoons are nocturnal _and_ we were outside most of the day anyway, the chickens could range in their yard oblivious to the threat across the driveway<span>. We made sure the chickens were inside on lock-down long before dusk set in. This worked well for a while.</span>

All this changed the other day when I heard a clicking sound over by the barn. There, on the side of the building was mother raccoon&#8230;it was only 5:30 in the evening! I scurried the chickens inside and locked down the coop. This was too close for comfort. Hubby phoned the wildlife department who put us in touch with a Nuisance Wildlife Operator. He came by at around 8pm at night and set a live-catch trap baited with bread and bacon grease.  Next morning, Hubby checked the trap and sure enough we had ourselves a raccoon. We call the Nuisance Wildlife Operator back. He came straight out and picked up the trap and scooped up the snarling, hissing babies, obviously now big enough to fight back!  He mentioned that the mother and babies would probably have moved on very soon anyway as the babies were quite well grown.

The Nuisance Wildlife Operator loaded  mother and babies into his truck and off they went to their new home. They will be relocated far enough away from homes to stop them getting into trouble and found a spot that is safe and hidden from predators.

All this cost us the grand total of $35! There&#8217;s got to be an easier way to make a living. <span>You gotta love Nova <span>Scotia</span> 😉</span>