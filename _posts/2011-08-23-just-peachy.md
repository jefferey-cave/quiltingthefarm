---
id: 2925
title: Just Peachy
date: 2011-08-23T07:00:42+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2925
permalink: /2011/08/23/just-peachy/
aktt_notify_twitter:
  - 'yes'
tinymce_signature_post_setting:
  - 'yes'
categories:
  - Garden
---
Of all the fruit and vegetables in my gardens, this is the most prized for me&#8230;**_the peach_**. Having a peach tree seems exotic, a little tropical even.

This peach tree has survived without help from us. We didn&#8217;t even know it was there until it started to set fruit. From the day I saw the first fuzzy little orbs on that tree I kept my fingers crossed. The tree isn&#8217;t in good shape, having grown at an odd angle that looks like it will topple over in a light gust of wind. Diseases are an issue here. Peach trees in Nova Scotia have a tough time with leaf curl, and our tree is no exception. With hopes of containing the damage, I picked off all the diseased leaves I could reach, crossed my fingers again, and waited. I researched leaf curl and found out the fruit will never ripen. I held out  little hope, eventually resigning myself to the fact that there would be no peaches this year. But those little peaches kept on growing.

Today the tree is so heavy with peaches that some of the limbs are almost touching the grass. I waited until yesterday to make sure the &#8220;ground&#8221; side of the fruit had turned yellow, and I picked my very first peach.

<a rel="attachment wp-att-2926" href="http://quiltingthefarm.vius.ca/2011/08/just-peachy/first-ripe-peach/"><img class="alignnone size-full wp-image-2926" title="first-ripe-peach" src="http://quiltingthefarm.plaidsheep.ca/files/2011/08/first-ripe-peach.jpg" alt="" width="412" height="309" /></a>

It&#8217;s not quite perfect, but that&#8217;s what you get when you use no pesticides.

I&#8217;m just waiting for the rain to clear up, then I&#8217;m going to harvest the rest of the ripe ones. Peach crisp sounds awfully enticing!