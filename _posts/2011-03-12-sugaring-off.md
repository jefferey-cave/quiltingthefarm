---
id: 1120
title: Sugaring Off
date: 2011-03-12T08:05:17+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=1120
permalink: /2011/03/12/sugaring-off/
tinymce_signature_post_setting:
  - ""
image: /files/2011/03/sugaring-off2.jpg
categories:
  - Rural Living
tags:
  - country life
  - food
  - friends
  - land
  - Nova Scotia
  - weather
---
<span id="The_sap_is_running">

<h2>
  The sap is running!!
</h2></span> 

Today our neighbour stopped by and asked if we wanted to help collect sap. I was in the middle of painting the trim in the dining room, but what the heck. I didn&#8217;t even have time to change so off we went, me in my painting overalls and hubby covered in sawdust (he&#8217;s putting together my headboard bench).

The neighbour has around 50 taps on his homestead, and he gets between 1 &#8211; 3 gallons of Maple Syrup each year. Apparently, you need about 40 gallons of sap for each gallon of syrup, _**no wonder Maple Syrup is expensive**_.

<a rel="attachment wp-att-1128" href="http://quiltingthefarm.vius.ca/2011/03/sugaring-off/sugaring-off-2/"><img class="alignnone size-medium wp-image-1128" title="sugaring-off" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/sugaring-off-300x199.jpg" alt="" width="300" height="199" /></a>

We checked most of the 50 taps, some were running well, and some were not. Surprisingly the smaller trees were running the best. Perhaps they warm up quicker and therefore the sap runs earlier. The sap is being collected in 2 litre pop bottles (I&#8217;m so glad I saved a whole load of those) as they work well and are free. The collected sap is then emptied into a larger 10 gallon bucket.

<a rel="attachment wp-att-1135" href="http://quiltingthefarm.vius.ca/2011/03/sugaring-off/pop-bottle-sap-collector/"><img class="alignnone size-medium wp-image-1135" title="pop-bottle-sap-collector" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/pop-bottle-sap-collector-267x300.jpg" alt="pop bottle maple sap collector" width="267" height="300" /></a>

All in all we ended up with almost 2 full buckets of sap.

<a rel="attachment wp-att-1130" href="http://quiltingthefarm.vius.ca/2011/03/sugaring-off/sugaring-off3/"><img class="alignnone size-medium wp-image-1130" title="sugaring-off3" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/sugaring-off3-200x300.jpg" alt="Tapping Maple trees" width="170" height="246" /></a>    <a rel="attachment wp-att-1129" href="http://quiltingthefarm.vius.ca/2011/03/sugaring-off/sugaring-off2/"><img class="alignnone size-medium wp-image-1129" title="sugaring-off2" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/sugaring-off2-259x300.jpg" alt="Tapping maple trees" width="192" height="246" /></a>

Did you know that Maple Syrup can be good for you? According to a professor from the University of Rhode Island, real Maple Syrup is full of compounds touted for their health benefits <a title="Health benefits of Maple Syrup" href="http://www.thestar.com/living/food/article/783558--maple-syrup-is-good-for-your-health-just-not-your-waist?bn=1" target="_blank">Read more.</a>

I wonder if we&#8217;ll get a little taste of what we collected today?

<p style="text-align: center">
  ******
</p>