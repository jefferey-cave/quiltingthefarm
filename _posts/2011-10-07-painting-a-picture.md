---
id: 3338
title: Painting a Picture
date: 2011-10-07T07:04:13+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=3338
permalink: /2011/10/07/painting-a-picture/
tinymce_signature_post_setting:
  - 'yes'
categories:
  - Crafts
---
Yesterday I painted. I don&#8217;t mean a wall or a ceiling, but a canvas.

I&#8217;ve always loved to draw and paint. At one time I was pretty good at it, but life got busy, and hobbies came and went and I haven&#8217;t painted in many, many years. When we decided to move here, to this life, I said I would get back into my art. Here seemed the perfect place to do that, but again life, farm life, got in the way.

While rummaging around in my sewing room I came across a painting canvas and I remembered. Sometime, it must have been a few years ago, I got this canvas for Christmas. With the canvas came watercolours. I had wanted the canvas, I had been excited to paint again, but unfortunately it sat, in pristine condition, on a shelf, then in a box, and back on a shelf. Once in a while I would look at the canvas and say to myself &#8220;I must paint a picture someday&#8221; Well, it was always some day never today, that was until yesterday.

I hauled out the canvas and played around. I waited for a while, for the perfect idea, but that didn&#8217;t come, so I just picked up a brush and started putting paint on the canvas. It felt good. I loved the way the colours flowed together, the sensation of the paint gliding across the rough canvas. I started with a little of this and a little of that. It was a sky and land&#8230;no it was water&#8230;no it was an abstract, then yet again perhaps it was water. I put paint on, I took paint off. Now the canvas sits drying on the table. I haven&#8217;t painted anything in particular, just painted, but it felt so good.