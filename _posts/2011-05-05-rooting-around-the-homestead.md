---
id: 1744
title: Rooting Around the Homestead
date: 2011-05-05T06:40:16+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=1744
permalink: /2011/05/05/rooting-around-the-homestead/
categories:
  - Rural Living
---
I&#8217;ve been enjoying rooting around our farm for treasures left behind from previous owners. I&#8217;ve found a few neat old things here and there like this old wooden box.

<a rel="attachment wp-att-1746" href="http://quiltingthefarm.vius.ca/2011/05/rooting-around-the-homestead/box1/"><img class="alignnone size-full wp-image-1746" title="box1" src="http://quiltingthefarm.plaidsheep.ca/files/2011/04/box1.jpg" alt="outside of box" width="412" height="549" /></a>

Look at the old chippy paint&#8230;it&#8217;s kind of cool.

<a rel="attachment wp-att-1745" href="http://quiltingthefarm.vius.ca/2011/05/rooting-around-the-homestead/box-inside/"><img class="alignnone size-full wp-image-1745" title="box-inside" src="http://quiltingthefarm.plaidsheep.ca/files/2011/04/box-inside.jpg" alt="inside of wooden box" width="412" height="549" /></a>

The wooden pieces acroos the sides just scream &#8220;bookcase&#8221; to me. What do you think?

<a rel="attachment wp-att-1747" href="http://quiltingthefarm.vius.ca/2011/05/rooting-around-the-homestead/box-handle/"><img class="alignnone size-full wp-image-1747" title="box-handle" src="http://quiltingthefarm.plaidsheep.ca/files/2011/04/box-handle.jpg" alt="rusted on handle" width="412" height="309" /></a>

The handle is rusted on and will not be coming off! I think it adds a certain something 🙂

<p style="text-align: center">
  ******
</p>