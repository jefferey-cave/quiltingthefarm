---
id: 968
title: The Kitchen Plan
date: 2011-03-05T09:28:39+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=968
permalink: /2011/03/05/the-kitchen-plan/
tinymce_signature_post_setting:
  - ""
categories:
  - Kitchen
tags:
  - country life
  - decorating
  - designing
  - DIY
  - frugal
  - home
---
After my company leaves in May (family coming to visit), hubby and I are going to give the kitchen of our ugly farmhouse a bit of a face lift. We are on a strict budget for this. We are not renovating the house now (if ever) just making it more liveable.

**_Here&#8217;s my kitchen_**

<div id="attachment_983" style="width: 308px" class="wp-caption alignnone">
  <a rel="attachment wp-att-983" href="http://quiltingthefarm.vius.ca/2011/03/the-kitchen-plan/kitchen-2/"><img class="size-full wp-image-983 " title="kitchen" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/kitchen.jpg" alt="kitchen" width="298" height="219" /></a>
  
  <p class="wp-caption-text">
    My kitchen (this is not my photo but the one from the listing realtor)
  </p>
</div>

_**Here is my inspiration pic**_

<a rel="attachment wp-att-969" href="http://quiltingthefarm.vius.ca/2011/03/the-kitchen-plan/kitchen/"><img class="size-full wp-image-969 alignnone" title="kitchen" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/kitchen.bmp" alt="inspiration kitchen" /></a>

This is the plan so far

  * Take down all upper cabinets and install open shelving each side of the window. I love the airy feeling of open shelving (had it in my last house).
  * Move the tall narrow cabinet to the other side of the fridge and extend the counter to the fridge.
  * Install taller window – The kitchen window was never installed properly, doesn’t close properly and leaks air everywhere, so we need a new one anyway. A taller window will let in more light in to the dark dining area
  * Take down awful vanity lights and put pendant light over sink instead.
  * Use some of the upper cabinets to build a floor cabinet and add a butcher block top.
  * Paint lower cabinets and add new hardware
  * Put in a new floor ( I love this <a title="plywood floor" href="http://littlehomesteadinthevalley.blogspot.com/2011/02/future-den-aftermath.html" target="_blank">floor</a> from Little Homestead in the Valley)
  * replace leaky sink faucet

I have probably forgotten something important, so if you see a missing link please let me know!