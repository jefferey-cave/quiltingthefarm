---
id: 2674
title: Growing and Harvesting
date: 2011-08-02T06:54:46+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2674
permalink: /2011/08/02/growing-and-harvesting/
tinymce_signature_post_setting:
  - 'yes'
categories:
  - Garden
---
This was yesterday&#8217;s harvest from my small garden.

<a rel="attachment wp-att-2675" href="http://quiltingthefarm.vius.ca/2011/08/growing-and-harvesting/veggie-harvest/"><img class="alignnone size-full wp-image-2675" title="veggie-harvest" src="http://quiltingthefarm.plaidsheep.ca/files/2011/08/veggie-harvest.jpg" alt="" width="412" height="309" /></a>

There are some successes, and some failures. The cucumbers are growing like bamboo. I&#8217;m sure I could sit and actually watch them grow! Last week the fruit was the thickness of a pencil&#8230;this week&#8230;wow! and there&#8217;s more of them like this. The tomatoes are turning red day by day. I&#8217;m looking to can some tomato sauce for pizza and spaghetti, I hope I get enough&#8230;even for a small batch. These are two of my successes.

The beets are getting big now, so it&#8217;s whole beets for supper, not just the thinnings! Under the beets in the basket, there&#8217;s loads of swiss chard and a little kale. The swiss chard is doing really well (success), the kale&#8230;not so much 🙁 (failure)

The lettuce is on its last legs&#8230;not that it ever really had legs to begin with 🙁 Mostly the lettuce waned, which is really disappointing because we eat salad every day during the summer. Lettuce is one of my big failures. I guess I&#8217;ll be buying it&#8230;again.

And finally&#8230;potatoes! I emptied out one of my potato tubs. They really aren&#8217;t quite ready yet, I should leave them a couple more weeks, but they are certainly better than I expected. At this point, potatoes are a success. I just hope it&#8217;s still like that when I get to harvest the rest of them.

<a rel="attachment wp-att-2676" href="http://quiltingthefarm.vius.ca/2011/08/growing-and-harvesting/new-potatoes/"><img class="alignnone size-full wp-image-2676" title="new-potatoes" src="http://quiltingthefarm.plaidsheep.ca/files/2011/08/new-potatoes.jpg" alt="" width="412" height="309" /></a>

There&#8217;s still carrots, beans, rutabagas, parsnips, squash, cabbage, corn and peas. Although they are not ready to be harvested yet, some, like the peas, seem to be a success, others (parsnips, beans) don&#8217;t look as good as I&#8217;d expect. It will be a waiting game now.

So far, it&#8217;s been a good learning experience for me. Next year there are things that I will keep the same and things that I&#8217;ll do differently. Hubby and I measured a space for the new garden. We have a 50ft x 50ft plot that will become garden, I&#8217;m just not sure how much we&#8217;ll till up for next year yet.