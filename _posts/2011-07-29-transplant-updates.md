---
id: 2647
title: Transplant Updates
date: 2011-07-29T06:55:27+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2647
permalink: /2011/07/29/transplant-updates/
tinymce_signature_post_setting:
  - 'yes'
categories:
  - Garden
---
A few weeks ago, our old realtor had given us some <a href="http://quiltingthefarm.vius.ca/2011/07/free-fruit-bushes-and-more/" target="_blank">fruit bushes and other stuff </a>from her garden as she was moving house. We went over and dug up red currant bushes, thornless blackberry bushes, rhubarb, horseradish, chives and celeriac. Here&#8217;s an update of those plants now that they have had time to adjust (or die) in my garden.

This blackberry bush has many new leaves. I think it will make it here. The other bush doesn&#8217;t look as good, but I have high hopes.

<a rel="attachment wp-att-2649" href="http://quiltingthefarm.vius.ca/2011/07/transplant-updates/blackberry/"><img class="alignnone size-full wp-image-2649" title="blackberry" src="http://quiltingthefarm.plaidsheep.ca/files/2011/07/blackberry.jpg" alt="" width="412" height="309" /></a>

The red currants aren&#8217;t doing much. I&#8217;m not sure if they&#8217;ll make it or not. There are no new leaves and I can see no signs of life. I hope they are just in shock or hibenation mode.

We did get the fruit bushes in the ground early the morning after we got them. Perhaps we waited too long, but it was dark when we got home so there was no doing them at that time.In the end we just dug a hole, watered&#8230;lots and put a little bit of manure over the top as well.

The rhubarb was left for 2 days before I had time to put it in the ground. So far I haven&#8217;t had much luck with rhubarb, so expected little this time. I was pleasantly surprised to see these.

<a rel="attachment wp-att-2652" href="http://quiltingthefarm.vius.ca/2011/07/transplant-updates/rhubarb1/"><img class="alignnone size-full wp-image-2652" title="rhubarb1" src="http://quiltingthefarm.plaidsheep.ca/files/2011/07/rhubarb1.jpg" alt="" width="412" height="309" /></a>

<a rel="attachment wp-att-2651" href="http://quiltingthefarm.vius.ca/2011/07/transplant-updates/rhubarb2/"><img class="alignnone size-full wp-image-2651" title="rhubarb2" src="http://quiltingthefarm.plaidsheep.ca/files/2011/07/rhubarb2.jpg" alt="" width="412" height="309" /></a>

Both plants are doing well and sending up new shoots.

The celeriac and horseradish also got left a couple of days, but here they are in all their glory.

<a rel="attachment wp-att-2650" href="http://quiltingthefarm.vius.ca/2011/07/transplant-updates/celeriac/"><img class="alignnone size-full wp-image-2650" title="celeriac" src="http://quiltingthefarm.plaidsheep.ca/files/2011/07/celeriac.jpg" alt="" width="412" height="309" /></a>

<a rel="attachment wp-att-2653" href="http://quiltingthefarm.vius.ca/2011/07/transplant-updates/horseradish/"><img class="alignnone size-full wp-image-2653" title="horseradish" src="http://quiltingthefarm.plaidsheep.ca/files/2011/07/horseradish.jpg" alt="" width="412" height="309" /></a>

It&#8217;s a little bit weedy&#8230;okay, a lot weedy, where I planted these. They seem to be competing fine, so perhaps they&#8217;ll block the weeds from coming up (wishful thinking, I know).

Oops, I forgot to take pics of the chives. I wasn&#8217;t sure they were doing anything, but withering away. Yesterday I noticed some new green bits poking up, so it does look like some rooted.

Unfortunately, the grape vine cuttings are dead. I tried rooting gel before potting them up, but it was a no-go. I&#8217;ll have to try again sometime, but I don&#8217;t really have a place for them yet, so it&#8217;s probably best to wait.