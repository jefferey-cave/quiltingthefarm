---
id: 2243
title: Sunshine and Rain
date: 2011-06-13T06:15:49+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2243
permalink: /2011/06/13/sunshine-and-rain/
categories:
  - Garden
---
There was sunshine and there was rain. Today was a day that couldn&#8217;t make up its mind. The chickens were in. The chickens were out. They don&#8217;t much care for the rain even though it was really just a light sprinkle.

We have been working on putting up a fence around the raised garden beds. Hubby cut down some scrub trees for fence posts. We  were lucky to have discovered the old garden &#8211; or at least an old planting area &#8211; as the ground is quite soft and the soil a sandy loam with hardly a rock. For this we give thanks to a previous owner or two. We had expectations of a thin layer of topsoil and then rock, and by that I mean granite boulders. It&#8217;s more than a pleasant surprise to find we are able to just push fenceposts in by more than a foot and pound them in from there, no digging required. We must have about a foot of topsoil which is awesome. I&#8217;m keeping my fingers crossed for a whole field of this, you never know.

We planted the raised beds on Wednesday and already we have green shoots. The green shoots may of course be weeds, we&#8217;ll have to wait a while to identify them before we do anything drastic . The cucumbers I transplanted don&#8217;t look so great but I did notice they have tiny cucumbers on them. Hopefully they will recover from whatever ails them.

The rest of my veggie planting is going on elsewhere. The planters outside the house have lettuce, radishes and peas coming along nicely. The herb gardens are full and lush (previous owner to thank here) except for the unfortunate episode of the vanishing chives and lavender. I am hoping for good things from my potatoes. A few weeks ago I planted potatoes in buckets, 15 buckets to be exact and they are all sitting on my deck. Some of them are now right at the top of the bucket and the others are not far behind.

We took a walk along the first part of our trail into the 100 acre woods and found it just full of raspberry and blackberry canes, all in flower, and some unidentified berries. So along with the berries we have apples, pears and hopefully a peach or two, It looks like it&#8217;s going to be a great fruit harvest this year. I can barely wait 🙂