---
id: 886
title: The Farm Apprentice
date: 2011-03-01T08:29:52+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=886
permalink: /2011/03/01/the-farm-apprentice/
image: /files/2011/03/apprentice1.png
categories:
  - Rural Living
tags:
  - chickens
  - country life
  - farm
  - farm animals
  - gardening
  - livestock
---
[](http://quiltingthefarm.plaidsheep.ca/files/2011/03/apprentice.png)

<h3 style="text-align: center">
  <a rel="attachment wp-att-892" href="http://quiltingthefarm.vius.ca/2011/03/the-farm-apprentice/apprentice-2/"><img class="alignleft size-full wp-image-892" title="apprentice" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/apprentice1.png" alt="The apprentice sign" width="131" height="199" /></a>It looks like Hubby has found a<br /> part-time position as a<br /> Farm Apprentice.
</h3>

As you all know, we don&#8217;t know a whole lot about farming, so what better way to learn than with hands-on experience at another Farm here in the Valley. Hubby will get to work with livestock (Highland Cattle, sheep, chickens), fencing, farm machinery and learn some gardening skills,

He still has to meet with the owners (waiting for better weather), I&#8217;m sure they&#8217;ll love him!

I&#8217;ll let you know how it goes.

<p style="text-align: center">
  ******
</p>