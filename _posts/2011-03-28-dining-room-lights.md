---
id: 1365
title: Dining Room Lights
date: 2011-03-28T07:23:01+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=1365
permalink: /2011/03/28/dining-room-lights/
image: /files/2011/03/new-lights-1.jpg
categories:
  - Rural Living
tags:
  - country life
  - designing
  - DIY
  - frugal
  - house
  - painting
---
These were my dining room light fixtures. I think we can all agree that they were very, very ugly 🙁

I had already painted the dining room a sunny yellow to brighten it up. I had also spray painted that little shelf and added coat hooks below. The dining room was coming together, well, except for those horrid fixtures!

<a rel="attachment wp-att-1366" href="http://quiltingthefarm.vius.ca/2011/03/dining-room-lights/old-lights-2/"><img class="alignnone size-full wp-image-1366" title="old-lights-2" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/old-lights-2.jpg" alt="Old dining room fixtures" width="412" height="309" /></a>

Last week we went shopping (we had a gift card from Home Depot for Christmas) for new lights. After searching for something nice and suitably priced (and finding nothing) we ended up in the outdoor fixtures section and came across these lanterns.

<div class="mceTemp">
  <dl id="attachment_1411" class="wp-caption alignnone" style="width: 393px">
    <dt class="wp-caption-dt">
      <a rel="attachment wp-att-1411" href="http://quiltingthefarm.vius.ca/2011/03/dining-room-lights/lantern/"><img class="size-full wp-image-1411" title="lantern" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/lantern.jpg" alt="outdoor lanterns" width="383" height="400" /></a>
    </dt>
    
    <dd class="wp-caption-dd">
      picture courtesy Home Depot
    </dd>
  </dl>
  
  <p>
    And best of all they were priced at $29.99 for a pair.
  </p>
</div>

<div class="mceTemp">
  Hubby installed them, but like all projects in this old farmhouse, the wiring gave us a few surprises and a rather long winded installation.
</div>

<div class="mceTemp">
  <a rel="attachment wp-att-1369" href="http://quiltingthefarm.vius.ca/2011/03/dining-room-lights/new-lights-3/"><img class="alignnone size-full wp-image-1369" title="new-lights-3" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/new-lights-3.jpg" alt="Outdoor lanterns" width="412" height="309" /></a>
</div>

<div class="mceTemp">
  Definitely an improvement!
</div>

<div class="mceTemp">
  <a rel="attachment wp-att-1367" href="http://quiltingthefarm.vius.ca/2011/03/dining-room-lights/new-lights-1/"></a>
</div>

<div class="mceTemp">
  <a rel="attachment wp-att-1368" href="http://quiltingthefarm.vius.ca/2011/03/dining-room-lights/new-lights-2/"><img class="alignnone size-full wp-image-1368" title="new-lights-2" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/new-lights-2.jpg" alt="lantern" width="412" height="309" /></a>
</div>

<div class="mceTemp">
  The matching one on the other side of the patio door.
</div>

<div class="mceTemp">
  Now we have to find a ceiling fixture for above the table. I really like this<a title="Revamped light fixture" href="http://thriftydecorchick.blogspot.com/2011/03/this-little-5-light-of-mine.html" target="_blank"> idea</a> from Thrifty Decor Chick, so all I need now is an old light fixture to breathe new life into.
</div>

<div class="mceTemp" style="text-align: center">
  ******
</div>