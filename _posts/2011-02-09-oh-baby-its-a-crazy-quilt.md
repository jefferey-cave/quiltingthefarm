---
id: 579
title: 'Oh Baby, It&#8217;s a Crazy Quilt!'
date: 2011-02-09T10:07:02+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=579
permalink: /2011/02/09/oh-baby-its-a-crazy-quilt/
image: /files/2011/02/quilttop.jpg
categories:
  - Crafts
tags:
  - babies
  - crafts
  - designing
  - fabric
  - quilting
  - quilts
  - sewing
---
I’m going to become a grandma for the very first time on or about the end of March. Our daughter “Pumpkin” is due to give birth to her first child and our very first grandchild (yes, I know the sex, but my lips are sealed!) on March 22, about 6 weeks away. It’s all very exciting.

Here’s the crazy baby quilt (crazy quilt, not crazy baby) I’m making out of all the fun and colourful scraps I have in my collection.

<div id="attachment_583" style="width: 310px" class="wp-caption aligncenter">
  <a href="http://quiltingthefarm.plaidsheep.ca/files/2011/02/quilttop.jpg"><img class="size-medium wp-image-583" title="quilttop" src="http://quiltingthefarm.plaidsheep.ca/files/2011/02/quilttop-300x225.jpg" alt="" width="300" height="225" /></a>
  
  <p class="wp-caption-text">
    Finished quilt top
  </p>
</div>

<p style="text-align: center">
   
</p>

I pieced 6 separate blocks crazy quilt syle; using a backing square and sewing random strips around <!--more-->a centre pentagon. The 6 blocks were then sew together and 2 borders added.

<div id="attachment_287" style="width: 310px" class="wp-caption aligncenter">
  <a href="http://quiltingthefarm.plaidsheep.ca/files/2011/01/kidquilt2.jpg"><img class="size-medium wp-image-287" title="kidquilt2" src="http://quiltingthefarm.plaidsheep.ca/files/2011/01/kidquilt2-300x225.jpg" alt="" width="300" height="225" /></a>
  
  <p class="wp-caption-text">
    The "crazy" blocks
  </p>
</div>

<div id="attachment_584" style="width: 310px" class="wp-caption aligncenter">
  <a href="http://quiltingthefarm.plaidsheep.ca/files/2011/02/border_pieces.jpg"><img class="size-medium wp-image-584" title="border_pieces" src="http://quiltingthefarm.plaidsheep.ca/files/2011/02/border_pieces-300x225.jpg" alt="" width="300" height="225" /></a>
  
  <p class="wp-caption-text">
    Strips for the border
  </p>
</div>

<p style="text-align: left">
  I didn’t have quite enough quilt backing fabric so I sewed some of the extra fabric from the border pieces together and added them to make the back piece large enough.
</p>

<div id="attachment_581" style="width: 310px" class="wp-caption aligncenter">
  <a href="http://quiltingthefarm.plaidsheep.ca/files/2011/02/quiltback.jpg"><img class="size-medium wp-image-581" title="quiltback" src="http://quiltingthefarm.plaidsheep.ca/files/2011/02/quiltback-300x225.jpg" alt="" width="300" height="225" /></a>
  
  <p class="wp-caption-text">
    The border fabrics in the back piece
  </p>
</div>

<div id="attachment_580" style="width: 310px" class="wp-caption aligncenter">
  <a href="http://quiltingthefarm.plaidsheep.ca/files/2011/02/snake_border.jpg"><img class="size-medium wp-image-580 " title="snake_border" src="http://quiltingthefarm.plaidsheep.ca/files/2011/02/snake_border-300x225.jpg" alt="" width="300" height="225" /></a>
  
  <p class="wp-caption-text">
    Look at those cute snakes
  </p>
</div>

Now all I have left to do is add the batting and tie or freeform quilt it all together.

I hope The “Little One” likes it

\***\***\***\***\***\***\***\***\*****