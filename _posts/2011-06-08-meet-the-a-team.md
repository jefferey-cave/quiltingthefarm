---
id: 2194
title: 'Meet the &#8220;A&#8221; Team'
date: 2011-06-08T06:30:55+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2194
permalink: /2011/06/08/meet-the-a-team/
categories:
  - Rural Living
---
Meet the &#8220;A&#8221; Team. In no specific order we have&#8230;

  * Amelia
  * Annabelle
  * Aubree
  * Agatha, and
  * Alexander the Great

The &#8220;A&#8221; Team are our new Cochins. We have four girls and a very quiet (she says hopefully) rooster. Cochins are great dual purpose birds. They are heavy birds and reasonable layers, laying an egg around every other day. This works well for us for now as we are a very small family and have no plans on selling eggs. Cochins are supposed to be docile and friendly. We have yet to experience the &#8220;friendly&#8221; part 🙂

Here are the girls on their first venture outside. They didn&#8217;t go far and took off back to the coop within a few minutes of being outside. There&#8217;s a lot for them to get used to.

<a rel="attachment wp-att-2195" href="http://quiltingthefarm.vius.ca/2011/06/meet-the-a-team/chickens1/"></a>

<a rel="attachment wp-att-2196" href="http://quiltingthefarm.vius.ca/2011/06/meet-the-a-team/chickens-in-coop/"></a>

<a rel="attachment wp-att-2195" href="http://quiltingthefarm.vius.ca/2011/06/meet-the-a-team/chickens1/"><img class="alignnone size-full wp-image-2195" title="chickens1" src="http://quiltingthefarm.plaidsheep.ca/files/2011/06/chickens1.jpg" alt="chickens in yard" width="412" height="309" /></a>

This is Amelia in the background. I haven&#8217;t figured out who&#8217;s who with the others yet!

<a rel="attachment wp-att-2199" href="http://quiltingthefarm.vius.ca/2011/06/meet-the-a-team/chickens3/"><img class="alignnone size-full wp-image-2199" title="chickens3" src="http://quiltingthefarm.plaidsheep.ca/files/2011/06/chickens3.jpg" alt="chickens" width="412" height="309" /></a>

And who could forget the man of the moment, Alexander the Great. He a handsome and easy going fellow.

<a rel="attachment wp-att-2198" href="http://quiltingthefarm.vius.ca/2011/06/meet-the-a-team/rooster/"><img class="alignnone size-full wp-image-2198" title="rooster" src="http://quiltingthefarm.plaidsheep.ca/files/2011/06/rooster.jpg" alt="The rooster" width="412" height="309" /></a>

Here they are contemplating whether to go outside in the big scary world. &#8220;Go to the light&#8230;go to the light!&#8221;

<a rel="attachment wp-att-2196" href="http://quiltingthefarm.vius.ca/2011/06/meet-the-a-team/chickens-in-coop/"><img class="alignnone size-full wp-image-2196" title="chickens-in-coop" src="http://quiltingthefarm.plaidsheep.ca/files/2011/06/chickens-in-coop.jpg" alt="chickens venturing outside" width="412" height="309" /></a>

Mostly we see chicken bums. I think this tells you what they think of us so far.

<a rel="attachment wp-att-2197" href="http://quiltingthefarm.vius.ca/2011/06/meet-the-a-team/chicken-bums/"><img class="alignnone size-full wp-image-2197" title="chicken-bums" src="http://quiltingthefarm.plaidsheep.ca/files/2011/06/chicken-bums.jpg" alt="chickens with bums in air" width="412" height="309" /></a>

We have had these guys now since Sunday and they are settling in to their new world quite nicely. Watch for lots of updates and photos.