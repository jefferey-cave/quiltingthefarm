---
id: 482
title: Making Apple Cider Vinegar
date: 2011-02-11T10:00:11+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=482
permalink: /2011/02/11/making-apple-cider-vinegar/
categories:
  - Kitchen
tags:
  - apples
  - food
  - frugal
  - recipes
---
With lots of apples of the homestead; you end up of lots of apple cores and (possibly) lots of apple peel. The frugal in me has found a way to use them&#8230; Apple Cider Vinegar!

 Apple Cider Vinegar has lots of culinery uses but also makes a great rinse for your hair.

Instructions:

Fill up a bowl or wide mouthed canning jar with cores and peel, cover with cheesecloth and let them air dry and brown for a few days. Top up the jar with cold water, cover with cheesecloth and let stand in a warm place, undisturbed, for at least a month. After a month, strain the resulting liquid through cheesecloth into a clean jar. Store in fridge.