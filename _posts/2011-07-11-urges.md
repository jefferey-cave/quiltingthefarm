---
id: 2509
title: Urges
date: 2011-07-11T06:59:05+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2509
permalink: /2011/07/11/urges/
tinymce_signature_post_setting:
  - 'yes'
categories:
  - Crafts
---
I had an urge to build something. I wasn&#8217;t sure what that something might be.

I paid a visit to Ana White&#8217;s <a href="http://ana-white.com/" target="_blank">website</a>, where she has plans and directions for building many, many easy pieces of furniture. I was trying to use up some of the scrap wood around the garage and barn and had no plans to purchase anything for this project.

<span>I found a really cute bathroom wall cupboard that I think will look great in place of the ugly old over-the-toilet cabinet we have now. I scrounged up all the needed pieces and got to work using the new (<span>ish</span>)</span> mitre saw, table saw and my trusty old scroll saw. It turned out to be a very easy and quick project, the only thing that slowed me down was mitering <span>the cove molding around the top. I really did a terrible job with the molding but nothing a little glue and sawdust couldn&#8217;t </span><del>hide</del> fix 🙂

I even got to prime the cupboard before the afternoon set in. I will give it a nice coat of paint today and find the perfect home for it.

<div id="attachment_2510" style="width: 422px" class="wp-caption alignnone">
  <a rel="attachment wp-att-2510" href="http://quiltingthefarm.vius.ca/2011/07/urges/cabinet/"><img class="size-full wp-image-2510" title="cabinet" src="http://quiltingthefarm.plaidsheep.ca/files/2011/07/cabinet.jpg" alt="" width="412" height="309" /></a>
  
  <p class="wp-caption-text">
    primed and ready to paint
  </p>
</div>

<div class="mceTemp">
   
</div>

<div class="mceTemp">
  <span>After my urge to build was satisfied, I had the urge to sit in my new adirondack chair under the apple tree with a glass of raspberry wine and a good book&#8230;so that&#8217;s exactly I did (Hubby joined me as he liked the idea too). Sunday perfect Sunday.</span>
</div>