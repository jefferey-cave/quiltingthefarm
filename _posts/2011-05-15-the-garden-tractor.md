---
id: 1936
title: The Garden Tractor
date: 2011-05-15T07:18:37+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=1936
permalink: /2011/05/15/the-garden-tractor/
categories:
  - Rural Living
---
We have purchased a garden tractor for our homestead. Hubby and my dad decided a garden tractor would be a very useful thing for our land, and although we are still looking for a larger tractor,  this little &#8216;Wheel Horse&#8217; does a fine job of cutting the lawn and the scrub and will even bring out a few smaller logs from the 100 acre forest.

<a rel="attachment wp-att-1938" href="http://quiltingthefarm.vius.ca/2011/05/the-garden-tractor/hubby-on-tractor2-2/"><img class="alignnone size-full wp-image-1938" title="hubby-on-tractor2" src="http://quiltingthefarm.plaidsheep.ca/files/2011/05/hubby-on-tractor21.jpg" alt="garden tractor" width="412" height="309" /></a>

We are going to purchase a small trailer and a snowblower and hopefully some small farm implements to tow behind.

It may not be large, but it&#8217;s a powerful little thing, and it gives us a way to manage our land in the interim.

<p style="text-align: center">
  ******
</p>