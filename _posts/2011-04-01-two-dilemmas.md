---
id: 1528
title: Two Dilemmas
date: 2011-04-01T06:45:07+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=1528
permalink: /2011/04/01/two-dilemmas/
image: /files/2011/04/chickens1.png
categories:
  - Rural Living
---
Yesterday Hubby and I took a trip into Middleton to order our laying hens. Unfortunately the date for ordering was yesterday not the middle of April as I thought. We were pretty disappointed.

_**So now we have a dilemma.**_ 

How do we go about obtaining some chickens? We could order chicks from the feed store, but these are just day-olds and we aren&#8217;t set up for babies nor do we want them this time around. Our other option is to find someone selling laying hens, though not having <!--more-->been here long and not knowing the community well, we don&#8217;t know where we might find some. So for now, we wait , look in the classifieds and carry on renovating our old shed into a fine chicken coop.

<a rel="attachment wp-att-1534" href="http://quiltingthefarm.vius.ca/2011/04/two-dilemmas/chickens-2/"><img class="alignnone size-full wp-image-1534" title="chickens" src="http://quiltingthefarm.plaidsheep.ca/files/2011/04/chickens1.png" alt="illustration of chickens and barn" width="326" height="230" /></a>

**_New dilemma._**

We aren&#8217;t quite sure how to revamp our shed. The shed is about 15 feet long by 8 feet wide by 10 feet tall at the high point of the gambrel. The floor is concrete. The shed has had the windows and doors boarded over on the outside with just one door remaining. The inside has been dry-walled right over where the windows and doors should be. So to access the window and door framing we need to remove all the insulation and drywall.

When we peeked in an area where the drywall is missing there doesn&#8217;t seem to be actual walls&#8230;just drywall and siding! The drywall isn&#8217;t done well and hasn&#8217;t been finished on the ends so you can put your hand right into the insulation.

Now what should we do? Remove all the drywall and insulation and put up OSB instead? The chicken coop won&#8217;t be insulated, but do the chickens need that anyway? Do we keep the drywall and insulation and repair the areas that we have to tear out and finish the bottom and sides properly? But then again if there are no walls, neither the drywall nor the siding is going to keep predators at bay.

Time to do some more research. **As always, with the little knowledge we have, we are open to suggestions&#8230;please 🙂**

**P.S.** I will post some pics tomorrow after I have bought some new batteries for my camera.

I&#8217;m linking this post to <a title="Farm Friend Friday" href="http://www.verdefarm.com/2011/03/farm-friend-friday-and-farmgirl-friday.html" target="_blank">Farm Friends Friday</a>

<p style="text-align: center">
  ******
</p>