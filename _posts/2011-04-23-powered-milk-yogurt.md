---
id: 1856
title: Powdered Milk Yogurt
date: 2011-04-23T06:41:06+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=1856
permalink: /2011/04/23/powered-milk-yogurt/
categories:
  - Kitchen
  - Recipes
---
<span id="Did_you_know_that_you_can_make_yogurt_from_dry_milk_powder_Well_you_can_and_it8217s_delicious.">

<h3>
  Did you know that you can make yogurt from dry milk powder? Well, you can and it&#8217;s delicious.
</h3></span> 

I&#8217;ve been experimenting with recipes from our pantry goods just recently. This means lots of bean recipes, different bread recipes and other ways to use what we store. I have lots of Dry Skim Milk Powder. We purchase it as neither of us actually drinks much milk or eats cold cereal, so basically I only use it for cooking. If I buy regular milk it tends to go bad before we use it. But we do  love yogurt, especially with homemade granola sprinkled on top. So how do I make yogurt when I rarely have milk on hand?

Yesterday I got out the milk powder and the yogurt I bought for the starter and set to work. Here&#8217;s the recipe.

  * 2 cups dry milk powder
  * 1 1/2 litres water
  * 3 tablespoons yogurt (live active culture for the starter)
  * 4 1/2 tablespoons sugar

Dissolve the sugar with a little hot water in a 2 litre container .

Top up to 1 1/2 litres with cold water and milk powder and whisk to get rid of all the lumps. Gently stir in the yogurt (I used this Balkan Style yogurt as it was the only one with live cultures listed)

<a rel="attachment wp-att-1862" href="http://quiltingthefarm.vius.ca/2011/04/powered-milk-yogurt/yogurt-starter/"><img class="alignnone size-full wp-image-1862" title="yogurt-starter" src="http://quiltingthefarm.plaidsheep.ca/files/2011/04/yogurt-starter.jpg" alt="starter yogurt" width="412" height="309" /></a>

Now you can either incubate the yogurt in this container or pour it into smaller ones. I used two large canning jars.

<a rel="attachment wp-att-1863" href="http://quiltingthefarm.vius.ca/2011/04/powered-milk-yogurt/yogurt-incubating/"><img class="alignnone size-full wp-image-1863" title="yogurt-incubating" src="http://quiltingthefarm.plaidsheep.ca/files/2011/04/yogurt-incubating.jpg" alt="yogurt incubating in cooler" width="412" height="309" /></a>

There are many ways to incubate yogurt, I incubated mine in a cooler filled with tap hot water.

I left the yogurt for about 8 hours before I checked. In the end I left mine all night (I topped up the hot water) but I didn&#8217;t see any noticable difference from when I had checked at 8 hours, though I do believe it&#8217;s slightly more tangy.

Once incubation is done place container(s) in fridge for a couple of hours before eating. You can add honey, jam, fruit, maple syrup, granola or whatever you like. This recipe produces a very, very slightly sweet yogurt. I actually ate some by itself and liked it.

**_I should point out that this yogurt does NOT have the taste of powdered milk. Neither Hubby nor I drink powdered milk; it tastes  nasty!_** 

The yogurt is not thick, Hubby likes it this way, but I think I&#8217;ll add a little whole milk powder next time to see what happens.

<p style="text-align: center">
  ******
</p>