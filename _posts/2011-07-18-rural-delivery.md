---
id: 2580
title: Rural Delivery
date: 2011-07-18T07:00:30+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2580
permalink: /2011/07/18/rural-delivery/
tinymce_signature_post_setting:
  - 'yes'
categories:
  - Rural Living
---
<span id="Yesterday_we_had_to_move_our_mailbox.">

<h3>
  Yesterday we had to move our mailbox.
</h3></span> 

Where we live there is still mail delivery to the ubiquitous black metal mailbox at the end of your driveway. Canada Post has decided that their mail deliverers need more safety rules in place and have been sending out notices to mailbox owners whose mailboxes are not in accordance with these new rules. When we first received the notice I laughed about it with Hubby, surely the 3 cars that drive our road in a day can&#8217;t possibly pose a hazard. Apparently I was wrong. Our mailbox has to be moved about 1 foot closer to the driveway and about 3 inches further back from the road. Canada Post has marked the spot we have to move our mailbox to, it&#8217;s right on the corner of the ditch. _I can&#8217;t fathom how they think that can be safer for their delivery drivers._

So _more_ fence post digging. Hubby wants a post holer, he&#8217;s already fed up with digging post holes and we haven&#8217;t even put up any fence yet! We found an old fence post in the barn and hauled it out the the orange flag marking the spot. _It all seemed so pointless_. Hubby had asked if we could just pick up our mail in town instead of home delivery, but that was a no-go. After lots of digging and pounding the new post is in. I picked up an old mailbox from a yard sale months ago, so I spray painted it a <span style="color: #3366ff">bright blue</span> and the hardware satin black. If you have to move your mailbox, you may as well make it worthwhile. I was going to try and spray paint it plaid, but I&#8217;m over that now, that, and we need to get it put up so we can have mail.

After the mailbox moving, we harvested some more blueberries. There&#8217;s just so many of them and more ripen every day. Hubby surprised me when he said that blueberry pancakes are probably his favourite food of all time. I guess they bring back happy childhood memories for him. Guess what? I made blueberry pancakes for lunch 🙂 I also havested some garden produce. Don&#8217;t get excited, it wasn&#8217;t much, just a few beet thinnings,some swiss chard and a handful of tiny new potatoes&#8230;oh, and a cucumber. My very first homegrown cucumber ever!