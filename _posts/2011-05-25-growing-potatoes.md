---
id: 2053
title: Growing Potatoes
date: 2011-05-25T06:30:45+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2053
permalink: /2011/05/25/growing-potatoes/
categories:
  - Garden
---
The wind was howling through the trees all night. This morning is a miserable, dreary and cold morning. I’m hard pressed to see spring. I am supposed to be planting my potatoes today.

When my parents were here they showed me how to grow potatoes in buckets. We don’t have much of a garden as yet. Time will change that, but it will be a couple of years before we can actually have any kind of “crop”. For now we will plant the two 8&#215;4 raised beds that we have built, with an assortment of vegetables that we can eat fresh and (hopefully) can for the winter months ahead. 

Planting potatoes in buckets takes up way less space. I can put the buckets on the deck so I won’t need anymore garden space this year. They should be easy to maintain i.e. weed and water, and be protected them garden invading critters that will find my back deck just a little too close for comfort.

“Eat what you grow”. I mentioned this yesterday. We aren’t actually big potato eaters. I like them well enough but Hubby prefers rice. We can’t grow rice. Oh, I’m sure someone around here has managed to grow rice experimentally, but that usually takes more resources than I am willing to commit (actually shipping the stuff from China is less resource intensive). We can (likely) grow potatoes; therefore we are going to be eating them as our main staple.

<div id="attachment_2055" style="width: 422px" class="wp-caption alignnone">
  <a rel="attachment wp-att-2055" href="http://quiltingthefarm.vius.ca/2011/05/growing-potatoes/potatoes-chitting/"><img class="size-full wp-image-2055" title="potatoes-chitting" src="http://quiltingthefarm.plaidsheep.ca/files/2011/05/potatoes-chitting.jpg" alt="chitting potatoes" width="412" height="307" /></a>
  
  <p class="wp-caption-text">
    my potatoes "chitting" on the window ledge waiting to be planted
  </p>
</div>

William Cobbett (1763-1835) wasn’t fond of the potato. He thought it the downfall of mankind. To quote “[The potato] has a tendency to bring [men] down to the state… but one removed from that of the pig, and of the ill-fed pig too.” Cobbett argued that bread was a much superior staple. Do I agree? I’m not sure, but I will be planting my own wheat patch one day soon.

Here&#8217;s a how-to on <a title="Growing potatoes in buckets" href="http://www.country-couples.co.uk/blog/grow-your-own-potatoes-in-containers/" target="_blank">growing potatoes in buckets</a>

<p style="text-align: center">
  ******
</p>