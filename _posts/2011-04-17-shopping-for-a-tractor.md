---
id: 1775
title: Shopping for a Tractor
date: 2011-04-17T06:33:15+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=1775
permalink: /2011/04/17/shopping-for-a-tractor/
categories:
  - Rural Living
---
Hubby has been looking into what type and size of tractor will be best for our farm. We need to plough the drive in winter, grade the driveway, mow the lawn and cut down all the scrubby trees and bushes. The tractor that seems the best suited for these tasks is a compact tractor.

Yesterday Hubby and I went shopping for a tractor. We have seen a place, not too far from us, offering good deals on Farm Pro compact tractors.

<a rel="attachment wp-att-1776" href="http://quiltingthefarm.vius.ca/2011/04/shopping-for-a-tractor/farm-pro-tractor/"><img class="alignnone size-full wp-image-1776" title="Farm Pro Tractor" src="http://quiltingthefarm.plaidsheep.ca/files/2011/04/Farm-Pro-Tractor.jpg" alt="Farm Pro tractor" width="400" height="398" /></a>

We want to buy a tractor. We don&#8217;t know much about tractors. We need assistance with picking the perfect tractor, and we have the cash to purchase a tractor. Can we get a salesman to actually sell us a tractor?  No!

We arrive at the sales lot. Hubby says we are interested in buying a tractor. The guy tells us he has nothing to sell us. Strange for an owner of a company but perhaps it was just rhetoric. Hubby presses a little harder, so he asks what we need.  Hubby says we aren&#8217;t really sure, we need some guidance. The guy then says the one we want is the one he&#8217;s just sold. Too bad. Ah, but there are  2 other tractors on the lot and we think we might be interested in either one. The larger one he points out, is a little overkill for us, but we think it is only a little above our requirements and the price is reasonable. We point out the other one. He says that&#8217;s a little small, but we think it still falls within our parameters.

Hubby asks what implements he has for the tractors. The guy shows him the bush hog attachment and then wanders away to chat on his cell phone. Hubby waits for him to finish and asks some other questions about the 2 tractors. The guy just isn&#8217;t interested in chatting with us. He&#8217;s not interested in selling anything to us. We don&#8217;t know why and eventually we leave.

We don&#8217;t have any idea why he didn&#8217;t try to sell us anything. Perhaps it was close to closing time and he didn&#8217;t want to do paperwork. Perhaps he thought we were not real customers. **_What I do know is he lost a sale today._**

**_ _**

<p style="text-align: center">
  <strong><em>******</em></strong>
</p>