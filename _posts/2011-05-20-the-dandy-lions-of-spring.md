---
id: 1989
title: 'The &#8216;Dandy Lions&#8217; of Spring'
date: 2011-05-20T06:33:48+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=1989
permalink: /2011/05/20/the-dandy-lions-of-spring/
categories:
  - Rural Living
---
Unlike many homeowners and farmers, every spring I wait with bated breath for my first crop of dandelions. Not only do I enjoy the carpet of yellow that heralds the new season, but my pet rabbit Stewie just devours all he can get.

After months of buying spring greens from the grocery store at $6 a package, I can now scavenge in my own garden and it doesn&#8217;t cost me a dime. Stewie thinks dandelions top anything I can buy at the store.

<a rel="attachment wp-att-1990" href="http://quiltingthefarm.vius.ca/2011/05/the-dandy-lions-of-spring/dandelion-root/"><img class="alignnone size-full wp-image-1990" title="dandelion-root" src="http://quiltingthefarm.plaidsheep.ca/files/2011/05/dandelion-root.jpg" alt="dandelion root" width="412" height="309" /></a>

Look at this monster. It just goes to show, we must actually have decent soil in some places; I pulled this straight up out of the ground without so much as a tug!

Dandelions have so many other uses too. They are considered a valuable herb with culinary and medicinal uses. Dandelions are a good source of some vitamins (A, B complex, C,  D), and minerals ( iron, potassium, and zinc).  The leaves can be added to salads (use young ones as the older ones get bitter) and teas. The roots can be ground for a coffee substitute (haven&#8217;t tried this yet), and the flowers can be made into a wonderful wine or deep fried for an extra special treat.

_The not-so-humble dandelion at your service._

<p style="text-align: center">
  ******
</p>