---
id: 2422
title: Using Wild Herbs
date: 2011-07-03T07:44:54+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2422
permalink: /2011/07/03/using-wild-herbs/
categories:
  - Rural Living
---
Long before we came to Nova Scotia I wanted to take classes in Herbal Medicine. I have looked at in-class and online schooling and found a couple of places I felt offered a reasonable diploma or degree program. Unfortunately, with new regulations in Europe and new regulations being talked about in Canada, it seems that the cost of the programs far exceeds the personal value I can derive from it, if all professional value is regulated away. For now, I have decided to just do some self study from online resources and the many books there are to be found on the subject.

In my travels around my garden and land I have started to come across many useful plants. Here are 3 that I will be picking and drying over the next few days.

<div id="attachment_2424" style="width: 422px" class="wp-caption alignnone">
  <a rel="attachment wp-att-2424" href="http://quiltingthefarm.vius.ca/2011/07/using-wild-herbs/yarrow-pink/"><img class="size-full wp-image-2424" title="yarrow-pink" src="http://quiltingthefarm.plaidsheep.ca/files/2011/07/yarrow-pink.jpg" alt="" width="412" height="309" /></a>
  
  <p class="wp-caption-text">
    yarrow
  </p>
</div>

<div id="attachment_2430" style="width: 422px" class="wp-caption alignnone">
  <a rel="attachment wp-att-2430" href="http://quiltingthefarm.vius.ca/2011/07/using-wild-herbs/yarrow-white/"><img class="size-full wp-image-2430" title="yarrow-white" src="http://quiltingthefarm.plaidsheep.ca/files/2011/07/yarrow-white.jpg" alt="" width="412" height="309" /></a>
  
  <p class="wp-caption-text">
    Yarrow
  </p>
</div>

<span id="Yarrow">

<h2>
  Yarrow
</h2></span> 

Yarrow is one of the most valuable medicinal herbs. There are many benefits to yarrow:

  * **Anti Bacterial.**
  * **Decongestant.**
  * **Astringent.**
  * **Infusion.** (for healing skin conditions, such as eczema)
  * **Anti-inflammatory.**
  * **Expectorant.**
  * **Promotes digestion.**

<div id="attachment_2425" style="width: 422px" class="wp-caption alignnone">
  <a rel="attachment wp-att-2425" href="http://quiltingthefarm.vius.ca/2011/07/using-wild-herbs/musk-mallow/"><img class="size-full wp-image-2425" title="musk-mallow" src="http://quiltingthefarm.plaidsheep.ca/files/2011/07/musk-mallow.jpg" alt="" width="412" height="309" /></a>
  
  <p class="wp-caption-text">
    Musk Mallow
  </p>
</div>

<span id="Musk_Mallow">

<h2>
  Musk Mallow
</h2></span> 

Mallows have been used to treat ailments for over 5000 years. Uses of the Mallow are:

  * **Anti-inflamatory.**
  * **Soothes irritation and inflammation of the mucous membranes**
  * **Lowers stomach acid**
  * **Expectorant. ** (Helps loosen and expel phlegm and congestion from the lungs.)
  * **Relieves sore throats and dry coughs.**
  * **Soothes the urinary tract** (to relieve cystitis and bladder infections**.**)

<p class="mceTemp">
  <dl id="attachment_2426" class="wp-caption alignnone" style="width: 422px">
    <dt class="wp-caption-dt">
      <a rel="attachment wp-att-2426" href="http://quiltingthefarm.vius.ca/2011/07/using-wild-herbs/oxeye-daisy/"><img class="size-full wp-image-2426" title="oxeye-daisy" src="http://quiltingthefarm.plaidsheep.ca/files/2011/07/oxeye-daisy.jpg" alt="" width="412" height="309" /></a>
    </dt>
    
    <dd class="wp-caption-dd">
      Ox eye Daisy
    </dd>
  </dl>
  
  <h2 class="mceTemp">
    Oxeye Daisy
  </h2>
  
  <p class="mceTemp">
    The Oxeye daisy was introduced from Europe. Like lots of wild herbs it is considered an invasive species. Oxeye Daisy has properties similar to Chamomile:
  </p>
  
  <ul>
    <li>
      <strong>Anti-inflammatory</strong>
    </li>
    <li>
      <strong>Anti-bacterial</strong><br /> <strong>Anti-fungal </strong>(for scalp and skin fungal infections)
    </li>
    <li>
      <strong>Anti-spasmodic</strong>
    </li>
    <li>
      <strong>Relieves coughing</strong>
    </li>
    <li>
      <strong>Decreases secretions</strong>
    </li>
    <li>
      <strong>External Disinfectant </strong>
    </li>
    <li>
      <strong>Insecticide</strong> (external)
    </li>
  </ul>
  
  <p>
    ******
  </p>