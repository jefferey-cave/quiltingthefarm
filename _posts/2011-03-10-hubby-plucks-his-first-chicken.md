---
id: 1087
title: Hubby Plucks His First Chicken
date: 2011-03-10T08:55:59+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=1087
permalink: /2011/03/10/hubby-plucks-his-first-chicken/
image: /files/2011/03/goats2.jpg
categories:
  - Rural Living
tags:
  - chickens
  - DIY
  - farm animals
---
Today we visited Limette and Limey over at <a title="Little Homestead in the Valley" href="http://www.littlehomesteadinthevalley.blogspot.com/" target="_blank">Little Homestead in the Valley</a>. Hubby had been invited to watch the _**Whizz Bang Chicken Plucker**_ in action.

<a rel="attachment wp-att-1090" href="http://quiltingthefarm.vius.ca/2011/03/hubby-plucks-his-first-chicken/plucker2/"><img class="alignnone size-medium wp-image-1090" title="plucker2" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/plucker2-300x225.jpg" alt="chicken plucker" width="300" height="225" /></a>

<a rel="attachment wp-att-1089" href="http://quiltingthefarm.vius.ca/2011/03/hubby-plucks-his-first-chicken/plucker-inside/"><img class="alignnone size-medium wp-image-1089" title="plucker-inside" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/plucker-inside-300x225.jpg" alt="inside chicken plucker" width="300" height="225" /></a>

Unfortunately the Chicken Plucker didn&#8217;t have a <!--more-->powerful enough motor for the job, so instead, hubby got to 

**_butcher_** and **_pluck_** (by hand) his very first chicken!

He was awfully proud 🙂

This was a really important step for hubby on our road to self reliance, one I&#8217;m not ready for, and I don&#8217;t know if I ever will be. 

<a rel="attachment wp-att-1093" href="http://quiltingthefarm.vius.ca/2011/03/hubby-plucks-his-first-chicken/boiler/"><img class="alignnone size-medium wp-image-1093" title="boiler" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/boiler-225x300.jpg" alt="boiler for chickens" width="225" height="300" /></a>

So I, wanting no part of the process, decided to stay and chat with Limette _and_ take these photos of her adorable goats.

<a rel="attachment wp-att-1091" href="http://quiltingthefarm.vius.ca/2011/03/hubby-plucks-his-first-chicken/goats2/"><img class="alignnone size-medium wp-image-1091" title="goats2" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/goats2-300x225.jpg" alt="2 goats at fence" width="300" height="225" /></a>

<a rel="attachment wp-att-1092" href="http://quiltingthefarm.vius.ca/2011/03/hubby-plucks-his-first-chicken/goats1/"><img class="alignnone size-medium wp-image-1092" title="goats1" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/goats1-300x225.jpg" alt="billy goat at fence" width="300" height="225" /></a>

I definitely I got the better deal!

<p style="text-align: center">
  ******
</p>