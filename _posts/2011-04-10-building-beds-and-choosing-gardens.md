---
id: 1670
title: Building Beds and Choosing Gardens
date: 2011-04-10T06:30:26+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=1670
permalink: /2011/04/10/building-beds-and-choosing-gardens/
categories:
  - Garden
tags:
  - garden
  - gardening
  - seeds
  - tractor
---
Today we went to check out a tractor for sale. We didn&#8217;t buy it; it wasn&#8217;t quite what we needed. It seems strangely sureal to be tractor shopping.

It turned into a beautiful sunny spring day. Hubby and I spent most of the afternoon dragging wood out of the barn and building some raised beds for our new garden. We have chosen to build 2 4&#215;8 beds this year. The chose 8 feet purely because we found 16 foot boards in the barn. The width of 4 feet was chosen as the ideal width for reaching across (2 feet on either side) from our book on _Square Foot Gardening_.

We checked out the spot that we had chosen earlier this year, but have decided to move the garden closer to the house. There is a relative flat spot with a slight slope to the south and west in front of the house. This spot gets sun all day long and is close to the pond, making future irrigation possible. This area I had originally planned for lavender, but on looking at it again now the snow has gone, there is way more space than I expected. The garden and lavender field can happily sit side by side.

<div class="mceTemp">
  <dl id="attachment_1671" class="wp-caption alignnone" style="width: 422px">
    <dt class="wp-caption-dt">
      <a rel="attachment wp-att-1671" href="http://quiltingthefarm.vius.ca/2011/04/building-beds-and-choosing-gardens/garden-and-pond/"><img class="size-full wp-image-1671" title="garden-and-pond" src="http://quiltingthefarm.plaidsheep.ca/files/2011/04/garden-and-pond.jpg" alt="garden area" width="412" height="309" /></a>
    </dt>
    
    <dd class="wp-caption-dd">
      It&#8217;s difficult to see the area; it&#8217;s just in front of the tree. There&#8217;s a large space that can all be tilled over in the future giving us a huge garden
    </dd>
  </dl>
  
  <p>
    My seedlings have been transplanted and are doing well. It won&#8217;t be long before they can make their new home outside in the sunshine.
  </p>
  
  <p style="text-align: center">
     
  </p>
  
  <p style="text-align: center">
    ******
  </p>
</div>