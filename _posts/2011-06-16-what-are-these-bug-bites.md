---
id: 2252
title: What Are These Bug Bites?
date: 2011-06-16T06:38:46+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2252
permalink: /2011/06/16/what-are-these-bug-bites/
categories:
  - Rural Living
---
While Hubby was building the fence on Saturday, he got these bites. They started out really small, like blackfly bites, but soon started weeping and getting larger.  Hubby remembers getting them and at first they felt like a pin prick and they throbbed. Last night the bites started itching and burning with definite swelling around them. The swelling has gone today. He took a trip to the walk-in clinic this morning, and even the doctors there don&#8217;t have any idea what it is. 

As you can see, the affected area is quite large. Hubby has three bites like this on his legs.

<a rel="attachment wp-att-2254" href="http://quiltingthefarm.vius.ca/2011/06/what-are-these-bug-bites/bug-bite1/"><img class="alignnone size-full wp-image-2254" title="bug-bite1" src="http://quiltingthefarm.plaidsheep.ca/files/2011/06/bug-bite1.jpg" alt="bug bite" width="412" height="309" /></a>

He had to shave the area and put antiseptic cream and bandaids on because the bites we very weepy at the beginning.

<a rel="attachment wp-att-2255" href="http://quiltingthefarm.vius.ca/2011/06/what-are-these-bug-bites/bug-bite2/"><img class="alignnone size-full wp-image-2255" title="bug-bite2" src="http://quiltingthefarm.plaidsheep.ca/files/2011/06/bug-bite2.jpg" alt="bug-bite" width="412" height="309" /></a>

Here&#8217;s a close-up. Any ideas?

<a rel="attachment wp-att-2253" href="http://quiltingthefarm.vius.ca/2011/06/what-are-these-bug-bites/bug-bite3/"><img class="alignnone size-full wp-image-2253" title="bug-bite3" src="http://quiltingthefarm.plaidsheep.ca/files/2011/06/bug-bite3.jpg" alt="close up bug bite" width="412" height="309" /></a>

I was also outside helping with the fence. I didn&#8217;t get any of these, or if I did they amounted to nothing. We are keeping our fingers crossed that it isn&#8217;t a Staph infection.