---
id: 29
title: French Toast with Mixed Berry Topping
date: 2011-01-18T23:31:05+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=29
permalink: /2011/01/18/french-toast-with-mixed-berry-topping/
categories:
  - Kitchen
  - Recipes
tags:
  - breads
  - breakfasts
  - food
  - home
  - recipes
---
Hubby loves French Toast, well actually hubby loves all food; he’s pretty easy to please.

Here’s a nice, sweet recipe just waiting for a special morning. I was going to take a photo for you but hubby started eating my display before I could stop him!

  * 8 slices of [Grandmothers’ Bread](http://quiltingthefarm.vius.ca/?p=15)
  * 4 [free range eggs](http://quiltingthefarm.vius.ca/?p=17)
  * Some milk (I guess at about half a cup, I didn’t measure)
  * ¼ tsp cinnamon
  * ½ tsp vanilla extract
  * <sup>1</sup>⁄<sub>8</sub> cup sugar
  * 1 cup frozen mixed berries
  * Maple syrup

Beat together eggs, milk, cinnamon, vanilla and sugar in medium bowl

Heat small amount of oil in frying pan or use non stick pan. Dip each slice of bread in the egg mixture, then transfer to frying pan and fry on both sides until golden brown.

To serve, heat the mixed berries in a pan with a little water and sugar. Pour over prepared French toast.

Serve with pure Maple syrup if desired.