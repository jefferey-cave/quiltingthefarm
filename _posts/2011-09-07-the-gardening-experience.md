---
id: 3080
title: The Gardening Experience
date: 2011-09-07T07:00:21+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=3080
permalink: /2011/09/07/the-gardening-experience/
tinymce_signature_post_setting:
  - 'yes'
categories:
  - Garden
---
With summer coming to an end, it&#8217;s time to take stock of the garden and the gardening experience.

This spring we planted our very first garden. All we could manage to get set up were 2 8&#215;4 raised beds. This year&#8217;s gardening was more of an experiment; a learning experience. We made some good decisions. We made some bad decisions. We learned what to do and we learned what not to do. I feel that perhaps I&#8217;ll always be a beginner gardening, much like I&#8217;m always a beginner skater or a beginner skier; every year starting anew. This is not a bad thing as far as I can see, it certainly keeps me interested and on my toes (or in the case of skating and skiing&#8230;on my behind!). Next year I will take with me some knowledge of tomato blossom end rot, but what about all the other disorders and diseases? What happened to my peas? They just shrivelled up and died, I don&#8217;t know why, nor am I trying to figure it out. What&#8217;s done is done for this year.

**Successes**

Actually the garden was a huge surprise. With a growing space the size of a tiny bedroom, I have been able to eat well _and_ put stuff up for the winter. I have a mountain of swiss chard that has been eaten fresh by us and our pet rabbit, and I have made pesto for future use.

 The beets have been eaten fresh, with the thinnings steamed early on in the summer, and beet bottoms and tops eaten for suppers throughout the season.

The carrots are still in the ground, but the few I&#8217;ve pulled have been sweet and tasty. There won&#8217;t be many, probably only a couple of meals worth, but we will look forward to the late harvest when not much else is around.

The tomatoes, well as I wrote about  couple of times, recovered from their strange disorder and have gone on to be prolific. We have eaten them fresh. I have frozen sauce and made salsa&#8230;all this from 5 plants!

Cucumbers were (and still are) the epitomy of success. I have lost count of how many were produced on the 2-3 plants I have, but we&#8217;ve eaten them fresh and have several jars of bread and butter pickles. Today, I checked and there are several more in various stages of growth.

The squash are just coming into their own, and I&#8217;m already behind on the harvest! Today I found out how to blanch and freeze them (this is a job for tomorrow) ready for winter.

And the rutabagas? well it&#8217;s not time for their harvest yet, but they are many and large. Rutabagas will be part of our winter eating regime as long as I can keep them fresh.

Another success story are the cabbages. I have only harvested one so far, but that alone gave us 2 litres of pickled cabbage. I hope I&#8217;ll be able to store the rest in the basement for future use, I&#8217;m not sure the moisture and temperature will be perfect, but I&#8217;m hopeful that they will keep for a few months.

Add to this the potatoes that I&#8217;ve harvested from the buckets on the back deck and the chili peppers in tubs by the door, just waiting for that last burst of sunshine to turn them a glowing red and I&#8217;d say&#8230;

_I don&#8217;t know, but I think all of this is a pretty big accomplishment on just 64 square feet of dirt._