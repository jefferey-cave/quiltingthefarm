---
id: 978
title: 'Spring and Summer &#8220;Doing&#8221;'
date: 2011-03-15T07:10:52+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=978
permalink: /2011/03/15/spring-and-summer-doing/
categories:
  - Rural Living
---
I&#8217;m looking forward to spring and looking forward to &#8220;doing&#8221; on the farm. This is where hubby and I are at this year.

<span id="Garden:">

<h2>
  Garden:
</h2></span> 

Most seeds have arrived in the mail, and I have just picked up the following at our local store.

  * carrots,
  * beets,
  * <span><span>swiss</span> chard</span>
  * radishes
  * kale
  * parsnips

I still need potatoes, onions, garlic and bell peppers.

<!--more-->

<span id="Kitchen_Garden:">

<h2>
  Kitchen Garden:
</h2></span> 

<span>As I had mentioned, I am also going to plant a small herb garden by the kitchen with (so far) thyme, chives,sweet basil and lavender.</span>

<span id="Planters:">

<h2>
  Planters:
</h2></span> 

<span>Hubby bought a package of Capsicum &#8216;twilight&#8217; hot, hot <span>chilli</span> peppers. Although I&#8217;m pretty sure I wont get those to grow without a greenhouse, I will try them in a planter on the deck.</span>

_The area we have decided on for the garden is already fenced, unfortunately there are 2 huge pine tress that need to come down to make it a viable option long term._

<span id="Livestock:">

<h2>
  Livestock:
</h2></span> 

We have decided to use the &#8220;shed&#8221; for the chicken coop. It is solid and insulated. We will have to do some work in there getting it ready for the chickens we have ordered for May.

It looks like we will be ordering 6 brown egg layers from the store unless there are other alternatives closer to the date.

<span id="Pond:">

<h2>
  Pond:
</h2></span> 

Hubby has done much research into pond maintenence and hopes to have the front pond cleaned and healthy by the end of 2011.

<span id="Orchard:">

<h2>
  Orchard:
</h2></span> 

I haven&#8217;t thought too much about this yet, although I would like to plant at least a couple of different fruit trees or bushes

<span id="House:">

<h2>
  House:
</h2></span> 

  * **Dining room:** I am painting the dining room. This is more of a freshen up than anything else. We have plans to open it up a bit (to let in more light) in the future.
  * **Kitchen**: We have a [<span>simple (<span>ish</span>) plan</span>](http://quiltingthefarm.vius.ca/2011/03/the-kitchen-plan/) to make the kitchen space more efficient and brighter. This wont be started until after May.
  * I have an <a title="art studio" href="http://quiltingthefarm.vius.ca/2011/03/new-studio-set-up/" target="_blank">art/craft studio</a> ****<span>area set up in the <span>sunroom</span>. I will be making jewellery, mosaics and, hopefully, many other things for sale.</span>

<span id="Outside_other">

<h2>
  Outside (other)
</h2></span> 

I am going to plant a small area of lavender. This will eventually become a large field that our bees (later) will get to enjoy.

<span id="Other:">

<h2>
  Other:
</h2></span> 

Hubby has a part time farm apprentice position. He is hoping to get a lot of hands on learning that will be helpful for us moving forward in our homesteading life.

I will leave you with some bright thoughts for spring

<div id="attachment_1006" style="width: 422px" class="wp-caption alignnone">
  <a rel="attachment wp-att-1006" href="http://quiltingthefarm.vius.ca/2011/03/spring-and-summer-doing/coloured-chairs/"><img class="size-full wp-image-1006 " title="coloured-chairs" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/coloured-chairs.jpg" alt="" width="412" height="170" /></a>
  
  <p class="wp-caption-text">
    I love these colourful chairs
  </p>
</div>