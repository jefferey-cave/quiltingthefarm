---
id: 1740
title: Old Collectables
date: 2011-05-01T06:45:09+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=1740
permalink: /2011/05/01/old-collectables/
categories:
  - Rural Living
---
Hubby bought some used tools from a moving sale the other weekend. He got a couple of badly needed power tools; circular saw and a jigsaw and a few old hand tools including  a couple of small planers, two  drills (one will be perfect for tapping our Maple trees) and a hammer. Just before we left I saw these bowls for sale for $3.

<a rel="attachment wp-att-1741" href="http://quiltingthefarm.vius.ca/2011/05/old-collectables/bowls/"><img class="alignnone size-full wp-image-1741" title="bowls" src="http://quiltingthefarm.plaidsheep.ca/files/2011/04/bowls.jpg" alt=" Swinnerton's &quot;The Ferry&quot; Staffordshire bowls" width="412" height="309" /></a>

They are Swinnerton&#8217;s &#8220;The Ferry&#8221; Staffordshire bowls. I think they will look lovely on my new dining room shelf (the one I don&#8217;t have yet)Yes, there are purple Easter &#8220;Peeps&#8221; you see in the jar behind 🙂