---
id: 3143
title: Sundried Tomatoes
date: 2011-09-15T07:00:10+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=3143
permalink: /2011/09/15/sundried-tomatoes/
tinymce_signature_post_setting:
  - 'yes'
categories:
  - Kitchen
---
So after much sauce and salsa making, I decided to go for something a wee bit different&#8230;and easier 🙂

&#8220;sun&#8221; dried tomatoes.

<a rel="attachment wp-att-3145" href="http://quiltingthefarm.vius.ca/2011/09/sundried-tomatoes/sundried-tomato2/"><img class="alignnone size-full wp-image-3145" title="sundried-tomato2" src="http://quiltingthefarm.plaidsheep.ca/files/2011/09/sundried-tomato2.jpg" alt="" width="412" height="298" /></a>

The &#8220;sun&#8221; is actually my oven on a very low temperature, as low as I can get without it turning off.

<a rel="attachment wp-att-3144" href="http://quiltingthefarm.vius.ca/2011/09/sundried-tomatoes/sundried-tomato/"><img class="alignnone size-full wp-image-3144" title="sundried-tomato" src="http://quiltingthefarm.plaidsheep.ca/files/2011/09/sundried-tomato.jpg" alt="" width="412" height="309" /></a>

These are so easy to make. Just slice, place on parchment paper (not sure if you need this but it was easy to peel them off) on a cookie tray. Sprinkle with sea salt and olive oil. Place in low oven for about 12 hours. I got up and 3 am just to take them out of the oven and they were done perfectly; leathery and bendy, but not at all hard or crisp. I have put mine in a baggie ready for the freezer, although you may be able to store them in oil, I have yet to look into that. When dried like this they take up little room in the freezer which is a good thing as I am quickly running out of space. I guess I should get motivated and make that blackberry jelly or jam I had planned on because that will free up some of the space occupied by the hundreds of pounds (hyperboyle I admit, but it sure feels that way) of blackberries I&#8217;ve picked.

<a rel="attachment wp-att-3146" href="http://quiltingthefarm.vius.ca/2011/09/sundried-tomatoes/sundried-tomatos/"><img class="alignnone size-full wp-image-3146" title="sundried-tomatos" src="http://quiltingthefarm.plaidsheep.ca/files/2011/09/sundried-tomatos.jpg" alt="" width="412" height="309" /></a>

I feel like making pasta tonight 🙂