---
id: 221
title: Haven’t Done That Yet.
date: 2011-01-26T22:31:57+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=221
permalink: /2011/01/26/havent-done-that-yet/
image: /files/2011/01/winterblizzard_barns.jpg
categories:
  - Rural Living
tags:
  - land
  - snow
  - trees
  - winter
---
Did I tell you that we haven’t seen most of property as yet? We had been waiting for the ground to dry up before we took a hike into the back forty. Unfortunately, before it dried out, it snowed. Now under a couple of feet of snow, the forest and whatever else is back there will have to wait until spring.

This is what the homestead looks like today.

 [<img class="alignnone size-medium wp-image-319" title="winterblizzard_barns" src="http://quiltingthefarm.plaidsheep.ca/files/2011/01/winterblizzard_barns-300x225.jpg" alt="" width="300" height="225" />](http://quiltingthefarm.plaidsheep.ca/files/2011/01/winterblizzard_barns.jpg)[](http://quiltingthefarm.plaidsheep.ca/files/2011/01/winter_wonderland.jpg)

Can&#8217;t wait till spring!