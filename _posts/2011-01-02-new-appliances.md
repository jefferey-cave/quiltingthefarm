---
id: 19
title: New Appliances
date: 2011-01-02T23:05:48+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=19
permalink: /2011/01/02/new-appliances/
categories:
  - Rural Living
tags:
  - appliances
  - home
---
For the first time in my life I have some brand new appliances.

A few days ago our new appliances arrived from Sears. We had ordered a washer, dryer and freezer at the beginning of December to be delivered Jan 3 (you will probably have noticed it’s not actually Jan 3 yet but that’s another story). Anyway, after having to toboggan our appliances down the hill to our basement, today we got to unpack them. This proved to be a lot more difficult and time consuming than we had imagined and we have only managed to unpack the freezer at this point.

The freezer looks a lot bigger than it did in the store. Hubby says we ordered the next size up, I’ll take his word for it although I have my doubts. I know I’ll appreciate its size next year when we, really, start our journey to self reliance, but right now it’s so overwhelming I’m considering filling it up with store bought bread just to have something taking up all that space!

**Update:** Apparently, filling the freezer is no longer a concern. Once we had it all unpacked and set up we noticed that the lid doesn’t close! We tried leveling it…made no difference, we tried twisting the lid a little (this did make a difference;  the other side didn’t close), we tried walking away and coming back later…still the same. Hubby even thought that it might close if we plugged it in, but decided that was a dumb idea.

Sears is sending someone out this week. I’m glad I didn’t buy all that bread!