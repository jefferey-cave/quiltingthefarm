---
id: 1490
title: Marketing the Farm, Part One
date: 2011-04-03T07:16:02+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=1490
permalink: /2011/04/03/marketing-the-farm-part-one/
image: /files/2011/04/sell.png
categories:
  - Rural Living
---
Hubby and I are not yet farmers, good lord&#8230;_**we aren&#8217;t even homesteaders yet!**_

In our city lives; the ones before we took this crazy leap and moved to this farm in Nova Scotia, we were both involved in Marketing; I in Graphic Design and Small Business Marketing and Hubby in Business Analysis. We now want to use those skills to help the farm earn its own keep.

<a rel="attachment wp-att-1572" href="http://quiltingthefarm.vius.ca/2011/04/marketing-the-farm-part-one/sell/"><img class="alignnone size-full wp-image-1572" title="sell" src="http://quiltingthefarm.plaidsheep.ca/files/2011/04/sell.png" alt="illustration with goat and chicken" width="388" height="258" /></a>

_**We have been brainstorming some ideas.**_

First and foremost our farm needs a name. We need a name to convey everything we might be in the future. A name that&#8217;s easy to remember and pronounce. A name that makes a short and easy URL for a website, but most of all a name that really &#8220;belongs&#8221; to our farm.

Speaking of URLs and websites; we already have our own domains and hosting. This is important, not only for the professional air it lends but also the fact that we aren&#8217;t advertising for somebody else. Google is big enough it doesn&#8217;t need our help!

With a name, comes a logo. Our logo needs to be simple, it needs to be memorable and, being a small homestead with limited means, it needs to be cheap to print on an ongoing basis (cards, labels etc). I have been toying with the idea of doing a relief work (linocut) logo that can be transferred to a rubber stamp. This way we can buy an ink pad or 2 and stamp our own.

Building a logo in black and white or one (spot) colour will be much cheaper for professional printing of business cards and for advertising in the newspaper.  Logos with gradients and those in full colour are much more expensive to maintain. Even if we never go to a professional printer, our home printer ink costs will be huge. This does not mean we can&#8217;t have full colour, it does mean we need to make sure our logo is adaptable.

We want our look to be handmade, homemade with a cottage industry feel. Having said that, the look still has to be professional. _We believe_ o_ur customers want to see idyllic country life, they don&#8217;t want to smell the manure!_

Marketing is about communicating with our potential customers. If we are going to sell our homesteading, small farm lifestyle, we must make sure we actually live it!

Stay tuned for part two!

<p style="text-align: center">
  ******
</p>