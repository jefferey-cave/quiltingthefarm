---
id: 2187
title: An Award and a Thank You
date: 2011-06-07T06:14:50+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2187
permalink: /2011/06/07/an-award-and-a-thank-you/
categories:
  - Uncategorized
---
A big &#8220;Thank you&#8221; goes out to Farmer of <a href="http://enjoyingthegoodlifeathiddenmeadowfarm.blogspot.com/" target="_blank">Hidden Meadow Farm</a>. Not only did she offer Hubby the farm apprenticeship, taking him under her wing and teaching him the right way to do the things we&#8217;ve only read about in books, she has sent him home with goodies from the garden and for my garden, and then yesterday she passed along  The Stylish Blogger Award. What an honour from a new friend and mentor.

Farmer has a wonderful <a href="http://enjoyingthegoodlifeathiddenmeadowfarm.blogspot.com/" target="_blank">blog</a> and farm. Check out her gorgeous Highland Cattle, cute little lambs and beautiful photographs.

The rules for this award state I am to tell you 7 things about myself that you might not know, and then to pass this award along to someone I think deserves it.

[<img class="alignnone size-full wp-image-2188" title="stylish-blogger1" src="http://quiltingthefarm.plaidsheep.ca/files/2011/06/stylish-blogger1.jpg" alt="" width="160" height="160" />](http://quiltingthefarm.plaidsheep.ca/files/2011/06/stylish-blogger1.jpg)

_**Here are my 7 things,**_

**1.** If we had not taken up this life , Hubby and I were going to backpack the world! First stop was to be the Trans Siberian Railway.

**2.** I&#8217;ve had a varied career from nursing to teaching and now graphic design. I always wanted to be an artist.

**3.** I detest shopping. I hate grocery shopping, shoe shopping, clothes shopping&#8230;I hate shopping. If I could have everything delivered I would.

**4.** I learned to play the guitar as a kid. My mom hooked me up with twice a week lessons from this &#8216;way out there&#8217; hippy couple who taught me to play (and sing) Puff the Magic Dragon (among other things).

**5.** I love to dance. I have taken lessons in Jazz, Ballet, Hip Hop and Tribal Belly dance in recent years. If my knees were better (I have bad knee issues) I would teach dance.

**6.** I&#8217;m scared of chickens!

**7.** I used to be vegetarian, but started eating meat again when I moved to Canada. Nobody catered to vegetarians  in Beef country (Alberta) back then. I still don&#8217;t eat that much meat, and to be honest I&#8217;m not sure I could eat one of my own animals. Time will tell.

Now on to the next part, (I have to admit finding 7 things about me was quite difficult),  who to pass the award on to.

First of all, I will have to  mention Leigh at <a href="http://my5acredream.blogspot.com/" target="_blank">5 acres and a dream</a>. Leigh has had this award a couple of times recently and she&#8217;s probably out of things to say, but I have to do it anyway as she has one of my favourite blogs. Heck, when I grow up I want to be Leigh!

Next goes to Deborah at <span><a href="http://antiquityoaks.blogspot.com/" target="_blank">antiquity oaks</a>. </span>I love her blog. Her and her hubby started off as city slickers just like us. They started in 2002 and have come along way since then. I hope we can do the same.

Thirdly, to Tipper over at <a href="http://www.blindpigandtheacorn.com/blind_pig_the_acorn/" target="_blank"><span>Blind</span> Pig and the Acorn</a> This is not a homesteading or farm blog but a blog reminiscing mountain folk, mountain music and the stories of Appalachia through her own eyes. This blog makes me smile.

I hope you check out the blogs and see what these great ladies are up to.