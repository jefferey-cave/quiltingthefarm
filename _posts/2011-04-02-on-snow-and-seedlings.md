---
id: 1544
title: On Snow and Seedlings
date: 2011-04-02T07:00:43+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=1544
permalink: /2011/04/02/on-snow-and-seedlings/
image: /files/2011/04/tomato-leaves.jpg
categories:
  - Garden
tags:
  - garden
  - gardening
  - seeds
  - sprouting
  - weather
  - winter
---
**It snowed yesterday**. It snowed a lot. The ground is covered in a wet, fluffy blanket of white.

What do my little seedlings think of this? &#8220;YIPEE, IT&#8217;S SPRING!&#8221; Yes, they are growing like weeds (I do hope they&#8217;re not!). The tomatoes, cucumbers, basil and thyme all have their true leaves now, the lavender has many baby shoots and the <!--more-->chili peppers are just peeking through the soft soil. They don&#8217;t care about the snow. They&#8217;re warm and snug in their temporary mini greenhouses enjoying the warmth of the house. 

_**What they do care about is being re-potted!**_

<a rel="attachment wp-att-1546" href="http://quiltingthefarm.vius.ca/2011/04/on-snow-and-seedlings/cucumber-leaves/"><img class="alignnone size-full wp-image-1546" title="cucumber-leaves" src="http://quiltingthefarm.plaidsheep.ca/files/2011/04/cucumber-leaves.jpg" alt="cucumber with first true leaves" width="412" height="309" /></a>

<a rel="attachment wp-att-1547" href="http://quiltingthefarm.vius.ca/2011/04/on-snow-and-seedlings/tomato-leaves/"><img class="alignnone size-full wp-image-1547" title="tomato-leaves" src="http://quiltingthefarm.plaidsheep.ca/files/2011/04/tomato-leaves.jpg" alt="tomato plant with first true leaves" width="412" height="309" /></a>

I bought a bag of potting soil from the store. I would like to make my own using some of our own soil but that&#8217;s a project for another year. I have many pots of all shapes and sizes that were left behind by a previous owner (thank you) to start my re-potting. I have never actually done this before, so I hope all goes well and I don&#8217;t do too much damage to my little plants.

<div class="mceTemp">
  <dl id="attachment_1548" class="wp-caption alignnone" style="width: 422px">
    <dt class="wp-caption-dt">
      <a rel="attachment wp-att-1548" href="http://quiltingthefarm.vius.ca/2011/04/on-snow-and-seedlings/stewie2/"><img class="size-full wp-image-1548" title="stewie2" src="http://quiltingthefarm.plaidsheep.ca/files/2011/04/stewie2.jpg" alt="Stewie the black rabbit" width="412" height="309" /></a>
    </dt>
    
    <dd class="wp-caption-dd">
      Gratuitous Bunny Photo!
    </dd>
  </dl>
  
  <p>
    I know, I know, this is not a gardening pic, but I wanted to share with you, a photo of my very special helper around the home 🙂 His name is Stewie. He is going to be 8 years old this summer and is as spry and bouncy as a toddler. He&#8217;s very inquisitive and likes to <span style="text-decoration: line-through">hinder</span> help with everything! Here he is assisting with the spool ottoman I&#8217;m building. He did a wonderful job of bitting a big chunk out of the foam I&#8217;m using! 
  </p>
  
  <p style="text-align: center">
    ******
  </p>
</div>