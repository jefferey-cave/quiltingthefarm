---
id: 2836
title: Chokecherry Jelly Redux
date: 2011-08-17T07:00:03+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2836
permalink: /2011/08/17/chokecherry-jelly-redux/
tinymce_signature_post_setting:
  - 'yes'
categories:
  - Kitchen
---
I made chokecherry jelly.

Yesterday _**I re-made chokecherry jelly.**_

Not one for following a recipe in its entirety, my chokecherry jelly wasn&#8217;t successful the first time around. I had collected quite a few berries, and I had more juice than the recommended amount.

<a rel="attachment wp-att-2838" href="http://quiltingthefarm.vius.ca/2011/08/chokecherry-jelly-redux/chokecherry-juice/"><img class="alignnone size-full wp-image-2838" title="chokecherry-juice" src="http://quiltingthefarm.plaidsheep.ca/files/2011/08/chokecherry-juice.jpg" alt="" width="412" height="309" /></a>

I tried to figure out the extra sugar I would need to use with the extra juice. As I was &#8220;winging&#8221; it, I found some pectin in the pantry and decided to add that for good measure. I couldn&#8217;t believe it didn&#8217;t set. Ok, I could believe it, but I didn&#8217;t want to. It was a lot of work. How can you add pectin and it not set? Well while I adjusted the juice and sugar, I forgot to adjust the pectin amount as well. I should have probably used two packages not one.

I opened one of the jars and gave the runny &#8220;jelly&#8221; a taste. It had a wonderful flavour, sort of sour and sweet at the same time. It would have made a good syrup for pancakes, icecream, or even on yogurt, but I really wanted jelly, so back into the pan it went.

I boiled the syrup up again, added about 1/2 cup sugar and boiled it for 2-3 minutes more. I poured it into the re-sterized jars, proccessed it, and held my breath. Phew, this time I have jelly!

<a rel="attachment wp-att-2837" href="http://quiltingthefarm.vius.ca/2011/08/chokecherry-jelly-redux/chokecherry-jelly/"><img class="alignnone size-full wp-image-2837" title="chokecherry-jelly" src="http://quiltingthefarm.plaidsheep.ca/files/2011/08/chokecherry-jelly.jpg" alt="" width="412" height="309" /></a>

Three large jars of chokecherry jelly. Just in time, as we have all but eaten the dandelion jelly I made in the spring.

I am hoping to supply all our jam/jelly needs this year. I have lots of blackberries picked and in the freezer, and the main harvest hasn&#8217;t even started yet. After that there will be the apples, and there should be a mountain of those. I also have crab apples and some peaches&#8230;but not a lot. Surprisingly there are no raspberry bushes on our property. I thought raspberries grew wild everywhere. In Alberta, all you had to do was walk down a back alley and you find raspberry bushes running rampant. I&#8217;m not complaining though, the blackberries send out more than their fair share of suckers, and seem to have managed to spread themselves over the home 6 acres with rapid ease.

One last thought&#8230;I&#8217;m finding the brush to some of the outlying blackberry bushes has been trampled down into trails. Now, I know I didn&#8217;t do this, and nobody else has been picking them. That means &#8220;something&#8221; has made the trails. A bear perhaps? :0 All I know is, if you come across me picking berries in the brush, you&#8217;ll  hear me singing loudly. No bear in his right mind would stick around for that!