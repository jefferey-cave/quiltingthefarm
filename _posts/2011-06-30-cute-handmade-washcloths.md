---
id: 2387
title: Cute Handmade Washcloths
date: 2011-06-30T06:53:00+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2387
permalink: /2011/06/30/cute-handmade-washcloths/
categories:
  - Crafts
---
I have been invited to a baby shower for my next door neighbour. We don&#8217;t know our neighbours very well, we have only lived here 6 months and we were basically snowed in for 3 of those! We have met a few times though, they are good neighbours.

We live out in the boonies, and although there are stores in Annapolis Royal, they don&#8217;t have much in the way of baby stuff. There&#8217;s the drug store and an odds and ends store that sell a few things but nothing I wanted to give. I wanted to give something unique, yet not too costly.

I went to the odds and ends store and bought some baby washcloths. Twelve in a bag for $5, not a bad deal. But washcloths are a lame gift, every mother needs them, but nobody wants to sit at their baby shower and open 20 packages of the same cheap washcloths? I wanted to come up with something special to make the mum-to-be smile.

_Here&#8217;s what I did&#8230;_

I started with these washcloths

<a rel="attachment wp-att-2388" href="http://quiltingthefarm.vius.ca/2011/06/cute-handmade-washcloths/washcloths/"><img class="alignnone size-full wp-image-2388" title="washcloths" src="http://quiltingthefarm.plaidsheep.ca/files/2011/06/washcloths.jpg" alt="" width="412" height="309" /></a>

As you can see, they are pretty flimsy and sooo not square!

I ironed them as flat as possible.

<a rel="attachment wp-att-2389" href="http://quiltingthefarm.vius.ca/2011/06/cute-handmade-washcloths/bought-washcloth/"><img class="alignnone size-full wp-image-2389" title="bought-washcloth" src="http://quiltingthefarm.plaidsheep.ca/files/2011/06/bought-washcloth.jpg" alt="" width="412" height="309" /></a>

Measured and cut out  81/2 inch squares of cute fabric. I just love these bright, happy prints.

<a rel="attachment wp-att-2395" href="http://quiltingthefarm.vius.ca/2011/06/cute-handmade-washcloths/squares-of-fabric/"><img class="alignnone size-full wp-image-2395" title="squares-of-fabric" src="http://quiltingthefarm.plaidsheep.ca/files/2011/06/squares-of-fabric.jpg" alt="" width="412" height="309" /></a>

Pinned them together.

<a rel="attachment wp-att-2391" href="http://quiltingthefarm.vius.ca/2011/06/cute-handmade-washcloths/pinned-washcloths/"><img class="alignnone size-full wp-image-2391" title="pinned-washcloths" src="http://quiltingthefarm.plaidsheep.ca/files/2011/06/pinned-washcloths.jpg" alt="" width="412" height="309" /></a>

Sewed around the edges leaving an opening for turning.

<a rel="attachment wp-att-2394" href="http://quiltingthefarm.vius.ca/2011/06/cute-handmade-washcloths/sewing-cloths/"><img class="alignnone size-full wp-image-2394" title="sewing-cloths" src="http://quiltingthefarm.plaidsheep.ca/files/2011/06/sewing-cloths.jpg" alt="" width="412" height="309" /></a>

Turned them right side out and ironed them flat.

<a rel="attachment wp-att-2392" href="http://quiltingthefarm.vius.ca/2011/06/cute-handmade-washcloths/right-side-out/"><img class="alignnone size-full wp-image-2392" title="right-side-out" src="http://quiltingthefarm.plaidsheep.ca/files/2011/06/right-side-out.jpg" alt="" width="412" height="309" /></a>

Top stitched around all the edges

<a rel="attachment wp-att-2393" href="http://quiltingthefarm.vius.ca/2011/06/cute-handmade-washcloths/sew-edges-of-cloths/"><img class="alignnone size-full wp-image-2393" title="sew-edges-of-cloths" src="http://quiltingthefarm.plaidsheep.ca/files/2011/06/sew-edges-of-cloths.jpg" alt="" width="412" height="309" /></a>

Fold, stack and tie with a ribbon.

<a rel="attachment wp-att-2390" href="http://quiltingthefarm.vius.ca/2011/06/cute-handmade-washcloths/finished-cloth/"><img class="alignnone size-full wp-image-2390" title="finished-cloth" src="http://quiltingthefarm.plaidsheep.ca/files/2011/06/finished-cloth.jpg" alt="fabric baby washclothes" width="412" height="309" /></a>

I ended up making 5 in total (only 3 shown here).

I also made something else to give the mom, but that&#8217;s another post for another day 🙂