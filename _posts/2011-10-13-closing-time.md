---
id: 3383
title: Closing Time
date: 2011-10-13T07:00:32+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=3383
permalink: /2011/10/13/closing-time/
tinymce_signature_post_setting:
  - ""
categories:
  - Rural Living
---
I have decided to close my blog. I will still be writing, but this time I&#8217;m going to concentrate on writing short stories that I hope, one day, will be published. I am also going to be working on some special art projects. Fame and fortune awaits (my fantasy, not reality 🙂 ).

Our farm has a new name and I will be setting up new farm website in a few weeks. I hope to see you there.