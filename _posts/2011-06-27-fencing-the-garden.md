---
id: 2355
title: Fencing the Garden
date: 2011-06-27T06:55:50+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2355
permalink: /2011/06/27/fencing-the-garden/
categories:
  - Garden
---
This week the weather played around again. Cold one day, warm the next. We got several jobs finished around the yard.

The raised beds needed a fence around them to stop, or at least slow down, any critter that thought about having a free lunch. Hubby cut down some hazel trees and used the larger ones for posts. He took the thinner, bendy ones and did a small bit of wattling at the bottom to add a bit of support to the fence, although he thinks this did exactly the opposite to what he wanted! We used 4ft chicken wire for the fence. I hope this is tall enough to keep any deer out. I don&#8217;t see them jumping in to this tiny 10 x 14 garden space. Next year when we add 2 more beds we will have to re-fence with bigger and better materials. The fence hasn&#8217;t been dug down so there is little prevention for small, burrowing animals like rabbits. I am keeping my fingers crossed that they just don&#8217;t find it.

<a rel="attachment wp-att-2356" href="http://quiltingthefarm.vius.ca/2011/06/fencing-the-garden/garden-fence/"><img class="alignnone size-full wp-image-2356" title="garden-fence" src="http://quiltingthefarm.plaidsheep.ca/files/2011/06/garden-fence.jpg" alt="fenced garden beds" width="412" height="309" /></a>

Critter patrol is a way of life and a full time job around here. I planted a small square of corn a couple of days ago. I re-planted the same square again yesterday as some unknown animal/bird had dug up and taken every last seed! This time I was smart and laid wire over the top. I do hope the thief isn&#8217;t smarter than me 😉

<div id="attachment_2357" style="width: 422px" class="wp-caption alignnone">
  <a rel="attachment wp-att-2357" href="http://quiltingthefarm.vius.ca/2011/06/fencing-the-garden/watching-the-action/"><img class="size-full wp-image-2357" title="watching-the-action" src="http://quiltingthefarm.plaidsheep.ca/files/2011/06/watching-the-action.jpg" alt="chicken watching me garden" width="412" height="309" /></a>
  
  <p class="wp-caption-text">
    The chickens find all this fence building facinating.
  </p>
</div>

I have been saving all our eggshells for fertilizer on the garden. A couple of days ago I baked the eggshells for just a short while in the toaster oven. After they cooled down I took my pestle and mortar and ground them into a (somewhat) fine powder. I should have ground it a little finer, but that would have required the blender, and I was just too lazy to get it out. Anyway, I sprinkled all the ground shells over the garden beds. Dried eggshells make good fertilizer for tomatoes because of the added calcium. The shells are also supposed to discourage slugs in the garden as they are unable to move over the sharp, broken pieces. I figured it was worth a try. I keep you posted on its effectiveness over the course of the summer.

Linking to <a title="Barn Hop" href="http://homesteadrevival.blogspot.com/2011/06/barn-hop-18.html" target="_blank">Homestead Revival Barn Hop</a>

[<img class="alignnone size-full wp-image-1317" title="Barn-Hop" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/Barn-Hop.jpg" alt="" width="250" height="250" />](http://quiltingthefarm.plaidsheep.ca/files/2011/03/Barn-Hop.jpg)

<p style="text-align: center">
  ******
</p>