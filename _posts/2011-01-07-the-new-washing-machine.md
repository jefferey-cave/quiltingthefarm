---
id: 31
title: The New Washing Machine
date: 2011-01-07T23:36:07+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=31
permalink: /2011/01/07/the-new-washing-machine/
image: /files/2011/01/Maytag.jpg
categories:
  - Rural Living
tags:
  - appliances
  - frugal
  - home
---
So, in keeping with the rest of our order from Sears; the washer is **not** the one I ordered. It is actually much nicer than the one I had picked out (which was almost the bottom of the line model) with water sensing, no agitator and a drum so large I could almost fit my whole bed in it!

[<img class="alignleft size-thumbnail wp-image-336" title="Maytag" src="http://quiltingthefarm.plaidsheep.ca/files/2011/01/Maytag-150x150.jpg" alt="" width="150" height="150" />](http://quiltingthefarm.plaidsheep.ca/files/2011/01/Maytag.jpg)I started it up yesterday, for the first time, to make sure it worked properly (see [freezer fiasco](http://quiltingthefarm.vius.ca/?p=19)) and to watch it go through the motions. It is one of the new high efficiency models that uses only just enough water to get the job done. The job certainly got done effectively; efficiency wise…it does, in fact, use a tiny amount of water.

[](http://quiltingthefarm.plaidsheep.ca/files/2011/01/Maytag.jpg)

With just the two of us on well and septic, we didn’t really need a low water washer but I’m sure my well and septic will be happier.