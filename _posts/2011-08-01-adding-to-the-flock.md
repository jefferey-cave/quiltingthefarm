---
id: 2672
title: Adding to the Flock
date: 2011-08-01T07:00:31+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2672
permalink: /2011/08/01/adding-to-the-flock/
tinymce_signature_post_setting:
  - 'yes'
categories:
  - Rural Living
---
Today we are picking up more chickens. We want to expand our flock a little and think around 12 hens will be the best and most sustainable number for us.

Somebody we know is selling their hens. They have 7  brown layers, just perfect for our needs.

Our coop is a nice size for 12 chickens, the chicken yard is about 50 ft x 50 ft _and_ we are going to start letting them free range around the farm wherever they want (except in the house!). I&#8217;d love to see the chickens all around the yard, but I am still a little paranoid about predators, although we haven&#8217;t seen any during the day.

Hopefully the Cochins (and apparently one Australorp) will accept their new flock mates and perhaps, just perhaps, poor little Agatha will get an opportunity to move up the pecking order a little. I wonder if the broody hens will be put out enough to leave their clutches and join the rest of the flock? One thing I do know&#8230; Alexander the Great will be more than little pleased to have 7 new hens in his harem 🙂