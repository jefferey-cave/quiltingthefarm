---
id: 1201
title: Power Tools
date: 2011-03-19T07:02:54+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=1201
permalink: /2011/03/19/power-tools/
categories:
  - Rural Living
tags:
  - DIY
  - tools
---
After Hubby put together the headboard bench, he came to the realization that we need some power tools. Yesterday we purchased a table saw.

Here is our Ryobi Table Saw. It was the cheapest one available, but as we have no idea what we will need anyway, it&#8217;s best to start small.

<a rel="attachment wp-att-1202" href="http://quiltingthefarm.vius.ca/2011/03/power-tools/5380844_image/"><img class="alignnone size-full wp-image-1202" title="5380844_image" src="http://quiltingthefarm.plaidsheep.ca/files/2011/03/5380844_image.jpg" alt="Ryobi Table Saw" width="400" height="400" /></a>

It&#8217;s nothing fancy, bit it will hopefully do the job quicker and more efficiently than hubby&#8217;s arms!

We took it out of the box yesterday afternoon, but decided there were just too many bits and pieces to put together&#8230;we&#8217;ll do it tomorrow instead.

<p style="text-align: center">
  ******
</p>