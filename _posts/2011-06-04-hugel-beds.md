---
id: 2139
title: Hugel Beds
date: 2011-06-04T06:57:27+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2139
permalink: /2011/06/04/hugel-beds/
categories:
  - Garden
---
There seems to be a shortage of topsoil in my area. We have put in calls to each and every excavating company within a 40 minute drive, but no one has attempted to call us back. We want to order 5 cubic yards but will take more if we have to. That&#8217;s a cost of between $160 &#8211; $300 and nobody can be bothered. I have to wonder how some companies stay in business when their customer service is non-existant.

Without topsoil my new raised beds stay empty. My plants sit in pots waiting and waiting.

Today, while I was researching some information on herbs, I came across this technique for hugel beds. The technique for hugel beds copies the basic principles of composting, but allows you to plant in the beds straight away. This is just what I needed.

For starters we mulched the bottoms of the beds with cardboard we had been saving for this very purpose. Hopefully it will stop a lot of the weeds from coming through. After we layed the cardboard I gave it a good soaking.

For the bottom layer of the bed we gathered old branches and small logs left behind after the last pass through with the brush hog, scraps of wood and some rotted out fence posts. This layer is supposed to establish a breeding ground for beneficial insects that will speed up decomposition. It also takes up lots of volume!

The next layer needed twigs and brambles. Luckely we have lots of those, so in they went. &#8230; This also creates more volume and gives a place for plants to anchor themselves.

On top of the twigs comes a layer of grass and hay. I raked up the grass clippings from Hubby&#8217;s mowing expeditions and threw that on. Next we went into our barn and hauled out a bunch of old hay that had been left in the stalls. We piled that on top. 

The final layer is supposed to be  mud or soil.We thought about mining our property for soil but leaves us with a problem of having holes everywhere. Fortunately, underneath all the hay in the barn, were several inches of well rotted manure. There have been no animals on this property for many years so the manure has composted down to a light fluffy &#8216;soil&#8217;. We spread this soil on top of the beds and voila! raised beds ready for planting.

<a rel="attachment wp-att-2141" href="http://quiltingthefarm.vius.ca/2011/06/hugel-beds/hugel-beds/"><img class="alignnone size-full wp-image-2141" title="hugel-beds" src="http://quiltingthefarm.plaidsheep.ca/files/2011/06/hugel-beds.jpg" alt="hugel beds" width="412" height="309" /></a><a rel="attachment wp-att-2141" href="http://quiltingthefarm.vius.ca/2011/06/hugel-beds/hugel-beds/"></a>

We are still going to add 1/2 yard of topsoil and sheep manure that has been offered to us. We will pick that up in our small trailer on Sunday.

_Monday will be planting day at last._