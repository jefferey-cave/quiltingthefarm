---
id: 2854
title: Pining for Pesto
date: 2011-08-19T07:03:05+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2854
permalink: /2011/08/19/pining-for-pesto/
tinymce_signature_post_setting:
  - 'yes'
aktt_notify_twitter:
  - 'yes'
categories:
  - Kitchen
---
So what do you do with a mountain of swiss chard? You feed some to the pet rabbit, you eat some steamed with supper, but what about all the rest? Swiss chard is like Zucchini, everybody who likes it already has too much!

The other night I was reminiscing about pizza. I know it&#8217;s strange to reminisce about food, but I have this one pizza I really like&#8230;pesto, artichoke and shrimp. Although we have pizza for supper every candy night (usually Fridays) it is not one of my favourite foods. I really only like the bread part and often just pick off the toppings and eat the crust! I am not a fusy eater, I guess pizza is just not my thing. But pesto, artichoke and shrimp&#8230;well that&#8217;s in a class by itself. The biggest problem I have is I never keep pesto in my pantry. For some reason it never gets put on the shopping list.

<a rel="attachment wp-att-2856" href="http://quiltingthefarm.vius.ca/2011/08/pining-for-pesto/making-pesto/"><img class="alignnone size-full wp-image-2856" title="making-pesto" src="http://quiltingthefarm.plaidsheep.ca/files/2011/08/making-pesto.jpg" alt="" width="412" height="309" /></a>

Back to the swiss chard. What if I could make pesto out of the swiss chard I have in the garden. I grew basil this year, so I have some of that on hand. I don&#8217;t have pine nuts, but I do have pecans and almonds. Hmmm.

Here is my recipe for pesto:

There are no real measurements, I just threw everything into the blender and tasted it as I went.

  * Bunch swiss chard
  * 2 cloves garlic
  * basil (as much as I could get)
  * oregano
  * lemon juice
  * parmesan cheese
  * pecans
  * olive oil
  * salt to taste ( I guess everything is to taste really)

<a rel="attachment wp-att-2855" href="http://quiltingthefarm.vius.ca/2011/08/pining-for-pesto/pesto-in-pots/"><img class="alignnone size-full wp-image-2855" title="pesto-in-pots" src="http://quiltingthefarm.plaidsheep.ca/files/2011/08/pesto-in-pots.jpg" alt="" width="412" height="309" /></a>

I whizzed this all up in the blender, adding a little water to help it along its way. I really should have used my Magic Bullet, but originally I was going to drizzle the oil in as it was blending (can&#8217;t do that with the MB). In the end I just dumped the whole lot in and blended it up.

The colour is such a bright green it almost looks sureal (or possibly alien). Everything went in a different shade of green than it came out&#8230;how does that happen? I divided the pesto up into small portions and into the freezer it went.

The pesto tastes really nice and I&#8217;m pleased to be rid of the swiss chard (for the time being). Now all I have to do is make some pizza crusts.