---
id: 132
title: City Girl, Country Girl
date: 2011-01-01T01:17:39+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=132
permalink: /2011/01/01/city-girl-country-girl/
categories:
  - Rural Living
tags:
  - country life
  - farm animals
  - home
  - land
  - Nova Scotia
  - ponds
---
[](http://quiltingthefarm.plaidsheep.ca/files/2011/01/winterbarns.jpg)[<img class="alignleft size-medium wp-image-113" title="winterbarns" src="http://quiltingthefarm.plaidsheep.ca/files/2011/01/winterbarns-300x225.jpg" alt="" width="300" height="225" />](http://quiltingthefarm.plaidsheep.ca/files/2011/01/winterbarns.jpg)Introducing Quilting the Farm; I look forward to you joining me on an undiscovered (on my part) journey into country life, farm life and more importantly homesteading life.  I’ll be patching together what little knowledge I have (and it is little) with trial and error – lots of error, and advice from awesome people along the way, to make this a vibrant, active, and self reliant homestead.

Our place is an old farmhouse on 107 acres of overgrown, rocky and mostly untamed land in Nova Scotia, Canada. We have 2 barns, 3 ponds, no fencing, and in all likelihood not much top soil. Yes, there’s work to be done here…we have a 50 year plan!

[](http://quiltingthefarm.plaidsheep.ca/files/2011/01/winterbarns.jpg)

First of all, let me point out; I know nothing about anything, farm-wise. I haven’t kept chickens before, the only goats I seen were at the petting zoo, and gardening has been pretty hit and miss (mostly miss) on my part. I’m a city girl, with a city hubby. We both like to think we have entered this life with open eyes, but only time will tell. I look forward to learning and to (hopefully) share with you, the things I learn around the homestead.

Please feel free to offer any suggestions or advice and, in turn I hope that some of you will be motivated to make the move into the rural abyss.