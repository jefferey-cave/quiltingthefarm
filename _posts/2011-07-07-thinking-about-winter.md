---
id: 2475
title: Thinking About Winter
date: 2011-07-07T07:00:15+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2475
permalink: /2011/07/07/thinking-about-winter/
tinymce_signature_post_setting:
  - 'yes'
categories:
  - Rural Living
---
Now that summer is here and it&#8217;s really warm outside it&#8217;s time to think about winter.

We didn&#8217;t buy our house until late last year so finding wood for our furnace was not very easy. We ended up with inferior, wet wood that was a devil to burn. This year we are ordering early. Our first 3 cords was delivered a few days ago.

<a rel="attachment wp-att-2477" href="http://quiltingthefarm.vius.ca/2011/07/thinking-about-winter/firewood/"><img class="alignnone size-full wp-image-2477" title="firewood" src="http://quiltingthefarm.plaidsheep.ca/files/2011/07/firewood.jpg" alt="" width="412" height="309" /></a>

The fun part begins now with all that stacking. Hubby has been busy stacking the 2 cords left over from last year into the basement. After the kerfuffle last year with the barn being frozen shut and all the digging out to get to the wood, we have decided to put some in the garage and pile some outside the basement. Hubby doesn&#8217;t much like the idea of leaving it outside but it seems to be the best solution so far.

So, with the wood delivered our thoughts turn to a cozy wood stove. Our house has a wood/oil combination furnace. The furnace works well and gives off lots of heat, but it does require a lot of up and down the basement stairs to keep it burning, won&#8217;t work well during a power outage (needs electric fan)_ and_ it won&#8217;t burn all night, causing the oil furnace to kick in when it does go out. A wood stove, installed in the living room, would supply the entire house with enough heat, keep burning all night, work when the power goes out, and use less fuel.  The Pacific Energy Spectrum is our choice so far.

 [<img class="alignnone size-full wp-image-2476" title="spectrumgreenblkarch" src="http://quiltingthefarm.plaidsheep.ca/files/2011/07/spectrumgreenblkarch.jpg" alt="" width="412" height="329" />](pacificenergy.net)

We have heard a lot of positive feedback on this stove. The Spectrum heats up to 2000 sq ft (our house is only 1500 sf) and burns very efficiently. We are having someone come over this week to give us a quote for the installation.

I&#8217;ve decided it&#8217;s far too hot to write about wood stoves anymore!