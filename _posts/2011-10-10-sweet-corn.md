---
id: 3352
title: Sweet Corn
date: 2011-10-10T07:00:11+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=3352
permalink: /2011/10/10/sweet-corn/
tinymce_signature_post_setting:
  - 'yes'
categories:
  - Garden
---
I picked our corn today; our one small ear of corn. But let me tell you, it was the sweetest corn I have ever tasted.

<a rel="attachment wp-att-3353" href="http://quiltingthefarm.vius.ca/2011/10/sweet-corn/sweetcorn/"><img class="alignnone size-full wp-image-3353" title="sweetcorn" src="http://quiltingthefarm.plaidsheep.ca/files/2011/10/sweetcorn.jpg" alt="" width="449" height="341" /></a>

I cooked it and sprinkled with a tiny bit of salt (too tasty for added butter) and served it up for a snack. Hubby and I had half the ear each, but it was enough to tell us we need to plant a whole lot more ( and a whole lot earlier) next year!