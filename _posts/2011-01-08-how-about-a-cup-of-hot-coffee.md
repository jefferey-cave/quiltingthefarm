---
id: 11
title: How about a Cup of Hot Coffee
date: 2011-01-08T00:00:07+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=11
permalink: /2011/01/08/how-about-a-cup-of-hot-coffee/
categories:
  - Rural Living
tags:
  - breakfasts
  - country life
  - frugal
  - home
---
<img class="alignleft size-thumbnail wp-image-49" title="Coffee Thermos" src="http://quiltingthefarm.plaidsheep.ca/files/2011/01/Coffee-Thermos-150x150.jpg" alt="" width="150" height="150" />

Hubby and I both enjoy percolated coffee. We have a stove top percolator that we use every morning. The issue we have been having is how to keep the freshly perked coffee steamy hot. Leaving it sitting on the stove element is not the greatest idea; it’s a waste of power and I don’t like to leave the house with the stove top on. The solution we have found is the coffee thermos.

We purchased a thermos by Home Presence from Home Hardware. This thermos is great and is supposed to keep coffee hot for up to 12 hours. We have yet to test this theory as a pot of coffee doesn’t last that long in this house.