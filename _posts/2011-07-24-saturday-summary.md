---
id: 2619
title: Saturday Summary
date: 2011-07-24T07:00:33+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2619
permalink: /2011/07/24/saturday-summary/
tinymce_signature_post_setting:
  - ""
categories:
  - Garden
---
Yesterday was going to be beach day. Hubby and I were going to drive to the South Shore and take a swim in the Atlantic Ocean to cool ourselves off from the oppressive heat we have been experiencing. We got up this morning and started getting ready to go, but neither of us relished the 2 hour drive in a non air conditioned car. The &#8216;going&#8217; wouldn&#8217;t be too bad, but the &#8216;coming home&#8217; would be stiffling. We gave it a miss. I am beginning to understand why everyone in Nova Scotia has a swimming pool in their backyard.

As it turned out the day wasn&#8217;t as sunny as forcast, actually getting a few spits of welcome rain in the afternoon. I spent the afternoon picking blueberries and Hubby spent that time weeding around the bushes. I picked about 5 cups before running out of energy. I froze some of them in one cup portions and kept the rest for fresh eating and pancakes this morning. Almost all the berries are ripe now and we need to be out there everyday picking them. The harvest time for lowbush blueberries is usually much later than this and people are surprised when we tell them our berries are ready now. I wonder if it&#8217;s just an early year for blueberries.

<p class="wp-caption-dt">
  <a rel="attachment wp-att-2623" href="http://quiltingthefarm.vius.ca/2011/07/saturday-summary/ripe-blueberries/"><img class="alignnone size-full wp-image-2623" title="ripe-blueberries" src="http://quiltingthefarm.plaidsheep.ca/files/2011/07/ripe-blueberries.jpg" alt="" width="412" height="309" /></a>
</p>

<a rel="attachment wp-att-2624" href="http://quiltingthefarm.vius.ca/2011/07/saturday-summary/ripe-blueberries2/"><img class="alignnone size-full wp-image-2624" title="ripe-blueberries2" src="http://quiltingthefarm.plaidsheep.ca/files/2011/07/ripe-blueberries2.jpg" alt="" width="412" height="309" /></a>

For supper I picked another batch of swiss chard and beet thinnings. My poor lettuces haven&#8217;t amounted to much and that&#8217;s a big disappointment. The new block I planted about 3 weeks ago has not produced one single sprout, talk about dud seeds, grrr. I emailed Annapolis Seeds and told them about the problem. Perhaps they are just having a bad year for some of their seeds.