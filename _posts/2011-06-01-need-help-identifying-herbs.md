---
id: 2108
title: Need Help Identifying Herbs
date: 2011-06-01T06:30:43+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2108
permalink: /2011/06/01/need-help-identifying-herbs/
categories:
  - Garden
---
I would love some help identifying the herbs (or weeds) in these old herb beds I just discovered. These are the beds. The first one is the least full but has more variety. The second is overgrown.

I have left the photos in their original size so that you can have a really good look. Just click on any photo to enlarge. The photo is beneath the question (or statement), just in case there&#8217;s any confusion.

[<img class="alignnone size-medium wp-image-2119" title="blog photos may 056" src="http://quiltingthefarm.plaidsheep.ca/files/2011/05/blog-photos-may-056-300x225.jpg" alt="old herb beds" width="300" height="225" />](http://quiltingthefarm.plaidsheep.ca/files/2011/05/blog-photos-may-056.jpg)

[<img class="alignnone size-medium wp-image-2110" title="herb bed" src="http://quiltingthefarm.plaidsheep.ca/files/2011/05/blog-photos-may-057-300x225.jpg" alt="herb bed" width="300" height="225" />](http://quiltingthefarm.plaidsheep.ca/files/2011/05/blog-photos-may-057.jpg)

[](http://quiltingthefarm.plaidsheep.ca/files/2011/05/blog-photos-may-057.jpg)

1. I think this may be fernleaf dill.  I can&#8217;t find anything else that looks close.

<div id="attachment_2109" style="width: 310px" class="wp-caption alignnone">
  <a href="http://quiltingthefarm.plaidsheep.ca/files/2011/05/blog-photos-may-058.jpg"><img class="size-medium wp-image-2109" title="blog photos may 058" src="http://quiltingthefarm.plaidsheep.ca/files/2011/05/blog-photos-may-058-300x225.jpg" alt="fernleaf dill?" width="300" height="225" /></a>
  
  <p class="wp-caption-text">
    fernleaf dill?
  </p>
</div>

2. This might be oregano&#8230;or might not be 🙂

<div id="attachment_2111" style="width: 310px" class="wp-caption alignnone">
  <a href="http://quiltingthefarm.plaidsheep.ca/files/2011/05/blog-photos-may-060.jpg"><img class="size-medium wp-image-2111" title="blog photos may 060" src="http://quiltingthefarm.plaidsheep.ca/files/2011/05/blog-photos-may-060-300x225.jpg" alt="oregano?" width="300" height="225" /></a>
  
  <p class="wp-caption-text">
    oregano?
  </p>
</div>

3. I&#8217;m hazarding a guess that this one is thyme as I have planted some of my own seed and it looks very similar.

<div id="attachment_2112" style="width: 310px" class="wp-caption alignnone">
  <a href="http://quiltingthefarm.plaidsheep.ca/files/2011/05/blog-photos-may-062.jpg"><img class="size-medium wp-image-2112" title="blog photos may 062" src="http://quiltingthefarm.plaidsheep.ca/files/2011/05/blog-photos-may-062-300x225.jpg" alt="thyme" width="300" height="225" /></a>
  
  <p class="wp-caption-text">
    thyme
  </p>
</div>

4. Do the next 2 photos show sheep sorrel?

<div id="attachment_2114" style="width: 310px" class="wp-caption alignnone">
  <a href="http://quiltingthefarm.plaidsheep.ca/files/2011/05/blog-photos-may-066.jpg"><img class="size-medium wp-image-2114 " title="blog photos may 066" src="http://quiltingthefarm.plaidsheep.ca/files/2011/05/blog-photos-may-066-300x225.jpg" alt="sheep sorrel" width="300" height="225" /></a>
  
  <p class="wp-caption-text">
    sheep sorrel?
  </p>
</div>

<div id="attachment_2113" style="width: 310px" class="wp-caption alignnone">
  <a href="http://quiltingthefarm.plaidsheep.ca/files/2011/05/blog-photos-may-063.jpg"><img class="size-medium wp-image-2113" title="blog photos may 063" src="http://quiltingthefarm.plaidsheep.ca/files/2011/05/blog-photos-may-063-300x225.jpg" alt="sheep sorrel?" width="300" height="225" /></a>
  
  <p class="wp-caption-text">
    more sheep sorrel?
  </p>
</div>

5. I have no idea what this is

<div id="attachment_2115" style="width: 310px" class="wp-caption alignnone">
  <a href="http://quiltingthefarm.plaidsheep.ca/files/2011/05/blog-photos-may-067.jpg"><img class="size-medium wp-image-2115" title="blog photos may 067" src="http://quiltingthefarm.plaidsheep.ca/files/2011/05/blog-photos-may-067-300x225.jpg" alt="unknown herb" width="300" height="225" /></a>
  
  <p class="wp-caption-text">
    unknown herb?
  </p>
</div>

6. or this

<div id="attachment_2116" style="width: 310px" class="wp-caption alignnone">
  <a href="http://quiltingthefarm.plaidsheep.ca/files/2011/05/blog-photos-may-068.jpg"><img class="size-medium wp-image-2116" title="blog photos may 068" src="http://quiltingthefarm.plaidsheep.ca/files/2011/05/blog-photos-may-068-300x225.jpg" alt="unknown herb2" width="300" height="225" /></a>
  
  <p class="wp-caption-text">
    more unknown herbs
  </p>
</div>

or this

<div id="attachment_2117" style="width: 310px" class="wp-caption alignnone">
  <a href="http://quiltingthefarm.plaidsheep.ca/files/2011/05/blog-photos-may-069.jpg"><img class="size-medium wp-image-2117" title="blog photos may 069" src="http://quiltingthefarm.plaidsheep.ca/files/2011/05/blog-photos-may-069-300x225.jpg" alt="unknown flower" width="300" height="225" /></a>
  
  <p class="wp-caption-text">
    mystery plant
  </p>
</div>

7. What about this&#8230;any ideas?

<div class="mceTemp">
  <dl id="attachment_2118" class="wp-caption alignnone" style="width: 310px">
    <dt class="wp-caption-dt">
      <a href="http://quiltingthefarm.plaidsheep.ca/files/2011/05/blog-photos-may-072.jpg"><img class="size-medium wp-image-2118" title="blog photos may 072" src="http://quiltingthefarm.plaidsheep.ca/files/2011/05/blog-photos-may-072-300x225.jpg" alt="possible weed" width="300" height="225" /></a>
    </dt>
    
    <dd class="wp-caption-dd">
      another mystery herb?
    </dd>
  </dl>
  
  <p>
    Thank you in advance for any help you can offer.
  </p>
</div>

<div class="mceTemp" style="text-align: center">
  ******
</div>