---
id: 2787
title: Farming the Plan
date: 2011-08-12T07:00:07+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2787
permalink: /2011/08/12/farming-the-plan/
tinymce_signature_post_setting:
  - 'yes'
categories:
  - Rural Living
---
Plans and goals get confused. Having a cow is a plan, raising chickens is a plan, but it&#8217;s only a means to an end. The plan is not the goal. Planning is often the easy part, the &#8216;farming&#8217;&#8230;the actual &#8216;doing&#8217; of the plan&#8230;not so much.

I want a dozen chickens, a cow, a couple of pastures, a large garden, and a hay field or two&#8230;or do I? No. These are just ways to get me to the place I want to be: the goal. What I really should be saying is &#8211; I want enough eggs to feed us, with a few left over to give away, and some meat to feed ourselves. I want to supply my own milk and put meat in my freezer for one year plus sell a freezer full to someone else. I want a place where the milk/beef producer can eat and roam at will with little intervention from me. I want to supply my own fruit and vegetables for the season and store some over winter so I don&#8217;t have to purchase at the store. I want to grow a field of hay to feed my milk/beef producers over the winter, and give bedding to my egg/meat producers.

If I could get one chicken that could lay 12 eggs, give me a litre of milk and day and 50 lb beef a year, I&#8217;d be all over like a dirty shirt. Unfortunately (or perhaps fortunately) that&#8217;s not how it works!

Hubby and I have been discussing whether we want goats, sheep or cows. We realise that we have been asking ourselves the wrong question. What we should be asking ourselves is &#8216;what is our goal?&#8217;

Our goal is to have our _brush cleared_, have fr_esh milk_, and a _marketable meat_.

We have been swayed toward Highland Cattle recently, but are they right for us? Their brush clearing abilities are excellent. Their milk, while not terribly abundant, will be plenty for us. Their marketable meat ability is what we can&#8217;t determine. We know of no market (except small farm gate sales) for highland beef. If we chose these cattle, we will have to find a viable market for ourselves.

Goats were the original livestock of choice. They are excellent brush clearers and they produce enough milk for a small family. The problem with goat is the meat tastes terrible. Most of the world may eat goat, but they curry it for a reason! Their is no consumer market for goat meat, and I can only eat so many curries!

Hubby really like the idea of sheep. Their brushclearing ability isn&#8217;t great, but they&#8217;ll keep the grass nice and short. They do produce milk, and although noses of most Canadian (including mine), would turn up at the thought of drinking sheep milk, it does in fact produce wonderful cheeses that we all consume without thinking.  Sheep also produce a very marketable meat, which as I write this, has a growing consumer market.

All three of these animals, get us close to our goal, but none are spot on. Will a different kind of cattle be better for us? What if we can find a market for goat meat? Are there sheep that thrive on brush? There are so many questions, and so few answers. Planning the farm seems easy, but actually farming that plan is way more complicated than we had ever imagined.

In the end, If the farm can&#8217;t sustain us, we can&#8217;t sustain it.