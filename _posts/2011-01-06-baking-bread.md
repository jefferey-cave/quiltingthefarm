---
id: 15
title: Baking Bread
date: 2011-01-06T10:06:42+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=15
permalink: /2011/01/06/baking-bread/
categories:
  - Kitchen
  - Recipes
tags:
  - baking
  - breads
  - recipes
---
[<img class="alignleft size-medium wp-image-98" title="bread" src="http://quiltingthefarm.plaidsheep.ca/files/2011/01/bread-300x225.jpg" alt="" width="300" height="225" />](http://quiltingthefarm.plaidsheep.ca/files/2011/01/bread.jpg)I haven’t done much in the way of baking bread in my life and it&#8217;s one of the first things on my list of homesteading goals. I have, at times, kept a sour-dough starter and baked bread, but it was too time consuming (this was city life). Recently I tried the 5 minute Artisan bread. It’s quick, it’s definitely easy, but the wet dough doesn’t lend itself to making a sandwich type loaf. Enter <a title="Grandmother's Bread recipe" href="http://chickensintheroad.com/cooking/grandmother-bread/" target="_blank">Grandmother’s Bread</a>. Not my Grandmother, but the Grandmother of Suzanne (she has a wonderful blog called <a title="Chicken's in the Road" href="http://chickensintheroad.com/" target="_blank">Chickens in the Road</a>). It’s an easy recipe, not quite as easy is the 5 min Artisan bread as there is some kneading to be done, but it bakes 2 wonderfully tall, flavourful sandwich loaves.