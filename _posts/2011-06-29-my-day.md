---
id: 2371
title: My Day
date: 2011-06-29T07:20:49+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2371
permalink: /2011/06/29/my-day/
categories:
  - Rural Living
---
> _Come, sit awhile_ 
> 
> _under the old apple tree._ 
> 
> _While away your day,_ 
> 
> _rest your eyes,_ 
> 
> _dream your dreams,_ 
> 
> _and best of all_
> 
> _watch the chickens play._

<a rel="attachment wp-att-2372" href="http://quiltingthefarm.vius.ca/2011/06/my-day/garden-chair/"><img class="alignnone size-full wp-image-2372" title="garden-chair" src="http://quiltingthefarm.plaidsheep.ca/files/2011/06/garden-chair.jpg" alt="new garden chair" width="309" height="412" /></a>

_My birthday gift&#8230;a garden colourful chair._

Yesterday was a beautiful day. The sun shone brightly, the skies blue, only slightly wavering to grey in the late afternoon. The chickens laid under the shade tree, unmoving as the sun crept across the sky. The day did itself proud.

Hubby and I went out for lunch, a birthday lunch, something we rarely do. We chose a place overlooking the river in Annapolis Royal.

<a rel="attachment wp-att-2377" href="http://quiltingthefarm.vius.ca/2011/06/my-day/lunch-in-annapolis-royal/"><img class="alignnone size-full wp-image-2377" title="lunch-in-Annapolis-Royal" src="http://quiltingthefarm.plaidsheep.ca/files/2011/06/lunch-in-Annapolis-Royal.jpg" alt="lunch overlooking the river" width="412" height="309" /></a>

We took a walk along the Boardwalk

<a rel="attachment wp-att-2375" href="http://quiltingthefarm.vius.ca/2011/06/my-day/annapolis-royal-boardwalk/"><img class="alignnone size-full wp-image-2375" title="annapolis-royal-boardwalk" src="http://quiltingthefarm.plaidsheep.ca/files/2011/06/annapolis-royal-boardwalk.jpg" alt="boardwalk in Annapolis Royal" width="412" height="309" /></a>

The Wharf. Many moons ago this was a bustling place.

<a rel="attachment wp-att-2374" href="http://quiltingthefarm.vius.ca/2011/06/my-day/wharf/"><img class="alignnone size-full wp-image-2374" title="wharf" src="http://quiltingthefarm.plaidsheep.ca/files/2011/06/wharf.jpg" alt="the wharf" width="412" height="309" /></a>

This is one of the oldest buildings in Annapolis Royal. It is now a museum.

<a rel="attachment wp-att-2376" href="http://quiltingthefarm.vius.ca/2011/06/my-day/oldest-building/"><img class="alignnone size-full wp-image-2376" title="oldest-building" src="http://quiltingthefarm.plaidsheep.ca/files/2011/06/oldest-building.jpg" alt="one of the oldest buildings in Annapolis Royal" width="412" height="309" /></a>

Fort Anne, the centre of Annapolis Royal since 1634

<a rel="attachment wp-att-2373" href="http://quiltingthefarm.vius.ca/2011/06/my-day/fort-anne/"><img class="size-full wp-image-2373 alignnone" title="fort-anne" src="http://quiltingthefarm.plaidsheep.ca/files/2011/06/fort-anne.jpg" alt="Fort Anne" width="412" height="309" /></a>

Back home.

We have just had a call from the hardware store&#8230;our new cart/trailer (for the garden tractor) has arrived. That is going to make life around here a little easier.

<p style="text-align: center">
  ******
</p>