---
id: 2747
title: Free Ranging Freedom
date: 2011-08-08T07:00:41+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2747
permalink: /2011/08/08/free-ranging-freedom/
tinymce_signature_post_setting:
  - 'yes'
categories:
  - Rural Living
---
Yesterday we felt brave and decided to open the coop door and let the chickens have their first experience of free ranging.

We have not mixed the two flocks yet, but the comets and Rhode Island Reds (we just call all of them the &#8216;Reds&#8217;) were getting ansy to go outside. As the Cochins have the chicken yard, we couldn&#8217;t put them in there, so we just opened the &#8216;people&#8217; door and let the &#8220;Reds&#8221; roam around the yard.

<a rel="attachment wp-att-2748" href="http://quiltingthefarm.vius.ca/2011/08/free-ranging-freedom/free-ranging-chickens/"><img class="alignnone size-full wp-image-2748" title="free-ranging-chickens" src="http://quiltingthefarm.plaidsheep.ca/files/2011/08/free-ranging-chickens.jpg" alt="" width="412" height="309" /></a>

After all the worrying we did, it ended up with the &#8216;Reds&#8217; mostly hanging out just outside, or inside their coop. It&#8217;s a big bad world out there, and they were not going very far into it! It _was_ good to see them dive right into eating the grass and bugs, and hopefully that will be good for the pocket book as well &#8217;cause&#8230;_boy_ these Reds eat a lot compared to the Cochins. Having said that, they also lay a lot more eggs 🙂

<a rel="attachment wp-att-2749" href="http://quiltingthefarm.vius.ca/2011/08/free-ranging-freedom/tentative/"><img class="alignnone size-full wp-image-2749" title="tentative" src="http://quiltingthefarm.plaidsheep.ca/files/2011/08/tentative.jpg" alt="" width="412" height="309" /></a>

So the &#8220;Reds&#8221; spent their day of freedom taking dust baths, and relaxing in the shade of the apple trees.

<a rel="attachment wp-att-2751" href="http://quiltingthefarm.vius.ca/2011/08/free-ranging-freedom/dust-baths/"><img class="alignnone size-full wp-image-2751" title="dust-baths" src="http://quiltingthefarm.plaidsheep.ca/files/2011/08/dust-baths.jpg" alt="" width="412" height="309" /></a>

We have almost 107 acres, but everybody needed to be in the same dust hole! Go figure.

<a rel="attachment wp-att-2750" href="http://quiltingthefarm.vius.ca/2011/08/free-ranging-freedom/under-the-apple-trees/"><img class="alignnone size-full wp-image-2750" title="under-the-apple-trees" src="http://quiltingthefarm.plaidsheep.ca/files/2011/08/under-the-apple-trees.jpg" alt="" width="412" height="309" /></a>

A good day was had by all.