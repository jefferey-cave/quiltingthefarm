---
id: 857
title: 'It&#8217;s Almost Maple Syrup Time'
date: 2011-02-27T09:21:29+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=857
permalink: /2011/02/27/its-almost-maple-syrup-time/
categories:
  - Rural Living
tags:
  - food
  - land
  - Nova Scotia
  - trees
---
<span id="We_have_maple_trees">

<h3>
  We have maple trees!
</h3></span> 

There are lots of trees on our property, 103 acres of forest in fact, but I haven&#8217;t given much thought to the kind of trees we have. Yesterday our friend and snow clearing buddy tells us we have lots of maple trees and they are tap-able (not sure if that&#8217;s a word or not).

As soon as tapping season comes along we are headed to A&#8217;s place for a looky loo. He laughs, and says there&#8217;s nothing to it, but we know nothing about tapping for syrup. He has been tapping his trees, just for personal use, for many years. There are no maple trees in Alberta. As far as I&#8217;m concerned, Maple syrup is liquid magic!