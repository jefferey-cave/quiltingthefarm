---
id: 2067
title: Inside Out Banking
date: 2011-05-28T06:29:19+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2067
permalink: /2011/05/28/inside-out-banking/
categories:
  - Rural Living
---
_As if it wasn&#8217;t bad enough before, when we found our personal banker dressed as though she were teaching kindergarten, complete with a &#8216;cute kitty&#8217; sweater (with bells), and a pinafore dress&#8230;_

Whatever happened to banks?

We needed to swap our bank accounts. We have personal banking accounts and a business banking account, and out of Canada&#8217;s big five banks, the accounts we have are not at the two banks represented in small town Nova Scotia. This must be Murphy&#8217;s Law.

After driving for more than an hour on several occasions just to deposit cheques we decided the easiest thing would be to switch banks. One of the two banks in town is Scotiabank. Scotiabank was Hubby&#8217;s bank of choice until he had two security lapses with them. We don&#8217;t feel particularly good about banking with them. The other bank is RBC, so we headed there.

I&#8217;ve had my accounts at BMO for many years. I have had no problems with them AND I pay no fees as I keep the minium balance in my account. Now RBC do it differently. They aren&#8217;t interested in you keeping money in your account, they are more interested in you being indebted to them in the longterm. I can&#8217;t get a rebate on my account fees (amounting to $14.95 per month) unless I have a mortgage _and_ an investment account _and_ a credit card with them. Now, first of all why would a bank be selling the idea of investments to people who have a mortgage to pay. Surely people should be encouraged to pay down their debts before risking money they can ill afford. I&#8217;m not saying people with debt shouldn&#8217;t invest, but I do find the idea of a bank encouraging this risk distasteful. Secondly, you need a credit card! I&#8217;m not sure I can say anything to that, but if I could it wouldn&#8217;t be nice, and thirdly, what about the people who don&#8217;t need a mortgage, why are they being punished? It all seems so wrong.  

Years ago, when I was a teenager, I remember the formidable bank manager in my small town. He absolutely did NOT encourage debt. I almost had to sell my soul for a loan to buy my first (very cheap) car. Back then banks gave incentives to put, and keep money in your account.

As we no longer require a mortgage or a credit card and wish to do our own investing,  it will be an expense of $45 per month (between 2 personal accounts and one business accounts). Needless to say we walked away. I wonder how many people actually do. Do people actually shop around for a bank or do they just take what&#8217;s on the nearest corner? RBC is still in business and doing quite nicely.

Banks have changed. To encourage risk and debt and discourage saving seems to me, to be inside-out. They used to be the place you put your money and felt safe and secure about it, now they have the feel of that cheap talking salesman trying to sell you that lemon, down at the used car lot.