---
id: 1788
title: Crochet Dishcloth Pattern
date: 2011-04-19T06:30:09+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=1788
permalink: /2011/04/19/crochet-dishcloth-pattern/
categories:
  - Crafts
---
I&#8217;ve been working on my crochet skills every evening for a few weeks now and _I&#8217;m getting to like the things I&#8217;m making!_

Here is a very **simple dishcloth** that I designed myself. As this is my very first pattern design you&#8217;ll have to forgive me if the terminology is not quite right. _It is written for the US crocheter; I know that the UK terms are different_, I just don&#8217;t know what they are.

<a rel="attachment wp-att-1790" href="http://quiltingthefarm.vius.ca/2011/04/crochet-dishcloth-pattern/dishcloth1/"><img class="alignnone size-full wp-image-1790" title="dishcloth1" src="http://quiltingthefarm.plaidsheep.ca/files/2011/04/dishcloth1.jpg" alt="crochet dish cloth" width="412" height="309" /></a>

<span id="Ruffled_Dishcloth">

<h3>
  Ruffled Dishcloth
</h3></span> 

You will need:

**1 ball** (42.5 g or 1 1/2 oz) of worsted weight cotton. I used Bernat Handicrafter Ultrasoft Cotton.

**Size 5.5 mm hook**. (mine is a UK hook; I&#8217;m not sure of the equivalent in the US)

<span id="Abbreviations">

<h3>
  Abbreviations
</h3></span> 

sc = single crochet

dc = double crochet

ch = chain

<span id="Pattern">

<h3>
  Pattern
</h3></span> 

Chain 26

R1. ch 2, sc in 3rd ch, sc across to end.

R2. ch 2, sc to end

R3. ch 3, dc to end

Repeat rows 1 through 3  (5 times more)

Next 2 rows ch 2, sc to end

**_Ruffle row._** ch 3, dc in 4th ch from hook, [ch1, \*dc 3 times in next stitch, ch 1\*] until last stich, dc. Fasten off.

<a rel="attachment wp-att-1789" href="http://quiltingthefarm.vius.ca/2011/04/crochet-dishcloth-pattern/dishcloth2/"><img class="alignnone size-full wp-image-1789" title="dishcloth2" src="http://quiltingthefarm.plaidsheep.ca/files/2011/04/dishcloth2.jpg" alt="dishcloth ruffle" width="412" height="309" /></a>

You will need to make a ruffle for the other end of the dish cloth. To do this, follow the instructions for the ruffle row, but pick up 25 stiches along the bottom chain.

<p style="text-align: center">
  ******
</p>