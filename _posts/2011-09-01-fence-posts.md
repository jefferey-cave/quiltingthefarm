---
id: 3013
title: Fence Posts
date: 2011-09-01T07:07:33+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=3013
permalink: /2011/09/01/fence-posts/
tinymce_signature_post_setting:
  - 'yes'
categories:
  - Rural Living
---
There may only be two&#8230;

<a rel="attachment wp-att-3017" href="http://quiltingthefarm.vius.ca/2011/09/fence-posts/fenceposts/"><img class="alignnone size-full wp-image-3017" title="fenceposts" src="http://quiltingthefarm.plaidsheep.ca/files/2011/08/fenceposts.jpg" alt="" width="412" height="309" /></a>

but they&#8217;re going here&#8230;

<a rel="attachment wp-att-3014" href="http://quiltingthefarm.vius.ca/2011/09/fence-posts/fenceline/"><img class="alignnone size-full wp-image-3014" title="fenceline" src="http://quiltingthefarm.plaidsheep.ca/files/2011/08/fenceline.jpg" alt="" width="412" height="309" /></a>

and will be the beginnings of our new fence!

Hubby took off yesterday morning in search of the perfect fence-post tree. Unfortunately, with no vehicle capable of going up the trail, he had to look close to home, and that proved to be more difficult than first thought.

There was once a sawmill here, and the owners of that mill used most of the nice straight spruce trees long ago. There&#8217;s lots of maple, alder, birch, and even some oak, but they don&#8217;t work well for posts. There&#8217;s a whole forest of spruce way up in the back forty, but Hubby couldn&#8217;t cut them down and carry them all the way back to the house, that just wasn&#8217;t feasable. He needed something close by. Eventually Hubby decided that the small birch trees would have to do. Birch trees don&#8217;t make great posts because they rot quickly, but it&#8217;s what we have, so it&#8217;s what we&#8217;re using.

<a rel="attachment wp-att-3016" href="http://quiltingthefarm.vius.ca/2011/09/fence-posts/birch-post/"><img class="alignnone size-full wp-image-3016" title="birch-post" src="http://quiltingthefarm.plaidsheep.ca/files/2011/08/birch-post.jpg" alt="" width="412" height="309" /></a>

Of course, we&#8217;ll need many more posts. How many? we aren&#8217;t sure yet, but at this rate it will take Hubby a lifetime to fence the pasture. We need to find a better way. And on that note I&#8217;ll leave you to your morning coffee, as we&#8217;re off to look at tractors!