---
id: 3235
title: Dresser Revamp Finished
date: 2011-09-24T07:10:31+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=3235
permalink: /2011/09/24/dresser-revamp-finished/
tinymce_signature_post_setting:
  - 'yes'
categories:
  - Crafts
---
Remember this old dresser? It was in bad shape, but I loved the size and the style of it.

<a rel="attachment wp-att-2822" href="http://quiltingthefarm.vius.ca/2011/08/project-day/old-dresser/"><img class="alignnone size-full wp-image-2822" title="old-dresser" src="http://quiltingthefarm.plaidsheep.ca/files/2011/08/old-dresser.jpg" alt="" width="412" height="309" /></a>

I sanded down the top to discover beautiful wood underneath the many layers.

<a rel="attachment wp-att-2820" href="http://quiltingthefarm.vius.ca/2011/08/project-day/dresser-sanded-top/"><img class="alignnone size-full wp-image-2820" title="dresser-sanded-top" src="http://quiltingthefarm.plaidsheep.ca/files/2011/08/dresser-sanded-top.jpg" alt="" width="412" height="309" /></a>

I painted it in an &#8220;oops&#8221; cream colour that I purchased for $1 🙂 Then stained the top with a gel stain I found in the basement. Unfortunately I didn&#8217;t care for the orange colour it produced so I purchased a different stain.

<a rel="attachment wp-att-3171" href="http://quiltingthefarm.vius.ca/2011/09/its-all-in-the-details/cream-dresser-2/"><img class="alignnone size-full wp-image-3171" title="cream-dresser-2" src="http://quiltingthefarm.plaidsheep.ca/files/2011/09/cream-dresser-2.jpg" alt="" width="412" height="309" /></a>

The back was missing a couple of boards so I re-did it with beadboard taken off our walls!

The drawers were not salvageable so some found plywood was used to make shelves instead.

<a rel="attachment wp-att-3236" href="http://quiltingthefarm.vius.ca/2011/09/dresser-revamp-finished/shelves/"><img class="alignnone size-full wp-image-3236" title="shelves" src="http://quiltingthefarm.plaidsheep.ca/files/2011/09/shelves.jpg" alt="" width="412" height="309" /></a>

Because the shelves were plywood, the edges needed finishing somehow. This molding came from the stash we inherited in the garage.

<a rel="attachment wp-att-3240" href="http://quiltingthefarm.vius.ca/2011/09/dresser-revamp-finished/molding/"><img class="alignnone size-full wp-image-3240" title="molding" src="http://quiltingthefarm.plaidsheep.ca/files/2011/09/molding.jpg" alt="" width="412" height="309" /></a>

I glued and nailed the molding to the front of the shelf. The bottom shelf needed a different molding so I used scrap &#8220;crown&#8221; molding from the house. The moldings really finish off the edges well, plus they give the shelves a more substantial look.

<a rel="attachment wp-att-3237" href="http://quiltingthefarm.vius.ca/2011/09/dresser-revamp-finished/edge-molding/"><img class="alignnone size-full wp-image-3237" title="edge-molding" src="http://quiltingthefarm.plaidsheep.ca/files/2011/09/edge-molding.jpg" alt="" width="412" height="309" /></a>

The top is now stained a dark walnut and it&#8217;s ready to use as our new TV stand. You have to excuse all the wires nothing has been placed in there properly as yet. I also have to paint the inside of the sides (I forgot). I am really happy with how it turned out. I am in the process of covering some boxes with fabric to use as drawers on the bottom shelf.

<a rel="attachment wp-att-3239" href="http://quiltingthefarm.vius.ca/2011/09/dresser-revamp-finished/finished-stand/"><img class="alignnone size-full wp-image-3239" title="finished-stand" src="http://quiltingthefarm.plaidsheep.ca/files/2011/09/finished-stand.jpg" alt="" width="412" height="309" /></a>

<a rel="attachment wp-att-3237" href="http://quiltingthefarm.vius.ca/2011/09/dresser-revamp-finished/edge-molding/"></a>

<a rel="attachment wp-att-3237" href="http://quiltingthefarm.vius.ca/2011/09/dresser-revamp-finished/edge-molding/"></a>