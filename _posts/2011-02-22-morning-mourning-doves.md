---
id: 794
title: Morning Mourning Doves
date: 2011-02-22T09:00:05+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=794
permalink: /2011/02/22/morning-mourning-doves/
image: /files/2011/02/doves2.jpg
categories:
  - Rural Living
tags:
  - country life
  - home
  - land
  - snow
  - wildlife
---
This was the view from our kitchen window on Sunday morning.

<a rel="attachment wp-att-798" href="http://quiltingthefarm.vius.ca/2011/02/morning-mourning-doves/doves2/"><img class="aligncenter size-full wp-image-798" title="doves2" src="http://quiltingthefarm.plaidsheep.ca/files/2011/02/doves2.jpg" alt="Mourning Doves" width="412" height="309" /></a>

<a rel="attachment wp-att-796" href="http://quiltingthefarm.vius.ca/2011/02/morning-mourning-doves/dove_feeder/"><img class="aligncenter size-full wp-image-796" title="dove_feeder" src="http://quiltingthefarm.plaidsheep.ca/files/2011/02/dove_feeder.jpg" alt="Mourning Doves" width="412" height="309" /></a>

[<img class="aligncenter size-full wp-image-797" title="dove_on_feeder" src="http://quiltingthefarm.plaidsheep.ca/files/2011/02/dove_on_feeder.jpg" alt="Mourning Doves" width="412" height="309" />](http://quiltingthefarm.plaidsheep.ca/files/2011/02/dove_on_feeder.jpg)

<a rel="attachment wp-att-799" href="http://quiltingthefarm.vius.ca/2011/02/morning-mourning-doves/doves/"><img class="aligncenter size-full wp-image-799" title="doves" src="http://quiltingthefarm.plaidsheep.ca/files/2011/02/doves.jpg" alt="Mourning Doves" width="412" height="309" /></a>

<a rel="attachment wp-att-795" href="http://quiltingthefarm.vius.ca/2011/02/morning-mourning-doves/doves_tree/"><img class="aligncenter size-full wp-image-795" title="doves_tree" src="http://quiltingthefarm.plaidsheep.ca/files/2011/02/doves_tree.jpg" alt="" width="412" height="309" /></a>

A flock of Mourning Doves came to pay us a visit.