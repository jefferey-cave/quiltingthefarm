---
id: 2985
title: This Week in Review, Part One
date: 2011-08-28T07:00:36+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2985
permalink: /2011/08/28/this-week-in-review-part-one/
tinymce_signature_post_setting:
  - 'yes'
categories:
  - Rural Living
---
This week, like the last couple has been focused on picking fruit. It&#8217;s become my full-time job!

The peaches ripened, and although some were a bit misshapened they were aromatic and juicy. I picked most of them, leaving a few at the top of the tree, and peeled, chopped and bagged them for the freezer&#8230;it was a messy job. I made a light syrup to pack them in, so it will be interesting to give them a taste and see what we think.

I don&#8217;t know how many pounds of blackberries we&#8217;ve picked, but it&#8217;s a lot! I made blackberry and apple cobbler a couple of days ago, it was delicious. We ate the supper leftovers for breakfast&#8230;shhh, don&#8217;t tell my mother;)  Other than that, there are blackberries in the fridge, there are blackberries in the freezer, there are blackberries on the counter top and there are still sooo many on the bushes. It&#8217;s seems I&#8217;ve barely made a dent in the population. I am cut up. I am scratched up. But being &#8220;blackberry wealthy&#8221; has it&#8217;s own rewards. Deep in the dead of winter, I&#8217;ll have forgotten the pain.

The blueberries have ended. I have said that more than once, and there always seems to be more. I picked a large bowl earlier in the week, and after treating the chickens to a very small share, I froze the rest. My chickens love me&#8230;I&#8217;m sure of it.

Chokecherries jelly, chokecherry syrup, chokecherry fruit leather&#8230;it&#8217;s all been fun. Jelly has proved to be a challenge, but after a completely chaotic comedy of errors, I have another batch! Burnt fingers&#8230;wrong jars&#8230;lids too small&#8230;lids too big&#8230;canner too full&#8230;made for an interesting morning. We were laughing so hard at the end, it&#8217;s amazing I ended up with jelly at all.

I have been picking up windfall (or deer-fall) apples all week, it&#8217;s never ending. The deer are quite arrogant and prefer to do their munching during the day, within full view of us!  Hubby has collected some of the windfalls to throw at the deer. It doesn&#8217;t seem to be working. The throwing part works well. The scaring-the-deer-away part, well&#8230; that&#8217;s a different story. We need a dog.