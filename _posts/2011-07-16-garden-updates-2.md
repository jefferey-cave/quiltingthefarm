---
id: 2545
title: Garden Updates
date: 2011-07-16T07:00:45+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2545
permalink: /2011/07/16/garden-updates-2/
tinymce_signature_post_setting:
  - 'yes'
categories:
  - Garden
---
Things are coming along swimmingly in the garden&#8230;for the most part.

<a rel="attachment wp-att-2564" href="http://quiltingthefarm.vius.ca/2011/07/garden-updates-2/corn/"><img class="alignnone size-full wp-image-2564" title="corn" src="http://quiltingthefarm.plaidsheep.ca/files/2011/07/corn.jpg" alt="" width="412" height="309" /></a>

This is my experimental corn patch. The soil here is very sandy, Hubby thinks it used to be a riding ring once upon a time.

<a rel="attachment wp-att-2563" href="http://quiltingthefarm.vius.ca/2011/07/garden-updates-2/potatoes-blooming/"><img class="alignnone size-full wp-image-2563" title="potatoes-blooming" src="http://quiltingthefarm.plaidsheep.ca/files/2011/07/potatoes-blooming.jpg" alt="" width="412" height="309" /></a>

The potatoes are blossoming and are almost at &#8220;new&#8221; potato harvest time.

<a rel="attachment wp-att-2562" href="http://quiltingthefarm.vius.ca/2011/07/garden-updates-2/blueberry-patch/"><img class="alignnone size-full wp-image-2562" title="blueberry-patch" src="http://quiltingthefarm.plaidsheep.ca/files/2011/07/blueberry-patch.jpg" alt="" width="412" height="309" /></a>

Lots of blueberries. We picked all the ripe ones yesterday! I even feed a few to the chickens&#8230;sorry mom 😉

<a rel="attachment wp-att-2560" href="http://quiltingthefarm.vius.ca/2011/07/garden-updates-2/beans/"><img class="alignnone size-full wp-image-2560" title="beans" src="http://quiltingthefarm.plaidsheep.ca/files/2011/07/beans.jpg" alt="" width="412" height="309" /></a>

Beans:  doing well and have started to climb the frame.

<a rel="attachment wp-att-2559" href="http://quiltingthefarm.vius.ca/2011/07/garden-updates-2/tomatoes/"><img class="alignnone size-full wp-image-2559" title="tomatoes" src="http://quiltingthefarm.plaidsheep.ca/files/2011/07/tomatoes.jpg" alt="" width="412" height="309" /></a>

Tomatoes:  have taken over! These are bush tomatoes and they spread rapidly. There are lots of fair size, although still green, fruits on them.

<a rel="attachment wp-att-2558" href="http://quiltingthefarm.vius.ca/2011/07/garden-updates-2/lettuce-mix/"><img class="alignnone size-full wp-image-2558" title="lettuce-mix" src="http://quiltingthefarm.plaidsheep.ca/files/2011/07/lettuce-mix.jpg" alt="" width="412" height="309" /></a>

Lettuce: The lettuce mix I got from Annapolis seeds seems to be a failure. Out of the many hundreds? thousands? of tiny seeds in the package there have been only 5 that have germinated.

<a rel="attachment wp-att-2555" href="http://quiltingthefarm.vius.ca/2011/07/garden-updates-2/more-lettuce/"><img class="alignnone size-full wp-image-2555" title="more-lettuce" src="http://quiltingthefarm.plaidsheep.ca/files/2011/07/more-lettuce.jpg" alt="" width="412" height="309" /></a>

This is the rest of the lettuce. Most of the plants are pretty puny. 5 puny plants and hundreds of no-shows, not good value for money. I also planted a few old seeds from a package I had a couple of years ago. These plants are doing well and we have had a couple of small salads with them already.

<a rel="attachment wp-att-2557" href="http://quiltingthefarm.vius.ca/2011/07/garden-updates-2/swiss-chard/"><img class="alignnone size-full wp-image-2557" title="swiss-chard" src="http://quiltingthefarm.plaidsheep.ca/files/2011/07/swiss-chard.jpg" alt="" width="412" height="309" /></a>

Swiss chard:  doing well, at least the ones that germinated are doing well! Many no-shows.

<a rel="attachment wp-att-2556" href="http://quiltingthefarm.vius.ca/2011/07/garden-updates-2/squash/"><img class="alignnone size-full wp-image-2556" title="squash" src="http://quiltingthefarm.plaidsheep.ca/files/2011/07/squash.jpg" alt="" width="412" height="309" /></a>

Squash: Some kind of squash, also from Annapolis Seeds. Most of the seeds didn&#8217;t germinate. Fortunately, at this point, I don&#8217;t have room for a lot of squash.

<a rel="attachment wp-att-2554" href="http://quiltingthefarm.vius.ca/2011/07/garden-updates-2/purple-cabbage/"><img class="alignnone size-full wp-image-2554" title="purple-cabbage" src="http://quiltingthefarm.plaidsheep.ca/files/2011/07/purple-cabbage.jpg" alt="" width="412" height="309" /></a>

Purple cabbage: doing well. So far the slugs have stayed away.

<a rel="attachment wp-att-2553" href="http://quiltingthefarm.vius.ca/2011/07/garden-updates-2/kale/"><img class="alignnone size-full wp-image-2553" title="kale" src="http://quiltingthefarm.plaidsheep.ca/files/2011/07/kale.jpg" alt="" width="412" height="309" /></a>

Kale: Lots of no shows, so many re-plantings. Something has obviously enjoyed the plant at the back because all I can see is a couple of stalks left!

<a rel="attachment wp-att-2552" href="http://quiltingthefarm.vius.ca/2011/07/garden-updates-2/beets/"><img class="alignnone size-full wp-image-2552" title="beets" src="http://quiltingthefarm.plaidsheep.ca/files/2011/07/beets.jpg" alt="" width="412" height="309" /></a>

Beets: coming along nicely. The beet tops will be ready for steaming soon.

<a rel="attachment wp-att-2551" href="http://quiltingthefarm.vius.ca/2011/07/garden-updates-2/rutabagas/"><img class="alignnone size-full wp-image-2551" title="rutabagas" src="http://quiltingthefarm.plaidsheep.ca/files/2011/07/rutabagas.jpg" alt="" width="412" height="309" /></a>

Rutabaga: Doing well for the most part. Some has been eatendown by slugs?

<a rel="attachment wp-att-2550" href="http://quiltingthefarm.vius.ca/2011/07/garden-updates-2/peas/"><img class="alignnone size-full wp-image-2550" title="peas" src="http://quiltingthefarm.plaidsheep.ca/files/2011/07/peas.jpg" alt="" width="412" height="309" /></a>

Peas: Starting to climb trellis. Several no-shows here as well.

<a rel="attachment wp-att-2549" href="http://quiltingthefarm.vius.ca/2011/07/garden-updates-2/cucumber/"><img class="alignnone size-full wp-image-2549" title="cucumber" src="http://quiltingthefarm.plaidsheep.ca/files/2011/07/cucumber.jpg" alt="" width="412" height="309" /></a>

Cucumbers: This is the cucumber I started indoors way to early. Well it survived and thrived and has produced a nice size cucumber so far. One of the other early plants survived as well. I did plant a few more straight outside as soon as the weather was good just incase.

<a rel="attachment wp-att-2548" href="http://quiltingthefarm.vius.ca/2011/07/garden-updates-2/leeks/"><img class="alignnone size-full wp-image-2548" title="leeks" src="http://quiltingthefarm.plaidsheep.ca/files/2011/07/leeks.jpg" alt="" width="412" height="309" /></a>

Leeks: No problems at all.

<a rel="attachment wp-att-2546" href="http://quiltingthefarm.vius.ca/2011/07/garden-updates-2/hot-peppers/"><img class="alignnone size-full wp-image-2546" title="hot-peppers" src="http://quiltingthefarm.plaidsheep.ca/files/2011/07/hot-peppers.jpg" alt="" width="412" height="309" /></a>

Hot peppers: Growing slowly, but coming along.

Things I forgot to take pictures of:

Sweet peppers &#8211; lots of decent size peppers ripening.

Radishes &#8211; doing awesome. Have already eaten several.

Carrots: germination also wasn&#8217;t good so I have planted lots more.

Parsnips: I think they have germinated, can&#8217;t tell if it&#8217;s parsnips or weeds yet! I&#8217;ll wait and see in a couple of weeks.

All in all I&#8217;m pleased with my little garden. I am disappointed that the seeds I bought from Annapolis Seeds haven&#8217;t performed as well as the 99 cent ones from the corner store. Choosing  heirloom or heritage seeds is important from a self reliance stand point, but are obviously useless if they don&#8217;t germinate in the first place!