---
id: 3311
title: No Cider This Week
date: 2011-10-02T07:07:24+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=3311
permalink: /2011/10/02/no-cider-this-week/
tinymce_signature_post_setting:
  - 'yes'
categories:
  - Rural Living
---
I was all excited to post about about our apple pressing/cider making foray, but alas, it wasn&#8217;t to be. The guy who is going to press our apples expects we&#8217;ll get about 25 litres of cider from our 40kg of apples, unfortunately that alone isn&#8217;t enough to make it worth his while to set up his grinder and press, so we, and our apples will have to wait until next week (and hope more people want cider) for the pressing to happen. Meanwhile we wait on the mead and blackberry wine. Both of them are ready to rack this weekend. Perhaps we&#8217;ll have a little taster to see what&#8217;s in store for us in a few months 🙂<a rel="attachment wp-att-3312" href="http://quiltingthefarm.vius.ca/2011/10/no-cider-this-week/blog-photos-july-298/"></a>

<a rel="attachment wp-att-3313" href="http://quiltingthefarm.vius.ca/2011/10/no-cider-this-week/mead-and-wine/"><img class="alignnone size-full wp-image-3313" title="mead-and-wine" src="http://quiltingthefarm.plaidsheep.ca/files/2011/10/mead-and-wine.jpg" alt="" width="412" height="309" /></a>