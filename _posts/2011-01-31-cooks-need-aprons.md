---
id: 247
title: Cooks Need Aprons
date: 2011-01-31T00:46:58+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=247
permalink: /2011/01/31/cooks-need-aprons/
image: /files/2011/01/Paisley-Apron.jpg
categories:
  - Kitchen
tags:
  - home
  - wearables
---
<a href="http://quiltingthefarm.vius.ca/2011/01/baking-bread/" target="_blank">Baking bread</a> is a messy job. As hard as I try, I just can’t seem to bake anything without making a huge mess of my kitchen and me. I sometimes wonder if I waste more flour than I actually use. So, for the first time, as part of my new homesteading life, I have an apron.

** **I asked for one for Christmas; one of those fancy ones with large pockets and a size made for the western market.

This is what I wanted.

<div id="attachment_333" style="width: 132px" class="wp-caption alignnone">
  <a href="http://www.gourmandaproncreations.com/product.php?id=547&cat_id=4&vendor_id=31#"><img class="size-full wp-image-333 " title="Paisley Apron" src="http://quiltingthefarm.plaidsheep.ca/files/2011/01/Paisley-Apron.jpg" alt="" width="122" height="190" /></a>
  
  <p class="wp-caption-text">
    Gourmand Apron Creations
  </p>
</div>

Sadly, I had to pick one up for myself at the dollar store, you know the aprons I’m talking about, the ones that fit in a bag the size of a matchbox and would look good on an 8 year old girl!

I will NOT be uploading a picture.