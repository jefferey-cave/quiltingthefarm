---
id: 2073
title: Planting Potatoes
date: 2011-05-27T06:12:05+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2073
permalink: /2011/05/27/planting-potatoes/
categories:
  - Garden
---
Standing outside with the black flies and mosquitoes biting is no fun. Walking up the trail between forest and marsh is almost unbearable, but unfortunately that&#8217;s where my nice soil is. The mosquitoes &#8211; or were they black flies &#8211; have caused me no end of grief in the last couple of days. Up until a few days ago, I didn&#8217;t seem to have too much reaction from the bites, but that&#8217;s all changed now. I have lumps and bumps everywhere. Thank god for DEET and I&#8217;ll definitely remember to use it next time! I&#8217;m only sorry they don&#8217;t sell anything stronger.

Armed with a shovel and a wheelbarrow, Hubby and I trundled up our trail. I chose the wheelbarrow but realised my mistake as Hubby swatted the flies with his shovel (I remedied this on the way back). We dug up 3 buckets of soft soil from the old sawmill stand and a bucket of something black and putrescent (but nutritious I&#8217;m told) from the swampy area.

There is a large collection of old buckets and pails in our barn (along with a growing collection of very annoying barn swallows). We had asked the previous owner to leave them behind, and I have to say they are very handy to have (the pails not the swallows!). Most are ice-cream buckets that (unfortunately) don&#8217;t have lids. It didn&#8217;t seem so sacreligious to be drilling holes in the bottom of these buckets as it would have  the large commercial &#8220;pickle&#8221; type buckets that we have. I hope they make great potato growing containers.

After fighting the bone-picking &#8220;bugs&#8221; for a couple of hours, Hubby and I got all our potatoes planted in containers. They have all been fed and watered and are now lined up like soldiers on the west facing deck. We are hoping for good things, but realize that growing anything is an art we haven&#8217;t begun to master yet.