---
id: 2146
title: Mulching the Beds
date: 2011-06-05T06:57:34+00:00
author: Sharon
layout: post
guid: http://quiltingthefarm.vius.ca/?p=2146
permalink: /2011/06/05/mulching-the-beds/
categories:
  - Garden
---
Yesterday I was supposed to be using the weed wacker/strimmer to trim down the edges of the chicken yard. Unfortunately, the cord (blade) got too short too quickly and I had to give up early. Hubby was mowing the chicken yard and needed the strimmer to trim down over some rocks. He also had to give up early.  We turned our attention to other things that needed doing&#8230;and there are many. 

The strawberry beds at our farm are old and decrepit. There are still many viable strawberry and, although they are fighting for space with the many weeds and grasses they seem to holding their own. I had weeded one of the beds a couple of weeks ago, but the other bed has barely been touched by a gardener&#8217;s hand in a long, long while.

Weeding took a while and pulling up the wild carrots with their huge taproots was a good workout for the arms. After we weeded we mulched the beds by putting a thick layer of hay over the top of the soil and around the strawberries. We are hoping this keeps the weeds down a little.

<a rel="attachment wp-att-2147" href="http://quiltingthefarm.vius.ca/2011/06/mulching-the-beds/mulched-beds/"><img class="alignnone size-full wp-image-2147" title="mulched-beds" src="http://quiltingthefarm.plaidsheep.ca/files/2011/06/mulched-beds.jpg" alt="mulching strawberry beds" width="412" height="309" /></a>

Next year we&#8217;ll repair the boxes and add in some compost and fresh soil. One last thing we have to do is purchase some bird netting so we get an opportunity to taste the fruit ourselves!