---
id: 3363
title: Life, Simply
date: 2011-01-12T01:16:15+00:00
author: Sharon
layout: page
guid: http://quiltingthefarm.vius.ca/?page_id=2
---
Hubby and I moved from Calgary (Alberta) in June, and in December 2010, purchased an old farm in the foothills of the Appalacian Mountains, in rural Nova Scotia.

We have decided to give up life in the &#8220;rat race&#8217;, and instead opt for homesteading, self reliance, and living simply. Our goals are to have chickens (for eggs and meat), a couple of goats for milk, a huge garden and a well stocked pond, supplying some of our needs. I am learning to bake bread, can jams and syrups and to keep myself warm in a drafty old house!

I am a Graphic Designer by trade and in my spare time, I paint, draw, knit, quilt, sew, and have recently become interested in designing mosiacs and silver jewellery.